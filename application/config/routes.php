<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] 		= 'LoginController/login';
$route['index'] 					= 'LoginController/login';
$route['demo']                      = 'LoginController/demo';

$route['register']                  = 'LoginController/register';
$route['forgot-password']           = 'LoginController/forgot_pass';
 
// $route['login'] 	    					= 'LoginController/login';
$route['logout'] 	    			= 'LoginController/logout';
$route['register'] 					= 'LoginController/register';
$route['register_input']    		= 'LoginController/register_input';
// $route['forgot-password'] 			= 'LoginController/forgot_pass';
// $route['captcha'] 					= 'LoginController/captcha';

// $route['abank']   					= 'BankController/data_bank';
// $route['abank/(:any)'] 				= 'BankController/$1';
// $route['abank/(:any)/(:any)']  		= 'BankController/$1/$2'; 

// $route['balance_imi'] 				= 'BalanceController/balance_imi';
// $route['balance_ts'] 				= 'BalanceController/balance_ts';
// $route['balance_lsm'] 				= 'BalanceController/balance_lsm';

// $route['deposit_lists'] 		  	= 'BalanceController/deposit_lists';
// $route['withdraw_lists'] 		  	= 'BalanceController/withdraw_lists';

$route['deposit']  					= 'DepositController/index';
$route['deposit/(:any)'] 			= 'DepositController/$1';
// $route['deposit/(:any)/(:any)']  	= 'DepositController/$1/$2';

// $route['deposit_imi']  				= 'DepositController/deposit_imi';
// $route['deposit_ts']  				= 'DepositController/deposit_ts';
// $route['deposit_lsm']  				= 'DepositController/deposit_lsm';
// $route['deposit_list_ts']			= 'DepositController/deposit_list_ts';
// $route['deposit_list_w']			= 'DepositController/deposit_list_w';
// $route['deposit_list_d']			= 'DepositController/deposit_list_d';

$route['withdraw']  				= 'WithdrawController/index';
$route['withdraw/(:any)'] 			= 'WithdrawController/$1';


$route['login'] 				  		  	= 'LoginController/loginMe';
$route['login/(:any)'] 			  		  	= 'LoginController/$1';
$route['login/(:any)/(:any)']  	 		  	= 'LoginController/$1/$2';

$route['logout'] 				  		  	= 'MainController/logout';

// $route['newuser']   				      	= 'RegisterController/data_form';
// $route['newuser/(:any)'] 			      	= 'RegisterController/$1';
// $route['newuser/(:any)/(:any)']  	      	= 'RegisterController/$1/$2';

// $route['requestotpid'] 					  	= 'MyotpController/index';
// $route['requestotpid/(:any)'] 			  	= 'MyotpController/$1';
// $route['requestotpid/(:any)/(:any)']  	  	= 'MyotpController/$1/$2';
// $route['loginotp']   					  	= 'LoginController/loginotp';

// $route['otp']   					  	  	= 'MyotpController/otp';

$route['profile']              			 	= 'ProfileController/index';

$route['forgetpass']   					  	= 'ForgetpController/index';
$route['forgetpass/(:any)'] 			  	= 'ForgetpController/$1';
$route['forgetpass/(:any)/(:any)']  	  	= 'ForgetpController/$1/$2';
$route['forgetnewpass']   				  	= 'ForgetpController/forgetnewpass';

// $route['u_info']  					    = 'UserinfoController/user_info';
// $route['u_info/(:any)']  				= 'UserinfoController/$1';
// $route['u_info/(:any)/(:any)']  		= 'UserinfoController/$1/$2';

$route['main']  					= 'MainController/index';
$route['main/(:any)']  				= 'MainController/$1';

$route['statement']                 = 'StatementController/index';
$route['statement/(:any)']          = 'StatementController/$1';

$route['promotion']                 = 'PromotionController/index';
$route['promotion/(:any)']          = 'PromotionController/$1';

$route['manual']                    = 'ManualController/index';
$route['manual/(:any)']             = 'ManualController/$1';

$route['partner']                   = 'PartnerController/index';
$route['partner/(:any)']            = 'PartnerController/$1';

$route['invite']  							= 'MainController/invite';

// $route['submit_lg'] 	    				= 'ServiceController/submit_lg';
// $route['submit_fg'] 	    				= 'ServiceController/submit_fg';

$route['service']		  					= 'ServiceController';
$route['service/(:any)']  					= 'ServiceController/$1';

$route['404_override']	 					= '';
$route['translate_uri_dashes'] 				= FALSE;
