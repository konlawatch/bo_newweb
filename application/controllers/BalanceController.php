<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class BalanceController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct(); 
        
        
    }
    
    public function index(){

   
    }

    protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
        if ($method == "post"){
            if ($data == "") return false;
        }
        $ch = curl_init();
        if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
        else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post"){
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    public static function debug($model, $exit = null) {

        echo '<pre>';
        print_r($model);
        echo '</pre>';

        if ($exit == true) {
            exit;
        }
    }

    public function balance_imi(){
        
        $post = $this->input->post();
        // $ex = explode('-', $post['d_web']);
      // debug($post,true);
        $op = array(
            'web'       => $post['web'],
            'username'  => $post['user'],
            'token'     => $post['token']
            

        ); 
         // debug($op,true);
           
        if($op['web'] != '' && $op['username'] != '' && $op['token'] != ''){
               
            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_getbalance','post',$op);
            $json   =  json_decode($curl,true);
           
             echo $curl;
             debug($json,true);
         
             $this->session->set_userdata('balance_imi',$json);
               
              
            
        }else{
            echo 'web or tel === null';
     
        } 
       
    }

    
     public function balance_ts(){
        
        $post = $this->input->post(); 
        // $ex = explode('-', $post['d_web']);
      // debug($post,true);
        $op = array(
            'web'       => $post['web'],
            'username'  => $post['user'],
            'token'     => $post['token']
            

        ); 
         // debug($op,true);
           
        if($op['web'] != '' && $op['username'] != '' && $op['token'] != ''){
               
            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_getbalance','post',$op);
            $json   =  json_decode($curl,true);
           
             echo $curl;
             // debug($json,true);
         
             $this->session->set_userdata('balance_ts',$json);
               
              
            
        }else{
            echo 'web or tel === null';
     
        } 
       
    }


     public function balance_lsm(){
        
        $post = $this->input->post(); 
        // $ex = explode('-', $post['d_web']);
      // debug($post,true);
        $op = array(
            'web'       => $post['web'],
            'username'  => $post['user'],
            'token'     => $post['token']
            

        ); 
         // debug($op,true);
           
        if($op['web'] != '' && $op['username'] != '' && $op['token'] != ''){
               
            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_getbalance','post',$op);
            $json   =  json_decode($curl,true);
           
             echo $curl;
             // debug($json,true);
         
             $this->session->set_userdata('balance_lsm',$json);
               
              
            
        }else{
            echo 'web or tel === null';
     
        } 
       
    }
 public function deposit_lists()
      {
        $post = $this->input->post();
        
        //debug($post,true);
        $data = array(

        'webid'       => $post['webname'],
        'username'    => $post['username']

        );
         // debug($data,true);
        if ($data['webid'] !='' && $data['username']!='') 
        {

        $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_getdidlist','post',$data);
        $json= json_decode($curl,true);


        echo $curl;

         //debug($json,true); 
        $this->session->set_userdata('deposit_lists',$json);
                          
        }else{
        // echo 'web or tel === null';
        
        redirect('deposit_list_d');
        }

      }

    public function withdraw_lists()
    {
        $post = $this->input->post();
      
        //debug($post,true);
        $data = array(
            'webid'       => $post['webname'],
            'username'    => $post['username']
        );
         // debug($data,true);
        if ($data['webid'] !='' && $data['username']!='') 
        {

        $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_getwidlist','post',$data); 
        $json= json_decode($curl,true);


        echo $curl;

         //debug($json,true); 
        $this->session->set_userdata('withdraw_lists',$json);
                          
        }else{
        // echo 'web or tel === null';
        
        redirect('deposit_list_w');
        }

    }

}


