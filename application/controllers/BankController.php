<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class BankController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();  
        
        
    }
    

    public function data_bank(){
        $this->session->userdata();
        $data = array();
        $content['title']   = 'IMIBETONLINE.COM'; 
        $content['disc']    = 'bo imi disc';
        $content['tapber'] = 'home';
         $this->load->model('SettingModel');
                $this->session->userdata();
                $data = $this->SettingModel->getdata1();
                $set_data = array();
                foreach ($data as $k => $v) {
                $set_data[$v['key_all']] = $v;
                }
                $this->global['data'] = $set_data;
        $content['content'] = $this->load->view('register/form_bank',$this->global, true);

        // $content['content'] = '';
        $this->load->view('layout/app',$content);
    }

    

     protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
                if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
        }

         public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
            exit;
            }
            }
            public function Updatebank() {
            $post = $this->input->post();
            if($post['bankno'] == $post['bankno_confirm']) {
               $pm = array(

               'user'              => $post['user'],
               'token'             => $post['token'],
               'bankname'          => $post['name'].'-'.$post['lname'],
               'bankid'            => $post['bankid'],
               'bankno'            => $post['bankno'],
               'bankno_confirm'    => $post['bankno_confirm'],
               'name'              => $post['name'],
               'lname'             => $post['lname'] 

               );
            // debug($pm,true);
            if($pm['user'] != '' && $pm['token'] != '' && $pm['bankname'] !=''&& $pm['bankid'] != '' && $pm['bankno'] !=''&& $pm['bankno_confirm'] !='' ){



               $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_updatebank','post',$pm);
               $json   = json_decode($curl,true);
               $this->session->set_userdata($json);
            if($json['status']){// สำเร็จ

               $this->session->set_flashdata('สมัครสมาชิกสำเร็จ', 'success on save data');
               redirect('/main');

            }else{ // ไม่สำเร็จ

               $this->session->set_flashdata('error', 'มีหมายเลขโทรศัพท์นี้แล้ว');
               redirect('/abank');

            }
            }else{
               $this->session->set_flashdata('error', 'ข้มูลไม่ถูกต้้อง กรุณาตรวจสอบด้วย');
               redirect('/abank');
            }
            }else{
               $this->session->set_flashdata('error', 'หมายเลขบัญชีไม่ตรงกัน ');
               redirect('/abank');
            } 
         }


         public function updateprofi() {
            $post = $this->input->post();

            $pm = array(

            'user'              => $post['user'],
            'token'             => $post['token'],
            'name'              => $post['name'],
            'email'             => $post['email'],
            'lineid'            => $post['lineid']

            );

            if($pm['user'] != '' && $pm['token'] != '' && $pm['name'] !=''&& $pm['email'] != '' && $pm['lineid'] !=''){



               $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_updateinfo','post',$pm);
               $json   = json_decode($curl,true);
           
            if($json['status']){// สำเร็จ

               BankController::debug($json);
               $this->session->set_flashdata('สมัครสมาชิกสำเร็จ', 'success on save data');
               redirect('/home');

            }else{ 
            }
            }else{
               echo 'web or tel === null';

            } 
         }
    
}


?>