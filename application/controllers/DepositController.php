<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class DepositController extends BaseController
{
   /**
   * This is default constructor of the class
   */
   public function __construct()
   {
      parent::__construct();
      // $this->load->model('SettingModel'); 
      $this->isLoggedIn();
      $this->website           = $this->config->config['website'];
      $this->website_desc      = $this->config->config['website_desc']; 
      $this->key               = $this->config->config['sign_api']; 
      $this->global['website'] = $this->config->config['website'];
   }
   
   public function index(){
      $post = $this->input->post();
      // $post = $this->input->get();

      $content['title']   = $this->website;
      $content['disc']    = $this->website_desc;
      $content['tapber']  = 'deposit';
      // $data               = $this->SettingModel->getdata1();
      $token              = $this->session->userdata();
      $user_info          = json_decode(decode($token['isLoggedIn'],$this->key),true);
      // debug($post,true);
      // $set_data           = array();
      // foreach ($data as $k => $v) {
      //    $set_data[$v['key_all']] = $v;
      // }
      // $this->global['data'] = $set_data;
      // $post = array( 'did_amt' => '100','fdid_step2' => true);
      if($user_info['ag_imi'] == ''){
         $xx   = array(
            'webid' => 'IMI',
            'ag'    => $user_info['ag_imi'],
         );
         $curl = cUrl($this->config->config['apiurl'].'/member/m_getbankbyag','post',$xx); 
         $json = json_decode($curl,true);
         // debug($json,true);
         $this->global['bank'] = '';
         $this->global['web']  = 'IMI';
         if($json['status']){
            $this->global['bank'] = $json['data']['bank'];
         }
         $content['content']   = $this->load->view('deposit/rfdid',$this->global, true);
      }else{
         if($post){
            if(isset($post['fdid_step2'])){
               $xx   = array(
                  'webid' => 'IMI',
                  'ag'    => $user_info['ag_imi'],
               );

               $curl = cUrl($this->config->config['apiurl'].'/member/m_getbankbyag','post',$xx); 
               $json = json_decode($curl,true);
               // debug($json,true);
               $this->global['bank'] = '';
               $this->global['web']  = 'IMI';
               if($json['status']){
                  $this->global['bank'] = $json['data']['bank'];
               }

               $this->global['did_amt'] = $post['did_amt'];
               $content['content']   = $this->load->view('deposit/fdid_step2',$this->global, true);
            }else{
               $content['content']   = $this->load->view('deposit/fdid_step1',$this->global, true);
            }
         }else{
            $content['content']   = $this->load->view('deposit/fdid_step1',$this->global, true);
         }
      }
      $this->load->view('layout/app',$content);
   }

   public function deposit_list_w(){
      $data = array();
      $content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
      $content['tapber']  = 'setting';
      $this->session->userdata();
      $data = $this->SettingModel->getdata1();
      $set_data = array();
      foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
      }
      $this->global['data'] = $set_data;
      $content['session']   = $this->session->userdata();
      $content['content']   = $this->load->view('deposit_lists/main_list_w',$this->global, true);
      $this->load->view('layout/app',$content);
   }

   public function deposit_list_d(){
      $data = array();
      $content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
      $content['tapber']  = 'setting';
      $this->session->userdata();
      $data = $this->SettingModel->getdata1();
      $set_data = array();
      foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
      }
      $this->global['data'] = $set_data;
      $content['session']   = $this->session->userdata();
      $content['content']   = $this->load->view('deposit_lists/main_list_d',$this->global, true);
      $this->load->view('layout/app',$content);
   }

   public function deposit_ts(){
      $data = array();
      $content['title']   = $this->website;
	   $content['disc']    = $this->website_desc;
      $content['tapber']  = 'deposit';
      $this->session->userdata();  
      $data = $this->SettingModel->getdata1();
      $set_data = array();
      foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
      }
      $this->global['data'] = $set_data;
      $content['content']   = $this->load->view('deposit/register_ts',$this->global, true);
      $this->load->view('layout/app',$content);
   }

   public function deposit_lsm(){ 
      $data = array();
      $content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
      $content['tapber']  = 'deposit';
      $this->session->userdata();   
      $data = $this->SettingModel->getdata1();
      $set_data = array();
      foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
      }
      $this->global['data'] = $set_data;
      $content['content']   = $this->load->view('deposit/register_lsm',$this->global, true);
      $this->load->view('layout/app',$content);
   }

   public function deposit_imi(){
      $data = array();
      $content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
      $content['tapber']  = 'deposit';
      $this->session->userdata();
      $data     = $this->SettingModel->getdata1();
      $set_data = array();
      foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
      }
      $this->global['data'] = $set_data;
      $content['content']   = $this->load->view('deposit/register_imi',$this->global, true);
      $this->load->view('layout/app',$content);
   }

   public function mydeposit() {
      $post = $this->input->post();
      $bk   = explode('-', $post['frombank']); 
      $bank = '';
      $ex   = explode('-', $post['userid']);

      if ($post['webid'] != '') { 
         if ($post['amount'] != '' && $post['amount'] > 0) {
            $config = array(
               'upload_path'   => 'assets/uploads/bank_img',
               'allowed_types' => 'gif|jpg|png|jpeg',  
            );
            $this->load->library('upload', $config);
            if($_FILES['deposit_img']['name'] != ''){

               if (!$this->upload->do_upload('deposit_img')) { 
                  $err = $this->upload->display_errors();
               }else{
                  $img = $this->upload->data();
                  $post['img_url'] ='https://test.enjoy911.com/imibo' .'/'. $config['upload_path'].'/'. $img['file_name'];
               } 
            }else{
               $post['img_url'] = '';
            }

            if($post['bcode'] != 'Y'){
               $post['bvalue'] = 0;
            }

            $md = array(  
               'webid'      => $post['webid'],
               'userid'     => $post['userid'],
               'username'   => $post['username'],
               'name'       => $post['name'],
               'frombank'   => $post['frombank'],
               'tobank'     => $post['t_bank'],
               'amount'     => $post['amount'],
               'img_url'    => $post['img_url'],
               'cdate'      => $post['cdate'],
               'ctime'      => $post['depositHour'].':'.$post['depositMin'],
               'modeid'     => '3',    
               'bcode'      => $post['bcode'],        
               'type'       => '1',
               'fromdata'   => 'Manual'
            );
            if($md['userid'] != '' && $md['name'] != '' && $md['frombank'] !='' && $md['tobank'] !='' && $md['amount'] !='' && $md['amount'] > 0 && $md['modeid'] !='' && $md['cdate'] !='' && $md['ctime'] !='' && $md['img_url']!=''){
               $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_deposit','post',$md);
               $json   =  json_decode($curl,true);
               if($json['status']){
                  $this->session->set_flashdata('success', $json['msg']);
                  redirect('/deposit');
               }else{
                  $this->session->set_flashdata('error', $json['msg']);
                  redirect('/deposit');
               }
            }else{
               $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ กรุณาตรวจสอบข้อมูลรูปภาพ,สลิป');
               redirect('/deposit');
            } 
         }else{
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ กรุณาตรวจสอบยอดฝาก');
            redirect('/deposit');
         }
      } else{
         $this->session->set_flashdata('error', ' กรุณาเลือก เวบ '); 
         redirect('/deposit');
      }
   }

   public function deposit_rets(){
      $post = $this->input->post();
      $bk   = explode('-', $post['frombank']);
      $bank = '';  

      $config['upload_path'] = 'assets/uploads/bank_img';
      $config['allowed_types'] = 'jpg|png|gif|jpeg';
      $this->load->library('upload', $config);
      if ($post['amount'] != '' && $post['amount'] > 0) {
         switch ($bk[0]) {
            case 'KBANK': $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S1.jpg'; break;
            case 'KTB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S2.jpg'; break;
            case 'SCB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S3.jpg'; break;
            case 'BAY'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S4.jpg'; break;
            case 'BBL'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S5.jpg'; break;
            case 'TMB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S6.jpg'; break;
            default     : $bank = ''; break;
         }

         if($post['bcode'] != 'Y'){
            $post['bvalue'] = 0;
         }
                  
         $data = array(
            'webid'     => $post['webid'],
            'userid'    => $post['userid'],
            'name'      => $post['name'],
            'username'  => $post['username'],
            'frombank'  => $post['frombank'],
            'tobank'    => $post['tobank'],
            'amount'    => $post['amount'],
            'img_url'   => $bank,
            'cdate'     => $post['cdate'],   
            'ctime'     => $post['depositHour'].':'.$post['depositMin'],
            'modeid'    => '3',   
            'bcode'     => $post['bcode'],        
            'type'      => '1',
            'fromdata'  => 'Manual',
            'web'       => '',
            'agent'     => ''
         );

         if ($data['webid'] !='' && $data['userid'] !='' && $data['name'] !='' && $data['frombank'] !='' && $data['tobank'] !='' && $data['amount'] !='' && $data['amount'] > 0 && $data['img_url']) {
            $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_fdeposit','post',$data);
            $json = json_decode($curl,true);
            if($json['status']){// สำเร็จo
               $this->session->set_flashdata('success', 'แจ้งฝากสำเร็จ');
               redirect('/deposit_ts');
            }else{ // ไม่สำเร็จ
               $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูล');
               redirect('/deposit_ts');
            }                   
         }else{
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูลรูปภาพ');
            redirect('/deposit_ts');                   
         }
      }else{
         $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ กรุณาตรวจสอบยอดฝาก');
         redirect('/deposit_ts');
      }
   }

   public function deposit_relsm(){
      $post = $this->input->post();
      $bk   = explode('-', $post['frombank']);
      $bank = ''; 
      if ($post['amount']!='' && $post['amount']>0) {
         switch ($bk[0]) {
            case 'KBANK': $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S1.jpg'; break;
            case 'KTB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S2.jpg'; break;
            case 'SCB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S3.jpg'; break;
            case 'BAY'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S4.jpg'; break;
            case 'BBL'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S5.jpg'; break;
            case 'TMB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S6.jpg'; break;
            default: $bank=''; break;
         }
         if($post['bcode'] != 'Y'){
            $post['bvalue'] = 0;
         }
              
         $data = array(
            'webid'     => $post['webid'],
            'userid'    => $post['userid'],
            'name'      => $post['name'],
            'username'  => $post['username'],
            'frombank'  => $post['frombank'],
            'tobank'    => $post['tobank'],
            'amount'    => $post['amount'],
            'img_url'   => $bank,
            'cdate'     => $post['cdate'],
            'ctime'     => $post['depositHour'].':'.$post['depositMin'],
            'modeid'    => '3',   
            'bcode'     => $post['bcode'],        
            'type'      => '1',
            'fromdata'  => 'Manual',
            'web'       => '',
            'agent'     => ''
         );

         if ($data['webid'] !='' && $data['userid'] !='' && $data['name'] !='' && $data['frombank'] !='' && $data['tobank'] !='' && $data['amount'] !='' && $data['amount'] > 0 && $data['img_url']) {
            $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_fdeposit','post',$data);
            $json = json_decode($curl,true);
            if($json['status']){// สำเร็จo
               $this->session->set_flashdata('success', 'แจ้งฝากสำเร็จ');
               redirect('/deposit_lsm');
            }else{ // ไม่สำเร็จ
               $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูล');
               redirect('/deposit_lsm');
            }                   
         }else{
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูลรูปภาพ');
            redirect('/deposit_lsm');         
                          
         }
      }else{
         $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ กรุณาตรวจสอบยอดฝาก');
         redirect('/deposit_lsm');
      }
   }

   public function deposit_reimi(){
      $post = $this->input->post();
      $bk   = explode('-', $post['frombank']);
      $bank = ''; 
      if ($post['amount']!='' && $post['amount']>0) {
         switch ($bk[0]) {
            case 'KBANK': $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S1.jpg'; break;
            case 'KTB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S2.jpg'; break;
            case 'SCB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S3.jpg'; break;
            case 'BAY'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S4.jpg'; break;
            case 'BBL'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S5.jpg'; break;
            case 'TMB'  : $bank = 'https://test.enjoy911.com/assets/uploads/bank_img/S6.jpg'; break;
            default     : $bank = ''; break;
         }

         if($post['bcode'] != 'Y'){
            $post['bvalue'] = 0;
         }
                  
         $data = array(
           'webid'     => $post['webid'],
           'userid'    => $post['userid'],
           'name'      => $post['name'],
           'username'  => $post['username'],
           'frombank'  => $post['frombank'],
           'tobank'    => $post['tobank'],
           'amount'    => $post['amount'],
           'img_url'   => $bank,
           'cdate'     => $post['cdate'],
           'ctime'     => $post['depositHour'].':'.$post['depositMin'],
           'modeid'    => '3',   
           'bcode'     => $post['bcode'],        
           'type'      => '1',
           'fromdata'  => 'Manual',
           'web'       => '',
           'agent'     => ''
         );

         if ($data['webid'] !='' && $data['userid'] !='' && $data['name'] !='' && $data['frombank'] !='' && $data['tobank'] !='' && $data['amount'] !='' && $data['amount'] > 0 && $data['img_url']) {
            $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_fdeposit','post',$data);
            $json = json_decode($curl,true);
            if($json['status']){// สำเร็จo
               $this->session->set_flashdata('success', 'แจ้งฝากสำเร็จ');
               redirect('/deposit_imi');

            }else{ // ไม่สำเร็จ
               $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูล');
               redirect('/deposit_imi');
            }                   
         }else{
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูลรูปภาพ');
            redirect('/deposit_imi');
         }
      }else{
         $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ กรุณาตรวจสอบยอดฝาก');
         redirect('/deposit_imi');
      }
   }

   public function deposit_lists_ts(){
      $post  = $this->input->post();
      $wid   = 'TS911';
      $uname = '0882288774';  
      $ss    = $this->session->userdata();
      $data  = array(
         'webid'     =>  $wid,
         'username'  => $uname
      );
      if ($data['webid'] !='' && $data['username']!='') {
         $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_getdidlist','get',http_build_query($data));
         $json = json_decode($curl,true);

         if($json['status']){// สำเร็จo
            $this->session->set_userdata('lists',$json); 
            $this->session->set_flashdata('success', 'แจ้งฝากสำเร็จ');
            redirect('deposit_list_ts');
         }else{ 
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูล');
            redirect('deposit/deposit_list_ts');
         }                   
      }else{
         $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูลรูปภาพ');
         redirect('deposit/deposit_list_ts');
      }
   }

   public function deposit_lists_witdraw(){
      $post = $this->input->post();
      $ss   = $this->session->userdata();

      $data = array(
         'webid'       => $post['webname'],
         'username'    => $post['username']
      );
      if ($data['webid'] !='' && $data['username']!='') 
      {
         $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_getwidlist','post',$data); 
         $json = json_decode($curl,true);

         if($json['status']){
            $this->session->set_userdata('lists_w',$json); 
            $this->session->set_flashdata('success', 'แจ้งฝากสำเร็จ');
            redirect('deposit_list_w');

         }else{ // ไม่สำเร็จ
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูล');
            redirect('deposit_list_w');
         }                   
      }else{
         $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูลรูปภาพ');
         redirect('deposit_list_w');
      }
   }

   public function deposit_lists_deposit(){
      $post = $this->input->post();
      $ss   = $this->session->userdata();
      $data = array(
         'webid'       => $post['webname'],
         'username'    => $post['username']
      );
      if ($data['webid'] !='' && $data['username']!='') 
      {
         $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_getdidlist','post',$data);
         $json = json_decode($curl,true);

         if($json['status']){// สำเร็จo
            $this->session->set_userdata('lists',$json); 
            $this->session->set_flashdata('success', 'แจ้งฝากสำเร็จ');
            redirect('deposit_list_d');
         }else{ // ไม่สำเร็จ
            $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูล');
            redirect('deposit_list_d');
         }                   
      }else{
         $this->session->set_flashdata('error', 'แจ้งฝากไม่สำเร็จ *กรุณาตรวจสอบข้อมูลรูปภาพ');
         redirect('deposit_list_d');
      }
   }
}

