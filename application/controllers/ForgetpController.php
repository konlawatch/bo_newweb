<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class ForgetpController extends BaseController
{
         /**
         * This is default constructor of the class
         */
         public function __construct()
         {
         parent::__construct(); 


         }

         public function index(){

         $data = array();
         $content['title']   = 'IMIBETONLINE.COM';
         $content['disc']    = 'bo imi disc';
         $this->load->model('SettingModel');
         $this->session->userdata();
         $data = $this->SettingModel->getdata1();
         $set_data = array();
         foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
         }
         $this->global['data'] = $set_data;
         $content['content'] = $this->load->view('forgetpass/main',$this->global, true);
         $this->load->view('layout/login',$content);
         }



         public function forgetnewpass(){


         $data = array();
         $content['title']   = 'IMIBETONLINE.COM';
         $content['disc']    = 'bo imi disc';
         $this->load->model('SettingModel');
         $this->session->userdata();
         $data = $this->SettingModel->getdata1();
         $set_data = array();
         foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
         }
         $this->global['data'] = $set_data;
         $content['content'] = $this->load->view('forgetpass/otpforget',$this->global, true);
         $this->load->view('layout/login',$content);
         }

         public function fgnewpass(){


         $data = array();
         $content['title']   = 'IMIBETONLINE.COM';
         $content['disc']    = 'bo imi disc';
         $this->load->model('SettingModel');
         $this->session->userdata();
         $data = $this->SettingModel->getdata1();
         $set_data = array();
         foreach ($data as $k => $v) {
         $set_data[$v['key_all']] = $v;
         }
         $this->global['data'] = $set_data;
         $content['content'] = $this->load->view('forgetpass/newpassfg',$this->global, true);
         $this->load->view('layout/app',$content);
         }




         protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
            if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
            }

            public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
            exit;
            }
         }



         public function newpassforget(){

            $post = $this->input->post();
            $web = 'DEMO';
            if ($post['otp']!='') {
               if($post['newpass']==$post['cfnewpass'])
               {

                  $data = array(

                  'web'       => $web,
                  'tel'       => $post['tel'],
                  'ref'       => $post['ref'], 
                  'otp'       => $post['otp'],
                  'newpass'   => $post['newpass']

                  );

               if($data['web'] != '' &&$data['tel'] != '' && $data['ref']!= ''  && $data['otp']!= ''  && $data['newpass']!= '' ){

                  $curl   =  $this-> cUrl($this->config->config['apiurl'].'/member/m_forgetpass','post',$data);

                  $njson   = json_decode($curl,true);

                  $this->session->set_userdata($njson);  

                  if($njson['status']){
                     $this->session->set_flashdata('success', 'เปลี่ยนรหัสผ่าน สำเร็จ!!');
                     redirect('/index');

                  }else{ 
                     $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
                     redirect('/forgetnewpass');

                  } 

               }else{
                  $this->session->set_flashdata('error',  $json['msg']);
                  redirect('/forgetnewpass');
               } 
               }else{
                  $this->session->set_flashdata('error',  'รหัสผ่านไม่ถูกต้อง');
                  redirect('/forgetnewpass');
               } 

            }else{
               $this->session->set_flashdata('error',  'กรุณกรอกหมายเลข OTP ด้วย');
               redirect('/forgetnewpass');
            } 
         }



         public function otpnewpass() {


            $tel  = $this->session->userdata();
            $web  = 'DEMO';



            if($tel['user']!=''){// debug($tel,true);
            //$api_url = 'https://demoapi.botbo21.com';
            $op = array(
            'web' => $web, // web ที่ต้องการใช้งาน ตอนนี้ตัวทดลองให้ใช้ DEMO ไปก่อน
            'tel' => $tel['user']// ใส่เบอร์ที่ต้องการส่ง otp ไป

            );

            // debug($op,true);
            $this->session->set_userdata($op);

            if($op['web'] != '' && $op['tel'] != ''){

               $curl   =  $this->cUrl($this->config->config['apiurl'].'/otp/get_otp','get', http_build_query($op));
               $json   = json_decode($curl,true); 

               echo $curl;

            if($json['status']){

               $this->session->set_userdata($json); 
               redirect('/forgetnewpass');

            }else{ 
               debug($json,true);
               $this->session->set_flashdata('error',  $json['msg']);
               redirect('/');
            } 
            }else{
               $this->session->set_flashdata('error',  $json['msg']);
               redirect('/');

            }
            }else{
               $this->session->set_flashdata('error',  $json['msg']);
               redirect('/');
            } 

         }

         public function chkuseruse(){
            $post = $this->input->post();

            $data = array(
            'user' => $post['tel']
            );
            $this->session->set_userdata($data['user']);
            if ($data['user']!='') {

               $curl    =   $this->cUrl($this->config->config['apiurl'].'/member/m_checkexits','post',$data);
               $json    =   json_decode($curl,true);
               echo $curl; 

            if($json['status']){

               $this->session->set_flashdata('success', 'หมายเลขท่านไม่มีใน ระบบ กรุณาตรวจสอบหมายเลข');
               redirect('/forgetpass');

            }else{ 
          
               $this->session->set_userdata($data);
               redirect('/forgetpass/otpnewpass');

            }


            }else{
            echo 'web or tel === null';


            } 
         }

}


