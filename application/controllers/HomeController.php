 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
class HomeController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('DatatextModel');
		$this->isLoggedIn();
		
	}

	
	public function index(){
		
		
		$data = array();
		$content['title']   = 'BO-DEMO.COM';
		$content['disc']    = 'BO-DEMO';
    $content['tapber'] = 'home';
		
		// $this->session->userdata();
        
		$data = $this->DatatextModel->getdata();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        $this->global['data'] = $set_data;
        $this->global['xx'] =  $this->session->userdata();

        // debug($this->global['xx'],true);
		$content['content'] = $this->load->view('home/index',$this->global, true);
		$content['session'] = $this->session->userdata();
	
		$this->load->view('layout/user',$content);
		
	}



	  protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
                if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
        }

        public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
                exit;
            }
        }
    


  	public function setcookie(){

  			$tt = '1';

  			$data = array(
          
         			'session_ads' => 'hide'
      		);

  			$this->session->set_userdata($data);
  			redirect('/home');	
  		}

  	public function user_info(){

            $ss = $this->session->userdata();
            $post = $this->input->post();

            $data = array(
                  'user'     =>  $ss['data']['userid'],
                  'token'    =>  $ss['data']['token']
                );   
        if ($data['user'] !='' && $data['token'] !='' ){

            $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_info','post',$data);
            $json = json_decode($curl,true);
            $this->session->set_userdata($json);

            if($json['status']!=true || $json['data']==''){
                  
                  $this->session->sess_destroy (); 
                  echo $curl;
              }           
          }else{
              
               $x = array('status' => false , 'msg' => 'logout');
               echo json_encode($x);

               } 


  }
}
