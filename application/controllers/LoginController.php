<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
class LoginController extends BaseController {

	public function __construct() {
		parent::__construct();

      $this->keyword      = $this->config->config['key'];
      $this->website      = $this->config->config['website'];
      $this->website_desc = $this->config->config['website_desc'];
	}

	public function index()
   {
      $this->isLoggedIn();

   }

   function isLoggedIn()
   {
      $isLoggedIn = $this->session->userdata('isLoggedIn');
      if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
      {
         $data = array();
         
         $content['title']   = $this->website;
         $content['disc']    = $this->website_desc;

         // $this->load->model('SettingModel');
         // $data     = $this->SettingModel->getdata1();
         // $set_data = array();
         // foreach ($data as $k => $v) {
         //    $set_data[$v['key_all']] = $v;
         // }

         // $this->global['data']    = $set_data;

         // $curl = $this->cUrl('https://back-office.enjoy911.com/service/config_setting_api','post','slideshow');
         // $json = json_decode($curl,true);
         // //debug($json,true);
         // $content['slide111'] = $this->session->set_userdata($json);

         $this->global['website'] = $this->website;
         $content['content']      = $this->load->view('login',$this->global, true);
         $this->load->view('layout/login',$content);  
      }else{
         redirect('/main');
      }
   }  

	public function login(){
		$data = array();
		$get = $this->input->get();

		if(!isset($get['username'])){
			$data    = array();
         $content = array();
         $content['title']   = $this->website;
         $content['disc']    = $this->website_desc;
         // $this->load->model('SettingModel');
         // $this->session->userdata();
         
         // $data = $this->SettingModel->getdata1();
         //     $set_data = array();
         //     foreach ($data as $k => $v) {
         //     $set_data[$v['key_all']] = $v;
         //     }
         // // debug($set_data,true);
         // $this->global['data']    = $set_data;
         $this->global['website'] = $this->website;
         $content['content'] = $this->load->view('login',$this->global, true);
         $this->load->view('layout/login',$content);
		}else{
			$data = array();
         $content['title']   = $this->website;
         $content['disc']    = $this->website_desc;
         $content['tapber']  = 'home';
         
         $data = $this->DatatextModel->getdata();
             $set_data = array();
             foreach ($data as $k => $v) {
             $set_data[$v['key_all']] = $v;
             }
         $this->global['data']    = $set_data;
         $this->global['xx']      =  $this->session->userdata();
         $this->global['website'] = $this->website;

         $content['content'] = $this->load->view('main',$this->global, true);
         $content['session'] = $this->session->userdata();
     
         $this->load->view('layout/app',$content);
		}
	}

	public function forgot_pass(){
		$data = array();
		$content['title']   = $this->website;
      $content['disc']    = $this->website_desc;
		$content['content'] = $this->load->view('forgot_pass',$data,true);
		$this->load->view('layout/login',$content);
	}

	public function register(){
		$content = array();
      $content['title']   = $this->website;
      $content['disc']    = $this->website_desc;
      $data = array(
         'banklist' => $this->config->config['banklist']
      );
      $content['content'] = $this->load->view('register',$data, true);
      $this->load->view('layout/login',$content);
	}

	public function register_input(){
		$data = array();
		$content['title']   = $this->website;
      $content['disc']    = $this->website_desc;
		$content['content'] = $this->load->view('register_input',$data,true);
		$this->load->view('layout/login',$content);
	}

	protected function isMobile() {
      return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
   }

   protected function generateKey($length = 4) {
	   $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	   $charactersLength = strlen($characters);
	   $randomString = '';
	   for ($i = 0; $i < $length; $i++) {
	      $randomString .= $characters[rand(0, $charactersLength - 1)];
	   }
	   return $randomString;
	}

   public function logout(){

      setcookie($this->keyword."auth2fa",'');
      redirect('login', 'refresh');
   }

   public function getConfig(){
      $get = $this->input->get();
      $data = array(
         'dataname' => $get['key'],
      );
      $curl = cUrl('https://back-office.enjoy911.com/service/config_promotion_api','post',$data);
      $json = json_decode($curl,true);

      debug($json,true);
   }

   public function getConfigSet(){
      $get = $this->input->get();
      $data = array(
         'dataname' => $get['key'],
      );
      $curl = cUrl('https://back-office.enjoy911.com/service/config_setting_api','post',$data);
      $json = json_decode($curl,true);

      debug($json,true);
   }

   public function demo()
   {
      $param = $this->input->get();

      if(isset($param['s']) && isset($param['p'])){
         if($param['s'] == $this->config->config['sign_api']){
            $token = 'IZQ1wlDmha2gcYicb6NfnG/NvAFN//of9krbSabKqwZfb/zBGkV1HAvP1mKvSHb6hBkTPhwdCnox3O3GBPz23pu6RFxgwi1XRq17Ju/6hpWMtRJR2D+Vi6lwnFK+mtpKuwfQ85ZQegupEDhSEc3sI5eFS/jm1ynab2pRcCtTqL6WH62u4fcnhngrFUWvjBOsFCfMBa52Hvx3A/nIbHrRDw14SENsYRm8Q2WNNFpmy0cy8/GZkhRhVOKgWwCbsb2Qwa838p11eBRAxPlH9hEZSe9pR3HJJdjFElV/6SBCBJiSkF67wZpTDluUf5flpYJJjpdum/GD77tc/d3+hQLjxH8xZ2b+YrvJsAgtM1tKH23eVYjmsairWE/i9x0WMHs40msKSGbk+hhM0hS1kPuDDv9LrTb20EjdR9dyajGA0SJfoEmzQo0ZNRRz10wE6asKBRUkGq4ycVFSqW2hjdebX+U14V47cs8UQJ32GaVJzLA0cDfh+uanlSsWlbDuOUUKm7iMCtr40xe96GA9cBAeD2UcIHH/KcRyfBLh/dpJMG/ZluczTwWhiqX6Wi2JWwJe';
            $de = json_decode(decode($token,$this->config->config['sign_api']),true);
            // debug($de,true);
            $json = array(
               'data' => array(
                  'userid' => $de['userid'],
                  'name'   => $de['name'],
                  'bank'   => $de['bank'],
                  'token'  => $de['userid'],
               ),
               'isLoggedIn' => $token,
            );
            $this->session->set_userdata($json); 
            redirect('/'.$param['p']);
         }else{
            echo 'denide';
         }
      }else{
         echo 'denide';
      }
   }

	public function loginMe()
   {
      $this->load->library('form_validation');
      $post = $this->input->post();
      $this->form_validation->set_rules('username', 'Username', 'required|max_length[32]');
      $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
      if($this->form_validation->run() == FALSE)
      {
         $this->index();
      }else{
         $username = $this->security->xss_clean($this->input->post('username'));       
         $password = $this->input->post('password');

         $pm = array(
            'user' => $username, 
            'pass' => $password 
         );
         if($pm['user'] != '' && $pm['pass'] != '')
         {
            $curl   = cUrl($this->config->config['apiurl'].'/member/m_login','post',$pm);
            // echo $curl;exit();
            $json   = json_decode($curl,true);
            // $json['session_ads'] = $this->global['data1']['popup']['value'];
            // $json['isLoggedIn']  = $json['data']['token'];
            // debug($json,true);
            if($json['status']){
               $json['isLoggedIn']  = $json['data']['token'];
               // setcookie($this->keyword."userid",'');
               // setcookie($this->keyword."name",'');
               // setcookie($this->keyword."token",'');
               // setcookie($this->keyword."userid",$json['data']['userid']);
               // setcookie($this->keyword."name",$json['data']['name']);
               // setcookie($this->keyword."token",$json['data']['token']);
               // debug($json,true);
               $this->session->set_userdata($json); 
               if($json['data']['name'] != '' && $json['data']['bank'] != '')
               {  
                  redirect('/main');
               }else{
                  // $this->session->set_flashdata('success', 'success');
                  redirect('/abank');
               }        
            }else{ // ไม่สำเร็จ
               $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
               redirect('/login'); 
            }
         }else{
            redirect('/login');
         }     
      }
   }
}
