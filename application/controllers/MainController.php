 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
class MainController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->isLoggedIn();  

   	$this->website 			 = $this->config->config['website'];
    	$this->website_desc 	 	 = $this->config->config['website_desc']; 
   	$this->global['website'] = $this->config->config['website'];
   	$this->key               = $this->config->config['sign_api']; 
   	$this->token             = $this->session->userdata();
	}

	public function index(){ 
		$data = array();
		$content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
    	$content['tapber']  = 'main';

    	$this->check_info();

     	$this->global['user_info'] = json_decode(decode($this->token['isLoggedIn'],$this->key),true);
		$content['content'] = $this->load->view('main',$this->global, true);
		$content['session'] = $this->session->userdata();
		 
		$this->load->view('layout/app',$content);
	}

	public function invite(){
		echo 'comming soon.';
	}


	protected function check_info(){
		$token = $this->session->userdata('isLoggedIn');
		$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
		if($token != ''){
			$pm = array(
				'user' 	=> (isset($xx['userid'])) ? $xx['userid'] : '',
				'token' 	=> (isset($xx['token'])) ? $xx['token'] : '',
			);
			$curl = cUrl($this->config->config['apiurl'].'/member/m_info','post', $pm);
			$json = json_decode($curl,true);
			// debug($json,true);
			if($json['status']){
				$this->session->set_userdata('isLoggedIn',$json['data']);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
