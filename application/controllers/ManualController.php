<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManualController extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index(){
		$data = array(
			'title' => $this->config->config['website'],
			'website' => $this->config->config['website'],
			'website_desc' => $this->config->config['website_desc'],
			'mlist' => array(
				'0' => array(
					'title' 		=> 'วิธีการสมัครสมาชิก',
					'video_url' => 'https://via.placeholder.com/150',
					'image_url' => 'https://via.placeholder.com/150',
				),
				'1' => array(
					'title' 		=> 'วิธีการฝากเงิน',
					'video_url' => 'https://via.placeholder.com/150',
					'image_url' => 'https://via.placeholder.com/150',
				),
				'2' => array(
					'title' 		=> 'วิธีการถอนเงิน',
					'video_url' => 'https://via.placeholder.com/150',
					'image_url' => 'https://via.placeholder.com/150',
				),
				'3' => array(
					'title' 		=> 'วิธีการเปลี่ยนบัญชี',
					'video_url' => 'https://via.placeholder.com/150',
					'image_url' => 'https://via.placeholder.com/150',
				),
				'4' => array(
					'title' 		=> 'วีธีการแก้ไขข้อมูลส่วนตัว',
					'video_url' => 'https://via.placeholder.com/150',
					'image_url' => 'https://via.placeholder.com/150',
				),
				'5' => array(
					'title' 		=> 'วิธีการแนะนำเพื่อน',
					'video_url' => 'https://via.placeholder.com/150',
					'image_url' => 'https://via.placeholder.com/150',
				),
			),
		);

		$content['content'] = $this->load->view('manual/main',$data, true);
		// $content['content'] = '';

		$this->load->view('layout/login',$content);
	}
}

