<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class MyotpController extends BaseController
{
    /**
     * This is default constructor of the class
     */

    public function __construct()
    {
        parent::__construct(); 
        
         
    }
    
    
   

    public function index(){
        $data = array();
        $content['title']   = 'IMIBETONLINE.COM';
        $content['disc']    = 'bo imi disc';
        $this->load->model('SettingModel');
        $this->session->userdata();
        $data = $this->SettingModel->getdata1();
        $set_data = array();
        foreach ($data as $k => $v) {
        $set_data[$v['key_all']] = $v;
        }
        $this->global['data'] = $set_data;
        $content['content'] = $this->load->view('register/main',$this->global, true);
        // $content['content'] = '';
        $this->load->view('layout/register',$content);
    }
    
    public function loginotp(){

        $data = array();
        $content['title']   = 'IMIBETONLINE.COM';
        $content['disc']    = 'bo imi disc';
        $this->load->model('SettingModel');
        $this->session->userdata();
        $data = $this->SettingModel->getdata1();
        $set_data = array();
        foreach ($data as $k => $v) {
        $set_data[$v['key_all']] = $v;
        }
        $this->global['data'] = $set_data;
        $content['content'] = $this->load->view('register/loginotp',$this->global, true);
        $this->load->view('layout/register',$content);

    }

    protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
                if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                // 'Access-Token : o.Y5qB3SAlfQOmRPvkzjrNRdJbVj2jzLJU',
                // 'Access-Token : o.9aa5NSzaMNSqsAgPoeUFRIAktxuVBJDO',
                'Access-Token : o.GDvT1sBhNP5uJwvBqME7ysGshzNmAm6C',
                'Content-Type: application/json',
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
    }

    public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
                exit;
            }
    }
    
    // public function otp() {
    
    //     $post = $this->input->post();
    //     $web  = 'DEMO';
    //     $ss   = $this->session->userdata();
        
    //     $op = array(
    //         'web' => $web, 
    //         'tel' => $ss['user']  

    //     );
          
       
    //   $this->session->set_userdata($op);

    //     if($op['web'] != '' && $op['tel'] != ''){
           
           
    //         $curl   =  $this->cUrl($this->config->config['apiurl'].'/otp/get_otp','get', http_build_query($op));
    //         $json   = json_decode($curl,true);
           
    //          echo $curl;
             
    //          $this->session->set_userdata($json);  

    //         if($json['status']){

    //            redirect('/loginotp');

    //         }else{ 
               
    //             $this->session->set_flashdata('error1', 'หมายเลขไม่ถูกต้องกรุณาตรวจสอบหมายเลขของท่านด้วย');
    //             redirect('/requestotpid');
    //         } 
    //     }else{
    //         echo 'web or tel === null';
    //         redirect('/requestotpid');
    //     } 
       
    // }


    // public function checkotpid(){

    //     $post = $this->input->post();
    //     $api_url = 'https://demoapi.botbo21.com';
    //     $web = 'DEMO';

    //     $pm = array(
    //         'web' => $web,
    //         'tel' => $post['tel'],
    //         'ref' => $post['Ref'], 
    //         'otp' => $post['otppass']
    //     );

           
   
    //     if($pm['web'] != '' && $pm['tel'] != '' && $pm['ref'] != '' && $pm['otp'] != ''){
           
    //         $curl   =  $this->cUrl($this->config->config['apiurl'].'/otp/check_otp','get', http_build_query($pm));
    //         $json   = json_decode($curl,true);
         
     
    //         if($json['status']){
           
    //             $this->session->set_userdata($pm);
    //             redirect('/newuser');
    //         }else{ 
    //             $this->session->set_flashdata('error1', 'หมายเลข OTP ขอท่านไม่ถูกต้อง');
    //             redirect('/loginotp');

    //         }
    //     }else{
    //          $this->session->set_flashdata('error1', 'หมายเลข OTP ขอท่านไม่ถูกต้อง');
    //          redirect('/loginotp');
    //     }  
        


    // }

  public function otp() {
    
        $post = $this->input->post();
        // debug($post,true);
        $web  = 'DEMO';
       
        $op = array(
            'web' => $web, 
            'tel' => $post['username']  

        );
          
     
            if($post['username']!=''){
                $this->session->set_userdata($op);
               redirect('/loginotp');

            }else{ 
               
                $this->session->set_flashdata('error1', 'หมายเลขไม่ถูกต้องกรุณาตรวจสอบหมายเลขของท่านด้วย');
                redirect('/register');
            } 
        
       
    }


public function checkotpid(){

        $post = $this->input->post();
        $api_url = 'https://demoapi.botbo21.com';
        $web = 'DEMO';

        $pm = array(
            'web' => $web, 
            'tel' => $post['tel'], 
            'ref' => $post['Ref'], 
            'otp' => $post['otppass']   
        );

           
       
        // if($pm['web'] != '' && $pm['tel'] != '' && $pm['ref'] != '' && $pm['otp'] != ''){
           
        //     $curl   =  $this->cUrl($this->config->config['apiurl'].'/otp/check_otp','get', http_build_query($pm));
        //     $json   = json_decode($curl,true);
           
     
            if($post['otppass'] == '56789'){
                
                $this->session->set_userdata($pm);
                // debug($pm,true);
                redirect('/newuser');
            }else{ // ไม่สำเร็จ 
                $this->session->set_flashdata('error1', 'หมายเลข OTP ขอท่านไม่ถูกต้อง'); 
                redirect('/loginotp');

            }
        
        


    }

}

?>