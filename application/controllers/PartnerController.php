<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class PartnerController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->isLoggedIn();
		$this->key  = $this->config->config['sign_api']; 
	}

	public function index(){
     	$res = array(
         'status' => false,
         'data'   => '',
         'msg'    => '',
     	);

     	$param = $this->input->get();
     	$info  = $this->session->userdata();

     	if(isset($info['isLoggedIn'])){
     		if(isset($param['partner'])){

     			$chkpartner = array('ts911','imi','lsm','ufa');
     			if(!in_array($param['partner'],$chkpartner)){
     				$res['msg'] = 'partner error';
     				echo json_encode($res);exit();
     			}

	     		$d 	= json_decode(decode($info['isLoggedIn'],$this->key),true);
	     		$user = $pass = '';
	     		switch (strtolower($param['partner'])) {
	     			case 'ts911':
	     				$user = $d['user_ts'];
	     				$pass = $d['pass_ts'];
     				break;
     				case 'imi':
	     				$user = $d['user_imi'];
	     				$pass = $d['pass_imi'];
     				break;
     				case 'lsm':
	     				$user = $d['user_lsm'];
	     				$pass = $d['pass_lsm'];
     				break;
     				case 'ufa':
	     				$user = $d['user_ufa'];
	     				$pass = $d['pass_ufa'];
     				break;
	     		}
	     		// debug($d,true);
	     		$time = time();
	         	$data = array(
		         	'web'  	  => strtoupper($param['partner']),
		         	'user' 	  => $user,
		         	'pass' 	  => $pass,
		         	'credit'  => '0',
		         	'website' => $this->config->config['website'],
		         	'login'   => md5(strtolower($param['partner']).$time.$this->key),
		         	'time'    => $time,
	         	);
	         	if($user == ''){
	         		$xx   = array(
		               'webid' => strtoupper($param['partner']),
		               'ag'    => '',
		            );

		            $curl = cUrl($this->config->config['apiurl'].'/member/m_getbankbyag','post',$xx); 
		            $json = json_decode($curl,true);
		            // debug($json,true);
		            $data['bank'] = array();
		            if($json['status']){
		               $data['bank'] = $json['data']['bank'];
		            }

		            $data['did_amt'] = '0';
	         		$res['data']   = $this->load->view('deposit/rfdid',$data,true);
	         	}else{
	         		$res['data']   = $this->load->view('partner/main',$data,true);
	         	}
	         	$res['status'] = true;
	      	}else{
	      		$res['msg'] = 'no param partner';
	      	}
     	}else{
         	$res['msg'] = 'please login.';
     	}
  		echo json_encode($res);
   	}

   	public function login_partner(){
   		$param = $this->input->get();
   		if($param){
   			if(isset($param['p']) && isset($param['k']) && isset($param['t'])){
   				$chk  = md5(strtolower($param['p']).$param['t'].$this->key);
   				if($param['k'] == $chk){
   					$token = $this->session->userdata('isLoggedIn');
   					$user  = $pass = '';
					if($token != ''){
						$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
						if(strtoupper($param['p']) == 'TS911'){
							$user = $xx['user_ts'];
							$pass = $xx['pass_ts'];
						}else if(strtoupper($param['p']) == 'LSM'){
							$user = $xx['user_lsm'];
							$pass = $xx['pass_lsm'];

							$data = array(
								'username' 		=> $user,
								'usernamelogin' => $user,
								'password' 		=> $pass,
							);

							echo $this->ApiLSM('https://lsm2019.com/web-login','post',$data);exit();
						}else if(strtoupper($param['p']) == 'IMI'){
							$user 	= $xx['user_imi'];
							$pass 	= $xx['pass_imi'];
							$mobile = '';
							$url 	= 'https://authapi.linkv2.com/api/player/login';
							
							$d = array(
								'UserName'   => $user,
								'Password'   => $pass,
								'Lang'  	 => 'EN', 
								'Com'  	 	 => 'IMIBET', 
								'IsMobile'   => $mobile, 
							);

							$curl = $this->ApiIMI($url,'post',json_encode($d));
							$res  = json_decode($curl,true);
							// debug($res,true);
							if($res['ErrorCode'] == '0'){
								header('Location: '.$res['RedirectUrl']);exit();
							}else{
								redirect('main');
							}
						}else if(strtoupper($param['p']) == 'UFA'){
							$user = $xx['user_ufa'];
							$pass = $xx['pass_ufa'];
						}else{
							redirect('main');
						}
				   		$curl = cUrl($this->config->config['apiurl'].'/api/getkey?sign=C5Z10zzL4M7BiOSmEgyoAcnw5g38CvO2','get'); 
						$json = json_decode($curl,true);
						$data = array(
							'u'   => $user,
							'p'   => $pass,
							'key' => $json,
						);
						$this->load->view('partner/login_'.strtolower($param['p']),$data);
					}else{
						redirect('main');
					}
				}else{
					redirect('main');
				}
			}else{
				redirect('main');
			} 
		}else{
			redirect('main');
		} 
   	}

   	public function getlsm(){
		$data = array(
			'username' => 'DTGTEST10001',
			'usernamelogin' => 'DTGTEST10001',
			'password' => 'aa@123456',
		);
		$xx = $this->Api('https://lsm2019.com/web-login','post',$data);
		// debug($xx,true);
	}

	protected function ApiIMI($url, $method = "get", $data = "", $ssl = false){
        if ($method == "post"){
            if ($data == "") return false;
        }
        $ch = curl_init();
        if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
        else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post"){
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

	protected function ApiLSM($url, $method = "get", $data = "", $ssl = false){
        if ($method == "post"){
            if ($data == "") return false;
        }
        $ch = curl_init();
        if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
        else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post"){
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_REFERER, "https://www.lsm99online.com");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
        ));
        $ip = '172.67.74.103:443';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "HTTP_X_FORWARDED_FOR: $ip"));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $content = curl_exec($ch);
        $info = curl_getinfo($ch);
        // debug($info);
        curl_close($ch);
        
        return $content;
    } 
}

