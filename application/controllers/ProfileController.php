 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
class ProfileController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->isLoggedIn();
		$this->website 			 = $this->config->config['website'];
      $this->website_desc 		 = $this->config->config['website_desc']; 
      $this->global['website'] = $this->config->config['website'];
      $this->key               = $this->config->config['sign_api']; 
   	$this->token             = $this->session->userdata();
	}
	
	public function index(){

		$data = array();
		$content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
		$content['tapber']  = 'setting';

  		$this->global['user_info'] = json_decode(decode($this->token['isLoggedIn'],$this->key),true);
		$content['content'] 	 = $this->load->view('profile/main',$this->global, true);
		$content['session'] 	 = $this->session->userdata();		
		$this->load->view('layout/app',$content);
	}

	public function r_p_ts(){

		$data = array();
		$content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
		$content['tapber'] = 'setting';
		
		$this->session->userdata();
        
		$data = $this->SettingModel->getdata1();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        $this->global['data'] = $set_data;
		$content['content'] = $this->load->view('profile/repassweb_ts',$this->global, true);
		$content['session'] = $this->session->userdata();		
		$this->load->view('layout/app',$content);
		
	}
	public function r_p_lsm(){

		$data = array();
		$content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
		$content['tapber'] = 'setting';
		
		$this->session->userdata();
        
		$data = $this->SettingModel->getdata1();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        $this->global['data'] = $set_data;
		$content['content'] = $this->load->view('profile/repassweb_lsm',$this->global, true);
		$content['session'] = $this->session->userdata();		
		$this->load->view('layout/app',$content);
		
	}
	public function r_p_imi(){

		$data = array();
		$content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
		$content['tapber'] = 'setting';
		
		$this->session->userdata();
        
		$data = $this->SettingModel->getdata1();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        $this->global['data'] = $set_data;
		$content['content'] = $this->load->view('profile/repassweb_imi',$this->global, true);
		$content['session'] = $this->session->userdata();		
		$this->load->view('layout/app',$content);
		
	}




	
	
		

		
	
	


	

}
