 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
class PromotionController extends BaseController {

	public function __construct() {
		parent::__construct();

		$this->isLoggedIn();  

   	$this->website 			 = $this->config->config['website'];
      $this->website_desc 		 = $this->config->config['website_desc']; 
   	$this->global['website'] = $this->config->config['website'];
   	$this->key               = $this->config->config['sign_api']; 
   	$this->token             = $this->session->userdata();
	}

	public function index(){
		$data = array();
		$content['title']   = $this->website;
		$content['disc']    = $this->website_desc;
    	$content['tapber']  = 'main';
		
		// $data 	 = $this->DatatextModel->getdata();
  //    	$set_data = array();
  //    	foreach ($data as $k => $v) {
  //       	$set_data[$v['key_all']] = $v;
  //    	}
     	// $this->global['data'] 	 	= $set_data;
     	$this->global['promotion_list'] = array(
			'0' => array(
				'main' 		=> 'วิธีรับคำแนะนำ',
				'title' 		=> 'เพียงแชร์ก็ได้เงินแล้ว ยิ่งแชร์มาก ยิ่งได้มาก',
				'image_url' => 'https://via.placeholder.com/900x200',
				'desc' 		=> '<p>ท่านสามารถนำลิงก์ด้านล่างนี้หรือนำป้ายแบนเนอร์ ไปแชร์ในช่องทางต่างๆ ไม่ว่าจะเป็น เว็บไชต์ส่วนตัว, Blog, Facebook หรือ Social Network อื่นๆ หากมีการสมัครสมาชิกโดยคลิกผ่านลิงก์ของท่านเข้ามา ลูกค้าที่สมัครเข้ามาก็จะอยู่ภายให้เครือข่ายของท่านทันที และหากลูกค้าภายใต้เครือข่ายของท่านมีการเดิมพันเข้ามา ทุกยอดการเดิมพัน ท่านจะได้รับส่วนแบ่งในการแนะนำ 
         <span class="revenue-sharing"> 8% </span>ทันทีโดยไม่มีเงื่อนไข</p>
         <p class="ml-3">ตัวอย่างการคำนวน</p>
         <p class="ml-3">ลูกค้า 1 คนแทง 1,000 ท่านจะได้รับ 80 บาท</p>
         <p class="ml-3">ลูกค้า 10 คนแทง 1,000 ท่านจะได้รับ 800 บาท</p>
         <p class="ml-3">ลูกค้า 100 คนแทง 1,000 ท่านจะได้รับ 8,000 บาท</p>
         <p>สามารถทำรายได้เดือน 100,000 บาทง่ายๆเลยทีเดียว เพราะทางเรามีหวยเปิดรับทายผลทุกวัน มีมากกว่าวันละ 200 รอบ เปิดรับแทงออนไลน์ตลอด 24 ชม. และรายได้ทุกบาททุกสตางค์ของท่านสามารถตรวจสอบได้ทุกขั้นตอน งานนี้แจกจริง จ่ายจริง ที่นี่ที่เดียวที่ให้คุณมากกว่าใคร คัดลอกลิงก์แนะนำและข้อความด้านล่างนี้ นำไปแชร์ได้เลย</p>
         <p style="color: rgb(0, 193, 158);"><strong>หมายเหตุ:</strong>&nbsp;รายได้การช่วยแชร์ช่วยแนะนำของท่านสามารถแจ้งถอนได้ทุกเวลา หากมียอดรายได้มากกว่า 500 บาทขึ้นไป</p>',
			),
			'1' => array(
				'main' 		=> 'วิธีรับคำแนะนำ 1',
				'title' 		=> 'เพียงแชร์ก็ได้เงินแล้ว ยิ่งแชร์มาก ยิ่งได้มาก 1',
				'image_url' => 'https://via.placeholder.com/900x300',
				'desc' 		=> '<p>ท่านสามารถนำลิงก์ด้านล่างนี้หรือนำป้ายแบนเนอร์ ไปแชร์ในช่องทางต่างๆ ไม่ว่าจะเป็น เว็บไชต์ส่วนตัว, Blog, Facebook หรือ Social Network อื่นๆ หากมีการสมัครสมาชิกโดยคลิกผ่านลิงก์ของท่านเข้ามา ลูกค้าที่สมัครเข้ามาก็จะอยู่ภายให้เครือข่ายของท่านทันที และหากลูกค้าภายใต้เครือข่ายของท่านมีการเดิมพันเข้ามา ทุกยอดการเดิมพัน ท่านจะได้รับส่วนแบ่งในการแนะนำ 
         <span class="revenue-sharing"> 8% </span>ทันทีโดยไม่มีเงื่อนไข</p>',
			),
		);
     	$this->global['user_info'] = json_decode(decode($this->token['isLoggedIn'],$this->key),true);
		$content['content'] = $this->load->view('promotion/main',$this->global, true);
		$content['session'] = $this->session->userdata();
	
		$this->load->view('layout/app',$content);
	}
}
