<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class RegisterController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct(); 
        
        
    }
    
    // public function index(){

    //     $data = array();
    //     $content['title']   = 'IMIBETONLINE.COM';
    //     $content['disc']    = 'bo imi disc';
    //     $this->load->model('SettingModel');
    //     $this->session->userdata();
    //     $data = $this->SettingModel->getdata1();
    //     $set_data = array();
    //     foreach ($data as $k => $v) {
    //     $set_data[$v['key_all']] = $v;
    //     }
    //     $this->global['data'] = $set_data;
      
    //     $content['content'] = $this->load->view('register',$this->global, true);
    //     $this->load->view('layout/register',$content);
    // }

     public function data_form(){
        $this->session->userdata();
        $data = array();
        $content['title']   = 'IMIBETONLINE.COM';
        $content['disc']    = 'bo imi disc';
        $this->load->model('SettingModel');
        $this->session->userdata();
        $data = $this->SettingModel->getdata1();
        $set_data = array();
        foreach ($data as $k => $v) {
        $set_data[$v['key_all']] = $v;
        }
        $this->global['data'] = $set_data;
        
        $content['content'] = $this->load->view('register/form_data',$this->global, true);

        // $content['content'] = '';
        $this->load->view('layout/login',$content);
    }

  
  
  


    protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
                if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
        }

        public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
                exit;
            }
        }



    public function create() {
        $post = $this->input->post();
        // debug($post,true);
        if($post['pass'] == $post['chkpass']){
            // debug($post,true);
            $data = array(
            
                'user'      => $post['username'],
                'pass'      => $post['pass'],
                'lineid'    => $post['lineid'],
                'tel'       => $post['tel']


            );
       // debug($data,true);
       //echo $cre['user'];
        

           if($data['user'] != '' && $data['pass'] != '' && $data['tel'] !='' ){
           
            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_register','post',$data);
            //echo $curl;exit();
            $json   = json_decode($curl,true);

           //debug($curl,true);
               

                if($json['status']){// สำเร็จ

                    //RegisterController::debug($json);
                    $this->session->set_flashdata('สมัครสมาชิกสำเร็จ', 'success on save data');
                    // redirect('/newusersuccess');
                    redirect('/index');

                }else{ // ไม่สำเร็จ
                	$this->session->set_flashdata('error', 'กรุณาตรวจสอบข้อมูล');
                    // echo 'ไม่สำเร็จ';
                    redirect('/newuser');
            
                }
            }else{
                echo 'web or tel === null';
                redirect('/newuser');

            } 
        
       }else{
            $this->session->set_flashdata('error', 'รหัสไม่ถูกต้อง');
            redirect('/newuser');
       }

    }





    function chkuseruse(){
        $post = $this->input->post();
          // debug($post,true);
            $data = array(
                'user' => $post['username']
            );
            $this->session->set_userdata($data);
       if ($data['user']!='') {
         
            $curl    =   $this->cUrl($this->config->config['apiurl'].'/member/m_checkexits','post',$data);
            $json    =   json_decode($curl,true);
            // echo $data['user'];
            echo $curl; 

             //debug($json,true);
             if($json['status']){// สำเร็จ

                //RegisterController::debug($json);
                $this->session->set_flashdata('success', 'success on save data');
                redirect('/requestotpid/otp');

            }else{ // ไม่สำเร็จ
                $this->session->set_flashdata('error', 'มีหมายเลขโทรศัพท์นี้แล้ว');
                // echo 'ไม่สำเร็จ';
                redirect('/register');
        
            }

        }else{
                $this->session->set_flashdata('error1', 'กรอกหมายเลขโทรศัพท์ด้วย ค่ะ!!!');
                redirect('/register');
            } 
        

    }


    function updateinfo(){
        $post = $this->input->post();

           $data = array(

                'user'              => $post['user'],
                'token'             => $post['token'],
                'name'              => $post['name'],
                'email'             => $post['email'],
                'formdata'          => $post['formdata'],
                'lineid'            => $post['lineid']
                
            );

       if($data['user'] != '' && $data['token'] != '' && $data['name'] !='' && $data['lineid'] !=''){  
     
       
       
        $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_updateinfo','post',$data);
       // echo $curl;exit();
        $json   = json_decode($curl,true);

       //debug($curl,true);
           

        if($json['status']){// สำเร็จ

            //RegisterController::debug($json);
            $this->session->set_flashdata('สมัครสมาชิกสำเร็จ', 'success on save data');
            redirect('/deposit');

        }else{ // ไม่สำเร็จ
            $this->session->set_flashdata('error', 'กรุณาตรวจสอบข้อมูล');
            echo 'ไม่สำเร็จ';
            redirect('/main');
    
        }
    }else{
        echo 'web or tel === null';


    } 


    }

      function chkuseruselogin(){
        $post = $this->input->post();
        
        

       if($post['username'] != ''){

        // echo $post['username'];
        
        $data = array(
            'user' => $post['username']
        );
       
        $curl   =  RegisterController::cUrl($this->config->config['apiurl'].'/member/m_checkexits','post',$data);
        // $res   = json_decode($curl,true);

        echo $curl; 

       
      
    }else{
        echo 'web or tel === null';

    }

    }


    

}


?>