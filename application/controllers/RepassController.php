<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class RepassController extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct(); 
        
        
    }
    
     public function index(){

        $data = array();
        $content['title']   = 'IMIBETONLINE.COM';
        $content['disc']    = 'bo imi disc';
        $this->load->model('SettingModel');
        $this->session->userdata();
        
        $data = $this->SettingModel->getdata1();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        // debug($set_data,true);
        $this->global['data'] = $set_data;
        $content['content'] = $this->load->view('repass/main',$this->global, true);
        $this->load->view('layout/user',$content);
    }

     public function successpass(){

        $data = array();
        $content['title']   = 'IMIBETONLINE.COM';
        $content['disc']    = 'bo imi disc';
        $this->load->model('SettingModel');
        $this->session->userdata();
        
        $data = $this->SettingModel->getdata1();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        // debug($set_data,true);
        $this->global['data'] = $set_data;
        $content['content'] = $this->load->view('repass/successpass',$this->global, true);
        $this->load->view('layout/user',$content);
    }


    protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
                if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
        }

        public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
                exit;
            }
        }


   public function otpnewpass() {

        $post = $this->input->post();
     

        $data = array(
            
            'tel' => $post['tel'] 
        );
               
        if($data['tel'] != ''  ){

            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_resetpass','post',$data);
            $njson   = json_decode($curl,true);
           
           
            echo $curl;
             

            if($njson['status']){// สำเร็จ

                
               $this->session->set_flashdata('success', 'success on save data');

               redirect('/login');

            }else{ // ไม่สำเร็จ
              
                //redirect('/repass');
                $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
                echo "ไม่สำเร็จ";
            } 
        }else{
            echo 'web or tel === null';

        } 
           
    }
   


    public function npass() {
    
        $post = $this->input->post();
       
        $data = array(
           

            'user' => $post['user'],
            'token' => $post['token'], 
            'oldpass' => $post['oldpass'],
            'newpass' => $post['newpass']
            
        );
          
        if($data['user'] != '' && $data['token']!= ''  && $data['oldpass']!= ''  && $data['newpass']!= '' ){

            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_changepass','post',$data);
            $json   = json_decode($curl,true);
           
            echo $curl;
            // debug($json,true);

            if($json['status']){// สำเร็จ

                
               $this->session->set_flashdata('success', 'success on save data');

               redirect('/successpass');

            }else{ // ไม่สำเร็จ
                $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
                redirect('/repass');
                
            } 
        }else{
             echo 'web or tel === null';
             redirect('/repass');    

        } 
           
    }

    public function repass_imi() {
    
        $post = $this->input->post();
        $data_repass_imi = $this->session->userdata();
        
        if ($post['oldpass'] == $data_repass_imi['data']['pass_imi']) {
        $data = array(
           

            'webid'     => $post['webid'],
            'username'  => $post['username'], 
            'newpass'   => $post['newpass'],
           
            
        );
        // debug($data,true); 
        if($data['webid'] != '' && $data['username']!= ''  && $data['newpass']!= ''){

            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_updatepassweb','post',$data);
            $json   = json_decode($curl,true);
           
            echo $curl;
            // debug($json,true);

            if($json['status']){

                
               $this->session->set_flashdata('success', 'เปลี่ยนรหัสผ่านสำเร็จ');

               redirect('/repass_imi');

            }else{ 
                $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
                redirect('/repass_imi');
                
            } 
        }else{
             echo 'web or tel === null';
             redirect('/repass_imi');    

        } 

        }else{ 
             $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
             redirect('/repass_imi');
        }
           
    }

    public function repass_ts() {
    
        $post = $this->input->post();
        $data_repass_ts = $this->session->userdata();
        if ($post['oldpass'] == $data_repass_ts['data']['pass_ts']) {
        $data = array(
           

            'webid'     => $post['webid'],
            'username'  => $post['username'], 
            'newpass'   => $post['newpass'],
           
            
        );
        // debug($data,true); 
        if($data['webid'] != '' && $data['username']!= ''  && $data['newpass']!= ''){

            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_updatepassweb','post',$data);
            $json   = json_decode($curl,true);
           
            echo $curl;
            // debug($json,true);

            if($json['status']){

                
               $this->session->set_flashdata('success', 'เปลี่ยนรหัสผ่านสำเร็จ');

               redirect('/repass_ts');

            }else{ 
                $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
                redirect('/repass_ts');
                
            } 
        }else{
             echo 'web or tel === null';
             redirect('/repass_ts');    

        } 

        }else{ 
             $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
             redirect('/repass_imi');
        }
           
    }

    public function repass_lsm() {
    
        $post = $this->input->post();
        $data_repass_lsm = $this->session->userdata();
        if ($post['oldpass'] == $data_repass_lsm['data']['pass_lsm']) {
         
        $data = array(
           

            'webid'     => $post['webid'],  
            'username'  => $post['username'], 
            'newpass'   => $post['newpass'],
           
            
        );
        // debug($data,true); 
        if($data['webid'] != '' && $data['username']!= ''  && $data['newpass']!= ''){

            $curl   =  $this->cUrl($this->config->config['apiurl'].'/member/m_updatepassweb','post',$data);
            $json   = json_decode($curl,true);
           
            echo $curl;
            // debug($json,true);

            if($json['status']){ 

                
               $this->session->set_flashdata('success', 'เปลี่ยนรหัสผ่านสำเร็จ');

               redirect('/repass_lsm');

            }else{ 
                $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
                redirect('/repass_lsm');
                
            } 
        }else{
             echo 'web or tel === null';
             redirect('/repass_lsm');    

        }

        }else{ 
             $this->session->set_flashdata('error', 'รหัสผ่านไม่ถูกต้อง');
             redirect('/repass_imi');
        } 
           
    }
 


}


?>