<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->apiurl = $this->config->config['apiurl'];
	}

	public function index(){
		echo 'api version 0.0.1';
	}

	public function submit_fg(){
		$res  = array(
			'status'  => false,
			'msg'     => '',
			'otp_key' => '',
		);
		$param = $this->input->post();
		if($param){
			if(isset($param['username'])){
				if($param['chkuser'] == ''){
					$pm = array(
						'user' 	=> $param['username'],
					);
					$curl   = cUrl($this->apiurl.'/m_checkexits','post', $pm);
					$json 	= json_decode($curl,true);
					if(!$json['status']){
						$res['status'] = true;
						$res['msg']    = 'รหัส OTP ส่งไปที่เบอร์ตามที่ระบุและสามารถตั้งรหัสผ่านใหม่ได้เลยค่ะ';
						// $res['otp_key']= 'XSDF';
						$cc   = cUrl('https://demoapi.botbo21.com/otp/get_otp?web='.$this->config->config['website'].'&tel='.$param['username'],'get', '');
						$jj   = json_decode($cc,true);
						if($jj['status']){
							$res['otp_key'] = $jj['key'];
						}else{
							$res['msg'] = $jj['msg'];
							echo json_encode($res);exit();
						}
					}else{
						$res['msg']    = 'ไม่มีเบอร์นี้ในระบบ กรุณาตรวจสอบ';
					}
				}else{
					if($param['username'] != '' && $param['password'] != '' && $param['chkuser'] != '' && $param['otp_val'] != ''){
						$pm = array(
							'web' 		=> $this->config->config['website'],
							'tel' 		=> $param['username'],
							'ref' 		=> $param['chkuser'],
							'otp' 		=> $param['otp_val'],
							'newpass' 	=> $param['password'],
						);
						$curl   = cUrl($this->apiurl.'/m_forgetpass','post', $pm);
						// echo $curl;exit();
						$json 	= json_decode($curl,true);
						if($json['status']){
							$res['status'] = true;
							$res['msg'] = 'รีเซ็ตรหัสผา่นสำเร็จ กรุณาล็อกอินระบบ';
						}else{
							$res['msg'] = $json['msg'];
						}
					}else{
						$res['msg'] = 'กรุณาป้อนข้อมูลให้ครบ !!!';
					}
				}
			}else{
				$res['msg'] = 'no param username !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function submit_rg(){
		$res  = array(
			'status' => false,
			'msg' => '',
		);
		
		$param = $this->input->post();
		if($param){
			if(isset($param['reg_username'])){
				$pm = array(
					'user' 	=> $param['reg_username'],
				);
				$curl   = cUrl($this->apiurl.'/member/m_checkexits','post', $pm);
				$json 	= json_decode($curl,true);
				if($json['status']){
					$res['status'] = true;
					// $res['msg']    = 'รหัส OTP ส่งไปที่เบอร์ตามที่ระบุ สามารถตั้งรหัสผ่านใหม่ได้เลยค่ะ';
					// $cc   = cUrl('https://demoapi.botbo21.com/otp/get_otp?web='.$this->config->config['website'].'&tel='.$param['username'],'get', '');
					// $jj   = json_decode($cc,true);
					// if($jj['status']){
					// 	$res['otp_key'] = $jj['key'];
					// }else{
					// 	$res['msg'] = $jj['msg'];
					// 	echo json_encode($res);exit();
					// }
				}else{
					$res['msg']    = $json['msg'];
				}
			}else{
				$res['msg'] = 'no param username !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function submit_lg(){
		$res  = array(
			'status' => false,
			'msg'    => '',
			'data'   => '',
		);
		
		$param = $this->input->post();
		// debug($param,true);
		if($param){
			if(isset($param['reg_username']) && isset($param['reg_password']) && isset($param['reg_con_password']) && isset($param['reg_bank']) && isset($param['reg_bankno']) && isset($param['reg_name']) && isset($param['reg_lname'])){
				if($param['reg_password'] != '' && $param['reg_con_password'] != '' && $param['reg_password'] == $param['reg_con_password']){
					if(!preg_match('/^[0-9]+$/',$param['reg_bankno']) ){ $res['msg'] .= "เลขที่บัญชีจะต้อง 0-9 เท่านั้น \n";}
	        		if(!preg_match('/^[a-zA-Z0-9_]+$/',$param['reg_con_password']) ){  $res['msg'] .= "รหัสผ่านต้องเป็น a-z,0-9 เท่านั้น \n";}
	        		if(strlen($param['reg_username']) != 10){ $res['msg'] .= "เบอร์โทรจะต้องมี 10 หลักเท่านั้น \n"; }
	        		if(strlen($param['reg_bankno']) < 10){ $res['msg'] .= "เลขบัญชีจะต้องไม่น้อยกว่า 10 หลัก \n"; }
	        		if($res['msg'] != ''){
	        			echo json_encode($res);exit();
	        		}
					$pm = array(
						'agent' 	=> $this->config->config['agent'],
						'prefix' 	=> $this->config->config['prefix'],
						'user' 		=> $param['reg_username'],
						'pass' 		=> $param['reg_con_password'],
						'tel' 		=> $param['reg_username'],
						'lineid' 	=> $param['reg_lineid'],
						'bankname' 	=> $param['reg_name'].' '.$param['reg_lname'],
						'bankid' 	=> $param['reg_bank'],
						'bankno' 	=> $param['reg_bankno'],
					);
					$curl   = cUrl($this->apiurl.'/member/m_register','post', $pm);
					// echo $curl;exit();
					$json 	= json_decode($curl,true);
					if($json['status']){
						$res['status'] = true;
						$res['msg']    = $json['msg'];
					}else{
						$res['msg']    = $json['msg'];
					}
				}else{
					$res['msg'] = 'รหัสผ่าน != ยืนยันรหัสผ่าน';
				}
			}else{
				$res['msg'] = 'no param !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function updateinfo(){
		$res  = array(
			'status' => false,
			'msg'    => '',
			'data'   => '',
		);
		
		$param = $this->input->post();
		if($param){
			if(isset($param['info_line']) && isset($param['info_email'])){
        		if(strlen($param['info_line']) > 100){ $res['msg'] .= "error line\n"; }
        		if(strlen($param['info_email']) > 100){ $res['msg'] .= "error email\n"; }
        		if($res['msg'] != ''){
        			echo json_encode($res);exit();
        		}
        		$token = $this->session->userdata('isLoggedIn');
				$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
				$pm = array(
					'user' 		=> (isset($xx['userid'])) ? $xx['userid'] : '',
					'token' 	=> (isset($xx['token'])) ? $xx['token'] : '',
					'email' 	=> $param['info_email'],
					'lineid' 	=> $param['info_line'],
				);
				$curl   = cUrl($this->apiurl.'/member/m_updateinfo','post', $pm);
				$json 	= json_decode($curl,true);
				if($json['status']){
					$xx['isLoggedIn']  = $json['data'];
               		$this->session->set_userdata($xx); 
					$res['status'] = true;
					$res['msg']    = $json['msg'];
				}else{
					$res['msg']    = $json['msg'];
				}
			}else{
				$res['msg'] = 'no param !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function changepass(){
		$res  = array(
			'status' => false,
			'msg'    => '',
			'data'   => '',
		);
		
		$param = $this->input->post();
		if($param){
			if(isset($param['old_pass']) && isset($param['new_pass']) && isset($param['con_new_pass'])){
				if($param['new_pass'] != '' && $param['con_new_pass'] != '' && $param['new_pass'] == $param['con_new_pass']){
	        		if(!preg_match('/^[a-zA-Z0-9_]+$/',$param['con_new_pass']) ){  $res['msg'] .= "รหัสผ่านต้องเป็น a-z,0-9 เท่านั้น \n";}
	        		if($res['msg'] != ''){
	        			echo json_encode($res);exit();
	        		}
	        		$token = $this->session->userdata('isLoggedIn');
					$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
					$pm = array(
						'user' 		=> (isset($xx['userid'])) ? $xx['userid'] : '',
						'token' 	=> (isset($xx['token'])) ? $xx['token'] : '',
						'oldpass' 	=> $param['old_pass'],
						'newpass' 	=> $param['new_pass'],
					);
					// debug($pm,true);
					$curl   = cUrl($this->apiurl.'/member/m_changepass','post', $pm);
					// echo $curl;exit();
					$json 	= json_decode($curl,true);
					if($json['status']){
						$res['status'] = true;
						$res['msg']    = $json['msg'];
					}else{
						$res['msg']    = $json['msg'];
					}
				}else{
					$res['msg'] = 'รหัสผ่าน != ยืนยันรหัสผ่าน';
				}
			}else{
				$res['msg'] = 'no param !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function check_login(){
		$res  = array(
			'status' => false,
			'data'   => '',
		);
		
		$token = $this->session->userdata('isLoggedIn');
		$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
		if($token != ''){
			$pm = array(
				'user' 		=> (isset($xx['userid'])) ? $xx['userid'] : '',
				'token' 	=> (isset($xx['token'])) ? $xx['token'] : '',
			);
			$curl   = cUrl($this->apiurl.'/member/m_checklogin','post', $pm);
			$json 	= json_decode($curl,true);
			if($json['status']){
				$res['status'] = true;
				$res['msg']    = $json['msg'];
			}else{
				$res['msg']    = $json['msg'];
			}
		}else{
			$res['msg']    = 'no token';
		}

		echo json_encode($res);
	}

	public function reg_deposit(){
		$res  = array(
			'status' => false,
			'msg'    => '',
			'data'   => '',
		);
		
		$param = $this->input->post();
		// debug($_FILES,true);
		if($param){
			$config = array(
	          	'upload_path' => 'assets/uploads',
	          	'allowed_types' => 'gif|jpg|jpeg|png',
	          	'overwrite'     => 0,
	          	'max_size'      => 2048,    
	          	'max_width'     => 1500,    
	          	'max_height'    => 1500,    
	        );
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file')) {
				// $res['msg'] = $this->upload->display_errors();
				// echo json_encode($res);exit();
				$pimg = '';
			}else{
				$img = $this->upload->data();
				$param['img'] = $img['file_name'];
				$pimg = 'https://enjoy911.com/assets/'.$param['img'];
			}

			if(isset($param['did_amt']) && isset($param['did_web']) && isset($param['did_date']) && isset($param['did_time'])){
        		if(intval($param['did_amt']) < 1){ $res['msg'] .= "ยอดฝากขั้นต่ำ 20 บาท \n"; }
        		if($res['msg'] != ''){
        			echo json_encode($res);exit();
        		}
        		$token = $this->session->userdata('isLoggedIn');
				$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
				$pm = array(
					'webid' 	=> $param['did_web'],
					'agent' 	=> $this->config->config['agent'],
					'prefix' 	=> $this->config->config['prefix'],
					'username' 	=> $xx['userid'],
					'name' 		=> $xx['name'],
					'frombank' 	=> $xx['bank'],
					'tobank' 	=> $param['did_bank'],
					'amount' 	=> $param['did_amt'],
					'bf_credit' => '0',
					'cdate' 	=> $param['did_date'],
					'ctime' 	=> $param['did_time'],
					'bcode' 	=> 'N',
					'img_url' 	=> $pimg,
				);
				// debug($xx,true);
				// debug($pm);
				$curl   = cUrl($this->apiurl.'/member/m_fdeposit','post', $pm);
				// echo $curl;exit();
				$json 	= json_decode($curl,true);
				if($json['status']){
					$res['status'] = true;
					$res['msg']    = $json['msg'];
				}else{
					$res['msg']    = $json['msg'];
				}
			}else{
				$res['msg'] = 'no param !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function deposit(){
		$res  = array(
			'status' => false,
			'msg'    => '',
			'data'   => '',
		);
		
		$param = $this->input->post();
		// debug($_FILES,true);
		if($param){
			$config = array(
	          	'upload_path' => 'assets/uploads',
	          	'allowed_types' => 'gif|jpg|jpeg|png',
	          	'overwrite'     => 0,
	          	'max_size'      => 2048,    
	          	'max_width'     => 1500,    
	          	'max_height'    => 1500,    
	        );
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file')) {
				// $res['msg'] = $this->upload->display_errors();
				// echo json_encode($res);exit();
				$pimg = '';
			}else{
				$img = $this->upload->data();
				$param['img'] = $img['file_name'];
				$pimg = 'https://enjoy911.com/assets/'.$param['img'];
			}

			if(isset($param['did_amt']) && isset($param['did_web']) && isset($param['did_date']) && isset($param['did_time'])){
        		if(intval($param['did_amt']) < 1){ $res['msg'] .= "ยอดฝากขั้นต่ำ 20 บาท \n"; }
        		if($res['msg'] != ''){
        			echo json_encode($res);exit();
        		}
        		$token = $this->session->userdata('isLoggedIn');
				$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
				// debug($xx,true);
				$pm = array(
					'webid' 	=> $param['did_web'],
					'agent' 	=> $this->config->config['agent'],
					'prefix' 	=> $this->config->config['prefix'],
					'username' 	=> $xx['userid'],
					'userid' 	=> $xx['user_imi'],
					'name' 		=> $xx['name'],
					'frombank' 	=> $xx['bank'],
					'tobank' 	=> $param['did_bank'],
					'amount' 	=> $param['did_amt'],
					'bf_credit'	=> $xx['credit'],
					'cdate' 	=> $param['did_date'],
					'ctime' 	=> $param['did_time'],
					'bcode' 	=> 'N',
					'img_url' 	=> $pimg,
				);
				// debug($xx,true);
				// debug($pm);
				$curl   = cUrl($this->apiurl.'/member/m_deposit','post', $pm);
				// echo $curl;exit();
				$json 	= json_decode($curl,true);
				if($json['status']){
					$res['status'] = true;
					$res['msg']    = $json['msg'];
				}else{
					$res['msg']    = $json['msg'];
				}
			}else{
				$res['msg'] = 'no param !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function withdraw(){
		$res  = array(
			'status' => false,
			'msg'    => '',
			'data'   => '',
		);
		
		$param = $this->input->post();
		// debug($_FILES,true);
		if($param){

			if(isset($param['wid_amt']) && isset($param['wid_web'])){
				$token = $this->session->userdata('isLoggedIn');
				$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
        		if(intval($param['wid_amt']) > intval($xx['credit'])){ $res['msg'] .= "ไม่สามารถถอนเกินเครดิตได้ เครดิตคงเหลือ ".$xx['credit']."\n"; }
        		if(intval($param['wid_amt']) < 1){ $res['msg'] .= "ยอดถอนขั้นต่ำ 100 บาท \n"; }
        		if($res['msg'] != ''){
        			echo json_encode($res);exit();
        		}
        		
				// debug($xx);
				$pm = array(
					'webid' 	=> $param['wid_web'],
					'agent' 	=> $this->config->config['agent'],
					'prefix' 	=> $this->config->config['prefix'],
					'username' 	=> $xx['userid'],
					'userid' 	=> $xx['user_imi'],
					'name' 		=> $xx['name'],
					'tobank' 	=> $xx['bank'],
					'amount' 	=> $param['wid_amt'],
					'bf_credit'	=> $xx['credit'],
				);
				// debug($xx,true);
				// debug($pm,true);
				$curl   = cUrl($this->apiurl.'/member/m_withdraw','post', $pm);
				// echo $curl;exit();
				$json 	= json_decode($curl,true);
				if($json['status']){
					$res['status'] = true;
					$res['msg']    = $json['msg'];
				}else{
					$res['msg']    = $json['msg'];
				}
			}else{
				$res['msg'] = 'no param !!!';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function get_statement(){
		$res  = array(
			'status' => false,
			'data'   => '',
		);
		$param = $this->input->get();

		if(isset($param['type'])){
			$token = $this->session->userdata('isLoggedIn');
			$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);
			if($token != ''){
				$pm = array(
					'user' 		=> (isset($xx['userid'])) ? $xx['userid'] : '',
					'token' 	=> (isset($xx['token'])) ? $xx['token'] : '',
				);
				// debug($pm,true);
				switch ($param['type']) {
					case 'all':
						$curl   = cUrl($this->apiurl.'/member/m_getdidwidlist','post', $pm);
					break;
					case 'deposit':
						$curl   = cUrl($this->apiurl.'/member/m_getdidlist','post', $pm);
					break;
					case 'withdraw':
						$curl   = cUrl($this->apiurl.'/member/m_getwidlist','post', $pm);
					break;
				}
				
				$json 	= json_decode($curl,true);
				// debug($json,true);
				if($json['status']){
					$res['status'] = true;
					$data = array(
						'type' => $param['type'],
						'data' => $json['data'],
					);
					$res['data']   = $this->load->view('statement/didwid',$data,true);
					$res['msg']    = $json['msg'];
				}else{
					$res['msg']    = $json['msg'];
				}
			}else{
				$res['msg']    = 'no token';
			}
		}else{
			$res['msg'] = 'no action !!!';
		}

		echo json_encode($res);
	}

	public function get_balance(){
		$res  = array(
			'status' => false,
			'data'   => '',
		);

		$param = $this->input->get();
		$token = $this->session->userdata('isLoggedIn');
		$xx    = json_decode(decode($token,$this->config->config['sign_api']),true);

		if(!isset($param['partner'])){
			$res['msg']    = 'no partner';
			echo json_encode($res);exit();
		}
		$userid = $ag = '';
		if($token != ''){
			if(strtoupper($param['partner']) == 'TS911'){
				$ag 	= $xx['ag_ts'];
				$userid = $xx['user_ts'];
			}else if(strtoupper($param['partner']) == 'LSM'){
				$ag 	= $xx['ag_lsm'];
				$userid = $xx['user_lsm'];
			}else if(strtoupper($param['partner']) == 'IMI'){
				$ag 	= $xx['ag_imi'];
				$userid = $xx['user_imi'];
			}else if(strtoupper($param['partner']) == 'UFA'){
				$ag 	= $xx['ag_ufa'];
				$userid = $xx['user_ufa'];
			}else{
				$res['msg']    = 'no partner';
				echo json_encode($res);exit();
			}
			$pm = array(
				'web' 		=> strtoupper($param['partner']),
				'ag' 		=> $ag,
				'userid' 	=> $userid,
				'user' 		=> (isset($xx['userid'])) ? $xx['userid'] : '',
				'token' 	=> (isset($xx['token'])) ? $xx['token'] : '',
			);
			// debug($pm,true);
			$curl   = cUrl($this->apiurl.'/member/m_getbalance','post', $pm);
			$json 	= json_decode($curl,true);
			if($json['status']){
				$res['status'] = true;
				$res['data']   = number_format($json['data']['total_bet_credit'],2);
				$res['msg']    = $json['msg'];
			}else{
				$res['msg']    = $json['msg'];
			}
		}else{
			$res['msg']    = 'no token';
		}

		echo json_encode($res);
	}
}
