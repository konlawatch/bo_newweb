 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
class UserinfoController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->model('SettingModel');
		$this->isLoggedIn();
		
	}


   public function index(){
    
    
    $data = array();
    $this->session->userdata();
    $this->load->model('SettingModel');
    $data = $this->SettingModel->getdata1();
            $set_data = array();
            foreach ($data as $k => $v) {
            $set_data[$v['key_all']] = $v;
            }
        // debug($set_data,true);
    $this->global['data'] = $set_data;
    $this->user_info();
  }
	

	  protected static function cUrl($url, $method = "get", $data = "", $ssl = false){
            if ($method == "post"){
                if ($data == "") return false;
            }
            $ch = curl_init();
            if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
            else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
            if ($method == "post"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $content = curl_exec($ch);
            curl_close($ch);
            return $content;
        }

        public static function debug($model, $exit = null) {

            echo '<pre>';
            print_r($model);
            echo '</pre>';

            if ($exit == true) {
                exit;
            }
        }
    


    public function user_info()
        {
            $ss = $this->session->userdata();
          $post = $this->input->post();

          // debug($post,true);   
              $data = array(

                'user'     =>  $ss['data']['userid'],
                'token'    =>  $ss['data']['token']

              ); 

                 // debug($data,true);
            if ($data['user'] !='' && $data['token'] !='' ){

                   $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_info','post',$data);
                    // $curl = $this->cUrl(' https://demoapi.botbo21.com/member/m_updateinfo','post',$data);
                    $json = json_decode($curl,true);

                    echo $curl;
                 
                    if($json['status'])
                      {// สำเร็จo 
                          $this->session->set_userdata($json);
                         
                          redirect('/main');

                      }else{ // ไม่สำเร็จ 
                         
                          redirect('/main');
                      }                   
                    }else{ 
                            if ($ss['data']['token']=='') 
                            {
                              redirect('/logout');
                            }else{redirect('/main');}
                      
                          }


        }



    public function up_psaa_ts()
        {
          $ss = $this->session->userdata();
          $post = $this->input->post();

          if($post['oldpass']==$ss['data']['pass_ts']){
            if ($post['newpass']==$post['cfnewpass']) {
              
            
       
              $data = array(

                'user'     =>  $ss['data']['userid'],
                'pass_ts'  => $post['newpass'],
                'token'    =>  $ss['data']['token']

              ); 

         
            if ($data['user'] !='' && $data['token'] !='' ){

                   $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_updateinfo','post',$data);
                   
                    $json = json_decode($curl,true);

                    echo $curl;
                    debug($json,true);
                 
                    if($json['status'])
                      {// สำเร็จo 
                          $this->session->set_userdata($json);
                          $this->session->set_flashdata('success', $json['msg']);
                          redirect('/profile');

                      }else{ // ไม่สำเร็จ  
                          $this->session->set_flashdata('error', $json['msg']);
                          redirect('/profile');
                      }                   
                    }
            }else{$this->session->set_flashdata('error', 'รหัสไม่เหมือนกัน');redirect('/profile');}       
          }else{ redirect('/profile');} 


        }

  public function up_psaa_lsm()
        {
          $ss = $this->session->userdata();
          $post = $this->input->post();

          if($post['oldpass']   ==$ss['data']['pass_lsm']){
            if ($post['newpass'] ==$post['cfnewpass']) {

              $data = array(

                'user'      =>  $ss['data']['userid'],
                'pass_lsm'  => $post['newpass'],
                'token'     =>  $ss['data']['token']

              ); 

         
            if ($data['user'] !='' && $data['token'] !='' ){

                   $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_updateinfo','post',$data);
                   
                    $json = json_decode($curl,true);

                    echo $curl;
                    // debug($json,true);
                 
                    if($json['status'])
                      {// สำเร็จo 
                          $this->session->set_userdata($json);
                          $this->session->set_flashdata('success', $json['msg']);
                          redirect('/profile');

                      }else{ // ไม่สำเร็จ  
                          $this->session->set_flashdata('error', $json['msg']);
                          redirect('/profile');
                      }                   
                    }
            }else{$this->session->set_flashdata('error', 'รหัสไม่เหมือนกัน');redirect('/profile');}       
          }else{ redirect('/profile');} 


        }

  public function up_psaa_imi(){
          $ss = $this->session->userdata();
          $post = $this->input->post();

          if($post['oldpass']==$ss['data']['pass_imi']){
            if ($post['newpass']==$post['cfnewpass']) {
              
            
       
              $data = array(

                'user'     =>  $ss['data']['userid'],
                'pass_imi'  => $post['newpass'],
                'token'    =>  $ss['data']['token']

              ); 

         
            if ($data['user'] !='' && $data['token'] !='' ){

                   $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_updateinfo','post',$data);
                   
                    $json = json_decode($curl,true);

                    echo $curl;
                    // debug($json,true);
                 
                    if($json['status'])
                      {// สำเร็จo 
                          $this->session->set_userdata($json);
                          $this->session->set_flashdata('success', $json['msg']);
                          redirect('/profile');

                      }else{ // ไม่สำเร็จ  
                          $this->session->set_flashdata('error', $json['msg']);
                          redirect('/profile');
                      }                   
                    }
            }else{$this->session->set_flashdata('error', 'รหัสไม่เหมือนกัน');redirect('/profile');}       
          }else{ redirect('/profile');} 


        }

  public function c_login(){
          $ss = $this->session->userdata();
          $post = $this->input->post();
          
          // debug($post,true);  
              // echo $post['token'];    
              $data = array(

                'user'     =>  $ss['data']['userid'],
                'token'    =>  $post['token']
              ); 

                 // debug($data,true);
            if ($data['user'] !='' && $data['token'] !='' ){

                   $curl = $this->cUrl($this->config->config['apiurl'].'/member/m_checklogin','post',$data);
                 
                    $json = json_decode($curl,true);

                    echo $curl; 
                    $this->session->set_userdata($json);  

                    if($json['status']!= true ){
                      echo "หมดละนะ";
                      $this->session->sess_destroy ();
                        redirect('/logout'); 
                    }  
                                    
            }  

  
        }

  

}
