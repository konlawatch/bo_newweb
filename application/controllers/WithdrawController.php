<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class WithdrawController extends BaseController
{
    /**
     * This is default constructor of the class
     */
   public function __construct()
   {
      parent::__construct(); 
      $this->isLoggedIn();
      $this->website = $this->config->config['website'];
      $this->key               = $this->config->config['sign_api']; 
      $this->website_desc      = $this->config->config['website_desc']; 
      $this->global['website'] = $this->config->config['website'];
   }
    
    
   public function index(){
      $data = array();
      $content['title']     = $this->website;
		$content['disc']      = $this->website_desc;
      $content['tapber']    = 'withdraw';
      $this->global['web']  = 'IMI';
      $content['content']   = $this->load->view('withdraw/main',$this->global, true);
      $this->load->view('layout/app',$content);
   }
}

?>