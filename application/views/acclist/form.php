<div class="row">
    <?php 
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
    ?>
    <?php if ($error) :?>
        <div class="pull-left alert alert-danger no-margin alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $error; ?>
        </div>
    <?php endif;?>
    <?php if ($success) :?>
        <div class="pull-left alert alert-success no-margin alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $success; ?>
        </div>
    <?php endif;?>
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo (isset($data->userId)) ? base_url('/acclist/update/' . $data->userId) : base_url('/acclist/create'); ?>">
            <div class="clearfix">
                <div class="pull-right">
                <button class="btn btn-sm btn-success" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    บันทึก
                </button>
                &nbsp; &nbsp;
                <a type="button" class="btn btn-sm" href="<?php echo base_url(); ?>acclist">ย้อนกลับ</a>
                </div>
            </div>
            <div class="form-group">
                <?php if(isset($data)):?>
                    <input type="hidden" name="oldval" value="<?php echo htmlspecialchars(json_encode($data)); ?>" />
                <?php endif;?>
                <label for="username" class="col-md-2 control-label">รหัสผู้ใช้งาน</label>
                <?php if (isset($data)) :?>
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="username" placeholder="รหัสผู้ใช้งาน" value="<?php echo $data->username; ?>" readonly>
                    </div>
                <?php else:?>
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="username" placeholder="รหัสผู้ใช้งาน" value="" >
                    </div>
                <?php endif;?>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">ชื่อผู้ใช้งาน</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="name" placeholder="ชื่อผู้ใช้งาน" value="<?php echo (isset($data->name)) ? $data->name : ''; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="pass" class="col-md-2 control-label">รหัสผ่าน</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" name="pass" placeholder="รหัสผ่าน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="conpass" class="col-md-2 control-label">ยืนยันรหัสผ่าน</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" name="conpass" placeholder="ยืนยันรหัสผ่าน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="bcode" class="col-md-2 control-label">สิทธิการใช้งาน</label>
                <div class="col-md-7">
                    <?php if (isset($data->roleId)) :?>
                        <select class="form-control" id="form-field-select-1" name="roleId">
                            <option value="">--- กรุณาเลือกสิทธิการใช้งาน ---</option>
                            <?php foreach ($rolelist as $k) :?>
                                <option value="<?php echo $k->roleId;?>" <?php echo ($k->roleId == $data->roleId) ? 'selected' : ''; ?>><?php echo $k->role;?></option>
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="form-field-select-1" name="roleId">
                            <option value="">--- กรุณาเลือกสิทธิการใช้งาน ---</option>
                            <?php foreach ($rolelist as $k) :?>
                                <option value="<?php echo $k->roleId;?>"><?php echo $k->role;?></option>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('#bcode').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });
</script>