<?php $ss = $this->session->userdata();?>
<?php if($ss['role'] == '1'&& $ss['roleText']=='SuperAdmin'):?>
<div class="row">
   <div class="col-xs-12">
      <div class="clearfix">
         <div class="pull-right">
             <button class="btn btn-sm btn-success pull-right fa fa-plus" data-toggle="modal" data-target="#exampleModaladd">เพิ่มผู้ใช้งาน</button>
         </div>
      </div>
      <div class="hr dotted"></div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>ลำดับ</th>
                           <th>รหัสผู้ใช้งาน</th>
                           <th>ชื่อผู้ใช้งาน</th>
                           <th>สิทธิการใช้งาน</th> 
                           <th>จัดการ</th>
                        </tr>
                     </thead> 
                     <tbody>
                        <?php $i = ($this->uri->segment(3) != '') ? (($this->uri->segment(3) - 1) * 50) + 1 : 1;?>
                        <?php if (isset($data_acclist) && count($data_acclist) >= 1): ?>
                           <?php foreach ($data_acclist as $item): ?>
                                 <tr>
                                    <td style="color: #fff;"><?php echo $i; ?></td>
                                    <td style="color: #fff;"><?php echo $item->username; ?></td>
                                    <td style="color: #fff;"><?php echo $item->name; ?></td>
                                    <td style="color: #fff;"><?php echo $item->role; ?></td>
                                    <td>
                                       <div class="btn-group">
                                         
                                           <button class="fa fa-pencil-square-o btn btn-minier btn-warning" data-toggle="modal" data-target="#exampleModal1" onclick="recuverysetvalue('<?php echo $item->userId; ?>','<?php echo $item->name; ?>');">แก้ไข
                                           </button> 

                                          <a class="fa fa-trash-o btn-minier btn btn-danger" onclick="return confirm('ต้องการลบ <?php echo $item->name; ?> ?')" href="<?php echo base_url('Dashboard/delete/' . $item->userId); ?>"> ลบ</a>
                                       </div>
                                   </td>
                              </tr> 
                              <?php $i++;?>
                           <?php endforeach;?>
                           <?php else: ?>
                              <tr>
                                 <td  colspan="10" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                              </tr>
                        <?php endif;?>
                     </tbody>
                  </table>
               </div> 
               <div class="col-md-12" style="text-align:center;"></div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title" id="exampleModalLabel1">แก้ไขรหัสผู้ใช้งาน</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo (isset($data->userId)) ? base_url('/acclist/update/' . $data->userId) : base_url('/acclist/create'); ?>">
           
            <div class="form-group">
               
                <!-- <label for="username" class="col-md-2 control-label">รหัสผู้ใช้งาน</label> -->
                
                    <div class="col-md-7">
                        <input type="hidden" class="form-control" name="username"id="username"  placeholder="รหัสผู้ใช้งาน" value="" readonly>
                    </div>

            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">ชื่อผู้ใช้งาน</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อผู้ใช้งาน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="pass" class="col-md-2 control-label">รหัสผ่าน</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" name="pass" placeholder="รหัสผ่าน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="conpass" class="col-md-2 control-label">ยืนยันรหัสผ่าน</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" name="conpass" placeholder="ยืนยันรหัสผ่าน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="bcode" class="col-md-2 control-label">สิทธิการใช้งาน</label>
                <div class="col-md-7">
                    <?php if (isset($rolelist->roleId)) :?>
                        <select class="form-control" id="form-field-select-1" name="roleId">
                            <option value="">--- กรุณาเลือกสิทธิการใช้งาน ---</option>
                            <?php foreach ($rolelist as $k) :?>
                                <option value="<?php echo $k->roleId;?>" <?php echo ($k->roleId == $data->roleId) ? 'selected' : ''; ?>><?php echo $k->role;?></option>
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="form-field-select-1" name="roleId">
                            <option value="">--- กรุณาเลือกสิทธิการใช้งาน ---</option>
                            <?php foreach ($rolelist as $k) :?>
                                <option value="<?php echo $k->roleId;?>"><?php echo $k->role;?></option>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                </div>
            </div>
             <div class="clearfix">
                <div class="pull-right">
                <button class="btn btn-sm btn-success" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    บันทึก
                </button>
                &nbsp; &nbsp;
                <a type="button" class="btn btn-sm" href="<?php echo base_url(); ?>acclist">ย้อนกลับ</a>
                </div>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModaladd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title" id="exampleModalLabel1">เพิ่มรหัสผู้ใช้งาน</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>Dashboard/create_admin">
           
            <div class="form-group">
               
                <!-- <label for="username" class="col-md-2 control-label">รหัสผู้ใช้งาน</label> -->
                
                    <div class="col-md-7">
                        <input type="hidden" class="form-control" name="username"id="username"  placeholder="รหัสผู้ใช้งาน" value="" readonly>
                    </div>

            </div>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label">ชื่อผู้ใช้งาน</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อผู้ใช้งาน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="pass" class="col-md-2 control-label">รหัสผ่าน</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" name="pass" placeholder="รหัสผ่าน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="conpass" class="col-md-2 control-label">ยืนยันรหัสผ่าน</label>
                <div class="col-md-7">
                    <input type="password" class="form-control" name="conpass" placeholder="ยืนยันรหัสผ่าน" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="bcode" class="col-md-2 control-label">สิทธิการใช้งาน</label>
                <div class="col-md-7">
                    <?php if (isset($rolelist->roleId)) :?>
                        <select class="form-control" id="form-field-select-1" name="roleId">
                            <option value="">--- กรุณาเลือกสิทธิการใช้งาน ---</option>
                            <?php foreach ($rolelist as $k) :?>
                                <option value="<?php echo $k->roleId;?>" <?php echo ($k->roleId == $data->roleId) ? 'selected' : ''; ?>><?php echo $k->role;?></option>
                            <?php endforeach;?>
                        </select>
                    <?php else :?>
                        <select class="form-control" id="form-field-select-1" name="roleId">
                            <option value="">--- กรุณาเลือกสิทธิการใช้งาน ---</option>
                            <?php foreach ($rolelist as $k) :?>
                                <option value="<?php echo $k->roleId;?>"><?php echo $k->role;?></option>
                            <?php endforeach;?>
                        </select>
                    <?php endif;?>
                </div>
            </div>
             <div class="clearfix">
                <div class="pull-right">
                <button class="btn btn-sm btn-success" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    บันทึก
                </button>
                &nbsp; &nbsp;
                <a type="button" class="btn btn-sm" href="<?php echo base_url(); ?>acclist">ย้อนกลับ</a>
                </div>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
function recuverysetvalue(username,name){
    $('#username').val(username);
    $('#name').val(name);  
  
  }
</script>
 <?php else:?>
<?php endif;?>    