<?php //debug($data1,true);?>						
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<h3 class="header smaller lighter green">Progress Bar</h3>

									<div class="col-sm-6">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab45">
												<li  class="active">
													<a data-toggle="tab" href="#popup_T">Title_PopUp</a>
												</li>
												<li>
													<a data-toggle="tab" href="#popup">image_PopUp</a>
												</li>
												
												<li>
													<a data-toggle="tab" href="#profile4">Status_PopUp</a>
												</li>

												
											</ul>

											<div class="tab-content">

												<div id="popup_T" class="tab-pane">
													<div class="row">
									                  <div class="col-xs-12">
									                    <table id="simple-table" class="table  table-bordered table-hover">
									                      <thead>
									                        <tr>
									                         
									                          
									                          <th class="center">ข้อความ</th>
									                          
									                          

									                        </tr>
									                      </thead>

									                      <tbody>
									                     
									                        <tr>  
									                          <td class="center"><?php echo (isset($data1['title_popup'])) ?  $data1['title_popup']['value']  : "ไม่มีข้อมูล"?></td>
									                        </tr>
									                      </tbody>
									                    </table>
									                  </div><!-- /.span -->
									                </div><!-- /.row -->
												</div>
												

												<div id="profile4" class="tab-pane">
													<div class="row">
									                  <div class="col-xs-12">
									                    <table id="simple-table" class="table  table-bordered table-hover">
									                      <thead>
									                        <tr>
									                         
									                          
									                          <th class="center">สถานะ</th>
									                          
									                          

									                        </tr>
									                      </thead>

									                      <tbody>
									                     
									                        <tr>  
									                          <td class="center"><?php echo (isset($data1['popup'])) ?  $data1['popup']['value']  : "ไม่มีข้อมูล"?></td>
									                        </tr>
									                      </tbody>
									                    </table>
									                  </div><!-- /.span -->
									                </div><!-- /.row -->
												</div>

												<div id="popup" class="tab-pane">
													 <div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                      <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['img_popup'])) ?  $data1['img_popup']['value']  : ""?>" data-rel="colorbox">
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['img_popup'])) ?  $data1['img_popup']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : PopUp</div>
										                        </div>
										                      </a>
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
											</div>
										</div>
									</div><!-- /.col -->

									<div class="vspace-6-sm"></div>

									<div class="col-sm-6">

										<div class="tabbable">
											<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
												<li class="active">
													<a data-toggle="tab" href="#linkline">linkline</a>
												</li>

												<li>
													<a data-toggle="tab" href="#image_line">image_line@</a>
												</li>

											
											</ul>

											<div class="tab-content">
												<div id="linkline" class="tab-pane in active">
													<div class="row">
									                  <div class="col-xs-12">
									                    <table id="simple-table" class="table  table-bordered table-hover">
									                      <thead>
									                        <tr>
									                         
									                          <th class="center">Link_line@</th>
									                          <th class="center">Status</th>
									                          <th class="center">Date</th>
									                          

									                        </tr>
									                      </thead>

									                      <tbody>
									                     
									                        <tr>
									                          
									                          <td class="center"><?php echo (isset($data1['linkline@'])) ?  $data1['linkline@']['value']  : "ไม่มีข้อมูล"?></td>
									                          <td class="center"><?php echo (isset($data1['linkline@'])) ?  'ใช้งาน' : "";?></td>
									                          <td class="center"><?php echo (isset($data1['linkline@'])) ?  $data1['linkline@']['cdate']  : "";?></td>
									                          
									                          
									                        </tr>
									                      </tbody>
									                    </table>
									                  </div><!-- /.span -->
									                </div><!-- /.row -->
												</div>

												<div id="image_line" class="tab-pane">
													<div class="row">
											              <div class="col-xs-12">
											                <!-- PAGE CONTENT BEGINS -->
											                <div>
											                  <ul class="ace-thumbnails clearfix">
											                    <li>
											                      <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['line_img'])) ?  $data1['line_img']['value']  : ""?>" data-rel="colorbox">
											                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['line_img'])) ?  $data1['line_img']['value']  : "255x150.png"?>">
											                
											                        <div class="text">
											                          <div class="inner">LOGO : PopUp</div>
											                        </div>
											                      </a>

											                      <div class="tools tools-bottom">
											                       
											                       <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('line_img');">
											                          <i class="fa fa-cloud-upload"></i>uplode
											                       </button>
											                      </div>
											                    </li>
											                  </ul>
											                </div><!-- PAGE CONTENT ENDS -->
											              </div><!-- /.col -->                        
											            </div><!-- /.row -->
												</div>

												
											</div>
										</div>
									</div>
								</div><!-- /.row -->

								<div class="space"></div>

								<div class="row">
									<div class="col-sm-6">
										<div class="tabbable tabs-left">
											<ul class="nav nav-tabs" id="myTab3">
												<li class="active">
													<a data-toggle="tab" href="#home3">
														<i class="pink ace-icon fa fa-tachometer bigger-110"></i>
														Head
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#profile3">
														<i class="blue ace-icon fa fa-user bigger-110"></i>
														Body-Sline
													</a>
												</li>

												
												<li>
													<a data-toggle="tab" href="#profile_p">
														<i class="ace-icon fa fa-rocket"></i>
														Body-Post
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#footer">
														<i class="ace-icon fa fa-rocket"></i>
														Footer
													</a>
												</li>
											</ul>

											<div class="tab-content">
												<div id="home3" class="tab-pane in active">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                    
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_menubar'])) ?  $data1['l_menubar']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : Menubar</div>
										                        </div>
										                      
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>

												<div id="profile3" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s1'])) ?  $data1['l_body_s1']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : Body-Sline1</div>
										                        </div>
										                     
										                    </li>

										                    <li>
										                   
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s2'])) ?  $data1['l_body_s2']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : Body-Sline2</div>
										                        </div>
										                  
										                    </li>

										                    <li>
										                    
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s3'])) ?  $data1['l_body_s3']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : Body-Sline3</div>
										                        </div>
										                  
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>

											
												<div id="profile_p" class="tab-pane">
													 <div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                      
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_p1'])) ?  $data1['l_body_p1']['value']  : "255x150.png"?>"  />
										                        <div class="text">
										                          <div class="inner">LOGO : Body Post1</div>
										                        </div>
										                  
										                      
										                    </li>

										                     <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_p2'])) ?  $data1['l_body_p2']['value']  : "255x150.png"?>"  />
										                        <div class="text">
										                          <div class="inner">LOGO : Body Post2</div>
										                        </div>
										            
										                      
										                    </li>

										                     <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_p3'])) ?  $data1['l_body_p3']['value']  : "255x150.png"?>"  />
										                        <div class="text">
										                          <div class="inner">LOGO : Body Post3</div>
										                        </div>
										                      

										                     
										                    </li>

										                     <li>
										                      
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_p4'])) ?  $data1['l_body_p4']['value']  : "255x150.png"?>"  />
										                        <div class="text">
										                          <div class="inner">LOGO : Body Post4</div>
										                        </div>
										                      

										                     
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
												<div id="footer" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_footer'])) ?  $data1['l_footer']['value']  : "255x150.png"?>"  />
										                        <div class="text">
										                          <div class="inner">LOGO : Footer</div>
										                        </div>
										              

										                      
										                    </li>

										                      <ul class="ace-thumbnails clearfix">
											                    <li>
											                      
											                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_bank'])) ?  $data1['l_bank']['value']  : "255x150.png"?>"  />
											                        <div class="text">
											                          <div class="inner">LOGO : Bannk</div>
											                        </div>
											                      

											                    
											                    </li>                   
											                  </ul>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->

										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
											</div>
										</div>
									</div><!-- /.col -->

									<div class="vspace-6-sm"></div>
										<div class="tabbable tabs-left">
											<ul class="nav nav-tabs" id="myTab3">
												<li class="active">
													<a data-toggle="tab" href="#Deposit">
														<i class="pink ace-icon fa fa-tachometer bigger-110"></i>
														Deposit
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#Withdraw">
														<i class="blue ace-icon fa fa-user bigger-110"></i>
														Withdraw
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#Register">
														<i class="ace-icon fa fa-rocket"></i>
														Register
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#Forget">
														<i class="ace-icon fa fa-rocket"></i>
														Forget
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#Agen">
														<i class="ace-icon fa fa-rocket"></i>
														Agen
													</a>
												</li>
											</ul>

											<div class="tab-content">
												<div id="Deposit" class="tab-pane in active">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_deposit'])) ?  $data1['l_deposit']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : Deposit</div>
										                        </div>
										                      
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>

												<div id="Withdraw" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                      
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_withdraw'])) ?  $data1['l_withdraw']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : Withdraw</div>
										                        </div>
										                     

										                     
										                    </li>

										                   
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>

												<div id="Register" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_register'])) ?  $data1['l_register']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO :Register</div>
										                        </div>
										                      

										                     
										                    </li>

										                   

										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
												<div id="Forget" class="tab-pane">
													 <div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                      <a href="assets/admins/images/gallery/image-3.jpg" data-rel="colorbox">
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_p1'])) ?  $data1['l_body_p1']['value']  : "255x150.png"?>"  />
										                        <div class="text">
										                          <div class="inner">LOGO : Forget</div>
										                        </div>
										                      </a>

										                      
										                    </li>

										                  
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
												<div id="Agen" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                      <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen1'])) ?  $data1['Agen1']['value']  : ""?>" data-rel="colorbox">
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen1'])) ?  $data1['Agen1']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : Agen1</div>
										                        </div>
										                      </a>

										                    </li>

										                    <li>
										                      <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen2'])) ?  $data1['Agen2']['value']  : ""?>" data-rel="colorbox">
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen2'])) ?  $data1['Agen2']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : Agen2</div>
										                        </div>
										                      </a>

										                     
										                    </li>

										                    <li>
										                      <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen3'])) ?  $data1['Agen3']['value']  : ""?>" data-rel="colorbox">
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen3'])) ?  $data1['Agen3']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : Agen3</div>
										                        </div>
										                      </a>

										                     
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												
												</div>
											</div>
										</div>



									<div class="col-sm-6">
										
									</div><!-- /.col -->
								</div><!-- /.row -->
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->

								<h3 class="header smaller lighter green">Promotion</h3>
									<div class="vspace-6-sm"></div>
										<div class="tabbable tabs-left">
											<ul class="nav nav-tabs" id="myTab3">
												<li class="active">
													<a data-toggle="tab" href="#Promotion1">
														<i class="pink ace-icon fa fa-tachometer bigger-110"></i>
														Promotion1
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#Promotion2">
														<i class="blue ace-icon fa fa-user bigger-110"></i>
														Promotion2
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#Promotion3">
														<i class="ace-icon fa fa-rocket"></i>
														Promotion3
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#Promotion4">
														<i class="ace-icon fa fa-rocket"></i>
														Promotion4
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#Promotion5">
														<i class="ace-icon fa fa-rocket"></i>
														Promotion5
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#Promotion6">
														<i class="ace-icon fa fa-rocket"></i>
														Promotion6
													</a>
												</li>
											</ul>

											<div class="tab-content">
												<div id="Promotion1" class="tab-pane in active">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_t1'])) ?  $data1['l_body_t1']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : promotion1</div>
										                        </div>
										                      </a> 
										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>

												<div id="Promotion2" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                      
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_t2'])) ?  $data1['l_body_t2']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : promotion2</div>
										                        </div>
										                      </a>

										                     
										                    </li>

										                      
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>

												<div id="Promotion3" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_t3'])) ?  $data1['l_body_t3']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : promotion3</div>
										                        </div>
										                      </a>

										                     
										                    </li>

										                


										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
												<div id="Promotion4" class="tab-pane">
													 <div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                     <li>
										                    
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_t4'])) ?  $data1['l_body_t4']['value']  : "255x150.png"?>" />
										                        <div class="text">
										                          <div class="inner">LOGO : promotion4</div>
										                        </div>
										                      </a>

										                      
										                    </li>

										                   
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												</div>
												<div id="Promotion5" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_t5'])) ?  $data1['l_body_t5']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : promotion5</div>
										                        </div>
										                      </a>

										                    </li>

										                  

										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												
												</div>

												<div id="Promotion6" class="tab-pane">
													<div class="row">
										              <div class="col-xs-12">
										                <!-- PAGE CONTENT BEGINS -->
										                <div>
										                  <ul class="ace-thumbnails clearfix">
										                    <li>
										                     
										                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_t6'])) ?  $data1['l_body_t6']['value']  : "255x150.png"?>">
										                
										                        <div class="text">
										                          <div class="inner">LOGO : promotion6</div>
										                        </div>
										                      </a>

										                    </li>
										                  </ul>
										                </div><!-- PAGE CONTENT ENDS -->
										              </div><!-- /.col -->                        
										            </div><!-- /.row -->
												
												</div>
											</div> 
						</div><!-- /.row -->