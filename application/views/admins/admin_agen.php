<!-- Button trigger modal -->
<?php $ss = $this->session->userdata();?>

            <?php
                $error = $this->session->flashdata('error');
                $success = $this->session->flashdata('success');
            ?>
            <?php if($error) :?>
                <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error; ?>                    
                </div>
            <?php endif;?>
            <?php if($success) :?>
                <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $success; ?>                    
                </div>
            <?php endif; ?>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel1"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg_agen">
                                <input type="hidden"  name="keyname"        id="keyname"        value="">       
                                <input type="hidden"  name="status"         id="status"         value="1">
                                <div>
                                    <label for="form-field-8">image</label>
                                    <input type="file" name="deposit_img" accept="image/*">
                                </div>                      
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div> 
                    </div>
                </div>
            </div>


            <div class="modal fade" id="exampleModal_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content">
                        <div class=" tab-content">
                            <div class="modal-body">
                                <?php echo $error; ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" style="border-radius: 30px;" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if(isset($error)):?>
                <script type="text/javascript">
                    $(document).ready(function(){
                    $('#exampleModal_error').modal('show');
                    });
                </script>
                <?php endif;?>

                <?php if(isset($session['session_ads'])):?>
                <?php if($session['session_ads'] == 'show'):?>
                <script type="text/javascript">
                    $(document).ready(function(){
                    $('#exampleModal').modal('show');
                    });
                </script>
                <?php endif;?>
                <?php endif;?>

                <?php if(isset($error)):?>
                <script type="text/javascript">
                    $(document).ready(function(){
                    $('#exampleModal_error').modal('show');
                    });
                </script>
            <?php endif;?>

                <script type="text/javascript"> 
                    function setvalueup(keyname){
                        $('#keyname').val(keyname);
                        document.getElementById("exampleModalLabel1").innerHTML = keyname;
                    }

                </script>



            <h2 class="header smaller lighter green">Logo  : Agent</h2>
                <div class="row">
                    <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                        <div>
                            <ul class="ace-thumbnails clearfix">
                                <li>
                                    <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen1'])) ?  $data1['Agen1']['value']  : ""?>" data-rel="colorbox">
                                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen1'])) ?  $data1['Agen1']['value']  : "255x150.png"?>">

                                        <div class="text">
                                            <div class="inner">LOGO : Agent1</div>
                                        </div>
                                    </a>

                                    <div class="tools tools-bottom">

                                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('Agen1');">
                                        <i class="fa fa-cloud-upload"></i>uplode
                                        </button>
                                    </div>
                                </li>

                                <li>
                                    <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen2'])) ?  $data1['Agen2']['value']  : ""?>" data-rel="colorbox">
                                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen2'])) ?  $data1['Agen2']['value']  : "255x150.png"?>">

                                        <div class="text">
                                            <div class="inner">LOGO : Agent2</div>
                                        </div>
                                    </a>

                                    <div class="tools tools-bottom">

                                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('Agen2');">
                                        <i class="fa fa-cloud-upload"></i>uplode
                                        </button>
                                    </div>
                                </li>

                                <li>
                                    <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen3'])) ?  $data1['Agen3']['value']  : ""?>" data-rel="colorbox">
                                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['Agen3'])) ?  $data1['Agen3']['value']  : "255x150.png"?>">

                                        <div class="text">
                                            <div class="inner">LOGO : Agent3</div>
                                        </div>
                                    </a>

                                    <div class="tools tools-bottom">

                                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('Agen3');">
                                        <i class="fa fa-cloud-upload"></i>uplode
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->                        
                </div><!-- /.row -->
            <h2 class="header smaller lighter green"></h2>

  