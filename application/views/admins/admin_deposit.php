      <?php
      date_default_timezone_set('Asia/Bangkok');

      ##=========================================================================================

      //util class
      class Util {

          public static function cUrl($url, $method = "get", $data = "", $ssl = false){
              if ($method == "post"){
                  if ($data == "") return false;
              }
              $ch = curl_init();
              if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
              else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
              if ($method == "post"){
                  curl_setopt($ch, CURLOPT_POST, true);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

              }
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
              curl_setopt($ch, CURLOPT_TIMEOUT, 200);
              $content = curl_exec($ch);
              curl_close($ch);
              return $content;
          }
      };

      $curl = Util::cUrl('https://demoapi.botbo21.com/api/getbanksys?sign=C5Z10zzL4M7BiOSmEgyoAcnw5g38CvO2','get');


      $banksys = json_decode($curl,true);
         
      ?>



      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content bbox" >
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center">คุณต้องการเพิ่มข้อมูลธนาคารนี้ ใช่หรือไม่</h3>
                  <h4 class="modal-title text-primary mb-0 center" id="exampleModalLabel"></h4>
                  <h4 class="modal-title text-primary mb-0 center" id="exampleModalLabel2"></h4>
                  <h4 class="modal-title text-primary mb-0 center" id="exampleModalLabel3"></h4>
                  <hr class="x-hr-border-glow">      
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/create_bank" style="width: 100%;">
                     <input type="hidden"    name="key"        id="key"        value="">
                     <input type="hidden"    name="bankid"     id="bankid"     value="">
                     <input type="hidden"    name="bankname"   id="bankname"   value="">
                     <input type="hidden"    name="bankno"     id="bankno"     value="">
                     <input type="hidden"    name="status"     id="status"     value="1">
                     <button type="submit" id="submit" name="submit" class="btn btn-primary mx-auto" style="border-radius: 10px; width: 30%;">ใช่</button>
                     <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ไม่</button>
                  </form>
               </div> 
           </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <div class="modal-header"><h1 class="modal-title textgold noise center" >รูปโปรโมชั่น</h1></div>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg_depo">
                        <input type="hidden"  name="keyname"      id="keyname"      value="">           
                        <input type="hidden"  name="status"    id="status"    value="1">      
                        <div>
                           <input class="input-file" id="deposit_img" name="deposit_img" accept="image/*"  type="file">
                           <label tabindex="0" for="deposit_img" class="input-file-trigger" style="">เลือกรูป...</label> 
                        </div>       
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>


      <script type="text/javascript">
             
         function setvalue(key,bankid,bankname,bankno){
              //$('#name').val(val);
            $('#key').val(key);
            $('#bankid').val(bankid);
            $('#bankname').val(bankname);
            $('#bankno').val(bankno);
            document.getElementById("exampleModalLabel").innerHTML  = bankid;
            document.getElementById("exampleModalLabel2").innerHTML = bankname;
            document.getElementById("exampleModalLabel3").innerHTML = bankno;
             
         }
         function setvalueup(keyname,){
            $('#keyname').val(keyname);
             
            document.getElementById("exampleModalLabel1").innerHTML = keyname;
         }


         function delvalue(id_del){
            $('#id_del').val(id_del);
    
         }
         
      </script>


      <!-- <h2 class="header smaller lighter green">รูปโปรโมชั่น</h2> -->
      <div class="row">
        <!--  <div class="col-xs-12">
        
            <div style="margin-left: auto;margin-right: auto;width: 50%;">
               <ul class="ace-thumbnails clearfix">
                  <li>
                     <a href="" data-rel="colorbox">
                       <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_deposit1'])) ?  $data1['l_deposit1']['value']  : "255x150.png"?>">
                
                        <div class="text">
                           <div class="inner">LOGO : Deposit_Sline1</div>
                        </div>
                     </a>

                     <div class="tools tools-bottom"> 
                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('l_deposit1');">
                           <i class="fa fa-cloud-upload"></i>uplode
                        </button>
                     </div>
                  </li>
                  <li>
                     <a href="" data-rel="colorbox">
                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_deposit2'])) ?  $data1['l_deposit2']['value']  : "255x150.png"?>">
                
                        <div class="text">
                           <div class="inner">LOGO : Deposit_Sline2</div>
                        </div>
                     </a>

                     <div class="tools tools-bottom">  
                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('l_deposit2');">
                           <i class="fa fa-cloud-upload"></i>uplode
                        </button>
                     </div>
                  </li>
                     <li>
                        <a href="" data-rel="colorbox">
                           <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_deposit3'])) ?  $data1['l_deposit3']['value']  : "255x150.png"?>">
                           <div class="text">
                              <div class="inner">LOGO : Deposit_Sline3</div>
                           </div>
                        </a>

                        <div class="tools tools-bottom">              
                           <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('l_deposit3');">
                              <i class="fa fa-cloud-upload"></i>uplode
                           </button>
                        </div>
                     </li>
            </ul>
         </div>
      </div> -->


            </div><!-- /.row -->
      <h2 class="header smaller lighter green"></h2>
      <h2 class="header smaller lighter green">ธนาคารที่ใช้แสดงบนหน้าเว็บไซต์</h2>
      <div class="row">
         <div class="col-xs-12">
            <table id="simple-table" class="table  table-bordered " style="background-color:#c6c6c6;">
               <thead >
                  <tr >
                     <th class="center" >ID</th>
                     <th class="center">LOGO</th>
                     <th class="center">ธนาคาร</th>
                     <th class="center">ชื่อ บัญชี</th>
                     <th class="center">หมายเลขบัญชี</th>
                     <th class="center">Date</th>
                     <th></th>
                          
                  </tr>
               </thead>
               <tbody>
                  <tr >
                     <td class="center" style="min-height: 10em; display: table-cell;vertical-align: middle;"><?php echo (isset($data1['bank_depo'])) ?  $data1['bank_depo']['id']  : "ไม่มีข้อมูล"?></td>
                     <td class="center"><img src="<?php echo base_url();?>assets/img/logo-bank/<?php echo (isset($data1['bank_depo']['but_name'])) ?  $data1['bank_depo']['but_name']  : "255x150"?>.png" style="width: 50px; height: 50px;"></td>  
                     <td class="center" style="min-height: 10em; display: table-cell;vertical-align: middle;"><?php echo (isset($data1['bank_depo'])) ?  $data1['bank_depo']['but_name']  : "ไม่มีข้อมูล"?></td>
                     <td class="center" style="min-height: 10em; display: table-cell;vertical-align: middle;"><?php echo (isset($data1['bank_depo'])) ?  $data1['bank_depo']['value']  : "ไม่มีข้อมูล"?></td>
                     <td class="center" style="min-height: 10em; display: table-cell;vertical-align: middle;"><?php echo (isset($data1['bank_depo'])) ?  $data1['bank_depo']['detail'] : "ไม่มีข้อมูล";?></td>
                     <td class="center" style="min-height: 10em; display: table-cell;vertical-align: middle;"><?php echo (isset($data1['bank_depo'])) ?  $data1['bank_depo']['cdate']  : "ไม่มีข้อมูล";?></td>
                     <td class="">
                        <div class="hidden-sm hidden-xs btn-group">                                      
                           <button class="btn btn-app btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal_del"onclick="delvalue('<?php echo (isset($data1['bank_depo'])) ?  $data1['bank_depo']['id']  : "ไม่มีข้อมูล"?>');">
                              <i class="ace-icon fa fa-trash-o bigger-200"></i>
                                            Delete
                           </button>

                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div><

      <h2 class="header smaller lighter red">ธนาคารในระบบ</h2>
      
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left " id="dataTable" width="100%" cellspacing="0"  style="background-color:#c6c6c6;">
                     <thead>
                        <tr>
                           <th class="text-center">no</th>
                           <th class="text-center">ธนาคาร</th>
                           <th class="text-center">ชื่อบัญชี</th>
                           <th class="text-center">เลขบัญชี</th>
                          
                           <th class="text-center"></th>
                          
                        </tr> 
                     </thead>
                     <tbody>
                       <?php //debug($banksys,true);
                       $i = 0;
                       ?>
                        <?php if (isset($banksys['status'])!='' && $banksys['data']!=''): ?>

                           <?php foreach ($banksys['data'] as $item): ?>
                                  


                                 <tr class="">
                                    
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td class="text-center"><?php echo $item['bankid'];?></td>
                                    <td class="text-center"><?php echo $item['bankname'];?></td>
                                    <td class="text-center"><?php echo $item['bankno'];?></td>
                                    <td>
                                      <div class="hidden-sm hidden-xs btn-group">
                                        <button class="btn btn-app btn-yellow btn-xs"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('bank_depo','<?php echo $item['bankid'];?>','<?php echo $item['bankname'];?>','<?php echo $item['bankno'];?>');">
                                          <i class="ace-icon fa fa-check bigger-160"></i>
                                          เลือก 
                                        </button> 
                                      
                                    </div> 
                                  </td>
                                    
                              </tr>
                              <?php $i++;?>
                           <?php endforeach;?>
                           <?php else: ?>
                              <tr>
                                 <td  colspan="21" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                              </tr>
                           <?php endif;?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      

      <?php
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
      ?>
      <div class="modal fade" id="exampleModal_del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content bbox" >
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center">คุณต้องการลบข้อมูลธนาคาร ใช่หรือไม่</h3>
                  <hr class="x-hr-border-glow">      
               </div>
            <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
               <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/del_bank" style="width: 100%;">
                  <input type="hidden"  name="id_del"    id="id_del"    value="">
                  <button type="submit" id="submit" name="submit" class="btn btn-primary mx-auto" style="border-radius: 10px; width: 30%;">ใช่</button>
                  <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ไม่</button>
               </form>
            </div> 
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModal_up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;">
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center"><?php echo $success; ?> </h3>
                  <hr class="x-hr-border-glow">
                  </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
               </div>
            </div>
         </div>
      </div>
      <?php if(isset($success)):?>
                           
         <script type="text/javascript">
            $(document).ready(function(){
            $('#exampleModal_up').modal('show');
            });
         </script>
                         
            <?php endif;?>
               <?php if(isset($error)):?>               
                  <script type="text/javascript">
                     $(document).ready(function(){
                     $('#exampleModal_up').modal('show');
               });
                  </script>
                               
              <?php endif;?>


<h2 class="header smaller lighter green"></h2>

