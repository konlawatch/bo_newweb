
      <?php $ss = $this->session->userdata();?>
<?php //debug($ss['edit_post']['data'],true);?>
<style type="text/css">
  textarea {
    font-size: .8rem;
    letter-spacing: 1px;
}
textarea {
    padding: 10px;
    line-height: 1.5;
    border-radius: 5px;
    border: 1px solid #ccc;
    box-shadow: 1px 1px 1px #999;
}
.aff__product__icon__slider {
    max-width: 100%;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    margin: 0 auto;
    justify-content: center;
}
</style>
 
 <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 -->

            <div class="modal fade" id="exampleModal_img" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">  
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                        <div class="modal-header">
                            <h1 class="modal-title textgold noise center" id="title_name_img_logo"></h1>
                        </div><br>
                        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg_logo">
                            <input type="hidden"  name="key_img_logo"  id="key_img_logo"      value="">           
                            <div> 
                                <input class="input-file" id="deposit_img1" name="deposit_img1" accept="image/*"  type="file">
                                <label tabindex="0" for="deposit_img1" class="input-file-trigger" style="">เลือกรูป...</label> 
                                <p class="file-return" style="color: #fff !important;"></p>
                            </div><br>       
                            <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                            </div>
                        </form> 
                        </div>
                    </div> 
                </div>
            </div>

            <div class="modal fade" id="exampleModal_remove_img" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="title_remove_img"></h1>
                            </div>
                            <br>
                            <hr class="x-hr-border-glow">
                        </div>
                        <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                            <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/remove_img" style="width: 100%;">
                                <input type="hidden"  name="re_img"  id="re_img"      value="">           
                                <input type="hidden"  name="status"   id="status"       value="0"> 

                                <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                <button type="reset" class="btn btn-secondary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal_remove_post" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="title_remove_post"></h1>
                            </div>
                            <br>
                            <hr class="x-hr-border-glow">
                            <p style="color: #fff;">ท่านต้องการยกเลิก รูปสไลด์ </p>
                        </div>
                        <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                            <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/remove_post" style="width: 100%;">
                                <input type="hidden"    name="re_post"  id="re_post"        value="">           
                                <input type="hidden"    name="status"   id="status"         value="1"> 
                                <input type="hidden"    id="other"      name="other"        value="0" > 
                               
                                <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                <button type="reset" class="btn btn-secondary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal_remove_icon" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="title_remove_icon"></h1>
                            </div>
                            <br>
                           
                            <p style="color: #fff;">ท่านต้องการยกเลิก รูป หรือไม่ </p>
                        </div>
                        <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                            <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/remove_icon" style="width: 100%;">
                                <input type="hidden"    name="re_icon"  id="re_icon"        value="">           
                                <input type="hidden"    name="status"   id="status"         value="1"> 
                                <input type="hidden"    id="other"      name="other"        value="0" > 
                                <input type="hidden"    name="value"    id="value"          value="151x151.jpg">
                                <input type="hidden"    name="but_name"    id="but_name"          value="ไม่มีข้อมูล">
                                <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                <button type="reset" class="btn btn-secondary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="exampleModal_edit_post" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="title_remove_post"></h1>
                            </div>
                            <br>
                            <hr class="x-hr-border-glow">
                            <p style="color: #fff;">ท่านต้องการแกไขข้อมูล นี้หรือไม่</p>
                        </div>
                        <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                            <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/remove_post" style="width: 100%;">
                                <input type="hidden"  name="re_post"  id="re_post"      value="">           
                                <input type="hidden"  name="status"   id="status"       value="1"> 
                                <input type="hidden"    id="other"     name="other"     value="0" > 

                                <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                <button type="reset" class="btn btn-secondary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal_img_sl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="title_name_img"></h1>
                            </div><br>
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/update_img_sl">
                                <input type="hidden"  name="key_img"  id="key_img"      value="">           
                                <div> 
                                    <input class="input-file1" id="post_img" name="post_img" accept="image/*"  type="file">
                                    <label tabindex="0" for="post_img" class="input-file-trigger1" style="">เลือกรูป...</label> 
                                    <p class="file-return1" style="color: #fff !important;"></p>
                                </div>
                                <br>      
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <h1 class="text-primary mb-0 center  textgold noise" id="exampleModalLabel"></h1>
                            <hr class="x-hr-border-glow">
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/create">
                                <input type="hidden"  name="key"    id="key"   value="">
                                <input type="hidden"  name="type"   id="type"   value="">
                                <input type="hidden"  name="status" id="status" value="1">
                                <div>
                                    <label for="text-primary mb-0 textsilver noise " style="color: #fff;">หัวข้อ</label>
                                    <input type="text" class="form-control" id="but_name" name="but_name" placeholder="">
                                </div>
                                <div>
                                    <label for="text-primary mb-0 textsilver noise" style="color: #fff;">รายละเอียดเนื้อหา 1</label>
                                    <textarea class="form-control" id="value" name="value" placeholder="หัวข้อ" style="width: 100%; height: 250px; text-align: center;"></textarea>
                                </div>
                                <div>
                                    <label for="text-primary mb-0 textsilver noise" style="color: #fff;">รายละเอียดเนื้อหา 2</label>
                                    <textarea class="form-control" id="detail" name="detail" placeholder="รายละเอียดเนื้อหา"style="width: 100%; height: 250px; text-align: center;"></textarea>
                                </div> 
                                <br>
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div> 
                    </div> 
                </div>
            </div> 


            <div class="modal fade" id="exampleModal-post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="exampleModalLabel1"></h1>
                            </div>
                            <hr class="x-hr-border-glow">
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg">
                                <input type="hidden"  name="keyname"      id="keyname"      value="">           
                                <input type="hidden"  name="status"       id="status"       value="1"> 

                                <div> 
                                    <input class="input-file" id="deposit_img" name="deposit_img" accept="image/*"  type="file">
                                    <label tabindex="0" for="deposit_img" class="input-file-trigger" style="">เลือกรูป...</label> 
                                    <p class="file-return" style="color: #fff !important;"></p>
                                </div>
                                <br> 
                                <div>
                                    <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ชื่อปุ่ม</label>
                                    <input type="text" class="form-control" id="but_name" name="but_name" placeholder="">
                                </div>
                                <br>      
                                <br>
                                <div>
                                    <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ข้อความ1</label>
                                    <textarea type="text" class="form-control" id="detail" name="detail" placeholder=""></textarea>
                                </div>
                                <br>
                                <div>
                                    <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ข้อความ2</label>
                                    <textarea type="text" class="form-control" id="detail2" name="detail2" placeholder=""></textarea>
                                </div>
                                <br>
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header">
                                <h1 class="modal-title textgold noise center" id="exampleModalLabel1_edit"></h1>
                            </div>
                            <hr class="x-hr-border-glow">
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/edit_post"> 
                                <input type="test"  name="keypostedit"      id="keypostedit"      value="">           
                                <input type="hidden"  name="status"    id="status"    value="1">  
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModals_post_img" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <div class="modal-header"><h1 class="modal-title textgold noise center">test</h1></div>
                            <div class="modal-header"><<h2 class="modal-title textgold noise center">fffffff</h2></div>
                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/updatestatus_post">
                                <input type="hidden"  name="key_id"          id="key_id"          value="">
                                <input type="hidden"  name="status_line"    id="status_line"    value="">  
                                <br>
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal_footer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <h1 class="text-primary mb-0 textgold noise center">เนื้อหาส่วนท้าย(Footer)</h1>
                            <hr class="x-hr-border-glow">

                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/create_footer">
                                <input type="hidden"  name="key_f"      id="key_f"      value="">
                                <input type="hidden"  name="status"     id="status"     value="1">

                                    <label for="form-field-8" style="color: #fff;">รายละเอียดเนื่อหา</label>
                                    <textarea class="form-control" id="value" name="value" placeholder="Default Text"></textarea>    
                                <br>
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal_run" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body">
                            <h1 class="text-primary mb-0 textgold noise center">ข้อความวิ่ง</h1>
                            <hr class="x-hr-border-glow">

                            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/create_run">
                                <input type="hidden"  name="key_r"    id="key_r"   value="">
                                <input type="hidden"  name="status"   id="status" value="1">
                                
                                    <label for="form-field-8" style="color: #fff;">รายละเอียดเนื่อหา</label>
                                    <textarea class="form-control" id="value" name="value" placeholder="Default Text"></textarea>    
                                <br>
                                <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                                    <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                                    <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>

            



            <script type="text/javascript">


                function setvalue(key,type,title_data){
                //$('#name').val(val);
                    $('#key').val(key);
                    $('#type').val(type);
                    document.getElementById("exampleModalLabel").innerHTML = title_data;
                }

                function setvalue_footer(key_f){
                //$('#name').val(val);
                    $('#key_f').val(key_f);

                    document.getElementById("footer").innerHTML = key_f;
                }

                function setvalue_run(key_r){
                //$('#name').val(val);
                    $('#key_r').val(key_r);

                    document.getElementById("t_r").innerHTML = key_r;
                }

                function setvalueup(keyname,title_name){
                    $('#keyname').val(keyname);

                    document.getElementById("exampleModalLabel1").innerHTML = title_name;


                }

                function setimg_post(keypost,name_img_post){
                    $('#keypost').val(keypost);

                    document.getElementById("testti").innerHTML = name_img_post;


                }
                function setimg_post_edit(keypostedit,name_img_edit){
                    $('#keypostedit').val(keypostedit);

                    document.getElementById("exampleModalLabel1_edit").innerHTML = name_img_edit;


                }



                function s_popup(key_id,status_line){
                    $('#key_id').val(key_id);
                    $('#status_line').val(status_line);

                    document.getElementById("s_popup").innerHTML = 'สถานะปัจจุบัน : '+ status_line;


                }

                function setimg_logo(key_img_logo,title_name_img_logo){
                    $('#key_img_logo').val(key_img_logo);

                    document.getElementById("title_name_img_logo").innerHTML = title_name_img_logo;


                }

                function setimg(key_img,title_name_img){
                    $('#key_img').val(key_img);

                    document.getElementById("title_name_img").innerHTML = title_name_img;


                }



                function remove_img(re_img,title_remove_img){
                    $('#re_img').val(re_img);

                    document.getElementById("title_remove_img").innerHTML = title_remove_img;


                }
                function remove_post(re_post,title_remove_post){
                    $('#re_post').val(re_post);

                    document.getElementById("title_remove_post").innerHTML = re_post;


                }

                 function remove_icon(re_icon,title_remove_icon){
                    $('#re_icon').val(re_icon);

                    document.getElementById("title_remove_icon").innerHTML ="ลบรูป : "+re_icon;


                }

                function edit_test(keypostedit){  
                    var keypostedit = keypostedit;
                    // var g= keypostedit;
                    // alert(g);
                    $.ajax({ 
                    url: '<?php echo base_url(); ?>Dashboard/edit_post',
                    type: 'POST',
                    data : {keypostedit},  
                    dataType: 'json',
                    success: function (res) {
                     //alert(res.status); 
                        if(res.status == true){                
                            $('#exampleModal_new .modal-body').html(res.data);

                            $('#exampleModal_new').modal('show');
                        }
                    }
                    });
                }

                function edit_img_s(keypostedit){  
                    var keypostedit = keypostedit;
                    $.ajax({ 
                        url: '<?php echo base_url(); ?>Dashboard/edit_img_s',
                        type: 'POST',
                        data : {keypostedit},  
                        dataType: 'json',
                        success: function (res) {
                        // alert(res.status); 
                            if(res.status == true){                
                                $('#exampleModal_new .modal-body').html(res.data);

                                $('#exampleModal_new').modal('show');
                            }
                        }
                    });
                }

                function edit_text(keypostedit){  
                    var keypostedit = keypostedit;
                    $.ajax({ 
                        url: '<?php echo base_url(); ?>Dashboard/edit_text',
                        type: 'POST',
                        data : {keypostedit},  
                        dataType: 'json',
                        success: function (res) {
                        // alert(res.status); 
                            if(res.status == true){                
                            $('#exampleModal_new .modal-body').html(res.data);

                            $('#exampleModal_new').modal('show');
                            }
                        }
                    });
                }

                function edit_textarea(keypostedit){  
                    var keypostedit = keypostedit;
                    $.ajax({ 
                        url: '<?php echo base_url(); ?>Dashboard/edit_textarea',
                        type: 'POST',
                        data : {keypostedit},  
                        dataType: 'json',
                        success: function (res) {
                        // alert(res.status); 
                            if(res.status == true){                
                                $('#exampleModal_new .modal-body').html(res.data);

                                $('#exampleModal_new').modal('show');
                            }
                        }
                    });
                }

                function icon_logo(keypostedit){  
                    var keypostedit = keypostedit;
                    $.ajax({ 
                        url: '<?php echo base_url(); ?>Dashboard/icon_logo',
                        type: 'POST',
                        data : {keypostedit},  
                        dataType: 'json',
                        success: function (res) {
                        // alert(res.status); 
                            if(res.status == true){                
                                $('#exampleModal_new .modal-body').html(res.data);

                                $('#exampleModal_new').modal('show');
                            }
                        }
                    });
                }


            </script>


<?php //debug($data1,true);?>

<div class="bg"><h2 class="textgold noise" style="margin-left: 5%;">จัดการรูปภาพ หน้าเว็บไซต์(index/home)<span style="font-size: 14px;" ></span></h2></div>
<div class="bg">
<h2 class="textgold noise" style="margin-left: 5%;">Menubar<span style="font-size: 14px;" >(ขนาดแนะนำ 250*100 px.)</span></h2>

            <div class="row">
                <div class="col-xs-12" style="margin-left: 5%;top: 15%;">
                    <div>
                        <ul class="ace-thumbnails clearfix">
                            <li>
                                <a href="" data-rel="colorbox">
                                    <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_menubar'])) ?  $data1['l_menubar']['value']  : "255x150.png"?>">
                                    <div class="text">
                                        <div class="inner">LOGO : Menubar</div>
                                    </div>
                                </a>

                                <div class="tools tools-bottom">
                                    <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal_img" onclick="setimg_logo('l_menubar','รูปโลโก้เว็บไซต์');">
                                        <i class="fa fa-cloud-upload"></i>uplode
                                    </button>
                                </div>
                            </li>
                        </ul>  
                    </div>
                </div>                       
            </div><!-- /.row -->

            <div class="row" style="max-width: 100%">
                <div class="col-xs-12 tab-content" style="width: 50%; margin-left: 25%;margin-right: 25%;">
                <h2 class="textgold noise" style="text-align: center;">Image Sline<span style="font-size: 14px;" >(ขนาดแนะนำ -*-.jpg/.png/.gif.)</span></h2>
                <hr class="x-hr-border-glow">

                    <div style="display: flex;justify-content: center;">

                        <table>

                            <tr>
                                <th>
                                    <?php if($data1['l_body_s1']['other'] =='1'):?>
                                        <ul class="ace-thumbnails clearfix">
                                            <li> 
                                                <a href="" data-rel="colorbox">
                                                    <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s1'])) ?  $data1['l_body_s1']['value']  : "255x150.png"?>" style="width: 210px;" >
                                                    <div class="text"> 
                                                        <div class="inner">LOGO : Body-1</div>
                                                    </div> 
                                                </a>

                                                <div class="tools tools-bottom">

                                                    <!-- <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s1','รูปสไลด์ : 1');">
                                                    <i class="fa fa-cloud-upload"></i>uplode
                                                    </button> -->
                                                    <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s1');">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                    </button>
                                                    <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s1','รูปสไลด์:1');">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s1','รูปสไลด์ : 1');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?>
                                </th>

                                <th>
                                    <?php if($data1['l_body_s2']['other'] =='1'):?>
                                        <ul class="ace-thumbnails clearfix">
                                            <li>
                                                <a href="#" data-rel="colorbox">
                                                    <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s2'])) ?  $data1['l_body_s2']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                    <div class="text">
                                                        <div class="inner">LOGO : Body-2</div>
                                                    </div>
                                                </a>

                                                <div class="tools tools-bottom">


                                                    <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s2');">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                    </button>
                                                    <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s2','รูปสไลด์:2');">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s2','รูปสไลด์ : 2');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?>
                                </th>

                                <th> 
                                    <?php if($data1['l_body_s3']['other'] =='1'):?>
                                        <ul class="ace-thumbnails clearfix">
                                            <li>
                                                <a href="" data-rel="colorbox">
                                                    <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s3'])) ?  $data1['l_body_s3']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                    <div class="text">
                                                        <div class="inner">LOGO : Body-3</div>
                                                    </div>
                                                </a>

                                                <div class="tools tools-bottom">

                                                    <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s3');">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                    </button>
                                                    <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s3','รูปสไลด์:3');">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s3','รูปสไลด์ : 3');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?>
                                </th>
                            </tr>

                            <tr>
                                <td> 
                                    <?php if($data1['l_body_s4']['other'] =='1'):?>
                                    <ul class="ace-thumbnails clearfix">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s4'])) ?  $data1['l_body_s4']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                <div class="text">
                                                <div class="inner">LOGO : Body-4</div>
                                            </div>
                                            </a>

                                            <div class="tools tools-bottom">

                                                <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s4');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s4','รูปสไลด์:4');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s4','รูปสไลด์ : 4');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?> 
                                </td>

                                <td> 
                                    <?php if($data1['l_body_s5']['other'] =='1'):?>
                                        <ul class="ace-thumbnails clearfix">
                                            <li>
                                                <a href="" data-rel="colorbox">
                                                    <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s5'])) ?  $data1['l_body_s5']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                    <div class="text">
                                                        <div class="inner">LOGO : Body-5</div>
                                                    </div>
                                                </a>

                                                <div class="tools tools-bottom">
                                                    <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s5');">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                    </button>
                                                    <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s5','รูปสไลด์:5');">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                    </button>
                                                </div>
                                            </li>
                                        </ul>
                                    <?php else:?>
                                            <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s5','รูปสไลด์ : 5');">
                                                <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                            </button>
                                    <?php endif?>
                                </td>

                                <td>
                                    <?php if($data1['l_body_s6']['other'] == '1'):?>
                                    <ul class="ace-thumbnails clearfix">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s6'])) ?  $data1['l_body_s6']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                <div class="text">
                                                    <div class="inner">LOGO : Body-6</div>
                                                </div>
                                            </a>

                                            <div class="tools tools-bottom">

                                            <!-- <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s6','รูปสไลด์ : 6');">
                                            <i class="fa fa-cloud-upload"></i>uplode
                                            </button> -->
                                                <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s6');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s6','รูปสไลด์:6');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s6','รูปสไลด์ : 6');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?>
                                </td>
                            </tr>


                            <tr>
                                <td>  <?php if($data1['l_body_s7']['other'] == '1'):?>
                                    <ul class="ace-thumbnails clearfix">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s7'])) ?  $data1['l_body_s7']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                <div class="text">
                                                    <div class="inner">LOGO : Body-7</div>
                                                </div>
                                            </a>

                                            <div class="tools tools-bottom">

                                                <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s7');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s7','รูปสไลด์:7');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>

                                            </div>
                                        </li>
                                    </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s7','รูปสไลด์ : 7');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?>
                                </td>
                            
                                <td> <?php if($data1['l_body_s8']['other'] == '1'):?>
                                    <ul class="ace-thumbnails clearfix">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s8'])) ?  $data1['l_body_s8']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                <div class="text">
                                                    <div class="inner">LOGO : Body-8</div>
                                                </div>
                                            </a>

                                            <div class="tools tools-bottom">

                                                <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s8');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s8','รูปสไลด์:8');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s8','รูปสไลด์ : 8');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?> 
                                </td>

                                <td> 
                                    <?php if($data1['l_body_s9']['other'] == '1'):?>
                                        <ul class="ace-thumbnails clearfix">
                                            <li>
                                                <a href="" data-rel="colorbox">
                                                    <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s9'])) ?  $data1['l_body_s9']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                    <div class="text">
                                                        <div class="inner">LOGO : Body-9</div>
                                                    </div>
                                                </a>

                                                <div class="tools tools-bottom">

                                                    <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s9');">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                    </button>
                                                    <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s9','รูปสไลด์:9');">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                    </button>
                                                </div> 
                                            </li>
                                        </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s9','รูปสไลด์ : 9');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?> 
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php if($data1['l_body_s10']['other'] == '1'):?>
                                    <ul class="ace-thumbnails clearfix">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_body_s10'])) ?  $data1['l_body_s10']['value']  : "255x150.png"?>"style="width: 210px;" >
                                                <div class="text">
                                                    <div class="inner">LOGO : Body-10</div>
                                                </div>
                                            </a>

                                            <div class="tools tools-bottom">

                                                <button class="btn btn-xs btn-warning" onclick="edit_img_s('l_body_s10');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('l_body_s10','รูปสไลด์:10');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php else:?>
                                        <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal_img_sl" onclick="setimg('l_body_s10','รูปสไลด์ : 10');">
                                            <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                        </button>
                                    <?php endif?>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>

                        </table> 
                    </div><!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->  
                                      
            </div><!-- /.row -->


            <h2 class="header smaller lighter black"></h2>
                <div class="row container m-auto justify-content-center" style="    text-align: center; margin: auto;">
                    <div class="col-xs-12 tab-content" style="width: 70%; margin-left: 15%;margin-right: 15%;">
                    <h2 class="textgold noise" style="text-align: center;">Image Post<span style="font-size: 14px;" >(ขนาดแนะนำ 1200*340px.jpg/.png/.gif.)</span></h2>
                    <hr class="x-hr-border-glow">

                        <table style="display: flex; justify-content: center;">
                        <div style="margin-left: 5%;">
                        <ul class="ace-thumbnails clearfix"> 

                                                   
                        <tr>
                            <td style="display: flex; justify-content: center;"> 
                                <?php if($data1['post1']['other'] == '1'):?>
                                    <div class=" text-center -box d-flex align-items-start d-md-block -box">

                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post1'])) ?  $data1['post1']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post1');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post1','รูปแนะนำโปร:1');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>

                                            </div>
                                        </li>
                                            <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post1'])) ?  $data1['post1']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                            <hr class="x-hr-border-glow">
                                            <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post1'])) ?  $data1['post1']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post1'])) ?  $data1['post1']['detail1']  : "ไม่มีข้อมูล"?>
                                            </span>

                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post1','รูปแนะนำโปร:1');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button> 
                                <?php endif?><br>
                            </td>

                            <td> 
                                <?php if($data1['post2']['other'] == '1'):?>
                                    <div class=" text-center -box d-flex align-items-start d-md-block -box"> 
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post2'])) ?  $data1['post2']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           

                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post2');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post2','รูปแนะนำโปร:2');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post2'])) ?  $data1['post2']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post2'])) ?  $data1['post2']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post2'])) ?  $data1['post2']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post2','รูปแนะนำโปร:2');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button> 
                                <?php endif?>

                            </td>

                            <td>
                                <?php if($data1['post3']['other'] == '1'):?>
                                    <div class=" text-center -box d-flex align-items-start d-md-block -box">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post3'])) ?  $data1['post3']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post3');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post3','รูปแนะนำโปร:3');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post3'])) ?  $data1['post3']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post3'])) ?  $data1['post3']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post3'])) ?  $data1['post3']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post3','รูปแนะนำโปร:3');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button> 
                                <?php endif?><br> 
                            </td>
                        </tr><br><br>

                        <tr>
                            <td>
                                <?php if($data1['post4']['other'] == '1'):?>
                                    <div class=" text-center -box d-flex align-items-start d-md-block -box">
                                        <li> 
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post4'])) ?  $data1['post4']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post4');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post4','รูปแนะนำโปร:4');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post4'])) ?  $data1['post4']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post4'])) ?  $data1['post4']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post4'])) ?  $data1['post4']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post4','รูปแนะนำโปร:4');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button> 
                                <?php endif?><br> 
                            </td> 

                            <td>
                                <?php if($data1['post5']['other'] == '1'):?>
                                    <div class="text-center -box d-flex align-items-start d-md-block -box">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post5'])) ?  $data1['post5']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post5');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post5','รูปแนะนำโปร:5');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post5'])) ?  $data1['post5']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post5'])) ?  $data1['post5']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post4'])) ?  $data1['post5']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post5','รูปแนะนำโปร:5');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button>
                                <?php endif?><br>
                            </td>
  
                            <td> 
                                <?php if($data1['post6']['other'] == '1'):?>
                                    <div class="text-center -box d-flex align-items-start d-md-block -box">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post6'])) ?  $data1['post6']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post6');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post6','รูปแนะนำโปร:6');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post6'])) ?  $data1['post6']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post6'])) ?  $data1['post6']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post6'])) ?  $data1['post6']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                    <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post6','รูปแนะนำโปร:6');">
                                    <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button>
                                <?php endif?><br>
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <?php if($data1['post7']['other'] == '1'):?>
                                    <div class="text-center -box d-flex align-items-start d-md-block -box">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post7'])) ?  $data1['post7']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post7');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>
                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post7','รูปแนะนำโปร:7');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post7'])) ?  $data1['post7']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post7'])) ?  $data1['post7']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post6'])) ?  $data1['post6']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post7','รูปแนะนำโปร:7');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button>
                                <?php endif?><br>
                            </td>

                            <td> 
                                <?php if($data1['post8']['other'] == '1'):?>

                                    <div class="text-center -box d-flex align-items-start d-md-block -box">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post8'])) ?  $data1['post8']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           
                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post8');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>

                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post8','รูปแนะนำโปร:8');">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>
                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post8'])) ?  $data1['post8']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post8'])) ?  $data1['post8']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post8'])) ?  $data1['post8']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post8','รูปแนะนำโปร:8');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button>
                                <?php endif?><br>
                            </td>

                            <td>
                                <?php if($data1['post9']['other'] == '1'):?>    
                                    <div class="text-center -box d-flex align-items-start d-md-block -box">
                                        <li>
                                            <a href="" data-rel="colorbox">
                                                <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['post9'])) ?  $data1['post9']['value']  : "255x150.png"?>" style="width: 220px;">
                                            </a>
                                            <div class="tools tools-bottom">           

                                                <button class="btn btn-xs btn-warning" onclick="edit_test('post9');">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                </button>


                                                <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_post" onclick="remove_post('post9','รูปแนะนำโปร:9');">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                </button>

                                            </div>
                                        </li>
                                        <h3 class="text-gray-lighter" style="color: #fff;"><span class="d-inline-block d-md-none" ></span><?php echo (isset($data1['post9'])) ?  $data1['post9']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                        <hr class="x-hr-border-glow">
                                        <span class="d-none d-lg-block text-muted-lighter f-5" style="color: #fff;"><?php echo (isset($data1['post9'])) ?  $data1['post9']['detail']  : "ไม่มีข้อมูล"?><br><?php echo (isset($data1['post9'])) ?  $data1['post9']['detail1']  : "ไม่มีข้อมูล"?>
                                        </span>
                                    </div>
                                <?php else:?>
                                    <button class="btn btn-xs btn-info"  data-toggle="modal" data-target="#exampleModal-post" onclick="setvalueup('post9','รูปแนะนำโปร:9');">
                                        <img src="assets/images/add-img.jpg" style="height: 150px !important;">
                                    </button>
                                <?php endif?><br>
                            </td>
                        </tr>
                        </ul>
                        </div>
                        </table>
                    </div>


                </div><br><br>



           <div class="row" style="max-width: 100%">
              <div class="col-xs-12 tab-content" style="width: 70%; margin-left: 15%;margin-right: 15%;">
                <h2 class="textgold noise" style="text-align: center;">Image Post<span style="font-size: 14px;" >(ขนาดแนะนำ 1200*340px.jpg/.png/.gif.)</span></h2>
                <hr class="x-hr-border-glow">
                 <div style="margin-left: 20%;">
                   <div class="col-lg-6 text-white position-relative">
                                    <h3 class="text-primary mb-0" style="text-align: center;"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['but_name']  : "ไม่มีข้อมูล"?></h3>
                                    <hr class="x-hr-border-glow">
                                    <p class="f-6 f-lg-5 px-4" style="text-align: center;  color: #fff;"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['detail']  : "ไม่มีข้อมูล"?></p>
                                    <p class="f-6 f-lg-5 pt-3 px-4" style="text-align: center;  color: #fff;"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['detail1'] : "ไม่มีข้อมูล";?></p>
                                    <br><br>
                                    <div>
                                     <button class="btn btn-app btn-info btn-xs  btn-warning center"  onclick="edit_textarea('promotion1');" style="left: 50%; transform: translate(-50%,-50%)!important;"> 
                                                      <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
                                                      แก้ไข                                            
                                    </button></div>
                    </div>
                  <ul class="ace-thumbnails clearfix">

                    <li>
                      <a href="" data-rel="colorbox">
                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['value']  : "255x150.png"?>" style="width: 220px;">
                        <div class="text">
                          <div class="inner">LOGO : Body-title</div>
                        </div>
                      </a>

                      <div class="tools tools-bottom">
                       
                     <!--   <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal_img" onclick="setimg('body_title1','test');">
                          <i class="fa fa-cloud-upload"></i>uplode
                       </button> -->
                      </div>
                    </li>

                    
                  </ul>
                </div><!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->                        
            </div><!-- /.row -->




            <br>
            <hr class="x-hr-border-glow">
           
            <div class="row">
                                <div class="deskTop">
                                    <div class="aff__bg____casino">
                                        <div class="aff__product__icon__slider">

                                            <div class="box__logo__">
                                                 <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                            <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon1'])) ?  $data1['icon1']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon1'])) ?  $data1['icon1']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon1</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning"  onclick="icon_logo('icon1','รูป icon1');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon1','รูป icon 1');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                           <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon2'])) ?  $data1['icon2']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon2'])) ?  $data1['icon2']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon2</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning" onclick="icon_logo('icon2','รูป icon2');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon2','รูป icon 2');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                             <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon3'])) ?  $data1['icon3']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon3'])) ?  $data1['icon3']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon3</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning" onclick="icon_logo('icon3','รูป icon3');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                           <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon3','รูป icon 3');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                 <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                             <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon4'])) ?  $data1['icon4']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon4'])) ?  $data1['icon4']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon4</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning" onclick="icon_logo('icon4','รูป icon4');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon4','รูป icon 4');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                 <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                             <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon5'])) ?  $data1['icon5']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon5'])) ?  $data1['icon5']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon5</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning"  onclick="icon_logo('icon5','รูป icon5');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon5','รูป icon 5');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                 <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                            <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon6'])) ?  $data1['icon6']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon6'])) ?  $data1['icon6']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon6</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning"  onclick="icon_logo('icon6','รูป icon6');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                           <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon6','รูป icon 6');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                            <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon7'])) ?  $data1['icon7']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon7'])) ?  $data1['icon7']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon7</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning"  onclick="icon_logo('icon7','รูป icon7');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon7','รูป icon 7');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                 <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                             <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon8'])) ?  $data1['icon8']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon8'])) ?  $data1['icon8']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon8</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning"  onclick="icon_logo('icon8','รูป icon8');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                           <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon8','รูป icon 8');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                             <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon9'])) ?  $data1['icon9']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon9'])) ?  $data1['icon9']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon9</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning" onclick="icon_logo('icon9','รูป icon9');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon9','รูป icon 9');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>

                                            <div class="box__logo__">
                                                <ul class="ace-thumbnails clearfix">
                                                    <li>
                                                        <a href="" data-rel="colorbox">
                                                             <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['icon10'])) ?  $data1['icon10']['value']  : "151x151.jpg"?>" class="p-2"  style="height: 151px;width: 151px;" >

                                                        <div class="text-primary center"><?php echo (isset($data1['icon10'])) ?  $data1['icon10']['but_name']  : "ไม่มีข้อมูล"?></div>

                                                        <div class="text">
                                                            <div class="inner">LOGO : icon10</div>
                                                        </div>
                                                        </a>
                                                        <div class="tools tools-bottom">
                                                            <button class="btn btn-xs btn-warning"  onclick="icon_logo('icon10','รูป icon10');">
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>Edit
                                                            </button>

                                                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModal_remove_icon" onclick="remove_icon('icon10','รูป icon 10');">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>Del
                                                            </button>

                                                        </div>
                                                    </li>                   
                                                </ul>
                                            </div>
                                       
              
                                        </div>
                                    </div>
                                </div>
                                </div>


            <br>

            <div class="row" style="max-width: 100%">
                <div class="col-lg-12 col-md-1">
                        <h2 class="text-primary mb-0 center textgold noise"><?php echo (isset($data1['footer_text'])) ?  $data1['footer_text']['value']  : "ไม่มีข้อมูล";?></h2> 
                        <hr class="x-hr-border-glow">     
                        <br><br>
                    <div>
                    <button class="btn btn-app btn-info btn-xs  btn-warning" onclick="edit_text('footer_text');"style="left: 50%; transform: translate(-50%,-50%)!important;"> 
                        <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
                        แก้ไข                                            
                    </button>
                    </div>
                </div>
            </div> 

             <h2 class="textgold noise" style="text-align: center;">Image Footer<span style="font-size: 14px;" >(ขนาดแนะนำ 562*70px .jpg/.png/.gif.)</span></h2>
                <hr class="x-hr-border-glow">
            <div class="row" style="max-width: 100%; display: flex;justify-content: center; ">
              <div class="col-xs-12 " style="width: 100%; margin-left: 10%;margin-right: 10%;">
               
                <div>
                  <ul class="ace-thumbnails clearfix">
                    <li>
                      <a href="" data-rel="colorbox">
                        <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['l_bank'])) ?  $data1['l_bank']['value']  : "255x150.png"?>" style="width: 100%">
                        <div class="text">
                          <div class="inner">LOGO : Footre</div>
                        </div>
                      </a>

                      <div class="tools tools-bottom">
                       <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal_img" onclick="setimg('l_bank','รูป Footer');">
                          <i class="fa fa-cloud-upload"></i>uplode
                       </button>
                      </div>
                    </li>                   
                  </ul>
                </div><!-- PAGE CONTENT ENDS -->

              </div><!-- /.col -->                        
            </div><!-- /.row -->  
    </div>






              <!--   <h2 class="header smaller lighter green"></h2>
                <h2 class="header smaller lighter red">ข้อมูลโปรโมชั่น</h2>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover"  style="background-color:#c6c6c6;">
                                <thead>
                                    <tr>
                                        <th class="center">No.</th>
                                        <th class="center">หัวข้อ</th>
                                        <th class="center">รายละเอียด ส่วนที่1</th>
                                        <th class="center">รายละเอียด ส่วนที่2</th>
                                        <th class="center">Date</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                
                                    <tr>
                                        <td class="center">1</td>
                                        <td class="center"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['but_name']  : "ไม่มีข้อมูล"?></td>
                                        <td class="center"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['value']  : "ไม่มีข้อมูล"?></td>
                                        <td class="center"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['detail'] : "";?></td>
                                        <td class="center"><?php echo (isset($data1['promotion1'])) ?  $data1['promotion1']['cdate']  : "";?></td>
                                        <td>
                                        
                                            <div class="hidden-sm hidden-xs btn-group">
                                        

                                                <button class="btn btn-app btn-info btn-xs  btn-warning"  onclick="edit_textarea('promotion1');"> 
                                                      <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
                                                      แก้ไข                                            
                                                </button>

                                          
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div> -->


<!--                 <h2 class="header smaller lighter green"></h2>
                <h2 class="header smaller lighter red">Footer</h2>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover"  style="background-color:#c6c6c6;">  
                                <thead>
                                    <tr>
                                        <th class="center">No.</th>

                                        <th class="center">หัวข้อ</th>

                                        <th class="center">Date</th>
                                        <th></th>

                                    </tr>
                                </thead>

                                <tbody>

                                    <tr>
                                        <td class="center">1</td>
                                        <td class="center"><?php echo (isset($data1['footer_text'])) ?  $data1['footer_text']['value']  : "ไม่มีข้อมูล"; ?></td>
                                        <td class="center"><?php echo (isset($data1['footer_text'])) ?  $data1['footer_text']['cdate']  : "";?></td>
                                        <td>
                                        <div class="hidden-sm hidden-xs btn-group">


                                         <button class="btn btn-app btn-info btn-xs"  data-toggle="modal" data-target="#exampleModal_footer" onclick="setvalue_footer('footer_text');"> 
                                        <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
                                        เพิม                                            
                                        </button>
                                        <button class="btn btn-app btn-info btn-xs  btn-warning" onclick="edit_text('footer_text');"> 
                                        <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
                                        แก้ไข                                            
                                        </button>
                                        </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
 -->
<h2 class="header smaller lighter green"></h2>
<h2 class="header smaller lighter red">ข้อความวิ่ง</h2>
<marquee direction="left" style="color: #fff;">ไม่มีข้อมูล</marquee>
<div class="row">
    <div class="col-xs-12">
        <table id="simple-table" class="table  table-bordered table-hover"  style="background-color:#c6c6c6;">
            <thead>
            <tr>
            <th class="center">No.</th>

            <th class="center">หัวข้อ</th>

            <th class="center">Date</th>
            <th></th>

            </tr>
            </thead>

            <tbody>

            <tr>
            <td class="center">1</td>
            <td class="center"><?php echo (isset($data1['text_run'])) ?  $data1['text_run']['value']  : "ไม่มีข้อมูล"; ?></td>
            <td class="center"><?php echo (isset($data1['text_run'])) ?  $data1['text_run']['cdate']  : "";?></td>
            <td>
            <div class="hidden-sm hidden-xs btn-group">


            <!--   <button class="btn btn-app btn-info btn-xs"  data-toggle="modal" data-target="#exampleModal_run" onclick="setvalue_run('text_run');"> 
            <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
            เพิม                                            
            </button> -->

            <button class="btn btn-app btn-info btn-xs  btn-warning" onclick="edit_text('text_run');"> 
            <i class="ace-icon fa fa-cloud-upload bigger-160"></i>
            แก้ไข                                            
            </button>
            </div>
            </td>
            </tr>


            </tbody>
        </table>
    </div>
</div>

                <?php 
            $error    = $this->session->flashdata('error');
            $success  = $this->session->flashdata('success');

            // debug($success,true);
        ?>
      <div class="modal fade" id="exampleModal_suc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;">
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise"><?php echo $success; ?></h3>
                  <hr class="x-hr-border-glow">
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <button type="button"  class="btn btn-primary" style="border-radius: 10px; width: 100%;" data-dismiss="modal">Close</button>
               </div>
            </div> 
         </div>
      </div>


      <div class="modal fade" id="exampleModal_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" > 
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;">  
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise"><?php echo $error; ?></h3>
                  <hr class="x-hr-border-glow">
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important; background-color: #eff3f800; width: 100%">
                  <button type="button"  class="btn btn-primary" style="border-radius: 10px; width: 100%;" data-dismiss="modal">Close</button>
               </div>
            </div>  
         </div>
      </div>

     <!--   <div class="modal fade" id="exampleModal_edit222" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <div class="modal-header"><h1 class="modal-title textgold noise center" id="exampleModalLabel1_edit"></h1></div>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg_edit"> 
                        <input type="hidden"  name="key_edit"      id="key_edit"      value="<?php echo $ss['edit_post']['data']['0']['key_all']?>">           
                        <input type="hidden"  name="status"    id="status"    value="1"> 
                        <input type="hidden"  name="v1"    id="v1"    value="<?php echo $ss['edit_post']['data']['0']['value']?>"> 


                       
                       <div style="display: flex; justify-content: center;"><img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($ss['edit_post']['data']['0']['value'])) ?  $ss['edit_post']['data']['0']['value']  : "255x150.png"?>" style="width: 220px;"></div>
                        <div> 
                           <input class="input-file2" id="edit_post_img" name="edit_post_img" accept="image/*"  type="file">
                           <label tabindex="0" for="edit_post_img" class="input-file-trigger2" style="">เลือกรูป...</label> 
                           <p class="file-return2" style="color: #fff !important;"></p>
                        </div><br>

                         
                         <div>
                        <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ชื่อปุ่ม</label>
                        <input type="text" class="form-control" id="but_name" name="but_name" value="<?php echo $ss['edit_post']['data']['0']['but_name']?>">
                        </div><br>      
                 
                      <div>
                        <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ข้อความ1</label>
                        <input type="text" class="form-control" id="detail" name="detail" value="<?php echo $ss['edit_post']['data']['0']['detail']?>">
                        </div><br>
                         <div>
                        <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ข้อความ2</label>
                        <input type="text" class="form-control" id="detail2" name="detail2" value="<?php echo $ss['edit_post']['data']['0']['detail1']?>">
                        </div><br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>
 -->

            <?php if(isset($success)):?>      
              <script type="text/javascript">
                $(document).ready(function(){
                $('#exampleModal_edit222').modal('show');
                });
              </script>       
            <?php endif;?>
            <?php if(isset($error)):?>          
              <script type="text/javascript">
                $(document).ready(function(){
                $('#exampleModal_error').modal('show');   
                });
              </script>                   
            <?php endif;?>





