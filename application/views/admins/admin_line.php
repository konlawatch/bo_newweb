      <?php  
         $ss = $this->session->userdata();
        // echo($data1['but_lsm']['value']);
        // debug($data1,true);
      ?>
      <?php
         $error   = $this->session->flashdata('error');
         $success = $this->session->flashdata('success');
      ?>
      <h2 class="header smaller lighter red">ตั้งค่าการแจ้งเตือน</h2>
        <div class="row" style="display: flex;justify-content: center;">
          <div class="tab-content" style="width: 500px;height: auto;">
            
          
           <div style="display: flex;justify-content: center;">
              <img width="450" height="450" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['img_popup'])) ?  $data1['img_popup']['value']  : "255x150.png"?>"><br>
           </div>
             <div style="display: flex;justify-content: center;">
              <h2 style="color: #fff;"><?php echo (isset($data1['img_popup'])) ?  $data1['img_popup']['detail']  : "ไม่มีข้อมูล"?></h2>
            </div>
         
           </div><br><br>
            <div style="display: flex;justify-content: center;">
           
          </div>
        </div>
        <div style="display: flex;justify-content: center;">
            
            <button class="btn btn-app btn-info btn-xs  btn-warning center"  onclick="edit_popup('img_popup');"style="width: 200px;"> 
             <i class="ace-icon fa fa-pencil-square-o bigger-230"></i>
              แก้ไข 
            </button>

            </div>
       
     
        
     <div style="display: flex;justify-content: center;">
               <h2 class="textgold noise">ตั้งค่า ปิด/เปิด การแจ้งเตือน</h2> 
          </div> 
          <div class="row" style="display: flex;justify-content: center;">

          <div class="tab-content" style="width: 500px;height: auto;">
            
          
              <div style="display: flex;justify-content: center;">
              <h2 class="textgold noise" style="color: #fff;">สถานะ : <?php echo ($data1['popup']['value']=='show') ?  "เปิดการเเจ้งเตือน"  : "ปิดการเเจ้งเตือน"?></h2>
            </div>
         
           </div><br><br>
          
        </div>
        <div style="display: flex;justify-content: center;">
        <button class="btn  btn-warning center"  data-toggle="modal" data-target="#exampleModals_pop" onclick="s_popup('<?php  echo($data1['popup']['key_all']);?>','<?php  echo($data1['popup']['value']);?>');" style="width: 400px;">
             
             
               <?php if($data1['popup']['value'] == 'show'):?>
                  <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                                            <?php echo 'ปิดการเเจ้งเตือน';?>
                                            <?php else:?> 
                                            <?php echo 'เปิดการเเจ้งเตือน';?> 
                                            <?php endif?>
            </button>
          </div>
       
         <h2 class="header smaller lighter red"></h2>  
         <h2 class="header smaller lighter red">ตั้งค่า ปิด/เปิด การให้บริการ : TS911</h2>    

         <div class="row" style="display: flex;justify-content: center;">
            <div class="tab-content" style="width: 500px;height: auto;">
               <div style="display: flex;justify-content: center;">
                  <h1 class="textgold noise" style="color: #fff;"><?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['but_name']  : "ไม่มีข้อมูล"?></h1>
               </div>
               <div style="display: flex;justify-content: center;">
                  <h2 class="textgold noise" style="color: #fff;"><?php echo ($data1['but_ts']['detail']!='') ? "ปิดปรับปรุงระบบ : ".$data1['but_ts']['detail']  : "เปิดให้ใช้บริการ"?></h2>
               </div>
               <div style="display: flex;justify-content: center;">
                  <button class="btn btn-warning center"  data-toggle="modal" data-target="#exampleModals_webservice_ts" onclick="wec_services_ts('but_ts','<?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['value'] : "";?>','<?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['but_name']  : "ไม่มีข้อมูล"?>');" style="width: 400px;"> 
                     
                     <?php if($data1['but_ts']['value'] == 'show'):?>
                        <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                        <?php echo 'ปิดปรับปรุงระบบ';?>
                     <?php else:?> 
                        <?php echo 'เปิดให้ปริการ';?> 
                     <?php endif?>
                  </button>
               </div>

            </div><br><br>

         </div>

          <h2 class="header smaller lighter red"></h2>  
         <h2 class="header smaller lighter red">ตั้งค่า ปิด/เปิด การให้บริการ : LSM</h2>    

         <div class="row" style="display: flex;justify-content: center;">
            <div class="tab-content" style="width: 500px;height: auto;">
               <div style="display: flex;justify-content: center;">
                  <h1 class="textgold noise" style="color: #fff;"><?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['but_name']  : "ไม่มีข้อมูล"?></h1>
               </div>
               <div style="display: flex;justify-content: center;">
                  <h2 class="textgold noise" style="color: #fff;"><?php echo ($data1['but_lsm']['detail']!='') ? "ปิดปรับปรุงระบบ : ".$data1['but_lsm']['detail']  : "เปิดให้ใช้บริการ"?></h2>
               </div>
               <div style="display: flex;justify-content: center;">
                  <button class="btn   btn-warning center"  data-toggle="modal" data-target="#exampleModals_webservice_lsm" onclick="wec_services_lsm('but_lsm','<?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['value'] : "";?>','<?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['but_name']  : "ไม่มีข้อมูล"?>');" style="width: 400px;"> 
                   

                     <?php if($data1['but_lsm']['value'] == 'show'):?>
                        <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                        <?php echo 'ปิดปรับปรุงระบบ';?>
                     <?php else:?> 
                        <?php echo 'เปิดให้ปริการ';?> 
                     <?php endif?>
                  </button>
               </div>

            </div><br><br>

         </div>

          <h2 class="header smaller lighter red"></h2>  
         <h2 class="header smaller lighter red">ตั้งค่า ปิด/เปิด การให้บริการ : IMI</h2>    

         <div class="row" style="display: flex;justify-content: center;">
            <div class="tab-content" style="width: 500px;height: auto;">
               <div style="display: flex;justify-content: center;">
                  <h1 class="textgold noise" style="color: #fff;"><?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['but_name']  : "ไม่มีข้อมูล"?></h1>
               </div>
               <div style="display: flex;justify-content: center;">
                  <h2 class="textgold noise" style="color: #fff;"><?php echo ($data1['but_imi']['detail']!='') ? "ปิดปรับปรุงระบบ : ".$data1['but_imi']['detail']  : "เปิดให้ใช้บริการ"?></h2>
               </div>
               <div style="display: flex;justify-content: center;">
                  <button class="btn btn-warning center"  data-toggle="modal" data-target="#exampleModals_webservice_imi" onclick="wec_services_imi('but_imi','<?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['value'] : "";?>','<?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['but_name']  : "ไม่มีข้อมูล"?>');" style="width: 400px;" > 
                    

                     <?php if($data1['but_imi']['value'] == 'show'):?>
                        <i class="ace-icon fa fa-exclamation-triangle bigger-120"></i>
                        <?php echo 'ปิดปรับปรุงระบบ';?>
                     <?php else:?> 
                        <?php echo 'เปิดให้ปริการ';?> 
                     <?php endif?>
                  </button>
               </div>

            </div><br><br>

         </div>
       
       
      <!-- <h2 class="header smaller lighter red">ปิด/เปิดปุ่ม login web TS911,IMI,LSM </h2>    
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left "  style="background-color:#c6c6c6;"  id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">ลำดับที่</th>
                           <th class="text-center">ชื่อปุ่ม</th>
                           <th class="text-center">สถานะ</th>
                           <th class="text-center">ช่วงเวลา ปิด/เปิด</th>
                           <th class="text-center">status</th>
                           <th class="text-center">edit</th>   
                        </tr>
                     </thead>
                     <tbody>
                         <tr>
                           <td class="center">1</td>
                           <td class="center"><?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['but_name']  : "ไม่มีข้อมูล"?></td>
                           <td class="center"><?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['value']  : "ไม่มีข้อมูล"?></td>
                           <td class="center"><?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['detail'] : "";?></td>
                           <td class="center"><?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['status']  : "";?></td>
                           <td>
                              <div class="hidden-sm hidden-xs btn-group">
                                 <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModals_webservice_ts" onclick="wec_services_ts('but_ts','<?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['value'] : "";?>','<?php echo (isset($data1['but_ts'])) ?  $data1['but_ts']['but_name']  : "ไม่มีข้อมูล"?>');"> 
                                     <?php if($data1['but_ts']['value'] == 'show'):?>
                                          <?php echo 'ปิดปรับปรุง';?>
                                       <?php else:?> 
                                          <?php echo 'เปิดใช้งาน';?>
                                      <?php endif?>
                                 </button>
                              </div>
                           </td>
                        </tr>


                        <tr>
                          <td class="center">2</td>
                          <td class="center"><?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['but_name']  : "ไม่มีข้อมูล"?></td>
                          <td class="center"><?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['value']  : "ไม่มีข้อมูล"?></td>
                          <td class="center"><?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['detail'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['status']  : "";?></td>
                          <td>
                              <div class="hidden-sm hidden-xs btn-group">
                                 <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModals_webservice_lsm" onclick="wec_services_lsm('but_lsm','<?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['value'] : "";?>','<?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['but_name']  : "ไม่มีข้อมูล"?>');"> 
                                    <?php if($data1['but_lsm']['value'] == 'show'):?>
                                          <?php echo 'ปิดปรับปรุง';?>
                                       <?php else:?> 
                                          <?php echo 'เปิดใช้งาน';?>
                                      <?php endif?>
                                 </button>
                              </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">3</td>
                          <td class="center"><?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['but_name']  : "ไม่มีข้อมูล"?></td>
                          <td class="center"><?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['value']  : "ไม่มีข้อมูล"?></td>
                          <td class="center"><?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['detail'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['status']  : "";?></td>
                          <td>
                              <div class="hidden-sm hidden-xs btn-group">
                                 <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModals_webservice_imi" onclick="wec_services_imi('but_imi','<?php echo (isset($data1['but_imi'])) ?  $data1['but_imi']['value'] : "";?>','<?php echo (isset($data1['but_lsm'])) ?  $data1['but_lsm']['but_name']  : "ไม่มีข้อมูล"?>');">
                                      <?php if($data1['but_imi']['value'] == 'show'):?>
                                          <?php echo 'ปิดปรับปรุง';?>
                                       <?php else:?> 
                                          <?php echo 'เปิดใช้งาน';?>
                                      <?php endif?>
                                 </button>      
                              </div> 
                          </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div> -->
      
      <h2 class="header smaller lighter red"></h2>
      <h2 class="header smaller lighter green">ตั้งค่า ข้อมูล/รูปภาพ line@</h2>
   <!--    <div class="row">
         <div class="col-xs-12">
            <div>
               <ul class="ace-thumbnails clearfix">
                  <li>
                     <a href="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['line_img'])) ?  $data1['line_img']['value']  : ""?>" data-rel="colorbox">
                     <img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['line_img'])) ?  $data1['line_img']['value']  : "255x150.png"?>">     
                        <div class="text">
                           <div class="inner">ไลน์แอด</div>
                        </div>
                     </a>
                     <div class="tools tools-bottom">
                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('line_img','รูปภาพไลน์แอด');">
                           <i class="fa fa-cloud-upload"></i>เพิ่มรูป
                        </button>
                     </div>
                  </li>
               </ul>
            </div>
         </div>                      
      </div -->

       <!-- <h2 class="header smaller lighter red">รูปภาพ  : หน้าต่างแจ้งเตือน</h2> -->
        <div class="row" style="display: flex;justify-content: center;">
          <div class="tab-content" style="width: 500px;height: auto;">
            
          
           <div style="display: flex;justify-content: center;">
              <img width="450" height="450" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data1['line_img'])) ?  $data1['line_img']['value']  : "255x150.png"?>"><br>
           </div>
             <div style="display: flex;justify-content: center;">
              <h2 style="color: #fff;"><?php echo (isset($data1['line_img'])) ?  $data1['line_img']['detail']  : "ไม่มีข้อมูล"?></h2>
            </div>
         
           </div><br><br>
            <div style="display: flex;justify-content: center;">
           
          </div>
        </div>
        <div style="display: flex;justify-content: center;">
            
            <button class="btn btn-app btn-info btn-xs  btn-warning center"  onclick="edit_line_add('line_img');" style="width: 200px;"> 
              <i class="ace-icon fa fa-pencil-square-o bigger-230"></i>
              แก้ไข 
            </button>

            </div>

     <!--  <h2 class="header smaller lighter green">ประวัติรูป : ไลน์แอด </h2>     
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left "  style="background-color:#c6c6c6;" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">ลำดับที่</th>
                           <th class="text-center">เลขที่</th>
                           <th class="text-center">รหัสรูป</th>
                           <th class="text-center">ชื่อรูป</th>
                           <th class="text-center">รูปภาพ</th>
                          
                           <th class="text-center">edit</th> 
                        </tr>
                     </thead>
                     <tbody>
                        
                           $i = 1;
                        ?>
                        <?php if (isset($getimg_line) && count($getimg_line) >= 1): ?>
                           <?php foreach ($getimg_line as $item): ?>
                              <tr class="">     
                                 <td class="text-center"><?php echo $i; ?></td>
                                 <td class="text-center"><?php echo $item->id; ?></td>
                                 <td class="text-center"><?php echo $item->key_all; ?></td>
                                 <td class="text-center"><?php echo $item->value; ?></td>
                                 <td class="text-center"><img width="150" height="150" alt="150x150" src="<?php echo base_url();?>assets/uploads/<?php echo $item->value; ?>"></td>
                                
                                 <td class="text-center">
                                    <div class="hidden-sm hidden-xs btn-group">
                                      
                                       <button class="btn btn-app btn-success btn-sm" data-toggle="modal" data-target="#exampleModalre" onclick="recuverysetvalue('<?php echo $item->id; ?>','<?php echo $item->key_all; ?>','ประวัติรูป : ไลน์แอด','<?php echo $item->value; ?>');" style="font-size: 12px;">
                                          <i class="ace-icon fa fa-refresh bigger-200"></i>
                                          นำกลับไปใช้   
                                        </button>
                                       
                                        <button class="btn btn-app btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal_del" onclick="delvalue('<?php echo $item->id; ?>','<?php echo $item->value; ?>');"style="font-size: 12px;">
                                            <i class="ace-icon fa fa-trash-o bigger-200"></i>
                                            ลบ
                                        </button>
                                    </div>
                                 </td>     
                              </tr>
                           <?php $i++;?>
                           <?php endforeach;?>
                           <?php else: ?>
                              <tr>
                                 <td  colspan="21" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                              </tr>
                        <?php endif;?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div> -->
      
<!--       <h2 class="header smaller lighter green">ลิงค์ไลน์แอด</h2>
      <div class="row">
         <div class="col-xs-12">
            <table id="simple-table" class="table  table-bordered "  style="background-color:#c6c6c6;">
               <thead>
                  <tr>
                     <th class="center">No.</th>
                     <th class="center">Key</th>
                     <th class="center">Value</th>
                     <th class="center">Status</th>
                     <th class="center">Date</th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>                  
                  <tr>
                     <td class="center">1</td>
                     <td class="center">linkline</td>
                     <td class="center"><?php echo (isset($data1['linkline@'])) ?  $data1['linkline@']['value']  : "ไม่มีข้อมูล"?></td>
                     <td class="center"><?php echo (isset($data1['linkline@'])) ?  $data1['linkline@']['status'] : "";?></td>
                     <td class="center"><?php echo (isset($data1['linkline@'])) ?  $data1['linkline@']['cdate']  : "";?></td>
                     <td>
                        <div class="hidden-sm hidden-xs btn-group">
                           <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('linkline@','text');"> 
                              <i class="fa fa-cloud-upload"></i>
                           </button>                             
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div> -->
    <!--   <h2 class="header smaller lighter green">ประวัติการใช้ : ลิงค์ไลน์แอด</h2>      
      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left "  style="background-color:#c6c6c6;" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">no</th>
                           <th class="text-center">id</th>
                           <th class="text-center">keyname</th>
                           <th class="text-center">value</th>
                           <th class="text-center">status</th>
                           <th class="text-center">edit</th>                    
                        </tr>
                     </thead>
                  <tbody>
                    
                        $i = 0;
                     ?>
                     <?php if (isset($data_re_link) && count($data_re_link) >= 1): ?>
                        <?php foreach ($data_re_link as $item): ?>
                           <tr class="">                
                              <td class="text-center"><?php echo $i; ?></td>
                              <td class="text-center"><?php echo $item->id; ?></td>
                              <td class="text-center"><?php echo $item->key_all; ?></td>
                              <td class="text-center"><?php echo $item->value; ?></td>
                              <td class="text-center"><?php echo $item->status; ?></td>
                              <td class="text-center">
                                 <div class="hidden-sm hidden-xs btn-group">
                                    <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModalre" onclick="recuverysetvalue('<?php echo $item->id; ?>','<?php echo $item->key_all; ?>');">
                                       <i class="ace-icon fa fa-undo bigger-200"></i>
                                       Recuvery
                                    </button>
                                 </div>
                              </td>     
                           </tr>
                        <?php $i++;?>
                        <?php endforeach;?>
                        <?php else: ?>
                           <tr>
                              <td  colspan="21" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                           </tr>
                     <?php endif;?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div> -->
      
     

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h2 class="text-primary mb-0 textgold noise center" >ข้อมูล ลิงค์ไลน์แอด</h2>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/create_line">
                     <input type="hidden"  name="key"    id="key"    value="">
                     <input type="hidden"  name="type"   id="type"   value="">
                     <input type="hidden"  name="status" id="status" value="1">
                        <div>
                           <label for="form-field-8" class="textsilver noise">ลิงค์ใหม่</label>
                           <textarea class="form-control" id="value" name="value" placeholder="Default Text"></textarea>
                        </div> 
                    
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>

    
       <div class="modal fade" id="exampleModalre" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h2 class="text-primary mb-0 textgold noise center" id="recuvery"></h2>
                  <hr class="x-hr-border-glow">
                  <h4 class="text-primary mb-0 textsilver noise center">นำรูปนี้ไปใช้</h4>
                  <h5 class="text-primary mb-0 textsilver noise center" id="testtitle"></h5>
                  

                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/updatetitle_line">
                     <input type="hidden"  name="id"      id="id"       value="">
                     <input type="hidden"  name="name"    id="name"     value="">
                     <input type="hidden"  name="status"  id="status"   value="1">
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset"  class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>


       <div class="modal fade" id="exampleModals_pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h2 class="text-primary mb-0 textgold noise center">หน้าต่างการเเจ้งเตือน</h2>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/updatestatus_popup">
                     <input type="hidden"  name="key_id"          id="key_id"          value="">
                     <input type="hidden"  name="status_line"    id="status_line"    value="">
                     <?php if($data1['popup']['value'] == 'show'):?>
                        
                         <h3 class="textsilver noise center center">ปิดการแจ้งเตือน</h3>
                         
                     <?php else:?>
                         
                           <h3 class="textsilver noise center center">เปิดการใช้งาน</h3>
                                 
                     <?php endif?>
                    
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>

     

      <div class="modal fade" id="exampleModals_webservice_ts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise" id="ts_ser"></h3>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/web_ts">
                     <input type="hidden"  name="key_but_ts"          id="key_but_ts"          value="">
                     <input type="hidden"  name="status_web_ts"       id="status_web_ts"       value="">
                     <?php if($data1['but_ts']['value'] == 'show'):?>
                         <p style="color: #fff;" class="center">กำหนดช่วงเวลา ปิด/เปืด</p>
                         <div class="center">
                           <input type="text"  name="data_time"    id="data_time"    value="">
                         </div>
                           <?php else:?>
                              <p style="color: #fff;" class="center">เปิดการใช้งาน</p>
                                 <input type="hidden"  name="data_time"    id="data_time"    value="" style="border-radius: 10px !important;"> 
                     <?php endif?>
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>

       <div class="modal fade" id="exampleModals_webservice_imi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise" id="imi_ser"></h3>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/web_imi">
                     <input type="hidden"  name="key_but_imi"          id="key_but_imi"          value="">
                     <input type="hidden"  name="status_web_imi"       id="status_web_imi"       value="">
                     <?php if($data1['but_imi']['value'] == 'show'):?>
                         <p style="color: #fff;" class="center">กำหนดช่วงเวลา ปิด/เปืด</p>
                         <div class="center">
                           <input type="text"  name="data_time"    id="data_time"    value="">
                         </div>
                           <?php else:?>
                              <p style="color: #fff;" class="center">เปิดการใช้งาน</p>
                                 <input type="hidden"  name="data_time"    id="data_time"    value="" style="border-radius: 10px !important;"> 
                     <?php endif?>
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModals_webservice_lsm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise" id="lsm_ser"></h3>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/web_lsm">
                     <input type="hidden"  name="key_but_lsm"          id="key_but_lsm"          value="">
                     <input type="hidden"  name="status_web_lsm"       id="status_web_lsm"       value="">
                     <?php if($data1['but_lsm']['value'] == 'show'):?>
                         <p style="color: #fff;" class="center">กำหนดช่วงเวลา ปิด/เปืด</p>
                         <div class="center">
                           <input type="text"  name="data_time"    id="data_time"    value="">
                         </div>
                           <?php else:?>
                              <p style="color: #fff;" class="center">เปิดการใช้งาน</p>
                                 <input type="hidden"  name="data_time"    id="data_time"    value="" style="border-radius: 10px !important;"> 
                     <?php endif?>
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>
      

     

      <!-- button -->
      <div class="modal fade" id="exampleModals_button" tabindex="-1" role="dialog" aria-labelledby="s_popup" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h3 class="modal-title" id="s_popup"></h3>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                     </button>
               </div>
               <div class="modal-body">
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/updatestatus_popup">
                     <input type="hidden"  name="key_id"          id="key_id"          value="">
                     <input type="hidden"  name="status_line"     id="status_line"      value="">
                   <?php ?>
                     <label class="text-primary mb-0 center textgold noise">ชั่วโมง</label>
                     <input type="text"  name="wc"     id="wc"      value="">
                     
                     <label class="text-primary mb-0 center textgold noise"> นาที </label>
                     <input type="text"  name="wc"     id="wc"      value="">
                           
                     
                     <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-primary">บันทึก</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>


      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center" id="exampleModalLabel1"></h3>
                  <hr class="x-hr-border-glow">
                 
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg_line">
                     <input type="hidden"  name="keyname"      id="keyname"      value="">
                     <input type="hidden"  name="status"    id="status"    value="1">           
                        <div>
                            <input class="input-file" id="deposit_img" name="deposit_img" accept="image/*"  type="file">
                            <label tabindex="0" for="deposit_img" class="input-file-trigger" style="">เลือกรูป...</label> 
                        </div>  
                     <br>
                        <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                           <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                           <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                        </div>
                  </form> 
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModal_del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content bbox" >
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center">คุณต้องการลบรูปนี้ ใช้หรือไม่</h3>
                  <hr class="x-hr-border-glow">
                  <h5 class="text-primary mb-0 textsilver noise center" id="t_del_id"></h5>
                  <h5 class="text-primary mb-0 textsilver noise center" id="t_del"></h5>
               <br>    
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>Dashboard/del_line" style="width: 100%;">
                     <input type="hidden"  name="id_del"    id="id_del"    value="">
                     <button type="submit" id="submit" name="submit" class="btn btn-primary mx-auto" style="border-radius: 10px; width: 30%;">ใช่</button>
                     <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ไม่</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

         <?php //debug($data1,true);?>
         <div class="modal fade" id="exampleModal_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content  tab-content">
               <div class=" tab-content">
               <div class="modal-body">
                 <?php echo $error; ?>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" style="border-radius: 30px;" data-dismiss="modal">Close</button>
                
               </div>
               </div>
             </div>
           </div>
         </div>

         <div class="modal fade" id="exampleModal_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  tab-content" style="background-color: #181c23;"> 
                        <div class="modal-body"></div>
                    </div>
                </div>
            </div>

         <?php if(isset($error)):?>                             
            <script type="text/javascript">
               $(document).ready(function(){
               $('#exampleModal_error').modal('show');
               });
            </script>                           
         <?php endif;?>

         <script type="text/javascript">

            function edit_popup(keypostedit){  
                    var keypostedit = keypostedit;
                    $.ajax({ 
                        url: '<?php echo base_url(); ?>Dashboard/edit_popup',
                        type: 'POST',
                        data : {keypostedit},  
                        dataType: 'json',
                        success: function (res) {
                        // alert(res.status); 
                            if(res.status == true){                
                                $('#exampleModal_new .modal-body').html(res.data);

                                $('#exampleModal_new').modal('show');
                            }
                        }
                    });
                }

                function edit_line_add(keypostedit){  
                    var keypostedit = keypostedit;
                    $.ajax({ 
                        url: '<?php echo base_url(); ?>Dashboard/edit_line_add',
                        type: 'POST',
                        data : {keypostedit},  
                        dataType: 'json',
                        success: function (res) {
                        // alert(res.status); 
                            if(res.status == true){                
                                $('#exampleModal_new .modal-body').html(res.data);

                                $('#exampleModal_new').modal('show');
                            }
                        }
                    });
                }
            
            function setvalue(key,type){
             //$('#name').val(val);
               $('#key').val(key);
               $('#type').val(type);
               document.getElementById("exampleModalLabel").innerHTML = key;
            }

            function setvalueup(keyname,title){
               $('#keyname').val(keyname);
               document.getElementById("exampleModalLabel1").innerHTML = title;
            }

            function recuverysetvalue(id,name,title_reco,img_title){ 
               $('#id').val(id);
               $('#name').val(name);
               document.getElementById("recuvery").innerHTML = title_reco;
               document.getElementById("testtitle").innerHTML = img_title;
            }

            function s_popup(key_id,status_line){
               $('#key_id').val(key_id);
               $('#status_line').val(status_line); 
               document.getElementById("s_popup").innerHTML = key_id;
            }

            function wec_services_ts(key_but_ts,status_web_ts,title_web_ts){
               $('#key_but_ts').val(key_but_ts);
               $('#status_web_ts').val(status_web_ts); 
               document.getElementById("ts_ser").innerHTML = title_web_ts;
            }

            function wec_services_lsm(key_but_lsm,status_web_lsm,title_web_lsm){
               $('#key_but_lsm').val(key_but_lsm);
               $('#status_web_lsm').val(status_web_lsm); 
               document.getElementById("lsm_ser").innerHTML = title_web_lsm;
            }

            function wec_services_imi(key_but_imi,status_web_imi,title_web_imi){
               $('#key_but_imi').val(key_but_imi);
               $('#status_web_imi').val(status_web_imi); 
               document.getElementById("imi_ser").innerHTML = title_web_imi;
            }

             function delvalue(id_del,title_del){
               $('#id_del').val(id_del);
               document.getElementById("t_del").innerHTML = "ชื่อรูป : " + title_del;
               document.getElementById("t_del_id").innerHTML ="เลขที่ : " + id_del;
            }
      </script>
