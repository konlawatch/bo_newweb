<!-- Button trigger modal -->
<?php $ss = $this->session->userdata();?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/create">
                        <input type="hidden" name="key" id="key" value="">
                        <input type="text" name="value" id="value" value="">
                        <input type="hidden" name="type" id="type" value="">
                        <input type="hidden" name="status" id="status" value="1">
                    <div class="form-group">
                        <label>วันที่</label>
                        <input class="form-control" name="cdate" id="cdate" type="text" data-date-format="dd-mm-yyyy" value="<?php echo (isset($data->cdate)) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y'); ?>" style="border:0px; ">
                       
                    </div>
                        <div class="form-group">
                                <div class="form-row">                  
                                    <div class="col">           
                                        <select class="form-control" name="Hour" id="Hour" style="display: none">
                                           <option value="" >ชั่วโมง</option>
                                              <?php for($i = 0;$i <= 23; $i++):?>
                                                <option value="<?php echo sprintf('%02d',$i);?>" <?php echo (date('H') == $i) ? 'selected' : ''; ?>><?php echo sprintf('%02d',$i);?></option>
                                              <?php endfor;?>
                                        </select>
                                      </div>
                                      <div class="col">
                                         <select class="form-control" name="Min" id="Min" style="display: none">
                                            <option value="">นาที</option>
                                                <?php for($i = 0;$i <= 59; $i++):?>
                                                 <option value="<?php echo sprintf('%02d',$i);?>" <?php echo (date('i') == $i) ? 'selected' : ''; ?>><?php echo sprintf('%02d',$i);?></option>
                                                <?php endfor;?>
                                         </select>
                                      </div>
                                </div>

                       </div>
                       <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Modal update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/updatekey">
                        <input type="text" name="name" id="name" value="">
                        <input type="text" name="value" id="value" value="">
                        
                   
                       
                       <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>


<button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#exampleModal" onclick="setvalue('title','text');">test</button>

<button type="button" class="btn btn-primary"   data-toggle="modal" data-target="#exampleModal"  onclick="setvalue('img','text');">test</button>



<script type="text/javascript">
   
  function setvalue(key,type){
    //$('#name').val(val);
    $('#key').val(key);
    $('#type').val(type);
    document.getElementById("exampleModalLabel").innerHTML = key;
  }

   function setvalueup(name){
    $('#name').val(name);
   
   
  
  }
// var tagButton = document.getElementById ( "key" );
// tagButton.onclick = function()
// {
     
//      document.getElementById("name").value=tagButton.value;
// }

// var tagButton1 = document.getElementById ( "key1" );
// tagButton1.onclick = function()
// {
     

//      document.getElementById("name").value=tagButton1.value;
//    }
</script>

      <div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">no</th>
                           <th class="text-center">key</th>
                           <th class="text-center">value</th>
                           <th class="text-center">status</th>
                           <th class="text-center">edit</th>
                          
                        </tr>
                     </thead>
                     <tbody>
                       <?php //debug($data1,true);
                       $i = 0;
                       ?>
                        <?php if (isset($data) && count($data) >= 1): ?>
                           <?php foreach ($data as $item): ?>
                                 <tr class="">
                                    
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $item->key; ?></td>
                                    <td><?php echo $item->value; ?></td>
                                    <td><?php echo $item->status; ?></td>
                                    <td>
                                      <div class="hidden-sm hidden-xs btn-group">
                                 <button class="btn btn-app btn-purple btn-sm" data-toggle="modal" data-target="#exampleModal1" onclick="setvalueup('<?php echo $item->key; ?>');">
                                          <i class="ace-icon fa fa-cloud-upload bigger-200"></i>
                                          Upload
                                        </button>

                                      
                                    </div>
                                  </td>
                                    
                              </tr>
                              <?php $i++;?>
                           <?php endforeach;?>
                           <?php else: ?>
                              <tr>
                                 <td  colspan="21" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                              </tr>
                        <?php endif;?>
                     </tbody>
                  
                  </table>
               </div>
            </div>
         </div>
      </div>




      <div class="row">
                  <div class="col-xs-12">
                    <table id="simple-table" class="table  table-bordered table-hover">
                      <thead>
                        <tr>
                          <th class="center">No.</th>
                          <th class="center">Key</th>
                          <th class="center">Value</th>
                          <th class="center">Status</th>
                          <th class="center">Date</th>
                          <th></th>

                        </tr>
                      </thead>

                      <tbody>
                     
                        <tr>
                          <td class="center">1</td>
                          <td class="center">B.Title1</td>
                          <td class="center"><?php echo (isset($data1['B.Title1'])) ?  $data1['B.Title1']['value']  : "ไม่มีข้อมูล"?></td>
                          <td class="center"><?php echo (isset($data1['B.Title1'])) ?  $data1['B.Title1']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title1'])) ?  $data1['B.Title1']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                              <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title1','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>

                              
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">2</td>
                          <td class="center">B.Title2</td>
                          <td class="center"><?php echo (isset($data1['B.Title2'])) ?  substr($data1['B.Title2']['value'],0,100)  : "ไม่มีข้อมูล"?></td>
                          <td class="center"><?php echo (isset($data1['B.Title2'])) ?  $data1['B.Title2']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title2'])) ?  $data1['B.Title2']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title2','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">3</td>
                          <td class="center">B.Title3</td>
                          <td class="center"><?php echo (isset($data1['B.Title3'])) ?  $data1['B.Title3']['value']  : "ไม่มีข้อมูล";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title3'])) ?  $data1['B.Title3']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title3'])) ?  $data1['B.Title3']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                              <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title3','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">4</td>
                          <td class="center">B.Title4</td>
                          <td class="center"><?php echo (isset($data1['B.Title4'])) ?  $data1['B.Title4']['value']  : "ไม่มีข้อมูล";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title4'])) ?  $data1['B.Title4']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title4'])) ?  $data1['B.Title4']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title4','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">5</td>
                          <td class="center">B.Title5</td>
                          <td class="center"><?php echo (isset($data1['B.Title5'])) ?  $data1['B.Title5']['value']  : "ไม่มีข้อมูล";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title5'])) ?  $data1['B.Title5']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title5'])) ?  $data1['B.Title5']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                              <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title5','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">6</td>
                          <td class="center">B.Title6</td>
                          <td class="center"><?php echo (isset($data1['B.Title6'])) ?  $data1['B.Title6']['value']  : "ไม่มีข้อมูล";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title6'])) ?  $data1['B.Title6']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title6'])) ?  $data1['B.Title6']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title6','text');">  
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">7</td>
                          <td class="center">B.Title7</td>
                          <td class="center"><?php echo (isset($data1['B.Title7'])) ?  $data1['B.Title7']['value']  : "ไม่มีข้อมูล";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title7'])) ?  $data1['B.Title7']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title7'])) ?  $data1['B.Title7']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title7','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">8</td>
                          <td class="center">B.Title8</td>
                          <td class="center"><?php echo (isset($data1['B.Title8'])) ?  $data1['B.Title8']['value'] : "ไม่มีข้อมูล";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title8'])) ?  $data1['B.Title8']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title8'])) ?  $data1['B.Title8']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                              <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title8','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="center">9</td>
                          <td class="center">B.Title9</td>
                          <td class="center"><?php echo (isset($data1['B.Title9'])) ?  $data1['B.Title9']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title9'])) ?  $data1['B.Title9']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title9'])) ?  $data1['B.Title9']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                              <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title9','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                         <tr>
                          <td class="center">10</td>
                          <td class="center">B.Title10</td>
                          <td class="center"><?php echo (isset($data1['B.Title10'])) ?   $data1['B.Title10']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title10'])) ?   $data1['B.Title10']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title10'])) ?   $data1['B.Title10']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title10','text');">  
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                         <tr>
                          <td class="center">11</td>
                          <td class="center">B.Title11</td>
                          <td class="center"><?php echo (isset($data1['B.Title11'])) ?  $data1['B.Title11']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title11'])) ?  $data1['B.Title11']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title11'])) ?  $data1['B.Title11']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                              <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title11','text');">  
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                         <tr>
                          <td class="center">12</td>
                          <td class="center">B.Title12</td>
                          <td class="center"><?php echo (isset($data1['B.Title12'])) ?  $data1['B.Title12']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title12'])) ?  $data1['B.Title12']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title12'])) ?  $data1['B.Title12']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title12','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                         <tr>
                          <td class="center">13</td>
                          <td class="center">F.title1</td>
                          <td class="center"><?php echo (isset($data1['B.Title13'])) ?  $data1['B.Title13']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title13'])) ?  $data1['B.Title13']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title13'])) ?  $data1['B.Title13']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title13','text');">  
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                         <tr>
                          <td class="center">14</td>
                          <td class="center">F.title2</td>
                          <td class="center"><?php echo (isset($data1['B.Title14'])) ?  $data1['B.Title14']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title14'])) ?  $data1['B.Title14']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title14'])) ?  $data1['B.Title14']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title14','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>

                         <tr>
                          <td class="center">15</td>
                          <td class="center">F.title3</td>
                          <td class="center"><?php echo (isset($data1['B.Title15'])) ?  $data1['B.Title15']['value']  : "ไม่มีข้อมูล"; ?></td>
                          <td class="center"><?php echo (isset($data1['B.Title15'])) ?  $data1['B.Title15']['status'] : "";?></td>
                          <td class="center"><?php echo (isset($data1['B.Title15'])) ?  $data1['B.Title15']['cdate']  : "";?></td>
                          <td>
                            <div class="hidden-sm hidden-xs btn-group">
                               <button class="btn btn-xs btn-info"   data-toggle="modal" data-target="#exampleModal" onclick="setvalue('B.Title15','text');"> 
                                <i class="fa fa-cloud-upload"></i>
                              </button>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div><!-- /.span -->
                </div><!-- /.row -->