 <?php $ss = $this->session->userdata();?>
    <?php
      $s=isset($data->cdate) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y');

       $d1 = explode('-', $s);
       $dd = $d1[2]."-".$d1[1]."-".$d1[0];

      ?>

<script src="assets/js/jquery-latest.js"></script>
<div class="form-page">
        <?php 
            $error    = $this->session->flashdata('error');
            $success  = $this->session->flashdata('success');
        ?>
      <div class="modal fade" id="exampleModal_suc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;">
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise"><?php echo $success; ?></h3>
                  <hr class="x-hr-border-glow">
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <button type="button"  class="btn btn-primary" style="border-radius: 10px; width: 100%;" data-dismiss="modal">Close</button>
               </div>
            </div> 
         </div>
      </div>


      <div class="modal fade" id="exampleModal_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;">  
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise"><?php echo $error; ?></h3>
                  <hr class="x-hr-border-glow">
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important; background-color: #eff3f800; width: 100%">
                  <button type="button"  class="btn btn-primary" style="border-radius: 10px; width: 100%;" data-dismiss="modal">Close</button>
               </div>
            </div>  
         </div>
      </div>

      <?php if(isset($success)):?>      
        <script type="text/javascript">
          $(document).ready(function(){
          $('#exampleModal_suc').modal('show');
          });
        </script>       
      <?php endif;?>
      <?php if(isset($error)):?>          
        <script type="text/javascript">
          $(document).ready(function(){ 
          $('#exampleModal_error').modal('show');
          });
        </script>                   
      <?php endif;?>

      <div class="text-center animate__animated animate__fadeIn mb-4 animate__slower 3s container">
         <div class="justify-content-md-center mb-4 row">
            <div class="col-lg-10">
               <div id="withdraw" class="mt-3 mb-3">
                  <div class="dv90yj-3 ekmdOJ text-left d-flex bg-header">
                     <p class="ml-2">แจ้งฝากเงิน</p>
                  </div>

        
                  <div class="mb-3 row">
                     <div class="col">
                        <div class="card">
                           <div class="card-header bg-header">
                              <h5 class=" textsilver noise">บัญชีธนาคารระบบ</h5>
                           </div>
                              <div class="card-body">
                                 <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="deposit/mydeposit"> 
                                    <input type="hidden" name="tobank" value=" <?php echo (isset($data['bank_depo']['but_name'])) ? $data['bank_depo']['but_name'].'-'.$data['bank_depo']['detail'] : "ไม่มีข้อมูล"?>">
                                    <input type="hidden" name="username" value="<?php echo $ss['data']['userid'];?>">
                                    <input type="hidden" name="modeid" value="1">
                                    <input type="hidden" name="webid" id="webid" value="">
                                    <div class="justify-content-center row">
                                       <div class="col-lg-6 col-md-8 col-12">
                                          <div  class="tab-content" style="text-align: center;">                                       
                                             <input type="hidden" name="t_bank" id="t_bank" value=" <?php echo (isset($data['bank_depo']['but_name'])) ? $data['bank_depo']['but_name'].'-'.$data['bank_depo']['detail'] : "ไม่มีข้อมูล"?>">
                                             <div>
                                                <img src="<?php echo base_url();?>assets/img/logo-bank/<?php echo (isset($data['bank_depo']['but_name'])) ?  $data['bank_depo']['but_name']  : "255x150"?>.png" style="width: 150px; height: 150px;">
                                                <h5 class=" textsilver noise"><?php echo (isset($data['bank_depo'])) ?  $data['bank_depo']['value']  : "ไม่มีข้อมูล"?></h5>                                            
                                                <h5 id="pwd_spn " class="password-span  textsilver noise" > <?php echo (isset($data['bank_depo']['detail'])) ? $data['bank_depo']['detail'] : "ไม่มีข้อมูล"?></h5>
                                            
                                                <!-- <?php echo (isset($data['bank_depo']['detail'])) ? $data['bank_depo']['detail'] : "ไม่มีข้อมูล"?> -->
                                                <div class="nk-gap"></div>
                                                <a href=""  id="cp_btn" class="get-started-btn scrollto" >Copy</a>
                                                <hr class="x-hr-border-glow">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <hr><p class=" textsilver noise">เลือกเวบที่ต้องการฝาก</p>
                                    <div class="justify-content-center mt-3 row">
                                       <div class="was-validated">
                                          <table style="width:100%" >
                                          <tr>
                                             <th>
                                                <?php if($ss['data']['user_ts'] != ''):?>
                                                   <?php if($ss['data']['user_ts'] == 'pending'):?>  
                                                      <?php else:?>
                                                         <div class="custom-control custom-radio" id="ts_g" >
                                                            <input type="radio" class="custom-control-input" id="web_ts1" name="web" onClick="show_table(this.value);" value="TS911" >
                                                            <label class="custom-control-label" for="web_ts1" ><img src="assets/images/client-111.png" alt=""style="height: 100px; max-width: 100%;"><hr class="x-hr-border-glow"></label>
                                                            <input type="hidden" name="TS" id="TS" value="<?php echo $ss['data']['user_ts']; ?>">
                                                         </div>
                                                      <?php endif;?>
                                                   <?php else:?>
                                                <?php endif;?>
                                             </th> 
                                             <th>
                                                <?php if($ss['data']['user_lsm'] != ''):?>
                                                   <?php if($ss['data']['user_lsm'] == 'pending'):?>  
                                                      <?php else:?>
                                                         <div class="custom-control custom-radio" id="ls_g">
                                                            <input type="radio" class="custom-control-input" id="web_ls1" name="web" onClick="show_table(this.value);" value="LSM" >
                                                            <label class="custom-control-label" for="web_ls1" ><img src="assets/images/client-333.png" alt=""style="height: 100px; max-width: 100%;"><hr class="x-hr-border-glow"></label>
                                                            <input type="hidden" name="LS" id="LS" value="<?php echo $ss['data']['user_lsm']; ?>">
                                                         </div> 
                                                      <?php endif;?> 
                                                   <?php else:?>
                                                <?php endif;?>
                                             </th>
                                             <th> 
                                                <?php if($ss['data']['user_imi'] != ''):?>
                                                   <?php if($ss['data']['user_imi'] == 'pending'):?>  
                                                      <?php else:?>
                                                         <div class="custom-control custom-radio" id="imi_g" >
                                                            <input type="radio" class="custom-control-input" id="web_imi" name="web" onClick="show_table(this.value);" value="IMI" >
                                                            <label class="custom-control-label" for="web_imi" ><div style="position: relative;"><div id="img_imi"><img src="assets/images/client-2222.png"  alt=""style="height: 100px; max-width: 100%;"></div></div><hr class="x-hr-border-glow"></label>
                                                            <input type="hidden" name="IMI" id="IMI" value="<?php echo $ss['data']['user_imi']; ?>">
                                                         </div>
                                                      <?php endif;?> 
                                                   <?php else:?>
                                                <?php endif;?>
                                             </th>
                                          </tr>
                                          </table>
                                       </div>  
                                    </div>
                                    <div class="justify-content-center mt-3 row">
                                       <div class="col-md-8 col-12">
                                          <div class="form-group">
                                             <input type="hidden" class="form-control" name="name" id="name" placeholder="ชื่อ-สกุล" value="<?php echo $ss['data']['name'];?>"  >
                                          </div>
                                          <div class="form-group">
                                             <label class=" mb-0 center  textsilver noise ">ยูชเซอร์เนม</label>
                                             <input type="text" class="form-control" name="userid" id="userid"  placeholder="ยูชเซอร์เนม"  style="border-radius: 10px !important;" readonly>
                                          </div>
                                          <div class="form-group">
                                             <label class=" mb-0 center  textsilver noise ">จำนวนเงินที่ฝาก</label>
                                             <input type="text" class="form-control" name="amount" id="amount" placeholder="ยอดฝาก" value="" onkeyup="tttt();" style="border-radius: 10px !important;" ><br>
                                                                  
                                          </div>
                                          <div class="form-group">
                                             <input type="hidden" class="form-control" name="frombank" id="frombank"  value="<?php echo $ss['data']['bank1'];?>"   style="border-radius: 10px !important;">
                                          </div>
                                          <div class="form-group">   
                                             <label class=" mb-0 center  textsilver noise ">วันเวลาที่โอน<span class="text-danger">*</span></label> 
                                             <input class="form-control " name="cdate" id="cdate" type="date" value="<?php echo $dd;?>" style="border-radius: 10px !important;">
                                          </div>
                                          <div class="form-group">
                                             <div class="form-row">
                                                <div class="col">
                                                   <label class=" mb-0 center  textsilver noise ">ชั่วโมง</label>
                                                   <div class="deposit_date">
                                                      <select class="form-control" name="depositHour" id="depositHour" style="border-radius: 10px !important;">
                                                         <option value="">ชั่วโมง</option>
                                                            <?php for($i = 0;$i <= 23; $i++):?>
                                                               <option value="<?php echo sprintf('%02d',$i);?>" <?php echo (date('H') == $i) ? 'selected' : ''; ?>><?php echo sprintf('%02d',$i);?></option>
                                                            <?php endfor;?>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="col">
                                                   <label class=" mb-0 center  textsilver noise "> นาที </label>
                                                   <div class="deposit_date">
                                                      <select class="form-control" name="depositMin" id="depositMin" style="border-radius: 10px !important;">
                                                      <option value="">นาที</option>
                                                         <?php for($i = 0;$i <= 59; $i++):?>
                                                            <option value="<?php echo sprintf('%02d',$i);?>" <?php echo (date('i') == $i) ? 'selected' : ''; ?>><?php echo sprintf('%02d',$i);?></option>
                                                         <?php endfor;?>                                                                   
                                                      </select>
                                                   </div> 
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class=" mb-0 center  textsilver noise ">สลิปโอนเงิน</label>
                                             <hr class="x-hr-border-glow">
                                             <div class="input-file-container col-md-0" style="width: 60%; left: 20%;">                     
                                                <input class="input-file" id="deposit_img" name="deposit_img" accept="image/*"  type="file" OnChange="showPreview(this)">
                                                <label tabindex="0" for="deposit_img" class="input-file-trigger" style="">เลือกรูป...</label>                        
                                                <p class="file-return" style="color: #525454 !important;"></p>
                                                <div style="display: flex;justify-content: center;">
                                                <img id="imgAvatar" style="width: 350px;height: auto;">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                             <label class=" mb-0 center  textsilver noise ">โบนัส</label> 
                                             <hr class="x-hr-border-glow"><br>                                                     
                                             <div class="was-validated">  
                                                <div class="form-row">
                                                   <div class="col">
                                                      <div class="custom-control custom-radio">
                                                         <input type="radio" class="custom-control-input" id="bcode1" name="bcode" value='N' checked="">
                                                         <label class="custom-control-label  textsilver noise" for="bcode1 " >ไม่รับโบนัส</label>
                                                      </div>
                                                   </div>
                                                <div class="col">
                                                   <div class="custom-control custom-radio">
                                                      <input type="radio" class="custom-control-input" id="bcode2" name="bcode" value='Y' >
                                                      <label class="custom-control-label  textsilver noise" for="bcode2" >รับโบนัส</label><br>                
                                                   </div>
                                                </div>
                                                </div>
                                             </div>
                                          </div>
                                          <hr>                       
                                       </div>
                                       <div class="text-left col-md-8 col-12">
                                          <p class=" textsilver noise">* ถอนขั้นต่ำ 100 บาท</p><p class=" textsilver noise">* ถอนสูงสุดต่อครั้ง 1,000,000 บาท</p><p class=" textsilver noise">* ถอนสูงสุดต่อวัน 10,000,000 บาท</p>
                                       </div>
                                       <div class="mb-2 col-md-8 col-12">
                                          <button type="submit" class="btn btn-success btn-block btn-lg">ฝาก</button>
                                       </div>
                                       <div class="col-md-8 col-12">
                                          <a href="<?php base_url();?>main"><button type="button" class="btn btn-danger btn-block btn-lg">ยกเลิก</button></a>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
  <script>
              function myFunction() 
              {
                  var copyText = document.getElementById("myInput");
                  copyText.select();
                  copyText.setSelectionRange(0, 99999)
                  document.execCommand("copy");
                  alert("คัดลอกข้อความ:  " + copyText.value);
              }
            </script>

            <script type="text/javascript">
                  document.getElementById("cp_btn").addEventListener("click", copy_password);

              function copy_password() 
              {
                  var copyText = document.getElementById("pwd_spn");
                  var textArea = document.createElement("textarea");
                  textArea.value = copyText.textContent;
                  textArea.setAttribute('readonly', '');
                  textArea.style.position = 'absolute';
                  textArea.style.left = '-9999px';
                  document.body.appendChild(textArea);
                  textArea.select();
                  document.execCommand("Copy");
                  document.body.removeChild(textArea);
                  alert("คัดลอกข้อความ:  " + textArea.value); 
              }
            </script>

            <script>
              function show_table(id)
              {
                     
                  var radio=document.getElementsByName("web").checked;
                  if(id == "LSM")
                  {   
                         
                       var name = document.getElementById("LS").value;
                       document.getElementById("userid").value = name;
                       document.getElementById('webid').value = 'LSM'

                  }   

                  if(id == "TS911")
                  {   
                         var name = document.getElementById("TS").value;
                       document.getElementById("userid").value = name;
                       document.getElementById('webid').value = 'TS911'
                     
                  }  
 
                   if(id == "IMI")
                  {   
                         var name = document.getElementById("IMI").value;
                       document.getElementById("userid").value = name;
                       document.getElementById('webid').value = 'IMI'
                      

                  }  
                                     
              }
            </script>
             <script language="JavaScript">
              function showPreview(ele)
              {
                  $('#imgAvatar').attr('src', ele.value); // for IE
                        if (ele.files && ele.files[0]) {
                  
                            var reader = new FileReader();
                    
                            reader.onload = function (e) {
                                $('#imgAvatar').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(ele.files[0]);
                        }
              }
            </script>
          