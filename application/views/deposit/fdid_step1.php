<?php 
   $ss = $this->session->userdata();
   // debug($ss);
   $s  = isset($data->cdate) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y');
   $d1 = explode('-', $s);
   $dd = $d1[2]."-".$d1[1]."-".$d1[0];

   $sitelist = $this->config->config['sitelist'];
   $banklist = $this->config->config['banklist'];
   $banklist_color = $this->config->config['banklist_color'];

   $ubank = explode('-', $ss['data']['bank']);
   $user_bank_name   = $ss['data']['name'];
   $user_bank        = $ubank[0];
   $user_bank_desc   = $banklist[$ubank[0]];
   $user_bank_number = $ubank[1];
   // $user_bank = 'CIMB';
   // debug($banklist,true); 
?>
<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>
<style type="text/css">
   .btn-dark {
      background-color: #3a9a82;
   }
</style>
<div id="deposit" class="mt-3 mb-3">
   <div class="dv90yj-3 ekmdOJ text-left d-flex" style="background-color: rgb(0, 191, 161); border: none;">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="donate" class="svg-inline--fa fa-donate fa-w-16 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="font-size: 1.4em;">
         <path fill="currentColor" d="M256 416c114.9 0 208-93.1 208-208S370.9 0 256 0 48 93.1 48 208s93.1 208 208 208zM233.8 97.4V80.6c0-9.2 7.4-16.6 16.6-16.6h11.1c9.2 0 16.6 7.4 16.6 16.6v17c15.5.8 30.5 6.1 43 15.4 5.6 4.1 6.2 12.3 1.2 17.1L306 145.6c-3.8 3.7-9.5 3.8-14 1-5.4-3.4-11.4-5.1-17.8-5.1h-38.9c-9 0-16.3 8.2-16.3 18.3 0 8.2 5 15.5 12.1 17.6l62.3 18.7c25.7 7.7 43.7 32.4 43.7 60.1 0 34-26.4 61.5-59.1 62.4v16.8c0 9.2-7.4 16.6-16.6 16.6h-11.1c-9.2 0-16.6-7.4-16.6-16.6v-17c-15.5-.8-30.5-6.1-43-15.4-5.6-4.1-6.2-12.3-1.2-17.1l16.3-15.5c3.8-3.7 9.5-3.8 14-1 5.4 3.4 11.4 5.1 17.8 5.1h38.9c9 0 16.3-8.2 16.3-18.3 0-8.2-5-15.5-12.1-17.6l-62.3-18.7c-25.7-7.7-43.7-32.4-43.7-60.1.1-34 26.4-61.5 59.1-62.4zM480 352h-32.5c-19.6 26-44.6 47.7-73 64h63.8c5.3 0 9.6 3.6 9.6 8v16c0 4.4-4.3 8-9.6 8H73.6c-5.3 0-9.6-3.6-9.6-8v-16c0-4.4 4.3-8 9.6-8h63.8c-28.4-16.3-53.3-38-73-64H32c-17.7 0-32 14.3-32 32v96c0 17.7 14.3 32 32 32h448c17.7 0 32-14.3 32-32v-96c0-17.7-14.3-32-32-32z"></path>
      </svg>
      <p class="ml-2">แจ้งเติมเครดิต</p>
   </div>
   <div>
      <div class="rsw_2Y">
         <div class="rsw_2f  rsw_3G">
            <div class="card">
               <div class="card-header">
                  <h4 class="mb-0"><span class="badge badge-pill badge-success">STEP 1</span> ตรวจสอบบัญชีและแจ้งเติมเครดิต</h4>
               </div>
               <div class="text-left pb-0 card-body">
                  <div class="justify-content-center mb-1 row">
                     <div class="col-lg-7 col-md-8 col-12">
                        <form class="" role="form" method="post" action="<?php echo base_url();?>deposit">
                           <div class="justify-content-center row">
                              <!-- <div class="mb-3 col-12">
                                 <div class="form-group">
                                    <select class="form-control" name="did_web">
                                       <option value="">เลือกพาทเนอร์</option>
                                       <?php foreach($sitelist as $k => $v):?>
                                          <?php if(isset($web)):?>
                                             <?php if($web == $v):?>
                                                <option value="<?php echo $v;?>" selected><?php echo $v;?></option>
                                             <?php else:?>
                                                <option value="<?php echo $v;?>"><?php echo $v;?></option>
                                             <?php endif;?>
                                          <?php else:?>
                                             <option value="<?php echo $v;?>"><?php echo $v;?></option>
                                          <?php endif;?>
                                       <?php endforeach;?>
                                    </select>
                                 </div>
                              </div> -->
                              <div class="mb-3 col-12">
                                 <div class="card-bank card" style="border-color: <?php echo $banklist_color[$user_bank];?>;">
                                    <div class="text-center card-header" style="background-color: <?php echo $banklist_color[$user_bank];?>;">บัญชีธนาคารของลูกค้า</div>
                                    <div class="sc-9f4hzg-1 jGZAsY">
                                       <div class="text-center card" style="display: grid; justify-items: center;">
                                          <div class="text-left p-2" style="width: fit-content;">
                                             <div class="d-flex">
                                                <img width="26px" height="26px" alt="img" src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $user_bank;?>.png">
                                                <h5 class="pl-2 pt-0 mb-1" style="color: <?php echo $banklist_color[$user_bank];?>;"><?php echo $user_bank_desc;?></h5>
                                             </div>
                                             <div>
                                                <p class="mb-0 text-secondary">ชื่อบัญชี: <?php echo $user_bank_name;?></p>
                                                <p class="mb-0 text-secondary">เลขที่บัญชี : <?php echo $user_bank_number;?></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="sc-9f4hzg-0 hJkCQZ">
                                       <div class="text-left card-body">
                                          <img width="100%" alt="bank" src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $user_bank;?>.png">
                                          <div class="pl-2 pr-0 px-md-3">
                                             <b style="color: <?php echo $banklist_color[$user_bank];?>;"><?php echo $user_bank_desc;?></b><p class="mb-0">ชื่อบัญชี: <?php echo $user_bank_name;?> </p><p>เลขที่บัญชี : <?php echo $user_bank_number;?></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="mb-1 col-12">
                                 <div role="alert" class="fade alert01 alert alert-danger show">
                                    <img src="<?php echo base_url();?>assets/images/warning.png" width="100%" alt="">
                                    <span>โอนเงินจากบัญชีที่สมัคร <br> <?php echo $website;?> เท่านั้น!!</span>
                                 </div>
                              </div>
                              <div class="col-12">
                                 <div class="form-group">
                                    <div class="input-group">
                                       <div class="input-group-prepend">
                                          <span class="BasicAddon input-group-text">
                                             <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="money-check-alt" class="svg-inline--fa fa-money-check-alt fa-w-20 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M608 32H32C14.33 32 0 46.33 0 64v384c0 17.67 14.33 32 32 32h576c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zM176 327.88V344c0 4.42-3.58 8-8 8h-16c-4.42 0-8-3.58-8-8v-16.29c-11.29-.58-22.27-4.52-31.37-11.35-3.9-2.93-4.1-8.77-.57-12.14l11.75-11.21c2.77-2.64 6.89-2.76 10.13-.73 3.87 2.42 8.26 3.72 12.82 3.72h28.11c6.5 0 11.8-5.92 11.8-13.19 0-5.95-3.61-11.19-8.77-12.73l-45-13.5c-18.59-5.58-31.58-23.42-31.58-43.39 0-24.52 19.05-44.44 42.67-45.07V152c0-4.42 3.58-8 8-8h16c4.42 0 8 3.58 8 8v16.29c11.29.58 22.27 4.51 31.37 11.35 3.9 2.93 4.1 8.77.57 12.14l-11.75 11.21c-2.77 2.64-6.89 2.76-10.13.73-3.87-2.43-8.26-3.72-12.82-3.72h-28.11c-6.5 0-11.8 5.92-11.8 13.19 0 5.95 3.61 11.19 8.77 12.73l45 13.5c18.59 5.58 31.58 23.42 31.58 43.39 0 24.53-19.05 44.44-42.67 45.07zM416 312c0 4.42-3.58 8-8 8H296c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h112c4.42 0 8 3.58 8 8v16zm160 0c0 4.42-3.58 8-8 8h-80c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h80c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H296c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h272c4.42 0 8 3.58 8 8v16z"></path>
                                             </svg>
                                          </span>
                                       </div>
                                       <input placeholder="ระบุจำนวนเงินที่ต้องการโอน" class="form-control form-control-lg" maxlength="8" type="number" name="did_amt"value="">
                                       <input type="hidden" name="fdid_step2" value="fdid_step2">
                                       <div class="text-center text-danger w-100 pt-2">
                                          <h5 class="mb-0" style="font-size: 1rem;">*** โอนขั้นต่ำ "ครั้งละ 20 บาท" ***</h5>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="mb-2 col-12">
                                 <hr>
                                 <button type="submit" class="btn btn-success btn-block btn-lg">เติมเงิน</button>
                              </div>
                              <div class="mb-0 pb-0 col-12">
                                 <button type="button" class="btn btn-danger btn-block btn-lg" onclick="goMenu('deposit');">ยกเลิก</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
       
<script type="text/javascript">
   $('#did_form').submit(function() { // check form
      if($('#userid').val() == ''){
         Swal.fire('Oops...', 'กรุณาเลือกเวป', 'error');
         return false;
      }else{
         return true; 
      }
   });

   function copy_text(TextToCopy) {
      var TempText = document.createElement("input");
      TempText.value = TextToCopy;
      document.body.appendChild(TempText);
      TempText.select();
     
      document.execCommand("copy");
      document.body.removeChild(TempText);
      Swal.fire('Done...', 'คัดลอกข้อความ '+TempText.value, 'success');
   }
   function show_table(id)
   {  
      var radio=document.getElementsByName("web").checked;
      if(id == "LSM")
      {   
         // var name = document.getElementById("LS").value;
         var name = $('#LS').val();
         //document.getElementById("userid").value = name;
         $('#userid').val(name);
         $('#webid').val('LSM');
         // document.getElementById('webid').value = 'LSM'
      }   
      if(id == "TS911")
      {   
         // var name = document.getElementById("TS").value;
         var name = $('#TS').val();
         // document.getElementById("userid").value = name;
         $('#userid').val(name);
         $('#webid').val('TS911');
         // document.getElementById('webid').value = 'TS911' 
      }  
      if(id == "IMI")
      {   
         // var name = document.getElementById("IMI").value;
         var name = $('#IMI').val();
         // document.getElementById("userid").value = name;
         $('#userid').val(name);
         $('#webid').val('IMI');
         // document.getElementById('webid').value = 'IMI'
      }                        
   }

   function showPreview(ele)
   {
      $('#imgAvatar').attr('src', ele.value); // for IE
      if (ele.files && ele.files[0]) {
      
         var reader = new FileReader();
         reader.onload = function (e) {
              $('#imgAvatar').attr('src', e.target.result);
         }
         reader.readAsDataURL(ele.files[0]);
      }
   }
</script>
          