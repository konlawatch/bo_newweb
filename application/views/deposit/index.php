<!-- 
<div class="text-left pb-0 card-body">
   <div class="justify-content-center mb-1 row">
      <div class="col-lg-7 col-md-8 col-12">
         <form class="">
            <div class="justify-content-center row">
               <div class="mb-3 col-12">
                  <div class="card-bank card" style="border-color: rgb(19, 143, 45);">
                     <div class="text-center card-header" style="background-color: rgb(19, 143, 45);">บัญชีธนาคารของลูกค้า</div>
                        <div class="sc-9f4hzg-1 jGZAsY">
                           <div class="text-center card" style="display: grid; justify-items: center;">
                           <div class="text-left p-2" style="width: fit-content;">
                                 <div class="d-flex">
                                 <img width="26px" height="26px" alt="img" src="https://ruayruay.s3.amazonaws.com/KBANK.png">
                                 <h5 class="pl-2 pt-0 mb-1" style="color: rgb(19, 143, 45);">ธนาคารกสิกรไทย</h5>
                              </div>
                              <div>
                                 <p class="mb-0 text-secondary">ชื่อบัญชี: ชาญชัย ปุกคำ</p>
                                 <p class="mb-0 text-secondary">เลขที่บัญชี : 0753789766</p>
                              </div>
                              </div>
                           </div>
                        </div>
                        <div class="sc-9f4hzg-0 hJkCQZ">
                        <div class="text-left card-body">
                           <img width="100%" alt="bank" src="https://ruayruay.s3.amazonaws.com/KBANK.png">
                           <div class="pl-2 pr-0 px-md-3"><b style="color: rgb(19, 143, 45);">ธนาคารกสิกรไทย</b>
                              <p class="mb-0">ชื่อบัญชี: ชาญชัย ปุกคำ </p><p>เลขที่บัญชี : 0753789766</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="mb-1 col-12">
                  <div role="alert" class="fade alert01 alert alert-danger show">
                     <img src="/images/warning.png" width="100%" alt="">
                     <span>โอนเงินจากบัญชีที่สมัคร <br> HUAYLIKE เท่านั้น!!</span>
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-group">
                     <div class="input-group">
                        <div class="input-group-prepend">
                           <span class="BasicAddon input-group-text">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="money-check-alt" class="svg-inline--fa fa-money-check-alt fa-w-20 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                           <path fill="currentColor" d="M608 32H32C14.33 32 0 46.33 0 64v384c0 17.67 14.33 32 32 32h576c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zM176 327.88V344c0 4.42-3.58 8-8 8h-16c-4.42 0-8-3.58-8-8v-16.29c-11.29-.58-22.27-4.52-31.37-11.35-3.9-2.93-4.1-8.77-.57-12.14l11.75-11.21c2.77-2.64 6.89-2.76 10.13-.73 3.87 2.42 8.26 3.72 12.82 3.72h28.11c6.5 0 11.8-5.92 11.8-13.19 0-5.95-3.61-11.19-8.77-12.73l-45-13.5c-18.59-5.58-31.58-23.42-31.58-43.39 0-24.52 19.05-44.44 42.67-45.07V152c0-4.42 3.58-8 8-8h16c4.42 0 8 3.58 8 8v16.29c11.29.58 22.27 4.51 31.37 11.35 3.9 2.93 4.1 8.77.57 12.14l-11.75 11.21c-2.77 2.64-6.89 2.76-10.13.73-3.87-2.43-8.26-3.72-12.82-3.72h-28.11c-6.5 0-11.8 5.92-11.8 13.19 0 5.95 3.61 11.19 8.77 12.73l45 13.5c18.59 5.58 31.58 23.42 31.58 43.39 0 24.53-19.05 44.44-42.67 45.07zM416 312c0 4.42-3.58 8-8 8H296c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h112c4.42 0 8 3.58 8 8v16zm160 0c0 4.42-3.58 8-8 8h-80c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h80c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H296c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h272c4.42 0 8 3.58 8 8v16z">

                           </path>
                           </svg>
                           </span>
                        </div>
                           <input inputmode="numeric" placeholder="ระบุจำนวนเงินที่ต้องการโอน" class="form-control form-control-lg" maxlength="8" type="tel" value="">
                        <div class="text-center text-danger w-100 pt-2">
                           <h5 class="mb-0" style="font-size: 1rem;">*** โอนขั้นต่ำ "ครั้งละ 20 บาท" ***</h5>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="mb-2 col-12"><hr>
               <button type="submit" class="btn btn-success btn-block btn-lg">เติมเงิน</button>
               </div>
               <div class="mb-0 pb-0 col-12">
               <button type="button" class="btn btn-danger btn-block btn-lg">ยกเลิก</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

 -->