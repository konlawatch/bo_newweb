 <?php $ss = $this->session->userdata();?>
    <?php
      $s=isset($data->cdate) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y');

       $d1 = explode('-', $s);
       $dd = $d1[2]."-".$d1[1]."-".$d1[0];

      ?>

<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>
    
               <div id="withdraw" class="mt-3 mb-3">
               <div class="dv90yj-3 ekmdOJ text-left d-flex bg-header" >    
                  <p class="ml-2">แจ้งสมัครสมาชิก IMI</p>
               </div>
               <div class="mb-3 row">
                  <div class="col">
                     <div class="card">
                     <div class="card-header bg-header">
                        <h5>บัญชีธนาคารระบบ</h5> 
                     </div>
                        <div class="card-body">
                           <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="deposit/deposit_reimi"> 
                              <input type="hidden" name="username" value="<?php echo $ss['data']['userid'];?>">
                              <input type="hidden" name="tobank" value=" <?php echo (isset($data['bank_depo']['but_name'])) ? $data['bank_depo']['but_name'].'-'.$data['bank_depo']['detail'] : ""?>">
                              <input type="hidden" name="userid" value="<?php echo $ss['data']['userid'];?>">
                              <input type="hidden" name="modeid" value="1">
                              <input type="hidden" class="form-control" name="userid" id="userid" value="<?php echo $ss['data']['userid'];?>"  readonly>
                              <input type="hidden" name="webid" id="webid" value="IMI">
                              <input type="hidden" class="form-control" name="name" id="name" placeholder="ชื่อ-สกุล" value="<?php echo $ss['data']['name'];?>"  readonly>
                              
                              <div class="justify-content-center row">
                                <div class="col-lg-6 col-md-8 col-12">
                                  <div  class="tab-content" style="text-align: center;">                                       
                                    <input type="hidden" name="t_bank" id="t_bank" value=" <?php echo (isset($data['bank_depo']['but_name'])) ? $data['bank_depo']['but_name'].'-'.$data['bank_depo']['detail'] : "ไม่มีข้อมูล"?>">
                                  <div>
                                    <img src="<?php echo base_url();?>assets/img/logo-bank/<?php echo (isset($data['bank_depo']['but_name'])) ?  $data['bank_depo']['but_name']  : "255x150"?>.png" style="width: 150px; height: 150px;">
                                    <h5 class=" textsilver noise"><?php echo (isset($data['bank_depo'])) ?  $data['bank_depo']['value']  : "ไม่มีข้อมูล"?></h5>                                             
                                    <h5 id="pwd_spn " class="password-span  textsilver noise" > <?php echo (isset($data['bank_depo']['detail'])) ? $data['bank_depo']['detail'] : "ไม่มีข้อมูล"?></h5>

                                  <!-- <?php echo (isset($data['bank_depo']['detail'])) ? $data['bank_depo']['detail'] : "ไม่มีข้อมูล"?> -->
                                  <div class="nk-gap"></div>
                                    <a href="#"  class="get-started-btn scrollto" onclick="copy_text('<?php echo (isset($data['bank_depo']['detail'])) ? $data['bank_depo']['detail'] : "ไม่มีข้อมูล"?>')">Copy</a>
                                    <hr class="x-hr-border-glow">
                                  </div>
                                  </div>
                                </div>
                              </div>
                                <hr>เลือกเวบที่ต้องการฝาก
                              <div class="justify-content-center mt-3 row">
                                 <img src="assets/images/client-2222.png" alt=""style="height: 100px; max-width: 100%;">
                              </div>
                              <div class="justify-content-center mt-3 row">
                              <div class="col-md-8 col-12">
                                 <div class="form-group">
                                    <label class=" mb-0 ">จำนวนเงินที่ฝาก</label>
                                    <input type="text" class="form-control" name="amount" id="amount" placeholder="ยอดฝาก" value="" onkeyup="tttt();" style="border-radius: 10px !important;"><br>        
                                 </div>
                                 <div class="form-group">   
                                    <input type="hidden" class="form-control" name="frombank" id="frombank"  value="<?php echo $ss['data']['bank1'];?>"   style="border-radius: 10px !important;">
                                 </div>
                                 <div class="form-group">   
                                    <label class=" mb-0 center ">วันเวลาที่โอน<span class="text-danger">*</span></label> 
                                    <input class="form-control " name="cdate" id="cdate" type="date" value="<?php echo $dd;?>" style="border-radius: 10px !important;">
                                 </div>
                                 <div class="form-group">
                                    <div class="form-row">
                                       <div class="col">
                                          <label class=" mb-0 center ">ชั่วโมง</label>
                                          <div class="deposit_date">
                                             <select class="form-control" name="depositHour" id="depositHour" style="border-radius: 10px !important;">
                                                <option value="">ชั่วโมง</option>
                                                   <?php for($i = 0;$i <= 23; $i++):?>
                                                      <option value="<?php echo sprintf('%02d',$i);?>" <?php echo (date('H') == $i) ? 'selected' : ''; ?>><?php echo sprintf('%02d',$i);?></option>
                                                   <?php endfor;?>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col">
                                          <label class=" mb-0 center  "> นาที </label>
                                          <div class="deposit_date">
                                             <select class="form-control" name="depositMin" id="depositMin" style="border-radius: 10px !important;">
                                                <option value="">นาที</option>
                                                   <?php for($i = 0;$i <= 59; $i++):?>
                                                      <option value="<?php echo sprintf('%02d',$i);?>" <?php echo (date('i') == $i) ? 'selected' : ''; ?>><?php echo sprintf('%02d',$i);?></option>
                                                   <?php endfor;?>                                                                   
                                             </select>
                                          </div> 
                                       </div>
                                    </div>
                                 </div>
                               <!--   <div class="form-group">
                                    <label class=" mb-0 center  ">สลิปโอนเงิน</label>
                                    <hr class="x-hr-border-glow">
                                    <div class="input-file-container col-md-0" style="width: 60%; left: 20%;">                        
                                       <input class="input-file" id="deposit_img" name="deposit_img" accept="image/*"  type="file">
                                       <label tabindex="0" for="deposit_img" class="input-file-trigger" style="">เลือกรูป...</label>   
                                       <p class="file-return" style="color: #525454 !important;"></p>
                                    </div>
                                 </div> -->
                                 <div class="form-group">
                                    <label class=" mb-0 center  ">โบนัส</label>
                                    <hr class="x-hr-border-glow"><br>                                                     
                                    <div class="was-validated">  
                                       <div class="form-row">
                                          <div class="col">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="bcode1" name="bcode" value='N' checked="">
                                                <label class="custom-control-label" for="bcode1" >ไม่รับโบนัส</label>
                                             </div>
                                          </div>
                                          <div class="col">
                                             <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="bcode2" name="bcode" value='Y' >
                                                <label class="custom-control-label" for="bcode2" >รับโบนัส</label><br>                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <hr>     
                              </div>
                              <div class="text-left col-md-8 col-12">
                                 <p>* ถอนขั้นต่ำ 100 บาท</p><p>* ถอนสูงสุดต่อครั้ง 1,000,000 บาท</p><p>* ถอนสูงสุดต่อวัน 10,000,000 บาท</p>
                              </div>
                              <div class="mb-2 col-md-8 col-12">
                                 <hr>
                                 <button type="submit" class="btn btn-success btn-block btn-lg">ฝาก</button>
                              </div>
                              <div class="col-md-8 col-12">
                                 <a href="<?php base_url();?>main"><button type="button" class="btn btn-danger btn-block btn-lg">ยกเลิก</button></a>
                              </div>
                           </div>
                        </form>
                        </div>
                     </div>
                  </div>
               </div>
       
            <script>
             $('#did_form').submit(function() { // check form
                if($('#userid').val() == ''){
                   Swal.fire('Oops...', 'กรุณาเลือกเวป', 'error');
                   return false;
                }else{
                   return true; 
                }
             });

             function copy_text(TextToCopy) {
                var TempText = document.createElement("input");
                TempText.value = TextToCopy;
                document.body.appendChild(TempText);
                TempText.select();
               
                document.execCommand("copy");
                document.body.removeChild(TempText);
                Swal.fire('Done...', 'คัดลอกข้อความ '+TempText.value, 'success');
             }
             
             
              function showPreview(ele)
             {
                $('#imgAvatar').attr('src', ele.value); // for IE
                if (ele.files && ele.files[0]) {
                
                   var reader = new FileReader();
                   reader.onload = function (e) {
                        $('#imgAvatar').attr('src', e.target.result);
                   }
                   reader.readAsDataURL(ele.files[0]);
                }
             }
            </script>
          