<?php 
   $ss = $this->session->userdata();
   // debug($ss);
   $s  = isset($data->cdate) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y');
   $d1 = explode('-', $s);
   $dd = $d1[2]."-".$d1[1]."-".$d1[0];

   $sitelist = $this->config->config['sitelist'];
   $banklist = $this->config->config['banklist'];
   $banklist_color = $this->config->config['banklist_color'];

   $ubank = explode('-', $ss['data']['bank']);
   $user_bank_name   = $ss['data']['name'];
   $user_bank        = $ubank[0];
   $user_bank_desc   = $banklist[$ubank[0]];
   $user_bank_number = $ubank[1];
   // $user_bank = 'CIMB';
   // debug($banklist,true); 
   $sbank = explode('-',$bank);

   $sys_bank_name   = $sbank[2];
   $sys_bank        = $sbank[0];
   $sys_bank_desc   = $banklist[$sbank[0]];
   $sys_bank_number = $sbank[1];
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.css">
<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>
<style type="text/css">
   .btn-dark {
      background-color: #3a9a82;
   }
</style>
<style type="text/css">
   .bootstrap-datetimepicker-widget.dropdown-menu {
     border: 1px solid #34495e;
     border-radius: 0;
     box-shadow: none;
     margin: 10px 0 0 0;
     padding: 0;
     min-width: 300px;
     max-width: 100%;
     width: auto;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu.bottom:before, .bootstrap-datetimepicker-widget.dropdown-menu.bottom:after {
     display: none;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td,
   .bootstrap-datetimepicker-widget.dropdown-menu table th {
     border-radius: 0;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td.old,
   .bootstrap-datetimepicker-widget.dropdown-menu table td.new {
     color: #bbb;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td.today:before {
     border-bottom-color: #0095ff;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td.active,
   .bootstrap-datetimepicker-widget.dropdown-menu table td.active:hover,
   .bootstrap-datetimepicker-widget.dropdown-menu table td span.active {
     background-color: #0095ff;
     text-shadow: none;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td.active.today:before,
   .bootstrap-datetimepicker-widget.dropdown-menu table td.active:hover.today:before,
   .bootstrap-datetimepicker-widget.dropdown-menu table td span.active.today:before {
     border-bottom-color: #fff;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table th {
     height: 40px;
     padding: 0;
     width: 40px;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table th.picker-switch {
     width: auto;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table tr:first-of-type th {
     border-bottom: 1px solid #34495e;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td.day {
     height: 32px;
     line-height: 32px;
     padding: 0;
     width: auto;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu table td span {
     border-radius: 0;
     height: 77px;
     line-height: 77px;
     margin: 0;
     width: 25%;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .datepicker-months tbody tr td,
   .bootstrap-datetimepicker-widget.dropdown-menu .datepicker-years tbody tr td,
   .bootstrap-datetimepicker-widget.dropdown-menu .datepicker-decades tbody tr td {
     padding: 0;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .datepicker-decades tbody tr td {
     height: 27px;
     line-height: 27px;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .datepicker-decades tbody tr td span {
     display: block;
     float: left;
     width: 50%;
     height: 46px;
     line-height: 46px !important;
     padding: 0;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .datepicker-decades tbody tr td span:not(.decade) {
     display: none;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td {
     padding: 0;
     width: 30%;
     height: 20px;
     line-height: 20px;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td:nth-child(2) {
     width: 10%;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td a,
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td span,
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td button {
     border: none;
     border-radius: 0;
     height: 56px;
     line-height: 56px;
     padding: 0;
     width: 100%;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td span {
     color: #333;
     margin-top: -1px;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td button {
     background-color: #fff;
     color: #333;
     font-weight: bold;
     font-size: 1.2em;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .timepicker-picker table td button:hover {
     background-color: #eee;
   }

   .bootstrap-datetimepicker-widget.dropdown-menu .picker-switch table td {
     border-top: 1px solid #34495e;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .picker-switch table td a,
   .bootstrap-datetimepicker-widget.dropdown-menu .picker-switch table td span {
     display: block;
     height: 40px;
     line-height: 40px;
     padding: 0;
     width: 100%;
   }

   .todayText:before {
     content: "Today's Date";
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .next {
     position: inherit;
     color: #000;
   }
   .bootstrap-datetimepicker-widget.dropdown-menu .prev {
     position: inherit;
     color: #000;
   }
   
   .file {
      visibility: hidden;
      position: absolute;
   }
</style>
<div id="deposit" class="mt-3 mb-3">
   <div class="dv90yj-3 ekmdOJ text-left d-flex" style="background-color: rgb(0, 191, 161); border: none;">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="donate" class="svg-inline--fa fa-donate fa-w-16 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="font-size: 1.4em;">
         <path fill="currentColor" d="M256 416c114.9 0 208-93.1 208-208S370.9 0 256 0 48 93.1 48 208s93.1 208 208 208zM233.8 97.4V80.6c0-9.2 7.4-16.6 16.6-16.6h11.1c9.2 0 16.6 7.4 16.6 16.6v17c15.5.8 30.5 6.1 43 15.4 5.6 4.1 6.2 12.3 1.2 17.1L306 145.6c-3.8 3.7-9.5 3.8-14 1-5.4-3.4-11.4-5.1-17.8-5.1h-38.9c-9 0-16.3 8.2-16.3 18.3 0 8.2 5 15.5 12.1 17.6l62.3 18.7c25.7 7.7 43.7 32.4 43.7 60.1 0 34-26.4 61.5-59.1 62.4v16.8c0 9.2-7.4 16.6-16.6 16.6h-11.1c-9.2 0-16.6-7.4-16.6-16.6v-17c-15.5-.8-30.5-6.1-43-15.4-5.6-4.1-6.2-12.3-1.2-17.1l16.3-15.5c3.8-3.7 9.5-3.8 14-1 5.4 3.4 11.4 5.1 17.8 5.1h38.9c9 0 16.3-8.2 16.3-18.3 0-8.2-5-15.5-12.1-17.6l-62.3-18.7c-25.7-7.7-43.7-32.4-43.7-60.1.1-34 26.4-61.5 59.1-62.4zM480 352h-32.5c-19.6 26-44.6 47.7-73 64h63.8c5.3 0 9.6 3.6 9.6 8v16c0 4.4-4.3 8-9.6 8H73.6c-5.3 0-9.6-3.6-9.6-8v-16c0-4.4 4.3-8 9.6-8h63.8c-28.4-16.3-53.3-38-73-64H32c-17.7 0-32 14.3-32 32v96c0 17.7 14.3 32 32 32h448c17.7 0 32-14.3 32-32v-96c0-17.7-14.3-32-32-32z"></path>
      </svg>
      <p class="ml-2">กรุณาฝากเงินเพื่อเปิดรหัสพาทเนอร์ </p>
      <!-- <select name="cars" id="cars" class="border-0 bg-transparent mr-2" style="color: #fe0202;">
         <option value="">กรุณาเลือกเกมส์</option>
         <?php foreach($sitelist as $k => $v):?>
            <?php if($web == $v):?>
               <option value="<?php echo $v;?>" selected><?php echo $v;?></option>
            <?php else:?>
               <option value="<?php echo $v;?>"><?php echo $v;?></option>
            <?php endif;?>
         <?php endforeach;?>
      </select> -->
   </div>
   <div>
      <!-- <div class="hide_menu_game card">
         <div class="mt-2 d-flex justify-content-between">
            <i class="fas fa-w-14 fa-dice ml-2" style="font-size: 1.4em;"></i>
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="font-size: 1.6rem; cursor: pointer;"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
            <select name="cars" id="cars" class="border-0 bg-transparent mr-2">
               <option value="null">กรุณาเลือกเกมส์</option>
               <?php foreach($sitelist as $k => $v):?>
                  <?php if($web == $v):?>
                     <option value="<?php echo $v;?>" selected><?php echo $v;?></option>
                  <?php else:?>
                     <option value="<?php echo $v;?>"><?php echo $v;?></option>
                  <?php endif;?>
               <?php endforeach;?>
            </select>
         </div>
      </div> -->
      <div class="rsw_2Y d-flex">
         <div class="rsw_2f  rsw_3G">
            <div class="card">
               <div class="text-left pb-0 card-body">
                  <div class="justify-content-center mb-1 row">
                     <div class="col-lg-7 col-md-8 col-12">
                        <form  id="form-rdeposit"  enctype="multipart/form-data"  role="form" method="post" action="<?php echo base_url();?>service/reg_deposit">
                           <div class="justify-content-center row">
                              <!-- <div class="mb-3 col-12">
                                 <div class="form-group">
                                    <select class="form-control" name="did_web">
                                       <option value="">เลือกพาทเนอร์</option>
                                       <?php foreach($sitelist as $k => $v):?>
                                          <?php if(isset($web)):?>
                                             <?php if($web == $v):?>
                                                <option value="<?php echo $v;?>" selected><?php echo $v;?></option>
                                             <?php else:?>
                                                <option value="<?php echo $v;?>"><?php echo $v;?></option>
                                             <?php endif;?>
                                          <?php else:?>
                                             <option value="<?php echo $v;?>"><?php echo $v;?></option>
                                          <?php endif;?>
                                       <?php endforeach;?>
                                    </select>
                                 </div>
                              </div> -->
                              <div class="col-12">
                                 <div class="form-group">
                                    <div class="input-group">
                                       <div class="input-group-prepend">
                                          <span class="BasicAddon input-group-text">
                                             <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="money-check-alt" class="svg-inline--fa fa-money-check-alt fa-w-20 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M608 32H32C14.33 32 0 46.33 0 64v384c0 17.67 14.33 32 32 32h576c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zM176 327.88V344c0 4.42-3.58 8-8 8h-16c-4.42 0-8-3.58-8-8v-16.29c-11.29-.58-22.27-4.52-31.37-11.35-3.9-2.93-4.1-8.77-.57-12.14l11.75-11.21c2.77-2.64 6.89-2.76 10.13-.73 3.87 2.42 8.26 3.72 12.82 3.72h28.11c6.5 0 11.8-5.92 11.8-13.19 0-5.95-3.61-11.19-8.77-12.73l-45-13.5c-18.59-5.58-31.58-23.42-31.58-43.39 0-24.52 19.05-44.44 42.67-45.07V152c0-4.42 3.58-8 8-8h16c4.42 0 8 3.58 8 8v16.29c11.29.58 22.27 4.51 31.37 11.35 3.9 2.93 4.1 8.77.57 12.14l-11.75 11.21c-2.77 2.64-6.89 2.76-10.13.73-3.87-2.43-8.26-3.72-12.82-3.72h-28.11c-6.5 0-11.8 5.92-11.8 13.19 0 5.95 3.61 11.19 8.77 12.73l45 13.5c18.59 5.58 31.58 23.42 31.58 43.39 0 24.53-19.05 44.44-42.67 45.07zM416 312c0 4.42-3.58 8-8 8H296c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h112c4.42 0 8 3.58 8 8v16zm160 0c0 4.42-3.58 8-8 8h-80c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h80c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H296c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h272c4.42 0 8 3.58 8 8v16z"></path>
                                             </svg>
                                          </span>
                                       </div>
                                       <input placeholder="ระบุจำนวนเงินที่ต้องการโอน" class="form-control form-control-lg" maxlength="8" type="number" id="did_amt" name="did_amt" value="">
                                       <input type="hidden" id="did_web" name="did_web" value="<?php echo (isset($web)) ? $web : '';?>">
                                       <div class="text-center text-danger w-100 pt-2">
                                          <h5 class="mb-0" style="font-size: 1rem;">*** โอนขั้นต่ำ "ครั้งละ 20 บาท" ***</h5>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="mb-3 col-12">
                                 <div class="card-bank card" style="border-color: <?php echo $banklist_color[$sys_bank];?>;">
                                    <div class="text-center card-header" style="background-color: <?php echo $banklist_color[$sys_bank];?>;">รายละเอียดการโอน</div>
                                    <div class="sc-9f4hzg-1 jGZAsY">
                                       <div class="text-center card" style="display: grid; justify-items: center;">
                                          <div class="text-left p-2" style="width: fit-content;">
                                             <div class="d-flex">
                                                <img width="26px" height="26px" alt="img" src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $sys_bank;?>.png">
                                                <h5 class="pl-2 pt-0 mb-1" style="color: <?php echo $banklist_color[$sys_bank];?>;"><?php echo $sys_bank_desc;?></h5>
                                             </div>
                                             <div>
                                                <p class="mb-0 text-secondary">ชื่อบัญชี : <?php echo $sys_bank_name;?></p>
                                                <p class="mb-0 text-secondary">เลขที่บัญชี : <?php echo $sys_bank_number;?></p>
                                                <input type="hidden" id="did_bank" name="did_bank" value="<?php echo $sys_bank.'-'.$sys_bank_number;?>">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="sc-9f4hzg-0 hJkCQZ">
                                       <div class="text-left card-body">
                                          <img width="100%" alt="" src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $sys_bank;?>.png">
                                          <div class="px-2 px-md-3">
                                             <b style="color: <?php echo $banklist_color[$sys_bank];?>;"><?php echo $sys_bank_desc;?></b>
                                             <p class="mb-0">ชื่อบัญชี: <?php echo $sys_bank_name;?></p><p>เลขที่บัญชี : <?php echo $sys_bank_number;?></p>
                                          </div>
                                       </div>
                                    </div>
                                    <button type="button" class="btn btn-light" style="border-top: 1px solid <?php echo $banklist_color[$sys_bank];?>;" onclick="copy_text('<?php echo $sys_bank_number;?>');">
                                       <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="copy" class="svg-inline--fa fa-copy fa-w-14 fa-lg " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M320 448v40c0 13.255-10.745 24-24 24H24c-13.255 0-24-10.745-24-24V120c0-13.255 10.745-24 24-24h72v296c0 30.879 25.121 56 56 56h168zm0-344V0H152c-13.255 0-24 10.745-24 24v368c0 13.255 10.745 24 24 24h272c13.255 0 24-10.745 24-24V128H344c-13.2 0-24-10.8-24-24zm120.971-31.029L375.029 7.029A24 24 0 0 0 358.059 0H352v96h96v-6.059a24 24 0 0 0-7.029-16.97z"></path></svg> COPY
                                    </button>
                                 </div>
                              </div>
                              <div class="mb-3 col-12">
                                 <div class="form-group">
                                    <label class="mt-2"><i class="fas fa-calendar-alt"></i> วันที่โอน</label>
                                    <div class="input-group date" id="datepicker">
                                       <input class="form-control" name="did_date" id="did_date" placeholder="MM-DD-YYYY" value="<?php echo date('d-m-Y');?>" />
                                       <div class="input-group-append input-group-addon" data-target="#datepicker" >
                                          <button class="btn btn-outline-secondary" type="button"><i class="fas fa-calendar my-0"></i></button>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="mt-2"><i class="far fa-clock"></i> เวลาที่โอน </label>
                                    <div class="input-group time" id="timepicker">
                                       <input class="form-control" name="did_time" id="did_time" placeholder="HH:MM" value="<?php echo date('H:i');?>" />
                                       <div class="input-group-append input-group-addon" data-target="#timepicker">
                                          <button class="btn btn-outline-secondary" type="button"><i class="fas fa-clock"></i></button>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="mt-2"><i class="far fa-clock"></i> สลิปโอนเงิน </label>
                                    <input type="file" name="img" class="file" accept="image/*" id="fi">
                                    <div class="input-group">
                                       <input type="text" class="form-control" disabled placeholder="อัพโหลดสลิปโอนเงิน..." id="file">
                                       <div class="input-group-append">
                                          <button type="button" class="btn btn-primary" onclick="browse_file();">เลือก...</button>
                                       </div>
                                    </div>
                                    <img src="" id="preview" class="img-thumbnail">
                                 </div>
                              </div>
                              <div class="mb-3  col-md-6 col-12 order-md-1 order-2">
                                 <button type="reset" class="btn btn-danger btn-block btn-lg" onclick="clear_form();">ยกเลิกรายการ</button>
                              </div>
                              <div class="mb-3 col-md-6 col-12 order-md-2 order-1">
                                 <button type="button" class="btn btn-success btn-block btn-lg btn-submit" onclick="did_regdeposit();">ยืนยัน</button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="<?php echo base_url();?>assets/js/moment.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
   function copy_text(TextToCopy) {
      var TempText = document.createElement("input");
      TempText.value = TextToCopy;
      document.body.appendChild(TempText);
      TempText.select();
     
      document.execCommand("copy");
      document.body.removeChild(TempText);
      Swal.fire('Done...', 'คัดลอกข้อความ '+TempText.value, 'success');
   }

   function browse_file(){
      $('.file').click();
   }

   function clear_form(){
      $('#preview').attr('src','');
   }


   // $(document).on("click", "#browse", function() {
   //    var file = $(this).parents().find(".file");
   //    file.trigger("click");
   // });

   $('input[type="file"]').change(function(e) {
      var fileName = e.target.files[0].name;
      $("#file").val(fileName);

      var reader = new FileReader();
      reader.onload = function(e) {
         // get loaded data and render thumbnail.
         // document.getElementById("preview").src = e.target.result;
         $('#preview').attr('src',e.target.result);
      };
      // read the image file as a data URL.
      reader.readAsDataURL(this.files[0]);
   });

   if (/Mobi/.test(navigator.userAgent)) {
      // if mobile device, use native pickers
      $(".date input").attr("type", "date");
      $(".time input").attr("type", "time");
   } else {
      // if desktop device, use DateTimePicker
      $("#datepicker").datetimepicker({
         useCurrent: false,
         format: "L",
         showTodayButton: false,
         format: 'DD-MM-YYYY',
         icons: {
            next: "fa fa-chevron-right",
            previous: "fa fa-chevron-left",
            // today: 'todayText',
         }
      });
      $("#timepicker").datetimepicker({
         format: "T",
         format: 'HH:mm',
         icons: {
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down"
         }
      });
   }

   function did_regdeposit(e){
      $('.btn-submit').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
      $('.btn-submit').prop('disabled', true);

      if($('#did_amt').val() == ''){
         Swal.fire('Oops...', 'กรุณาป้อนจำนวนเงิน', 'error');
         $('.btn-submit').html('ยืนยัน');
         $('.btn-submit').prop('disabled', false); 
         // $('#did_amt').addClass('is-invalid').focus();
         return;
      }

      var fd = new FormData();
      var files = $('#fi')[0].files;
      // if(files.length > 0 ){
         fd.append('file',files[0]);
         fd.append('did_amt',$('#did_amt').val());
         fd.append('did_web',$('#did_web').val());
         fd.append('did_date',$('#did_date').val());
         fd.append('did_time',$('#did_time').val());
         fd.append('did_bank',$('#did_bank').val());
         $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>service/reg_deposit',
            // data: $('#form-rdeposit').serialize(),
            dataType: "json",
            // enctype: 'multipart/form-data',
            data: fd,
            contentType: false,
            processData: false,
            success: function(data, dataType, state){
               if(data.status){
                  $('.btn-submit').html('ยืนยัน');
                  Swal.fire('Done...', data.msg, 'success').then(function(){
                     window.location.assign("<?php echo base_url();?>statement");
                  });
                  
               }else {
                  Swal.fire('Opps...', data.msg, 'error');
                  // $('#reg_username').removeClass('is-valid');
                  // $('#reg_username').addClass('is-invalid');
                  $('.btn-submit').html('ยืนยัน');
                  $('.btn-submit').prop('disabled', false);          
               }
            }
         });
      // }else{
      //    Swal.fire('Oops...', 'กรุณาเลือกไฟล์', 'error');
      //    $('.btn-submit').html('ยืนยัน');
      //    $('.btn-submit').prop('disabled', false);     
      // }
   }
</script>
          