<?php $lists = $this->session->userdata();?>
<!-- <?php $xx = $this->session->userdata();?> -->
<?php //debug ($lists,true);?>
<div class="row">
         <div class="col-md-12">
            <div class="box box-primary">
               <div class="table-responsive">
                  <table class="table table-bordered text-left table-hover" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th class="text-center">ลำดับที่</th>
                           <th class="text-center">id</th>
                           <th class="text-center">keyname</th>
                           <th class="text-center">value</th>
                           <th class="text-center">status</th>
                           <th class="text-center">edit</th>
                          
                        </tr>
                     </thead>
                     <tbody> 
                       <?php  
                       $i = 1;
                       ?>

                        <?php if (isset($lists['lists']['data'])!=''): ?>
                           <?php foreach ($lists['lists']['data'] as $item): ?>
                                 <tr class="">
                                    <td class="text-center"><?php echo $i;?></td>
                                    <td class="text-center"><?php echo $item['id']; ?></td>
                                    <td class="text-center"><?php echo $item['web']; ?></td>
                                    <td class="text-center"><?php echo $item['userid']; ?></td>
                                    <td class="text-center"><?php echo $item['amount']; ?></td>
                                    <td class="text-center">
                                      <div class="hidden-sm hidden-xs btn-group">
                                        <button class="btn btn-xs btn-info">
                                          <i class="ace-icon fa fa-refresh bigger-200"></i>
                                          update_status
                                        </button>

                                      
                                    </div>
                                  </td>
                                    
                              </tr>
                              <?php $i++;?>
                           <?php endforeach;?>
                           <?php else: ?>
                              <tr>
                                 <td  colspan="21" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                              </tr>
                        <?php endif;?>
                     </tbody>
                  
                  </table>
               </div>
            </div>
         </div>
      </div>