<?php $ss = $this->session->userdata();?>
<?php //debug($ss,true);?>

<?php $lists = $this->session->userdata();?>

         <div class="sc-5g65yh-0 dxeklP">
            <div class=" d-flex justify-content-between pl-2 pr-2">
               <h1 style="font-size:1rem">ID : <?php echo $ss['data']['userid'];?></h1>
            </div>
               <div class="d-flex text-center justify-content-center pt-2"></div>
               <div class="d-flex justify-content-center">
                  <button class="sc-5g65yh-2 eYxMlR full-width" style="background-color:#00bfa1;border:none" onclick="goMenu('deposit');">
                     <div style="background-color:#fff" class="sc-5g65yh-1 jnuzDq">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="color:#00c19e">
                           <path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z">
                           </path>
                        </svg>
                     </div>
                     <span class="ml-2">เติมเงิน</span>
                  </button>
                  <button class="sc-5g65yh-2 eYxMlR full-width" style="background-color:#fe0000" onclick="goMenu('withdraw');">
                     <div style="background-color:#fff" class="sc-5g65yh-1 jnuzDq">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="minus" class="svg-inline--fa fa-minus fa-w-14 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="color:#fe0000">
                           <path fill="currentColor" d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z">
                           </path>
                        </svg>
                     </div>
                  <span class="ml-2">ถอนเงิน</span>
                  </button>
               </div>
         </div>
         <div class="dv90yj-3 ekmdOJ text-center mb-3" style="background-color: rgb(0, 191, 161); border: none;"><span>ประวัติการฝาก</span></div>
        <!--  <div class="row no-gutters" style="display: flex;justify-content: center;">
            <div class="mb-2 px-lg-1 col-lg-4 col-12">
               <button type="button" id="TS911" class="px-0 btn  btn-dark btn-block btn-lg">TS911</button>
            </div>
            <div class="mb-2 px-lg-1 col-lg-4 col-12">
               <button type="button" class="px-0  btn  btn-dark btn-block btn-lg">IMI</button>
            </div>
            <div class="mb-2 px-lg-1 col-lg-4 col-12">
               <button type="button" class="px-0  btn  btn-dark btn-block btn-lg">LSM</button>
            </div>
         </div> -->
         
         <div id="details">
            <div class="animate__animated animate__fadeIn">
               <div class="hide_menu_game card">
                  <div class="mt-2 d-flex justify-content-between">
                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="font-size: 1.6rem; cursor: pointer;">
                     <path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z">
                     </path>
                     </svg>
                      <p class=" textsilver noise">ID : <?php echo (isset($lists['lists']['data']['0']['userid'])? $lists['lists']['data']['0']['userid'] : 'ไม่มีข้อมูล' )?></p>
                  </div>
               </div>
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="deposit/deposit_lists_deposit"> 
               <input type="hidden" name="username" value="<?php echo $lists['data']['userid']?>">
                  <div class="was-validated">
                     <table style="width:100%" >
                        <tr>
                           <th class="text-center">
                              <?php if($ss['data']['user_ts'] != ''):?>
                              <?php if($ss['data']['user_ts'] == 'pending'):?>  
                              <?php else:?>
                                 <div class="custom-control custom-radio" id="ts_g" >
                                    <input type="radio" class="custom-control-input" id="web_ts1" name="webname" onClick="show_table(this.value);" value="TS911" >
                                    <label class="custom-control-label" for="web_ts1" ><img src="assets/images/client-11.png" alt=""style="height: 50px; max-width: 100%;"><hr class="x-hr-border-glow"></label>
                                 </div>
                              <?php endif;?>
                              <?php else:?>
                              <?php endif;?>
                           </th> 
                           <th class="text-center">
                              <?php if($ss['data']['user_lsm'] != ''):?>
                              <?php if($ss['data']['user_lsm'] == 'pending'):?>  
                              <?php else:?>
                                 <div class="custom-control custom-radio" id="ls_g">
                                    <input type="radio" class="custom-control-input" id="web_ls1" name="webname" onClick="show_table(this.value);" value="LSM" >
                                    <label class="custom-control-label" for="web_ls1" ><img src="assets/images/client-33.png" alt=""style="height: 50px; max-width: 100%;"><hr class="x-hr-border-glow"></label>      
                                 </div> 
                              <?php endif;?> 
                              <?php else:?>
                              <?php endif;?>

                           </th>
                           <th class="text-center"> 
                              <?php if($ss['data']['user_imi'] != ''):?>
                              <?php if($ss['data']['user_imi'] == 'pending'):?>  
                              <?php else:?>
                                 <div class="custom-control custom-radio" id="imi_g" >
                                    <input type="radio" class="custom-control-input" id="web_imi" name="webname" onClick="show_table(this.value);" value="IMI" >
                                    <label class="custom-control-label" for="web_imi" ><div style="position: relative;"><div id="img_imi"><img src="assets/images/client-222.png"  alt=""style="height: 50px; max-width: 100%;"></div></div><hr class="x-hr-border-glow"></label>
                                 </div>
                              <?php endif;?> 
                              <?php else:?>
                              <?php endif;?>
                           </th>
                        </tr>
                     </table>
                  </div>
                  <div class="center"> 
                     <button type="submit" class="custom-btn btn-12"   style="border-radius: 10px; width: 30%;place-content: center;">ค้นหา</button>
                     <hr class="x-hr-border-glow"> 
                  </div>
            </form>
            <div class="card" style="width: 100%;">
               <div role="tabpanel" class="tab-pane fade active show" id="tabs-1-1">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="box box-primary">
                           <div class="table-responsive">
                              <table class="table table-bordered text-left table-hover" id="dataTable" width="100%" cellspacing="0" style="color: #fff;">
                                 <tbody>  
                                    <?php  
                                    $i = 1;
                                    ?>
                                    <?php if (isset($lists['lists']['data'])!=''): ?>
                                    <?php foreach ($lists['lists']['data'] as $item): ?>
                                    <tr class="">
                                       <!-- <td class="text-center"><?php echo $i;?></td> -->
                                       <td>
                                          <div class="animate__animated animate__fadeIn">
                                             <div class="list-statement container">
                                                <div class="row">
                                                   <div class="col-img align-items-center pr-0 col-8">
                                                      <?php switch($item['web']): 
                                                            case "IMI": ?>
                                                          <img src="assets/images/client-222.png"  alt=""style="height: 30px; width: auto;">
                                                      <?php break; ?>
                                                      <?php case "TS911": ?>
                                                         <img src="assets/images/client-11.png"  alt=""style="height: 30px; width: auto;">
                                                      <?php break; ?>
                                                      <?php case "LSM": ?>
                                                          <img src="assets/images/client-33.png"  alt=""style="height: 30px; width: auto;">
                                                      <?php break; ?>
                                                      <?php endswitch; ?>
                                                      <span>เติมเครดิต อัตโนมัติ <?php echo $item['web']; ?></span>
                                                   </div>
                                                   <div class="text-center py-2 px-0 col-4" style="background: green; color: white;">
                                                      <span>สถานะ <?php echo 'ฝาก'; ?></span> <br>
                                                      <span>
                                                      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check-circle" class="svg-inline--fa fa-check-circle fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                      <path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z">

                                                      </path>
                                                      </svg> สำเร็จ</span>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="text-center text-md-left my-1 col-md-6 col-12">
                                                      <span>
                                                      <span class="badge badge-success">
                                                      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="folder-plus" class="svg-inline--fa fa-folder-plus fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                      <path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z">
                                                      </path>
                                                      </svg> เติมเครดิต</span> :  <?php echo $item['amount']; ?> บาท.</span>
                                                      </div>
                                                      <div class="text-center text-md-right my-1 col-md-6 col-12">
                                                      <span><?php echo $item['date']; ?></span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </td>

                                    </tr>
                                       <?php $i++;?>
                                       <?php endforeach;?>
                                       <?php else: ?>
                                    <tr>
                                       <td  colspan="21" class="text-danger text-center"> ไม่มีข้อมูล. </td>
                                    </tr>
                                    <?php endif;?>  
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="nk-gap"></div>
               </div> 
            </div>
         </div>
      </div>
      