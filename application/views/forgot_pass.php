<div class="welcome">
   <img src="<?php echo base_url();?>assets/images/rat.png" alt="">
</div>
<section id="login" style="display: block;
justify-content: center;
align-items: center;
/*margin-top: 10px;*/
margin-bottom: 100px;">
   <div class="container">
      <form id="form-forget" class="form-horizontal text-center" autocomplete="off"  method="post" action="<?php echo base_url();?>submit_fg">
      <h1>รีเซ็ตรหัสผ่าน</h1>
         <div class="alert" role="alert" style="display: none;" id="form_alert">
            <i class="material-icons list-icon" id="alert_icon"></i>
            <span id="alert_text" style="font-weight:bold;"></span>
            <input type="hidden" class="form-control" name="chkuser" id="chkuser" value="">
         </div>
      <!--    <div class="alert alert-success" role="alert">
            * รหัส OTP จะส่งไปที่เบอร์โทรศัพท์ของท่าน
         </div> -->
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
            </div>
            <input type="text" class="form-control text-center" name="username" id="username" placeholder="เบอร์โทรศัพท์" required="" value="0908928262" maxlength="10">
         </div>
         <div class="input-group mb-3 otp" style="display: none;">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fal fa-key"></i></span>
            </div>
            <input type="text" class="form-control text-center" name="otp_key" id="otp_key" disabled="disabled">
         </div>
         <div class="input-group mb-3 otp" style="display: none;">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fal fa-key"></i></span>
            </div>
            <input type="text" class="form-control text-center" name="otp_val" id="otp_val" placeholder="รหัส OTP ">
         </div>
         <div class="input-group mb-3" id="pass" style="display: none;">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fal fa-lock"></i></span>
            </div>
            <input type="password" class="form-control text-center" name="password" id="password" placeholder="รหัสผ่าน">
         </div>
         <div class="input-group mb-3" id="repass" style="display: none;">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fal fa-lock"></i></span>
            </div>
            <input type="password" class="form-control text-center" name="repassword" id="repassword" placeholder="พิมพ์รหัสผ่าน อีกครั้ง" >
         </div>
         <button type="submit" class="btn btn-primary btn-login"></i> ยืนยัน !</button>
      </form>
      <div class="menu-bottom">
         <ul>
            <li><a href="<?php echo base_url();?>login">เข้าสู่ระบบ !</a></li>
            <!-- <li><a href="<?php echo base_url();?>register">สมัครสมาชิก ?</a></li> -->
            <li>&nbsp;</li>
         </ul>
      </div>
   </div>
</section>