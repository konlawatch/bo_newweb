<h1 class="text-primary mb-0 textgold noise center">แก้ไข <?php echo $d['key_all'];?></h1>
<hr class="x-hr-border-glow">
<?php //debug($d);?>
<div style="display: flex; justify-content: center;"><img width="150" height="150" id="imgAvatar" alt="150x150" src="/assets/uploads/<?php echo $d['value'];?>" style="width: 220px;"></div><br><br>
<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>Dashboard/createimg_icon">
   <input type="hidden"  name="keyname"      id="keyname"   value="<?php echo $d['key_all'];?>">           
   <input type="hidden"  name="status"       id="status"    value="1">  
   <input type="hidden"  name="v1"           id="v1"        value="<?php echo $d['value']?>"> 
   <div> 
      <input class="input-file2" id="edit_post_img" name="edit_post_img" accept="image/*"  type="file" OnChange="showPreview(this)">
      <label tabindex="0" for="edit_post_img" class="input-file-trigger2" style="">เลือกรูป...</label> 
      <p class="file-return2" style="color: #fff !important;"></p>
   </div>
   <br> 
      <label for="text-primary mb-0 textsilver noise " style="color: #fff;">ข้อความ</label>
      <input type="text" class="form-control" id="but_name" name="but_name" placeholder="" value="<?php echo $d['but_name'];?>">
  
   <div class="modal-footer center" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
      <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
      <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
   </div>
</form> 

<script language="JavaScript">
              function showPreview(ele)
              {
                  $('#imgAvatar').attr('src', ele.value); // for IE
                        if (ele.files && ele.files[0]) {
                  
                            var reader = new FileReader();
                    
                            reader.onload = function (e) {
                                $('#imgAvatar').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(ele.files[0]);
                        }
              }
            </script>