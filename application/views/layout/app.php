<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo $title;?></title>
      <meta name="description" content="">
      <meta name="keywords" content="">
      <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
      <link href="https://fonts.googleapis.com/css2?family=Mitr:wght@200;300;400&amp;display=swap" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap2.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
      <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/swal2.css"> -->
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fontawesome.min.css">
      
      <script type="text/javascript" src="assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      
      <style type="text/css">
         @media only screen and (min-width: 641px){
            .dxeklP {
               width: 100%;
            }
            .jGZAsY {
               display: none;
            }
            .hJkCQZ {
               display: block;
            }
            .YgakY {
               height: 64px;
            }
            .ekmdOJ {
               font-size: 1.2em;
            }
            .dBPUzO {
               width: 100%;
               background: rgb(26, 41, 44);
               display: flex;
               -webkit-box-pack: center;
               justify-content: center;
               padding: 8px;
            }
            .YTeyD {
               width: 100%;
               border: none;
               margin: 0px 4px;
            }
            .jrlgrg {
                width: 100%;
                background: transparent;
                padding: 8px;
            }
         }
         .YTeyD {
            width: 100%;
            height: 75px;
            border: none;
            border-radius: 2px;
            outline: none !important;
         }
         .refActive {
            border: none;
            background-color: #3d4246;
            color: #00bfa1;
            outline: none!important;
         }
         .modal-backdrop{position: inherit !important;}
         .dxeklP {
            background-image: linear-gradient(to right, rgba(67, 160, 154, 0.73), rgb(11, 68, 65));
            margin: 16px 0px;
            border-radius: 8px;
            padding: 16px 8px 6px;
            color: rgb(255, 255, 255);
            display: grid;
            gap: 4px;
         }
         .jGZAsY {
            display: block;
         }
         .hJkCQZ {
            display: none;
            
         }
         .jGZAsY .input-group>.input-group-prepend {
             flex: 0 0 35%;
         }
         .jGZAsY .input-group .input-group-text {
             width: 100%;
         }

         .hJkCQZ .input-group>.input-group-prepend {
             flex: 0 0 35%;
         }
         .hJkCQZ .input-group{
             font-size: 15px !important;
         }
         .hJkCQZ .input-group .input-group-text {
             width: 100%;
         }


         .kOObDs {
            background-image: linear-gradient(to right, rgba(67, 160, 154, 0.73), rgb(11, 68, 65));
            margin: 16px 0px;
            height: fit-content;
            border-radius: 8px;
            color: rgb(255, 255, 255);
         }
         .YgakY {
            width: 100%;
            position: fixed;
            bottom: 0px;
            left: 0px;
            right: 0px;
            text-align: center;
            background-color: rgb(11, 44, 49);
            color: rgb(255, 255, 255);
            height: 60px;
            z-index: 30;
         }
         .bEDLoS {
            width: 100%;
            height: 100%;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
         }
         .ekmdOJ {
            width: 100%;
            height: 60px;
            padding: 16px 12px;
            border: none;
            border-radius: 8px;
            margin: 8px 0px;
            font-size: 1.2em;
            color: rgb(255, 255, 255);
         }
         .glow-on-hover {
            cursor: pointer;
         }
         .emhNln {
             width: 100%;
             background-color: rgb(255, 255, 255);
             padding: 0px;
             margin: 0px;
             border-radius: 8px;
         }
      </style>
      <style data-styled="">
         .hJkCQZ{display:none;}/*!sc*/
         .slick-dots {
             position: absolute;
             bottom: 0;
             display: -webkit-flex;
             display: -moz-box;
             display: flex;
             left: 0;
             right: 0;
             -webkit-justify-content: center;
             -moz-box-pack: center;
             justify-content: center;
             padding: 0;
             margin: 0;
             list-style: none;
             text-align: center;
         }
         .slick-dots li button:before {
             display: inline-block;
             width: 12px;
             height: 12px;
             border-radius: 50%;
             opacity: 1;
             box-shadow: none;
             -moz-transition: background .5s;
             transition: background .5s;
             border: 2px solid grey;
             padding: 0;
             margin: 0 6px 0 0;
             outline: 0;
             cursor: pointer;
             font-size: 12px;
         }
         .slick-dots li button:before {
             border: none;
             color: #fff;
         }
         .slick-dots li.slick-active button:before {
             color: #03caa6;
         }

         .hXpDUX {
             position: fixed;
             inset: 0px;
             background-color: rgba(0, 0, 0, 0.7);
             z-index: 19;
             text-align: -webkit-center;
             overflow: scroll;
         }

         .hNmSUG {
             margin: 40px 0px 0px;
             width: 100%;
             height: 100%;
             z-index: 29;
             -webkit-box-pack: center !important;
             justify-content: center !important;
         }

         .cyyUrM {
             background-color: rgb(0, 191, 161);
             color: rgb(255, 255, 255);
             height: 60px;
             width: 94%;
             z-index: 29;
             display: flex;
             padding: 14px 16px;
             -webkit-box-pack: justify !important;
             justify-content: space-between !important;
         }
         .cIBFsd {
             width: 94%;
             z-index: 29;
             -webkit-box-pack: center !important;
             justify-content: center !important;
         }

         #modal_download img {
             width: 100%!important;
             max-width: 680px!important;
         }

         @media only screen and (min-width:641px){
            .hJkCQZ{display:block;}}/*!sc*/
            data-styled.g50[id="sc-9f4hzg-0"]{content:"hJkCQZ,"}/*!sc*/
            .jGZAsY{display:block;}/*!sc*/
         @media only screen and (min-width:641px){
            .jGZAsY{display:none;}
            .hNmSUG {
                width: 680px;
                height: 100%;
            }
            .cyyUrM {
                width: 680px;
                margin: 0px 40px;
            }
            .cIBFsd {
                width: 680px;
                margin: 0px 40px;
            }
         }/*!sc*/
         data-styled.g51[id="sc-9f4hzg-1"]{content:"jGZAsY,"}/*!sc*/
         .btn-enjoy911 {
            background-color:#00bfa1;
            border:none;
            border-radius: 5px !important;
            color: #fff;
         }
         .img-enjoy911 { 
            border-radius: 50%;
            border: 3px solid #03bfa1;
         }
         .input-dis {
            background: #fff !important;
         }
         .btn-append {
            background: #03bfa1;
            border: 1px solid #03bfa1;
            color: #fff;
         }
         .fa, .fas {
            font-weight: 100;
         }
         .x-loading {
            color: #fff;
         }
      </style>
      <style type="text/css">
         @media only screen and (min-width:641px){}/*!sc*/
         data-styled.g1[id="stbi-0"]{content:"jVEzye,"}/*!sc*/
         .kgogCM{position:relative;margin-top:10px;display:inline-block;cursor:pointer;text-align:-webkit-right;}/*!sc*/
         data-styled.g3[id="stbi-2"]{content:"kgogCM,"}/*!sc*/
         .kkYNLJ{background:#30c29d;height:70px;text-align:center;display:grid;grid-template-columns:20% auto 25%;z-index:99;}/*!sc*/
         data-styled.g4[id="stbi-3"]{content:"kkYNLJ,"}/*!sc*/
         .jMioAk{margin:8px 0px;padding:4px;font-size:1.2em;text-align:center;border:none;border-radius:8px;background-color:#30c29d;}/*!sc*/
         @media only screen and (min-width:641px){}/*!sc*/
         data-styled.g7[id="stbi-6"]{content:"jMioAk,"}/*!sc*/
         .jQSVJh{display:grid;grid-template-columns:auto auto;grid-gap:0px 8px;}/*!sc*/
         data-styled.g8[id="stbi-7"]{content:"jQSVJh,"}/*!sc*/
         .kJUoaC{background-color:#ffffff29;margin:4px 0px;border-radius:4px;padding:16px 8px 6px 8px;color:#fff;display:grid;grid-gap:4px;width:130px;height:100px;text-align:-webkit-center;border:none;outline:0 !important;}/*!sc*/
         .kJUoaC.active{color:#00bfa1;background-color:#fff;border-top-left-radius:8px;}/*!sc*/
         @media only screen and (min-width:641px){.kJUoaC{width:130px;}}/*!sc*/
         @media only screen and (min-width:1025px){.kJUoaC{width:120px;}}/*!sc*/
         data-styled.g9[id="stbi-8"]{content:"kJUoaC,"}/*!sc*/
         .kOEVjz.active{color:#00bfa1;font-size:1em;background-color:#296d68;border-top-left-radius:8px;width:276px;}/*!sc*/
         data-styled.g10[id="stbi-9"]{content:"kOEVjz,"}/*!sc*/
         .YgakY{width:100%;position:fixed;bottom:0;left:0;right:0;text-align:center;background-color:#0b2c31;color:#fff;height:60px;display:-moz-inline-grid;z-index:30;}/*!sc*/
         @media only screen and (min-width:641px){.YgakY{height:64px;}}/*!sc*/
         data-styled.g18[id="o37fw8-0"]{content:"YgakY,"}/*!sc*/
         .kfBYhS{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;width:100%;-webkit-box-pack:space-around;-webkit-justify-content:space-around;-ms-flex-pack:space-around;justify-content:space-around;}/*!sc*/
         @media only screen and (min-width:641px){.kfBYhS{width:90%;}}/*!sc*/
         @media only screen and (min-width:1025px){.kfBYhS{width:1000px;}}/*!sc*/
         data-styled.g20[id="o37fw8-2"]{content:"kfBYhS,"}/*!sc*/
         .jwpKsX{font-size:0.7em;color:#5feacd;display:grid;width:100% !important;height:-webkit-fit-content;height:-moz-fit-content;height:fit-content;text-align:-webkit-center;padding:4px;-webkit-text-decoration:none;text-decoration:none;cursor:pointer;}/*!sc*/
         @media only screen and (min-width:641px){.jwpKsX{width:80px !important;}}/*!sc*/
         .jwpKsX:focus{background:rgb(0,191,161);}/*!sc*/
         .jwpKsX:hover{color:#fff !important;-webkit-text-decoration:none;text-decoration:none;}/*!sc*/
         .jwpKsX.active{color:#fff !important;background:rgb(0,191,161);}/*!sc*/
         data-styled.g21[id="o37fw8-3"]{content:"jwpKsX,"}/*!sc*/
         .jOKNNT{aspect-ratio:attr(auto) / attr(auto);-webkit-filter:contrast(2.5);filter:contrast(2.5);}/*!sc*/
         .jOKNNT:hover{-webkit-filter:grayscale(1);filter:grayscale(1);}/*!sc*/
         data-styled.g30[id="o37fw8-12"]{content:"jOKNNT,"}/*!sc*/
         .bqkdwN{font-size:0.8em;color:#fff;display:grid;grid-template-columns:auto;text-align:-webkit-center;-webkit-filter:brightness(0) invert(1);filter:brightness(0) invert(1);cursor:pointer;}/*!sc*/
         .bqkdwN:hover{color:#00bfa1;-webkit-filter:brightness();filter:brightness();}/*!sc*/
         data-styled.g37[id="sc-1qrnk4o-6"]{content:"bqkdwN,"}/*!sc*/
         .cpoYdY{font-size:0.8em;color:#fff;display:grid;grid-template-columns:auto;text-align:-webkit-center;cursor:pointer;}/*!sc*/
         .cpoYdY:hover{color:#00bfa1;}/*!sc*/
         data-styled.g38[id="sc-1qrnk4o-7"]{content:"cpoYdY,"}/*!sc*/
         .hYOUfD{width:40px;height:56px;margin:8px 0px 8px 0px;}/*!sc*/
         data-styled.g39[id="sc-1qrnk4o-8"]{content:"hYOUfD,"}/*!sc*/
         .gBUvoq{font-size:0.8rem;-webkit-line-clamp:2;overflow:hidden;text-overflow:ellipsis;display:-webkit-box;-webkit-box-orient:vertical;}/*!sc*/
         @media only screen and (min-width:641px){.gBUvoq{font-size:1rem;margin:8px 0px 16px 0px;}}/*!sc*/
         data-styled.g41[id="sc-1qrnk4o-10"]{content:"gBUvoq,"}/*!sc*/
         .dxeklP{background-image:linear-gradient(to right,#43a09aba,#0b4441);margin:16px 0px;border-radius:8px;padding:16px 8px 6px 8px;color:#fff;display:grid;grid-gap:4px;background-color:navBar;}/*!sc*/
         @media only screen and (min-width:641px){.dxeklP{width:100%;}}/*!sc*/
         data-styled.g42[id="sc-5g65yh-0"]{content:"dxeklP,"}/*!sc*/
         .jnuzDq{width:24px;height:24px;border-radius:32px;border:none;background:#3e6a6f;}/*!sc*/
         data-styled.g43[id="sc-5g65yh-1"]{content:"jnuzDq,"}/*!sc*/
         .eYxMlR{width:100%;padding:8px 8px;color:#fff;border:none;border-radius:8px;margin:8px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;gap:8px;}/*!sc*/
         @media only screen and (min-width:641px){.eYxMlR{width:200px;}}/*!sc*/
         data-styled.g44[id="sc-5g65yh-2"]{content:"eYxMlR,"}/*!sc*/
         .kOObDs{background-image:linear-gradient(to right,#43a09aba,#0b4441);margin:16px 0px;height:-webkit-fit-content;height:-moz-fit-content;height:fit-content;border-radius:8px;color:#fff;}/*!sc*/
         data-styled.g45[id="sc-5g65yh-3"]{content:"kOObDs,"}/*!sc*/
         .fIfwcw{margin:0px;height:auto;}/*!sc*/
         @media only screen and (min-width:641px){.fIfwcw{margin:0px;height:88px;}}/*!sc*/
         data-styled.g48[id="qveujy-2"]{content:"fIfwcw,"}/*!sc*/
         .hJkCQZ{display:none;}/*!sc*/
         @media only screen and (min-width:641px){.hJkCQZ{display:block;}}/*!sc*/
         data-styled.g50[id="sc-9f4hzg-0"]{content:"hJkCQZ,"}/*!sc*/
         .jGZAsY{display:block;}/*!sc*/
         @media only screen and (min-width:641px){.jGZAsY{display:none;}}/*!sc*/
         data-styled.g51[id="sc-9f4hzg-1"]{content:"jGZAsY,"}/*!sc*/
      </style>
   </head>
   <body>
      <div id="__next">
         <div style="background-color:#0b2c31;color:#fff" class="stbi-0 jVEzye">
            <picture class="pt-2 float-left" onclick="goMenu('main');" style="cursor: pointer;">
               <source srcset="assets/images/logo.png" type="image/webp"><img width="250" height="54" src="assets/images/logo.png" alt="ENJ911.COM" loading="lazy">
            </picture>
            <div class="stbi-2 kgogCM float-right" onclick="show_righmenu();" >
               <div class="bar1" style="background:#fff"></div>
               <div class="bar2" style="background:#fff"></div>
               <div class="bar3" style="background:#fff"></div>
            </div>
            <div class="sidenavHide" id="sidenav-menu">
               <div class="stbi-3 kkYNLJ">
                  <div class="stbi-2 kgogCM" onclick="hide_righmenu();">
                     <div class="bar1"></div>
                     <div class="bar2"></div>
                     <div class="bar3"></div>
                  </div>
                  <h2 class="pt-2">เมนู</h2>
               </div>
               <div style="margin:16px">
                  <div class="stbi-6 jMioAk">ระบบเงิน</div>
                  <div class="stbi-7 jQSVJh">
                     <button class="stbi-8 kJUoaC active" onclick="goMenu('main');">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="home" class="svg-inline--fa fa-home fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="font-size:2em">
                           <path fill="currentColor" d="M280.37 148.26L96 300.11V464a16 16 0 0 0 16 16l112.06-.29a16 16 0 0 0 15.92-16V368a16 16 0 0 1 16-16h64a16 16 0 0 1 16 16v95.64a16 16 0 0 0 16 16.05L464 480a16 16 0 0 0 16-16V300L295.67 148.26a12.19 12.19 0 0 0-15.3 0zM571.6 251.47L488 182.56V44.05a12 12 0 0 0-12-12h-56a12 12 0 0 0-12 12v72.61L318.47 43a48 48 0 0 0-61 0L4.34 251.47a12 12 0 0 0-1.6 16.9l25.5 31A12 12 0 0 0 45.15 301l235.22-193.74a12.19 12.19 0 0 1 15.3 0L530.9 301a12 12 0 0 0 16.9-1.6l25.5-31a12 12 0 0 0-1.7-16.93z"></path>
                        </svg>
                        <p style="font-size:0.8em">หน้าหลัก</p>
                     </button>
                     <button class="stbi-8 kJUoaC" onclick="goMenu('deposit');">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="credit-card" class="svg-inline--fa fa-credit-card fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="font-size:2em">
                           <path fill="currentColor" d="M0 432c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V256H0v176zm192-68c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H204c-6.6 0-12-5.4-12-12v-40zm-128 0c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H76c-6.6 0-12-5.4-12-12v-40zM576 80v48H0V80c0-26.5 21.5-48 48-48h480c26.5 0 48 21.5 48 48z"></path>
                        </svg>
                        <p style="font-size:0.8em">เติมเงิน</p>
                     </button>
                     <button class="stbi-8 kJUoaC" onclick="goMenu('withdraw');">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="hand-holding-usd" class="svg-inline--fa fa-hand-holding-usd fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="font-size:2em">
                           <path fill="currentColor" d="M271.06,144.3l54.27,14.3a8.59,8.59,0,0,1,6.63,8.1c0,4.6-4.09,8.4-9.12,8.4h-35.6a30,30,0,0,1-11.19-2.2c-5.24-2.2-11.28-1.7-15.3,2l-19,17.5a11.68,11.68,0,0,0-2.25,2.66,11.42,11.42,0,0,0,3.88,15.74,83.77,83.77,0,0,0,34.51,11.5V240c0,8.8,7.83,16,17.37,16h17.37c9.55,0,17.38-7.2,17.38-16V222.4c32.93-3.6,57.84-31,53.5-63-3.15-23-22.46-41.3-46.56-47.7L282.68,97.4a8.59,8.59,0,0,1-6.63-8.1c0-4.6,4.09-8.4,9.12-8.4h35.6A30,30,0,0,1,332,83.1c5.23,2.2,11.28,1.7,15.3-2l19-17.5A11.31,11.31,0,0,0,368.47,61a11.43,11.43,0,0,0-3.84-15.78,83.82,83.82,0,0,0-34.52-11.5V16c0-8.8-7.82-16-17.37-16H295.37C285.82,0,278,7.2,278,16V33.6c-32.89,3.6-57.85,31-53.51,63C227.63,119.6,247,137.9,271.06,144.3ZM565.27,328.1c-11.8-10.7-30.2-10-42.6,0L430.27,402a63.64,63.64,0,0,1-40,14H272a16,16,0,0,1,0-32h78.29c15.9,0,30.71-10.9,33.25-26.6a31.2,31.2,0,0,0,.46-5.46A32,32,0,0,0,352,320H192a117.66,117.66,0,0,0-74.1,26.29L71.4,384H16A16,16,0,0,0,0,400v96a16,16,0,0,0,16,16H372.77a64,64,0,0,0,40-14L564,377a32,32,0,0,0,1.28-48.9Z"></path>
                        </svg>
                        <p style="font-size:0.8em">ถอนเงิน</p>
                     </button>
                     <button class="stbi-8 kJUoaC" onclick="goMenu('statement');">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-invoice-dollar" class="svg-inline--fa fa-file-invoice-dollar fa-w-12 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" style="font-size:2em">
                           <path fill="currentColor" d="M377 105L279.1 7c-4.5-4.5-10.6-7-17-7H256v128h128v-6.1c0-6.3-2.5-12.4-7-16.9zm-153 31V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zM64 72c0-4.42 3.58-8 8-8h80c4.42 0 8 3.58 8 8v16c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8V72zm0 80v-16c0-4.42 3.58-8 8-8h80c4.42 0 8 3.58 8 8v16c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8zm144 263.88V440c0 4.42-3.58 8-8 8h-16c-4.42 0-8-3.58-8-8v-24.29c-11.29-.58-22.27-4.52-31.37-11.35-3.9-2.93-4.1-8.77-.57-12.14l11.75-11.21c2.77-2.64 6.89-2.76 10.13-.73 3.87 2.42 8.26 3.72 12.82 3.72h28.11c6.5 0 11.8-5.92 11.8-13.19 0-5.95-3.61-11.19-8.77-12.73l-45-13.5c-18.59-5.58-31.58-23.42-31.58-43.39 0-24.52 19.05-44.44 42.67-45.07V232c0-4.42 3.58-8 8-8h16c4.42 0 8 3.58 8 8v24.29c11.29.58 22.27 4.51 31.37 11.35 3.9 2.93 4.1 8.77.57 12.14l-11.75 11.21c-2.77 2.64-6.89 2.76-10.13.73-3.87-2.43-8.26-3.72-12.82-3.72h-28.11c-6.5 0-11.8 5.92-11.8 13.19 0 5.95 3.61 11.19 8.77 12.73l45 13.5c18.59 5.58 31.58 23.42 31.58 43.39 0 24.53-19.05 44.44-42.67 45.07z"></path>
                        </svg>
                        <p style="font-size:0.8em">รายงานการเงิน</p>
                     </button>
                  </div>
               </div>
               <hr class="hrTopStdMenu">
               <ul>
                  <li style="cursor:pointer">
                     <div class="stbi-9 kOEVjz">
                        <div class="d-flex justify-content-start pt-3 pl-3">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="book" class="svg-inline--fa fa-book fa-w-14 iconMenu mr-3" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="font-size:1.4em">
                              <path fill="currentColor" d="M448 360V24c0-13.3-10.7-24-24-24H96C43 0 0 43 0 96v320c0 53 43 96 96 96h328c13.3 0 24-10.7 24-24v-16c0-7.5-3.5-14.3-8.9-18.7-4.2-15.4-4.2-59.3 0-74.7 5.4-4.3 8.9-11.1 8.9-18.6zM128 134c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm0 64c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm253.4 250H96c-17.7 0-32-14.3-32-32 0-17.6 14.4-32 32-32h285.4c-1.9 17.1-1.9 46.9 0 64z"></path>
                           </svg>
                           <span class="" onclick="goMenu('manual');">วิธีการใช้งาน</span>
                        </div>
                        <hr class="hrTopStdMenu">
                     </div>
                  </li>
                  <li style="cursor:pointer">
                     <div class="stbi-9 kOEVjz">
                        <div class="d-flex justify-content-start pt-3 pl-3">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cog" class="svg-inline--fa fa-cog fa-w-16 iconMenu mr-3" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="font-size:1.4em">
                              <path fill="currentColor" d="M487.4 315.7l-42.6-24.6c4.3-23.2 4.3-47 0-70.2l42.6-24.6c4.9-2.8 7.1-8.6 5.5-14-11.1-35.6-30-67.8-54.7-94.6-3.8-4.1-10-5.1-14.8-2.3L380.8 110c-17.9-15.4-38.5-27.3-60.8-35.1V25.8c0-5.6-3.9-10.5-9.4-11.7-36.7-8.2-74.3-7.8-109.2 0-5.5 1.2-9.4 6.1-9.4 11.7V75c-22.2 7.9-42.8 19.8-60.8 35.1L88.7 85.5c-4.9-2.8-11-1.9-14.8 2.3-24.7 26.7-43.6 58.9-54.7 94.6-1.7 5.4.6 11.2 5.5 14L67.3 221c-4.3 23.2-4.3 47 0 70.2l-42.6 24.6c-4.9 2.8-7.1 8.6-5.5 14 11.1 35.6 30 67.8 54.7 94.6 3.8 4.1 10 5.1 14.8 2.3l42.6-24.6c17.9 15.4 38.5 27.3 60.8 35.1v49.2c0 5.6 3.9 10.5 9.4 11.7 36.7 8.2 74.3 7.8 109.2 0 5.5-1.2 9.4-6.1 9.4-11.7v-49.2c22.2-7.9 42.8-19.8 60.8-35.1l42.6 24.6c4.9 2.8 11 1.9 14.8-2.3 24.7-26.7 43.6-58.9 54.7-94.6 1.5-5.5-.7-11.3-5.6-14.1zM256 336c-44.1 0-80-35.9-80-80s35.9-80 80-80 80 35.9 80 80-35.9 80-80 80z"></path>
                           </svg>
                           <span class="" onclick="goMenu('profile');">ตั้งค่าบัญชี</span>
                        </div>
                        <hr class="hrTopStdMenu">
                     </div>
                  </li>
                  <li style="cursor:pointer">
                     <div class="stbi-9 kOEVjz">
                        <div class="d-flex justify-content-start pt-3 pl-3">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="address-book" class="svg-inline--fa fa-address-book fa-w-14 iconMenu mr-3" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="font-size:1.4em">
                              <path fill="currentColor" d="M436 160c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-20V48c0-26.5-21.5-48-48-48H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h320c26.5 0 48-21.5 48-48v-48h20c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-20v-64h20c6.6 0 12-5.4 12-12v-40c0-6.6-5.4-12-12-12h-20v-64h20zm-228-32c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zm112 236.8c0 10.6-10 19.2-22.4 19.2H118.4C106 384 96 375.4 96 364.8v-19.2c0-31.8 30.1-57.6 67.2-57.6h5c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h5c37.1 0 67.2 25.8 67.2 57.6v19.2z"></path>
                           </svg>
                           <span class="" data-toggle="modal" data-target="#modal_contact">ติดต่อเรา</span>
                        </div>
                        <hr class="hrTopStdMenu">
                     </div>
                  </li>
                  <li style="cursor:pointer">
                     <div class="stbi-9 kOEVjz">
                        <div class="d-flex justify-content-start pt-3 pl-3">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sign-out-alt" class="svg-inline--fa fa-sign-out-alt fa-w-16 iconMenu mr-3" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="font-size:1.4em">
                              <path fill="currentColor" d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"></path>
                           </svg>
                           <span class="" onclick="logout();">ออกจากระบบ</span>
                        </div>
                        <hr class="hrTopStdMenu">
                     </div>
                  </li>
               </ul>
               <br>
            </div>
            <div class="sidenavOverlayHide" id="sidenavOverlay-menu"></div>
         </div>
         <p class="marquee" style="background-color: rgb(14, 73, 70); color: rgb(255, 255, 255);">
            <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บพนันออนไลน์ที่มาแรงที่สุดตอนนี้</span>
         </p>
         <div class="text-center animate__animated animate__fadeIn mb-4 animate__slower 3s container">
            <div class="justify-content-md-center mb-4 row">
               <div class="col-lg-10">
                  <?php echo $content;?>
                  <div class="o37fw8-0 YgakY">
                     <div class="o37fw8-2 kfBYhS">
                        <div class="<?php echo ($tapber == 'main') ? 'active' : '';?>">
                           <a class="o37fw8-3 jwpKsX" onclick="goMenu('main');">
                           <img width="40" height="40" src="assets/images/homeAct.png" alt="ihome" class="o37fw8-12 jOKNNT">
                           <p>หน้าหลัก</p>
                           </a>
                        </div>
                        <div  class="<?php echo ($tapber == 'deposit') ? 'active' : '';?>">
                           <a class="o37fw8-3 jwpKsX" onclick="goMenu('deposit');">
                           <img width="40" height="40" src="assets/images/deposit.png" alt="ihome" class="o37fw8-12 jOKNNT">
                           <p>ฝาก</p>
                           </a>
                        </div>
                        <div  class="<?php echo ($tapber == 'withdraw') ? 'active' : '';?>">
                           <a class="o37fw8-3 jwpKsX" onclick="goMenu('withdraw');">
                           <img width="40" height="40" src="assets/images/withdraw.png" alt="ihome" class="o37fw8-12 jOKNNT">
                           <p>ถอน</p>
                           </a>
                        </div>
                        <div  class="<?php echo ($tapber == 'setting') ? 'active' : '';?>">
                           <a class="o37fw8-3 jwpKsX" onclick="goMenu('profile');">
                           <img width="40" height="40" src="assets/images/setting.png" alt="ihome" class="o37fw8-12 jOKNNT">
                           <p>ตั้งค่า</p>
                           </a>
                        </div>
                        <div>
                           <a class="o37fw8-3 jwpKsX" onclick="goMenu('logout');">
                           <img width="40" height="40" src="assets/images/logout.png" alt="ihome" class="o37fw8-12 jOKNNT">
                           <p>ออกจากระบบ</p>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <br><br>
      </div>
   </body>
   <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   <script type="text/javascript">
      $(document).ready(function(){
         $('#modal_contact').on('shown.bs.modal', function (e) {
           hide_righmenu();
         });
         check_login();
      });
      function copy_text(TextToCopy) {
         var TempText = document.createElement("input");
         TempText.value = TextToCopy;
         document.body.appendChild(TempText);
         TempText.select();
        
         document.execCommand("copy");
         document.body.removeChild(TempText);
         Swal.fire('Done...', 'คัดลอกข้อความ '+TempText.value, 'success');
      }
      function show_righmenu(){
         $('#sidenav-menu').removeClass('sidenavHide').addClass('sidenavMenu');
         $('#sidenavOverlay-menu').removeClass('sidenavOverlayHide').addClass('sidenavOverlay');
      }
      function hide_righmenu(){
         $('#sidenav-menu').removeClass('sidenavMenu').addClass('sidenavHide');
         $('#sidenavOverlay-menu').removeClass('sidenavOverlay').addClass('sidenavOverlayHide');
      }
      function goMenu(menu){
         window.location.assign("<?php echo base_url();?>"+menu);
      }
      function logout(){
         console.log('xxx');
         window.location.assign("<?php echo base_url();?>logout");
      }
      function profile(){
         console.log('xxx');
         window.location.assign("<?php echo base_url();?>profile");
      }

      function auto_dnum(id,val){
        $('#'+id).val(val);
      }

      function check_login(){

         // $.ajax({
         //    type: 'get',
         //    url: '<?php echo base_url();?>service/check_login',
         //    data: {},
         //    dataType: "json",
         //    success: function(data, dataType, state){
         //       if(!data.status){
         //          Swal.fire('Oops..', data.msg, 'error').then(function(){
         //             logout();
         //          });
                  
         //       }
         //    }
         // });
         var login = setInterval(function(){
          
            $.ajax({
               type: 'get',
               url: '<?php echo base_url();?>service/check_login',
               data: {},
               dataType: "json",
               success: function(data, dataType, state){
                  if(!data.status){
                     Swal.fire('Oops..', data.msg, 'error').then(function(){
                        logout();
                     });
                     
                  }
               }
            });
          
         }, 60000);
      }
   </script>
</html>