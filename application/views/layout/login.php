<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo $title;?></title>
      <meta name="description" content="">
      <meta name="keywords" content="">
      <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
      <link href="https://fonts.googleapis.com/css2?family=Mitr:wght@200;300;400&amp;display=swap" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap2.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/swal2.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fontawesome.min.css">
      <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <script type="text/javascript" src="assets/js/jquery.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      <style data-styled="">
         .hJkCQZ{display:none;}/*!sc*/
         .slick-dots {
             position: absolute;
             bottom: 0;
             display: -webkit-flex;
             display: -moz-box;
             display: flex;
             left: 0;
             right: 0;
             -webkit-justify-content: center;
             -moz-box-pack: center;
             justify-content: center;
             padding: 0;
             margin: 0;
             list-style: none;
             text-align: center;
         }
         .slick-dots li button:before {
             display: inline-block;
             width: 12px;
             height: 12px;
             border-radius: 50%;
             opacity: 1;
             box-shadow: none;
             -moz-transition: background .5s;
             transition: background .5s;
             border: 2px solid grey;
             padding: 0;
             margin: 0 6px 0 0;
             outline: 0;
             cursor: pointer;
             font-size: 12px;
         }
         .slick-dots li button:before {
             border: none;
             color: #fff;
         }
         .slick-dots li.slick-active button:before {
             color: #03caa6;
         }

         .hXpDUX {
             position: fixed;
             inset: 0px;
             background-color: rgba(0, 0, 0, 0.7);
             z-index: 19;
             text-align: -webkit-center;
             overflow: scroll;
         }

         .hNmSUG {
             margin: 40px 0px 0px;
             width: 100%;
             height: 100%;
             z-index: 29;
             -webkit-box-pack: center !important;
             justify-content: center !important;
         }

         .cyyUrM {
             background-color: rgb(0, 191, 161);
             color: rgb(255, 255, 255);
             height: 60px;
             width: 94%;
             z-index: 29;
             display: flex;
             padding: 14px 16px;
             -webkit-box-pack: justify !important;
             justify-content: space-between !important;
         }
         .cIBFsd {
             width: 94%;
             z-index: 29;
             -webkit-box-pack: center !important;
             justify-content: center !important;
         }

         #modal_download img {
             width: 100%!important;
             max-width: 680px!important;
         }

         @media only screen and (min-width:641px){
            .hJkCQZ{display:block;}}/*!sc*/
            data-styled.g50[id="sc-9f4hzg-0"]{content:"hJkCQZ,"}/*!sc*/
            .jGZAsY{display:block;}/*!sc*/
         @media only screen and (min-width:641px){
            .jGZAsY{display:none;}
            .hNmSUG {
                width: 680px;
                height: 100%;
            }
            .cyyUrM {
                width: 680px;
                margin: 0px 40px;
            }
            .cIBFsd {
                width: 680px;
                margin: 0px 40px;
            }
         }/*!sc*/
         data-styled.g51[id="sc-9f4hzg-1"]{content:"jGZAsY,"}/*!sc*/

      </style>
      <style type="text/css">
         @media only screen and (min-width: 641px){
            .dxeklP {
               width: 100%;
            }
            .jGZAsY {
               display: none;
            }
            .hJkCQZ {
               display: block;
            }
            .YgakY {
               height: 64px;
            }
            .ekmdOJ {
               font-size: 1.2em;
            }
         }
         .modal-backdrop{position: inherit !important;}
         .dxeklP {
            background-image: linear-gradient(to right, rgba(67, 160, 154, 0.73), rgb(11, 68, 65));
            margin: 16px 0px;
            border-radius: 8px;
            padding: 16px 8px 6px;
            color: rgb(255, 255, 255);
            display: grid;
            gap: 4px;
         }
         .jGZAsY {
            display: block;
         }
         .hJkCQZ {
            display: none;
         }
         .kOObDs {
            background-image: linear-gradient(to right, rgba(67, 160, 154, 0.73), rgb(11, 68, 65));
            margin: 16px 0px;
            height: fit-content;
            border-radius: 8px;
            color: rgb(255, 255, 255);
         }
         .YgakY {
            width: 100%;
            position: fixed;
            bottom: 0px;
            left: 0px;
            right: 0px;
            text-align: center;
            background-color: rgb(11, 44, 49);
            color: rgb(255, 255, 255);
            height: 60px;
            z-index: 30;
         }
         .bEDLoS {
            width: 100%;
            height: 100%;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
         }
         .ekmdOJ {
            width: 100%;
            height: 60px;
            padding: 16px 12px;
            border: none;
            border-radius: 8px;
            margin: 8px 0px;
            font-size: 1.2em;
            color: rgb(255, 255, 255);
         }
         a:hover {
            color: red;
         }
      </style>
      <style data-styled="">
         .hJkCQZ{display:none;}/*!sc*/
         .slick-dots {
             position: absolute;
             bottom: 0;
             display: -webkit-flex;
             display: -moz-box;
             display: flex;
             left: 0;
             right: 0;
             -webkit-justify-content: center;
             -moz-box-pack: center;
             justify-content: center;
             padding: 0;
             margin: 0;
             list-style: none;
             text-align: center;
         }
         .slick-dots li button:before {
             display: inline-block;
             width: 12px;
             height: 12px;
             border-radius: 50%;
             opacity: 1;
             box-shadow: none;
             -moz-transition: background .5s;
             transition: background .5s;
             border: 2px solid grey;
             padding: 0;
             margin: 0 6px 0 0;
             outline: 0;
             cursor: pointer;
             font-size: 12px;
         }
         .slick-dots li button:before {
             border: none;
             color: #fff;
         }
         .slick-dots li.slick-active button:before {
             color: #03caa6;
         }

         .hXpDUX {
             position: fixed;
             inset: 0px;
             background-color: rgba(0, 0, 0, 0.7);
             z-index: 19;
             text-align: -webkit-center;
             overflow: scroll;
         }

         .hNmSUG {
             margin: 40px 0px 0px;
             width: 100%;
             height: 100%;
             z-index: 29;
             -webkit-box-pack: center !important;
             justify-content: center !important;
         }

         .cyyUrM {
             background-color: rgb(0, 191, 161);
             color: rgb(255, 255, 255);
             height: 60px;
             width: 94%;
             z-index: 29;
             display: flex;
             padding: 14px 16px;
             -webkit-box-pack: justify !important;
             justify-content: space-between !important;
         }
         .cIBFsd {
             width: 94%;
             z-index: 29;
             -webkit-box-pack: center !important;
             justify-content: center !important;
         }

         .gFFUTF {
             border-radius: 50%;
             width: 56px;
             height: 56px;
             margin: 8px 0px;
             cursor: pointer;
         }
         .WQpA {
             position: absolute;
             top: 16px;
             left: 16px;
             color: rgb(255, 255, 255);
             border-radius: 50%;
             width: 28px;
             height: 28px;
             padding: 0px;
             border: 3px solid rgb(255, 255, 255);
             text-align: center;
             font-weight: bolder;
             cursor: pointer;
         }

         #modal_download img {
             width: 100%!important;
             max-width: 680px!important;
         }

         @media only screen and (min-width:641px){
            .hJkCQZ{display:block;}}/*!sc*/
            data-styled.g50[id="sc-9f4hzg-0"]{content:"hJkCQZ,"}/*!sc*/
            .jGZAsY{display:block;}/*!sc*/
         @media only screen and (min-width:641px){
            .jGZAsY{display:none;}
            .hNmSUG {
                width: 680px;
                height: 100%;
            }
            .cyyUrM {
                width: 680px;
                margin: 0px 40px;
            }
            .cIBFsd {
                width: 680px;
                margin: 0px 40px;
            }
            .gFFUTF {
                border-radius: 50%;
                width: 80px;
                height: 80px;
                margin: 8px;
            }
         }/*!sc*/
         data-styled.g51[id="sc-9f4hzg-1"]{content:"jGZAsY,"}/*!sc*/

      </style>
      <style type="text/css">
         @media only screen and (min-width:641px){}/*!sc*/
         data-styled.g1[id="stbi-0"]{content:"jVEzye,"}/*!sc*/
         .kgogCM{position:relative;margin-top:10px;display:inline-block;cursor:pointer;text-align:-webkit-right;}/*!sc*/
         data-styled.g3[id="stbi-2"]{content:"kgogCM,"}/*!sc*/
         .kkYNLJ{background:#30c29d;height:70px;text-align:center;display:grid;grid-template-columns:20% auto 25%;z-index:99;}/*!sc*/
         data-styled.g4[id="stbi-3"]{content:"kkYNLJ,"}/*!sc*/
         .jMioAk{margin:8px 0px;padding:4px;font-size:1.2em;text-align:center;border:none;border-radius:8px;background-color:#30c29d;}/*!sc*/
         @media only screen and (min-width:641px){}/*!sc*/
         data-styled.g7[id="stbi-6"]{content:"jMioAk,"}/*!sc*/
         .jQSVJh{display:grid;grid-template-columns:auto auto;grid-gap:0px 8px;}/*!sc*/
         data-styled.g8[id="stbi-7"]{content:"jQSVJh,"}/*!sc*/
         .kJUoaC{background-color:#ffffff29;margin:4px 0px;border-radius:4px;padding:16px 8px 6px 8px;color:#fff;display:grid;grid-gap:4px;width:130px;height:100px;text-align:-webkit-center;border:none;outline:0 !important;}/*!sc*/
         .kJUoaC.active{color:#00bfa1;background-color:#fff;border-top-left-radius:8px;}/*!sc*/
         @media only screen and (min-width:641px){.kJUoaC{width:130px;}}/*!sc*/
         @media only screen and (min-width:1025px){.kJUoaC{width:120px;}}/*!sc*/
         data-styled.g9[id="stbi-8"]{content:"kJUoaC,"}/*!sc*/
         .kOEVjz.active{color:#00bfa1;font-size:1em;background-color:#296d68;border-top-left-radius:8px;width:276px;}/*!sc*/
         data-styled.g10[id="stbi-9"]{content:"kOEVjz,"}/*!sc*/
         .YgakY{width:100%;position:fixed;bottom:0;left:0;right:0;text-align:center;background-color:#0b2c31;color:#fff;height:60px;display:-moz-inline-grid;z-index:30;}/*!sc*/
         @media only screen and (min-width:641px){.YgakY{height:64px;}}/*!sc*/
         data-styled.g18[id="o37fw8-0"]{content:"YgakY,"}/*!sc*/
         .kfBYhS{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;width:100%;-webkit-box-pack:space-around;-webkit-justify-content:space-around;-ms-flex-pack:space-around;justify-content:space-around;}/*!sc*/
         @media only screen and (min-width:641px){.kfBYhS{width:90%;}}/*!sc*/
         @media only screen and (min-width:1025px){.kfBYhS{width:1000px;}}/*!sc*/
         data-styled.g20[id="o37fw8-2"]{content:"kfBYhS,"}/*!sc*/
         .jwpKsX{font-size:0.7em;color:#5feacd;display:grid;width:100% !important;height:-webkit-fit-content;height:-moz-fit-content;height:fit-content;text-align:-webkit-center;padding:4px;-webkit-text-decoration:none;text-decoration:none;cursor:pointer;}/*!sc*/
         @media only screen and (min-width:641px){.jwpKsX{width:80px !important;}}/*!sc*/
         .jwpKsX:focus{background:rgb(0,191,161);}/*!sc*/
         .jwpKsX:hover{color:#fff !important;-webkit-text-decoration:none;text-decoration:none;}/*!sc*/
         .jwpKsX.active{color:#fff !important;background:rgb(0,191,161);}/*!sc*/
         data-styled.g21[id="o37fw8-3"]{content:"jwpKsX,"}/*!sc*/
         .jOKNNT{aspect-ratio:attr(auto) / attr(auto);-webkit-filter:contrast(2.5);filter:contrast(2.5);}/*!sc*/
         .jOKNNT:hover{-webkit-filter:grayscale(1);filter:grayscale(1);}/*!sc*/
         data-styled.g30[id="o37fw8-12"]{content:"jOKNNT,"}/*!sc*/
         .bqkdwN{font-size:0.8em;color:#fff;display:grid;grid-template-columns:auto;text-align:-webkit-center;-webkit-filter:brightness(0) invert(1);filter:brightness(0) invert(1);cursor:pointer;}/*!sc*/
         .bqkdwN:hover{color:#00bfa1;-webkit-filter:brightness();filter:brightness();}/*!sc*/
         data-styled.g37[id="sc-1qrnk4o-6"]{content:"bqkdwN,"}/*!sc*/
         .cpoYdY{font-size:0.8em;color:#fff;display:grid;grid-template-columns:auto;text-align:-webkit-center;cursor:pointer;}/*!sc*/
         .cpoYdY:hover{color:#00bfa1;}/*!sc*/
         data-styled.g38[id="sc-1qrnk4o-7"]{content:"cpoYdY,"}/*!sc*/
         .hYOUfD{width:40px;height:56px;margin:8px 0px 8px 0px;}/*!sc*/
         data-styled.g39[id="sc-1qrnk4o-8"]{content:"hYOUfD,"}/*!sc*/
         .gBUvoq{font-size:0.8rem;-webkit-line-clamp:2;overflow:hidden;text-overflow:ellipsis;display:-webkit-box;-webkit-box-orient:vertical;}/*!sc*/
         @media only screen and (min-width:641px){.gBUvoq{font-size:1rem;margin:8px 0px 16px 0px;}}/*!sc*/
         data-styled.g41[id="sc-1qrnk4o-10"]{content:"gBUvoq,"}/*!sc*/
         .dxeklP{background-image:linear-gradient(to right,#43a09aba,#0b4441);margin:16px 0px;border-radius:8px;padding:16px 8px 6px 8px;color:#fff;display:grid;grid-gap:4px;background-color:navBar;}/*!sc*/
         @media only screen and (min-width:641px){.dxeklP{width:100%;}}/*!sc*/
         data-styled.g42[id="sc-5g65yh-0"]{content:"dxeklP,"}/*!sc*/
         .jnuzDq{width:24px;height:24px;border-radius:32px;border:none;background:#3e6a6f;}/*!sc*/
         data-styled.g43[id="sc-5g65yh-1"]{content:"jnuzDq,"}/*!sc*/
         .eYxMlR{width:100%;padding:8px 8px;color:#fff;border:none;border-radius:8px;margin:8px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;gap:8px;}/*!sc*/
         @media only screen and (min-width:641px){.eYxMlR{width:200px;}}/*!sc*/
         data-styled.g44[id="sc-5g65yh-2"]{content:"eYxMlR,"}/*!sc*/
         .kOObDs{background-image:linear-gradient(to right,#43a09aba,#0b4441);margin:16px 0px;height:-webkit-fit-content;height:-moz-fit-content;height:fit-content;border-radius:8px;color:#fff;}/*!sc*/
         data-styled.g45[id="sc-5g65yh-3"]{content:"kOObDs,"}/*!sc*/
         .fIfwcw{margin:0px;height:auto;}/*!sc*/
         @media only screen and (min-width:641px){.fIfwcw{margin:0px;height:88px;}}/*!sc*/
         data-styled.g48[id="qveujy-2"]{content:"fIfwcw,"}/*!sc*/
         .hJkCQZ{display:none;}/*!sc*/
         @media only screen and (min-width:641px){.hJkCQZ{display:block;}}/*!sc*/
         data-styled.g50[id="sc-9f4hzg-0"]{content:"hJkCQZ,"}/*!sc*/
         .jGZAsY{display:block;}/*!sc*/
         @media only screen and (min-width:641px){.jGZAsY{display:none;}}/*!sc*/
         data-styled.g51[id="sc-9f4hzg-1"]{content:"jGZAsY,"}/*!sc*/
      </style>
   </head>
   <body>
      <?php echo $content;?>
   </body>
   
   <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="assets/js/enj911.js"></script>
   <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   <script type="text/javascript">
      // $(document).load(function(){
         $('.autoplay').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
         });

         $('.autoplay_news').slick({
           slidesToShow: 3,
           slidesToScroll: 1,
           autoplay: true,
           autoplaySpeed: 2000,
           dots : true
         });
      // });

      $(document).ready(function(){
         // $(".isnumeric").keydown(function(event) {
         //      if ( event.keyCode == 46 || event.keyCode == 8 ) {
         //      }
         //      else {
         //          if (event.keyCode < 48 || event.keyCode > 57 ) {
         //              event.preventDefault(); 
         //          }   
         //      }
         //  });
         $(".isnumeric").inputFilter(function(value) {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
         });
      });

      function goMenu(menu){
         window.location.assign("<?php echo base_url();?>"+menu);
      }
   </script>
   <script type="text/javascript">
      $(document).on('submit','form#form-login',function(e){
         $('.btn-login').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
         $('.btn-login').prop('disabled', true);

         e.preventDefault();

         $.ajax({
             type: $(this).attr('method'),
             url: $(this).attr('action'),
             data: $(this).serialize(),
             dataType: "json",
             success: function(data, dataType, state){
                 if(data.status == 0)
                 {
                     $('.btn-login').html('<i class="fa fa-unlock-alt" aria-hidden="true"></i> เข้าสู่ระบบ');
                     $('.btn-login').prop('disabled', false);
                     $('#form_alert').attr('class', 'alert alert-danger');
                     $('#alert_text').html(data.message);            
                     $("#form_alert").fadeTo(7000, 1000).slideUp(1000, function(){
                         $("#form_alert").slideUp(1000);
                     });
                 }
                 else
                 {
                     $('.btn-login').html('กำลังตรวจสอบข้อมูล <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                     $.ajax({
                      type: 'post',
                      url: '<?php echo base_url();?>service/balance',
                      data: { token: data.token },
                      dataType: "json",
                      success: function(data, dataType, state){
                           console.log(data);
                            $('#form_alert').attr('class', 'alert alert-success');
                            $('.btn-login').html('เสร็จสิ้น <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                            $('.btn-login').html('กำลังพาคุณเข้าสู่ระบบ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                            $('#alert_text').html('กรุณารอสักครู่ กำลังพาคุณเข้าสู่ระบบ');
                            $("#form_alert").fadeTo(2000, 1000).slideUp(1000, function(){
                                $("#form_alert").slideUp(1000);
                                window.location.replace("<?php echo base_url();?>");
                            });
                        }
                     });
                 }
             }
         });
      });

      $(document).on('submit','form#form-forget',function(e){
         $('.btn-login').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
         $('.btn-login').prop('disabled', true);

         e.preventDefault();

         $.ajax({
             type: $(this).attr('method'),
             url: $(this).attr('action'),
             data: $(this).serialize(),
             dataType: "json",
             success: function(data, dataType, state){
                 if(data.status){
                     $('#form_alert').attr('class', 'alert alert-success');
                     $('.btn-login').html('เสร็จสิ้น <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                     $('.btn-login').html('กำลังไปหน้าล็อกอิน <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                     $('#alert_text').html('กรุณารอสักครู่ กำลังไปหน้าล็อกอิน');
                     $("#form_alert").fadeTo(2000, 1000).slideUp(1000, function(){
                       $("#form_alert").slideUp(1000);
                       window.location.replace("<?php echo base_url();?>login");
                     });
                 }else {
                     $('.btn-login').html('<i class="fa fa-unlock-alt" aria-hidden="true"></i> รับรหัสผ่านใหม่ !');
                     $('.btn-login').prop('disabled', false);
                     $('#form_alert').attr('class', 'alert alert-danger');
                     $('#alert_text').html(data.msg);            
                     $("#form_alert").fadeTo(7000, 1000).slideUp(1000, function(){
                         $("#form_alert").slideUp(1000);
                     });
                 }
             }
         });
      });
   </script>
</html>