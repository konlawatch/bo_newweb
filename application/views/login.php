<?php 
   $ss = $this->session->userdata();
   
?>
 <?php //debug($ss,true); ?>
<?php 
   $ss = $this->session->userdata();
   $s  = isset($data->cdate) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y');
   $d1 = explode('-', $s);
   $dd = $d1[2]."-".$d1[1]."-".$d1[0];
?>
<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>

<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>

<div id="__next">
   <div class="sc-9f4hzg-1 jGZAsY animate__animated animate__fadeIn animate__slower  3s">
      <div class="stbi-0 jVEzye bg-green justify-center">
      <h1 class=" mt-2">
         <a href="/">
            <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
               <div style="box-sizing: border-box; display: block; max-width: 100%;">
                  <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUwIiBoZWlnaHQ9IjU0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIvPg==" style="max-width: 100%; display: block;">
               </div>
               <img alt="" src="assets/images/logo.png" srcset="assets/images/logo.png" decoding="async" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
            </div>
         </a>
      </h1>
   </div>
   <p class="marquee" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
      <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บพนันออนไลน์ที่มาแรงที่สุดตอนนี้</span>
   </p>
</div>
<div class="text-center animate__animated animate__fadeIn animate__slower  3s container">
   <div class="justify-content-md-center row">
      <div class="col-lg-10">
         <div class="sc-9f4hzg-0 hJkCQZ">
            <div class="stbi-0 jVEzye bg-green justify-center">
               <h1 class=" mt-2">
                  <a href="/">
                     <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                        <div style="box-sizing: border-box; display: block; max-width: 100%;">
                           <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUwIiBoZWlnaHQ9IjU0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIvPg==" style="max-width: 100%; display: block;">
                        </div>
                        <img alt="" src="assets/images/logo.png" srcset="assets/images/logo.png" decoding="async" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                     </div>
                  </a>
               </h1>
            </div>
            <p class="marquee" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
               <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บหวยออนไลน์ที่มาแรงที่สุดตอนนี้</span>
            </p>
         </div>
         <div id="login" class="pb-0 mt-3 mb-3">
            <div class="card">
               <div class="text-left card-body">
                 <form  class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>login">
                     <div class="justify-content-center row">
                        <div class="col-lg-6 col-12">
                           <div class="form-group">
                              <input name="username" id="username" placeholder="หมายเลขโทรศัพท์" minlength="10" maxlength="10" required="" autocomplete="off" type="text" class="form-control"  onkeypress="return CharacterFormat(this,event,1);" value="">
                           </div>
                        </div>
                        <div class="col-lg-6 col-12">
                           <div class="form-group">
                              <input name="password" id="password" placeholder="รหัสผ่าน" minlength="4" maxlength="20" required="" type="password" class="form-control" value="">
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-7">
                           <div class="form-group">
                              <button type="button" class="pl-0  btn btn-link">
                                 <div class="float-left pl-3 form-check">
                                    <input type="checkbox" id="formBasicCheckbox" class="form-check-input" checked="">
                                    <label title="" for="formBasicCheckbox" class="form-check-label">จำฉันไว้ในระบบ</label>
                                 </div>
                              </button>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-5">
                           <div class="form-group">
                               <!-- <a href="<?php echo base_url();?>forgetpass" class="text-decoration-none text-danger text-right px-0 pr-0 btn btn-link" style="" >ลืมรหัสผ่าน</a> -->
                              <!-- <button type="button" class="text-decoration-none text-danger text-right px-0 pr-0 btn btn-link">ลืมรหัสผ่าน</button> -->
                           </div>
                        </div>
                        <div class="mb-3 col-lg-3 col-md-3 col-12 order-lg-1 order-md-1 order-2">
                          <!--  <button type="button" class="animate__animated animate__pulse animate__infinite animated-button1 btn btn-danger btn-block btn-lg">สมัครสมาชิก<span></span><span></span><span></span><span></span>
                           </button> -->
                           <a href="<?php echo base_url();?>register" class="animate__animated animate__pulse animate__infinite animated-button1 btn btn-danger btn-block btn-lg">สมัครสมาชิก<span></span><span></span><span></span><span></span></a>
                        </div>
                        <div class="mb-3 col-lg-3 col-md-3 col-12 order-lg-2 order-md-2 order-1">
                           <button type="submit" class="btn-green animate__animated animate__pulse animate__infinite animated-button1 btn btn-primary btn-block btn-lg" >เข้าสู่ระบบ<span></span><span></span><span></span><span></span>
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="row no-gutters">
               <!-- <div class="mt-2 pl-1 col-lg-6 col-md-6 col-sm-6 col-5">
                  <button type="button" class="text-white text-decoration-none px-0 text-left btn btn-link btn-block">อัตราการจ่าย</button>
               </div>
               <div class="mt-2 pr-1 col-lg-6 col-md-6 col-sm-6 col-7">
                  <button type="button" class="text-white text-decoration-none px-0 text-right btn btn-link btn-block" data-toggle="modal"  data-target="#modal_rule">กฎกติกาและข้อบังคับ</button>
               </div> -->

               <div class="mt-2 mb-2 pr-1 col-lg-3 col-12">
                  <a href="<?php echo base_url();?>manual" class="px-0 btn btn-success btn-block" >
                     <i class="fa fa-book mr-2" aria-hidden="true"></i>คู่มือและวิธีการใช้งาน
                  </a>
               </div>
               <div class="mt-2 mb-2 px-lg-1 col-lg-3 col-12">
                  <button type="button" class="px-0 btn-orange btn btn-success btn-block" data-toggle="modal" data-target="#modal_secret" >
                     <i class="fa fa-user-secret mr-2" aria-hidden="true"></i>นโยบายความเป็นส่วนตัว
                  </button>
               </div>
               <!-- <div class="mb-2 pr-1 px-lg-1 col-lg-3 col-6">
                  <button type="button" class="btn-green btn btn-primary btn-block" data-toggle="modal" data-target="#modal_download">
                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" class="svg-inline--fa fa-download fa-w-16 mr-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                     </svg>
                     <span style="overflow: hidden;">ดาวน์โหลด</span>
                  </button>
               </div> --> 
               <div class="mt-2 mb-2 pr-1 px-lg-1 col-lg-3 col-6">
                  <button type="button" class="btn-green btn btn-primary btn-block" data-toggle="modal" data-target="#modal_rule">
                     <!-- <i class="fa fa-gavel mr-2" aria-hidden="true"></i> -->กติกาและข้อบังคับ
                  </button>
               </div>
               <div class="mt-2 mb-2 pl-1 col-lg-3 col-6">
                  <button type="button" class="btn-green btn btn-primary btn-block" data-toggle="modal" data-target="#modal_contact">
                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="headset" class="svg-inline--fa fa-headset fa-w-16 mr-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor" d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z"></path>
                     </svg>ติดต่อเรา
                  </button>
               </div>
            </div>
          
            <div class="mt-0 mb-4">
               <div class="autoplay" dir="ltr">
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/pgAct.jpeg" srcset="assets/images/pgAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/spadeAct.jpeg" srcset="assets/images/spadeAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/redAct.jpeg" srcset="assets/images/redAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/yggAct.jpeg" srcset="assets/images/yggAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/ambAct.jpeg" srcset="assets/images/ambAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/sexyAct.jpeg" srcset="assets/images/sexyAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/kingmakerAct.jpeg" srcset="assets/images/kingmakerAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
               </div>
               <span id="banner-movie">
                  <!-- <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['l_bank'])) ?  $data['l_bank']['value']  : "255x150.png"?>" alt="" width="100%" style="cursor: pointer;">
                  <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['l_bank'])) ?  $data['l_bank']['value']  : "255x150.png"?>" alt="" width="100%" style="cursor: pointer;"> -->
               </span>
            </div>
            <div class="mt-4">
               <!-- <div class="sc-12khjtj-1 iBWnNu">
                  <div class="sc-12khjtj-2 iLa-dcB  selectTab">ประกาศ</div>
                  <div class="sc-12khjtj-2 iLa-dcB  defaultTab">เลขเด็ด</div>
               </div> -->
               <div class="sc-12khjtj-0 gleQTT">
                  <div class="sc-12khjtj-4 dBEYHH">
                     <div style="width: 100%;">
                        <div class="sc-12khjtj-3 lbRzWc">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="newspaper" class="svg-inline--fa fa-newspaper fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="margin: 0px 8px;">
                              <path fill="currentColor" d="M552 64H88c-13.255 0-24 10.745-24 24v8H24c-13.255 0-24 10.745-24 24v272c0 30.928 25.072 56 56 56h472c26.51 0 48-21.49 48-48V88c0-13.255-10.745-24-24-24zM56 400a8 8 0 0 1-8-8V144h16v248a8 8 0 0 1-8 8zm236-16H140c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm208 0H348c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm-208-96H140c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm208 0H348c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm0-96H140c-6.627 0-12-5.373-12-12v-40c0-6.627 5.373-12 12-12h360c6.627 0 12 5.373 12 12v40c0 6.627-5.373 12-12 12z"></path>
                           </svg>
                           <span>ประกาศ</span>
                           <span style="font-size: 12px; margin: 7px;">ข่าวสารจากทีมงาน</span>
                        </div>
                        <div>
                          <!--  <div class="react-multi-carousel-list react-multi-carousel-track  autoplay_news">
                              <div data-index="1" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                 <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post2'])) ?  $data['post2']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                    <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">เงื่อนไขกติกา หากตรวจสอบพบว่าสมาชิกมีเจตนาทุจริตแอบแฝง</span>
                                       <span class="dv90yj-7 YmTEp">เนื่องจากปัจจุบันมีสมาชิกบางท่านได้กระทำผิดกฏกติกาทางบริษัท</span>
                                       <p class="sc-12khjtj-9 bhZmsW">27 พ.ย. 2563</p>
                                    </div>
                                 </div>
                              </div>
                           </div> -->
                        </div>
                     </div>
                  </div>
                  <div class="sc-12khjtj-4 dBEYHH"></div>
               </div>
            </div>
         </div>
         <div style="margin-top:0" class="sc-9f4hzg-0 hJkCQZ">
            <div class="o37fw8-1 oTMwd bg-green mt-0-4">
               <div class="text-center">
                  <h3 class="mb-0" style="cursor: pointer; font-size: 1rem; line-height: 70px;">2021 COPYRIGHT <?php echo $website;?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div style="margin-top:0" class="sc-9f4hzg-1 jGZAsY">
      <div class="o37fw8-1 oTMwd bg-green mt-0-4">
         <div class="text-center">
            <h3 class="mb-0" style="cursor: pointer; font-size: 1rem; line-height: 70px;">2021 COPYRIGHT <?php echo $website;?></h3>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="modal_download"><div class="sc-1de8oot-0 hXpDUX"><div class="sc-1de8oot-1 hNmSUG pr-2 pl-2 " style="max-width: 480px;"><div class="p-2 text-light bg-danger" style="cursor: pointer;"><span style="font-size: 1rem;" data-dismiss="modal">ปิด</span></div><img src="assets/images/app.jpg" class="sc-1de8oot-4 dyRFWT p-0"><div class="p-2 text-light" style="background-color: rgb(56, 193, 114); cursor: pointer;"><span style="font-size: 1.2rem;">ดาวน์โหลด</span></div></div></div>
</div>
<div class="modal" id="modal_rule">
   <div class="sc-1de8oot-0 hXpDUX"><div class="sc-1de8oot-1 hNmSUG"><div class="sc-1de8oot-3 cyyUrM"><h4>กฏและกติกา</h4><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" style="font-size: 1.4rem; cursor: pointer;" data-dismiss="modal"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></div><div class="sc-1de8oot-2 cIBFsd bg-light text-left pr-4 pl-4 pb-1 pt-4"><div class="pb-0"><p>ยินดีต้อนรับสู่หวยออนไลน์&nbsp;เพื่อให้ท่านทราบถึงสิทธิประโยชน์ของสมาชิกกรุณาอ่านข้อตกลงอย่างละเอียดก่อนที่จะลงทะเบียนสมัครเป็นสมาชิกและใช้บริการ <a href="ENJ911.COM">ENJ911.COM </a> เมื่อสมัครสมาชิกแล้วทางเว็บจะถือว่า สมาชิกได้อ่านและยอมรับ&nbsp;"<strong>นโยบายและเงื่อนไขการให้บริการของเว็บไซต์</strong>"</p><p><strong>การรักษาและช่วยเหลือการเดิมพันออนไลน์</strong></p><p>ถ้าสมาชิกหลุดออกจากเว็บไซต์ระหว่างการเดิมพัน บัญชีของท่านจะไม่มีผลกระทบต่อผลลัพธ์ของการเดิมพัน
ไม่ต้องวิตกกังวล ถ้าสมาชิกออกจากเว็บไซด์ระหว่างการเดิมพัน สมาชิกจะถูกออกจากระบบช่วงเวลาหนึ่ง จากนั้นสมาชิกสามารถเข้าสู่ระบบ  <a href="ENJ911.COM">ENJ911.COM </a>&nbsp;ได้อีกครั้งตามปกติ</p><p><strong>การให้ความคุ้มครอง</strong></p><p><a href="ENJ911.COM">ENJ911.COM </a>&nbsp;ได้มีการจัดเตรียม ระบบที่มีประสิทธิภาพ ความรวดเร็วและการบริการลูกค้าที่เป็นมิตร ถ้าหากท่านมีคำถามหรือคำแนะนำใด ๆ กรุณาอย่าลังเลใจที่จะโทรศัพท์มาทางฝ่ายบริการลูกค้า ซึ่งจะมีให้บริการตลอด 24 ชั่วโมง ทุกวันไม่มีวันหยุดและทางเราจะตอบกลับอย่างรวดเร็วที่สุด&nbsp; <a href="ENJ911.COM">ENJ911.COM </a>&nbsp;ขอรับรองว่าจะรักษาข้อมูลส่วนตัวของท่านไว้เป็นความลับอย่างปลอดภัย</p><p><strong>ข้อตกลงในการเดิมพัน</strong></p><p>เพื่อหลีกเลี่ยงปัญหาต่างๆในขณะที่ใช้งานเว็บไซต์ สมาชิกกรุณาอ่านกฎกติกาของบริษัทอย่างละเอียดและระมัดระวัง เมื่อสมาชิกเข้าสู่หน้าเดิมพันทางบริษัทจะถือว่าท่านได้ยอมรับข้อตกลงของทาง&nbsp; <a href="ENJ911.COM">ENJ911.COM </a>
การเดิมพันจะต้องอยู่ใน&nbsp;"ระยะเวลาที่กำหนด"&nbsp;มิฉะนั้นจะถือว่าการเดิมครั้งนั้นเป็น&nbsp;"<strong>โมฆะ</strong>"&nbsp;ถ้าการเดิมพันได้มีการยกเลิกหรือหยุดพักชั่วคราว เนื่องจากเหตุผลใดๆก็ตามก่อนการเล่น การเดิมพันครั้งนั้นจะถือว่าเป็น"โมฆะ"และทางบริษัทจะคืนเครดิตให้กับสมาชิก
เป็นความรับผิดชอบของสมาชิกที่จะคอยดูผลลัพธ์ จากช่องหน้าต่างที่แสดงให้เห็นถึงผลการเดิมพันแพ้หรือชนะ ทางบริษัทจะยึดถือข้อมูล&nbsp;"รายละเอียดการวางเดิมพัน"&nbsp;ของสมาชิก ในกรณีที่สมาชิกมีความสงสัย และต้องการตรวจสอบข้อมูลในการคำนวณของการเดิมพันนั้นๆ
ถ้าหากเกิดความผิดพลาดของระบบระหว่างการเดิมพันหรือความผิดพลาดโดยไม่ได้เจตนาของพนักงาน ทางบริษัทขอสงวนสิทธ์ที่จะแก้ไขผลลัพธ์ให้ถูกต้อง และจะมีการพิมพ์ข้อความที่ได้มีการแก้ไขเผยแพร่ไว้ที่แถบตัววิ่งในหน้าเว็บไซต์ บริษัทจะไม่มีการแจ้งสมาชิกเป็นการส่วนตัว
เป็นความรับชอบของสมาชิกที่จะทำให้แน่ใจว่าชื่อผู้ใช้และรหัสผ่านสำหรับเข้าเว็บไซต์นั้นถูกต้อง และกรุณาเปลี่ยนรหัสผ่านของสมาชิกอย่างน้อยเดือนละครั้ง เพื่อความปลอดภัย ถ้าหากท่านพบหรือสงสัยว่ามีผู้อื่นได้เข้าไปเล่นในบัญชีของท่านโดยไม่รับอนุญาต กรุณา แจ้งตัวแทนของท่านทันทีเพื่อเปลี่ยนรหัสส่วนตัว (หากมีการเดิมพันก่อนที่จะเปลี่ยนรหัสเดิมจะถือว่าการเดิมพันนั้นเป็นผล)
ก่อนเริ่มการเดิมพันแต่ละครั้ง ท่านสมาชิกควรจะตรวจสอบวงเงินของท่านเสียก่อน ถ้าหากท่านมีข้อสงสัยใดๆ เกี่ยวกับวงเงิน กรุณาแจ้งตัวแทนของท่านสมาชิกทันที
ในกรณีที่เหตุการณ์ไม่คาดคิดเกิดขึ้น อย่างเช่น ข้อมูลสูญหายเนื่องจากระบบอินเตอร์เน็ตขัดข้อง ทางบริษัทจะมีการประกาศให้รู้ถึงสาเหตุ และวิธีการแก้ปัญหา
ชั่วโมงการให้บริการ 24 ชั่วโมง/วัน และไม่มีวันหยุด
ระบบการรักษาความปลอดภัย</p><p>ข้อมูลทางด้านบัญชีเพื่อวัตถุประสงค์เฉพาะบุคคล ข้อมูลที่ทางสมาชิกให้มาในการเปิดบัญชีจะถูกเก็บเป็นความลับขั้นสูงสุด ทางบริษัทจะไม่เผยแพร่ข้อมูลนี้ให้กับบุคคลที่สามหรือองค์กรอื่นๆ ล่วงรู้ ไม่ว่าจะเป็นบัญชีอีเมล&nbsp;หรือข้อมูลอื่นๆ ที่ทางสมาชิกกรอกในใบสมัคร ข้อมูลเหล่านี้จะใช้ในจุดมุ่งหมายที่ตั้งใจเอาไว้ให้เท่านั้น เช่น การส่งเช็คเงินสดสำหรับผู้ชนะหรือข้อมูลอื่นๆ ตามที่สมาชิกร้องขอ
ความปลอดภัยทางบริษัทได้ใช้วิธีที่มีประสิทธิภาพมากที่สุดเพื่อทำให้ท่านแน่ใจในความปลอดภัยของข้อมูลของท่าน ทางเราจะพยายามให้ดีที่สุดที่จะรักษาความถูกต้องแน่นอน และความลับเฉพาะของข้อมูล เพื่อป้องกันไม่ให้ข้อมูลรั่วไหลหรือนำไปใช้อย่างไม่ถูกต้อง ถ้าท่านมีข้อสงสัยใดๆ ที่เกี่ยวข้องกับการรักษาความลับ ความปลอดภัยของข้อมูล กรุณาติดต่อฝ่ายบริการลูกค้า พวกเรายินดีให้บริการ 24 ชั่วโมง/วัน ทุกวันไม่มีวันหยุด
บริษัทมีความยินดีเป็นอย่างยิ่งที่ได้รับใช้ท่าน</p></div></div><div class="sc-1de8oot-2 cIBFsd bg-light text-left pt-0 pr-4 pl-4 pb-4 pt-4"><button type="button" class="btn btn-light btn-block" style="background-color: rgb(0, 191, 161); color: rgb(255, 255, 255);" data-dismiss="modal">ฉันเข้าใจและยอมรับ</button></div></div><br><br></div>
</div>
<div class="modal" id="modal_contact">
   <div class="sc-1de8oot-0 hXpDUX">
      <div class="sc-1de8oot-1 hNmSUG pr-2 pl-2 ">
         <div class="sc-1de8oot-3 cyyUrM" style="max-width: 480px;">
            <h4>เลือกช่องทางการติดต่อเรา</h4>
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" style="font-size: 1.4rem; cursor: pointer;" data-dismiss="modal">
               <path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
            </svg>
         </div>
         <div class="sc-1de8oot-2 cIBFsd p-3 bg-light" style="max-width: 480px;">
            <button type="button" class="mb-2 btn btn-success btn-block">ติดต่อผ่านไลน์</button>
            <div class="accordion">
               <button type="button" class="btn-block btn btn-danger">ติดต่อผ่านโทรศัพท์</button>
               <div class="collapse">
                  <div class="pb-0 card-body">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                           <span><b>เบอร์ติดต่อ 1 </b></span>
                           <button type="button" class="btn btn-success btn-block" style="background-color: rgb(0, 191, 161);">098-731-0564</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                           <span><b>เบอร์ติดต่อ 2 </b></span>
                           <button type="button" class="btn btn-success btn-block" style="background-color: rgb(0, 191, 161);">092-425-6870</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="modal_secret">
   <div class="sc-1de8oot-0 hXpDUX">
      <div class="sc-1de8oot-1 hNmSUG">
         <div class="sc-1de8oot-3 cyyUrM">
            <h4>นโยบายความเป็นส่วนตัว</h4>
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" style="font-size: 1.4rem; cursor: pointer;" data-dismiss="modal"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
         </div>
         <div class="sc-1de8oot-2 cIBFsd bg-light text-left pr-4 pl-4 pb-1 pt-4">
            <div class="pb-0"><div><p>
&nbsp; 
&nbsp;
&nbsp;นโยบายส่วนบุคคลนี้เป็นการอธิบายกฎขั้นพื้นฐานในการเข้าใช้งานบนเว็บไซต์ [ENJ911.COM](https://ENJ911.COM/) ถ้าท่านได้เข้าใช้งานแสดงว่าท่านได้ยอมรับเงื่อนไขและข้อตกลง และนโยบายความเป็นส่วนตัวนี้ หากท่านไม่เห็นด้วยหรือไม่ยอมรับตามข้อตกลงกรุณาหยุดใช้งานเว็บไซต์นี้
</p></div><p><strong>การแก้ไขและเปลี่ยนแปลงนโยบาย</strong></p><p>&nbsp;
&nbsp;
&nbsp;เราขอสงวนสิทธิ์ในการแก้ไขเปลี่ยนแปลงนโยบายนี้โดยไม่ต้องแจ้งให้ท่านทราบล่วงหน้า นโยบายนี้จะมีผลบังคับใช้เมื่อมีการโพสลงหน้าเว็บไซต์</p><p><strong>การยอมรับปฏิบัติ</strong></p><p>&nbsp;
&nbsp;
&nbsp;นโยบายความเป็นส่วนตัวนี้เป็นไปตามกฎหมายการป้องกันข้อมูลที่เกี่ยวข้อง รวมถึงพระราชบัญญัติการคุ้มครองข้อมูลของ สหราชอาณาจักรปี 1998  <a href="https://ENJ911.COM/">ENJ911.COM</a> ได้อ้างอิงกฎหมายส่วนบุคคลของ EU และคู่มือองค์การเพื่อความร่วมมือทางการค้าและการพัฒนา (OECD) อย่างสม่ำเสมอ เพื่อสร้างความมั่นใจว่าผู้เล่นจะได้รับการปกป้องในส่วนของข้อมูลส่วนบุคคล</p><p><strong>การรวบรวมข้อมูลข้อมูลทั่วไป</strong></p><p>&nbsp;
&nbsp;
&nbsp;เราได้รวบรวมข้อมูลส่วนบุคคลของท่าน เพื่อให้ระบุความเป็นตัวตนของท่าน ซึ่งมีข้อมูลดังต่อไปนี้ และถ้าหากต้องการอะไรเพิ่มเติมเราอาจแจ้งขอให้คุณบอกเราในภายหลัง</p><p><strong>ข้อมูลระบุตัวบุคคล</strong></p><ol><li><p>ข้อมูลที่ใช้ในการลงทะเบียนใช้งานบัญชีผู้เล่นเมื่อสมัครสมาชิกใหม่ เช่น ชื่อ, อีเมล, เบอร์โทรศัพท์, วันเกิด และข้อมูลที่จำเป็นอื่นๆที่ทำให้เราสามารถเปิดใช้และจัดการกับบัญชีของท่านได้</p></li><li><p>บันทึกการติดต่อของท่านที่ส่งหาเรา</p></li><li><p>ข้อมูลใด ๆ ที่ท่านเสนอให้แก่เรา อันเป็นผลมาจากการเข้าร่วมโปรโมชั่นทางการตลาดหรือการแข่งขัน</p></li><li><p>รายละเอียดการเดิมพันและความความเคลื่อนไหวต่าง ๆ บนเว็บไซต์</p></li><li><p>การสนทนาทางโทรศัพท์และแชดสดของท่านกับฝ่ายบริการลูกค้าของเรา ซึ่งการสนทนาเหล่านี้จะถูกบันทึกไว้เพื่อนำไปใช้ในการฝึกอบรม บริหารจัดการคุณภาพการบริการ และหาทางแก้ข้อสงสัยได้รวดเร็วขึ้น</p></li></ol><p>&nbsp;
&nbsp;
&nbsp;ยิ่งกว่านั้น ข้อมูลเหล่านี้จะทำให้เราสามารถติดต่อท่านตามจุดประสงค์ต่าง ๆ ได้ ซึ่งทำให้เราสามารถนำเสนอบริการที่ดีที่สุดแก่ท่านได้ ข้อมูลส่วนบุคคลนี้จะถูกนำมาใช้เพื่อจุดประสงค์ทางการตลาดในการแจ้งให้ท่านทราบทุกครั้งที่มีข้อเสนอพิเศษ โปรโมชั่น และกิจกรรมต่าง ๆ</p><p>&nbsp;
&nbsp;
&nbsp;ท่านยอมรับว่าข้อมูลที่ท่านให้ไว้นั้นเทียบได้กับข้อมูลส่วนตัวหรือข้อมูลที่ละเอียดอ่อนภายใต้กฎหมายการป้องกันข้อมูล 1998 และเมื่อท่านใช้งานเว็บไซต์นี้ ถือว่าท่านยินยอมให้ใช้ข้อมูลของท่านภายใต้นโยบายความเป็นส่วนตัวนี้</p><p><strong>การเปิดเผยและการใช้ข้อมูล</strong></p><p>&nbsp;
&nbsp;
&nbsp;เมื่อท่านลงทะเบียนเข้าใช้งานกับเราถือว่าท่านยินยอมให้ข้อมูลส่วนบุคคลของท่านแก่เราและเราสามารถให้บุคคลที่สามในการช่วยจัดการจัดเก็บข้อมูลส่วนบุคคลนี้</p><p>ข้อมูลของท่านจะได้รับการนำไปใช้ดังต่อไปนี้ แต่อาจมีเพิ่มเติมได้ในภายหลัง</p><ol><li><p>เพื่อบริหารจัดการบัญชีและดำเนินการฝาก-ถอน บนเว็บไซต์ของเรา</p></li><li><p>เพื่อจัดเตรียมการให้บริการเดิมพัน</p></li><li><p>เพื่อจุดประสงค์ในการตรวจสอบและระบุตัวตน เช่น ใช้ยืนยันว่าท่านมีอายุไม่ต่ำกว่าที่กฎหมายกำหนดและไม่อยู่ในพื้นที่ต้องห้ามดังที่ระบุไว้ในข้อตกลงและเงื่อนไขของเว็บไซต์</p></li><li><p>เพื่อออกแบบเครื่องมือทางการตลาดที่ตรงตามความต้องการของท่าน</p></li><li><p>เพื่อใช้ในการตรวจสอบและปรับปรุงเว็บไซต์</p></li><li><p>เพื่อสังเกตการณ์และพัฒนาบริการของเว็บไซต์</p></li><li><p>เพื่อจัดการความเสี่ยง ตรวจจับการฉ้อฉล และขอความร่วมมือในการตรวจสอบการฟอกเงิน</p></li><li><p>เพื่อขอความร่วมมือตามที่กฎหมายและข้อบังคับกำหนด</p></li><li><p>เพื่อนำเสนอให้แก่บริษัทผู้จัดทำซอฟต์แวร์ (ซึ่งอยู่ภายใต้ข้อกำหนดของนโยบายนี้) ในการจัดหาความสนับสนุนและบริการที่เกี่ยวข้อง</p></li></ol><p>&nbsp;
&nbsp;
&nbsp;เราขอสงวนสิทธิ์ในการวิเคราะห์พฤติกรรมและลักษณะของผู้ใช้ด้านสถิติ เพื่อวัดสิ่งที่สนใจและการใช้งานเว็บไซต์จากหลากหลายด้าน อีกทั้งเพื่อแจ้งข้อมูลและเปิดเผยจำนวนผู้ใช้หรือจำนวนผู้คลิกแบนเนอร์โฆษณาให้แก่ผู้โฆษณา โดยเราจะแจ้งบุคคลที่ 3 เพียงผลงานโดยรวมจากการวิเคราะห์เท่านั้น ENJ911.COM เป็นสมาชิกของหลากหลายกลุ่มที่มีเป้าหมายในการปกป้องศีลธรรมจรรยาในการแข่งขันกีฬาและการเดิมพัน เพื่อป้องกันการชักใยเบื้องหลังการแข่งขัน ดังนั้น กลุ่มของเรา(<a href="https://ENJ911.COM/">ENJ911.COM</a> และกลุ่มที่มีส่วนเกี่ยวข้อง) ขอสงวนสิทธิ์ในการเข้าถึงและใช้ข้อมูลของท่านร่วมกัน เพื่อสังเกตพฤติกรรมการเดิมพัน ท่านสามารถเลือกรับข้อมูลที่เกี่ยวกับข้อเสนอโปรโมชั่นต่าง ๆ ได้ในหน้าลงทะเบียน ซึ่งหากท่านประสงค์จะเปลี่ยนแปลงไม่รับข้อมูลอัพเดตเหล่านี้ในภายหลัง ท่านสามารถแจ้งความประสงค์ผ่านเจ้าหน้าที่ฝ่ายบริการลูกค้าได้</p><p><strong>ชนะเดิมพัน</strong></p><p>&nbsp;
&nbsp;
&nbsp;เราขอสงวนสิทธิ์ไม่เปิดเผยข้อมูลของท่านแก่สาธารณะ ผลงานหรือเงินรางวัลที่ท่านชนะ เพื่อผลประโยชน์ทางการตลาด เราขอสงวนสิทธิ์ในการเปิดเผยข้อมูลในบางส่วน เช่น ตัวย่อยูสเซอร์เนม หรือชื่อย่อของท่าน</p><p><strong>ความปลอดภัย</strong></p><p>&nbsp;
&nbsp;
&nbsp;เราดูแลและเก็บรักษาข้อมูลของท่านไว้เป็นอย่างดีและปลอดภัย ด้วยเทคโนโลยีที่เปลี่ยนข้อความให้เป็นรหัสในการโยกย้ายข้อมูล ซึ่งเมื่อเก็บรักษาไว้ในเซิร์ฟเวอร์ของเราแล้ว ข้อมูลเหล่านี้จะถูกเก็บไว้อย่างปลอดภัยโดยเทคโนโลยีไฟร์วอล ทั้งเว็บไซต์และซอฟต์แวร์ของเราใช้เครื่องมือทุกอย่างในการรักษาความถูกต้องและความเป็นส่วนตัวของข้อมูล เพื่อป้องกันไม่ใช้ข้อมูลของท่านถูกนำไปใช้ในทางที่ผิดหรือเกิดการสูญหาย</p><p>&nbsp;
&nbsp;
&nbsp;เนื่องจากการติดต่อสื่อสารผ่านทางอินเตอร์เน็ตนั้นไม่ปลอดภัย 100% ทำให้เราไม่สามารถรับประกันความปลอดภัยในการส่งมอบข้อมูลของท่านได้ ดังนั้น เมื่อท่านใช้งานเว็บไซต์นี้จะถือว่าท่านยินยอมและยอมรับความเสี่ยงที่อาจเกิดขึ้นอันเกี่ยวเนื่องจากความปลอดภัยในการใช้งานอินเตอร์เน็ต และเราไม่รับผิดชอบต่อความสูญเสีย/เสียหายทั้งทางตรง ผลกระทบ โดยมิได้ตั้งใจ หรือที่เป็นการลงทัณฑ์ใด ๆ ในช่วงเวลาใดก็แล้วแต่</p><p>&nbsp;
&nbsp;
&nbsp;เมื่อพิจารณาจุดประสงค์ที่เราได้รับข้อมูลของท่านแล้ว เราจะเก็บรักษาข้อมูลของท่านเป็นระยะเวลาตามที่เห็นสมควร ซึ่งในบางกรณี เราอาจเก็บรักษาข้อมูลของท่านไปเรื่อย ๆ โดยไม่มีกำหนด</p><p><strong>คุกกี้</strong></p><p>&nbsp;
&nbsp;
&nbsp;ผู้เล่นควรทราบว่าข้อมูลส่วนบุคคลต่าง ๆ จะถูกเก็บและสะสมโดยอัตโนมัติระหว่างการดำเนินการลงทะเบียนผ่านการใช้คุกกี้ คุกกี้เป็นข้อมูลขนาดเล็กที่ส่งโดยเซิร์ฟเวอร์ของเว็บไซต์สู่เว็บเบราว์เซอร์ ซึ่งอนุญาติให้เซิร์ฟเวอร์เก็บรวบรวมข้อมูลจากเบราว์เซอร์ได้ เราใช้คุกกี้เพื่อติดตามลักษณะการใช้งานเบราว์เซอร์และสร้างโพรไฟล์ที่เกี่ยวกับสถิติจำนวนประชากรขึ้น การเฝ้าดูลักษณะการใช้งานเหล่านี้และการสะสมรวบรวมข้อมูล ทำให้เราสามารถพัฒนาบริการเพื่อนำเสนอแก่ท่านได้ ระบบพันธมิตรก็ใช้คุกกี้เป็นส่วนหนึ่งของการดำเนินงานพันธมิตรด้วยเช่นเดียวกัน</p><p>&nbsp;
&nbsp;
&nbsp;เบราว์เซอร์ส่วนใหญ่มีขั้นตอนที่เรียบง่ายที่อนุญาติให้ท่านปิดหรือเปิดใช้คุกกี้ หากท่านไม่ต้องการเก็บสะสมข้อมูลผ่านการใช้คุกกี้ ท่านสามารถตั้งค่าใหม่ได้ด้วยตนเอง และโปรดทราบว่าการปิดใช้คุกกี้นั้นอาจส่งผลกระทบต่อการบริการเฉพาะเจาะจงที่ตรงกับความต้องการของท่านเอง</p><p><strong>การทำลายข้อมูล</strong></p><p>&nbsp;
&nbsp;
&nbsp;สื่อทุกแขนงที่มีความเกี่ยวข้องกับเว็บไซต์ผู้สร้างเกมเดิมพันแบบอินเตอร์แอคทีฟทั้งหมดต้องถูกทำลาย โดยไม่สนใจว่าเป็นข้อมูลใด เพื่อรักษาความปลอดภัย</p><p><strong>การเข้าถึงข้อมูลและการอัพเดท</strong></p><p>&nbsp;
&nbsp;
&nbsp;ผู้ใช้สามารถเขียนความต้องการขอสำเนาข้อมูลส่วนตัวของท่านที่เราเก็บรักษาไว้และส่งมาตามการติดต่อด้างล่าง เรายินดีแจ้งข้อมูลส่วนบุคคลของท่านที่เรามีอยู่ โดยท่านสามารถแจ้งการอัพเดต เปลี่ยนแปลง และแก้ไขให้ถูกต้องได้ทาง เจ้าหน้าที่บริการลูกค้า ในการร้องขอของท่าน เราจะลบข้อมูลผู้ใช้ส่วนสรุปโดยย่อในฐานข้อมูลของเรา อย่างไรก็ตาม อาจจะเป็นไปไม่ได้ที่จะลบข้อมูลเดิมของท่านทั้งหมด เนื่องจากเราจัดเก็บข้อมูลสำรองและบันทึกการลบไว้</p><p>&nbsp;
&nbsp;
&nbsp;เราขอสงวนสิทธิ์ในการปรับปรุง เปลี่ยนแปลง หรือแก้ไขนโยบายนี้โดยมิต้องแจ้งให้ทราบล่วงหน้า หากมีการอัพเดตใดๆ เราจะถือว่าเป็นความรับผิดชอบของท่านในการตรวจสอบนโยบายส่วนบุคคลที่ได้รับการอัพเดตล่าสุด</p><p><strong>ติดต่อสอบถาม</strong></p><p>&nbsp;
&nbsp;
&nbsp;หากท่านมีข้อสงสัยหรือต้องการติดต่อสอบถามกรุณาติดต่อเราผ่านทาง แชทสด เบอร์โทรศัพท์ หรืออีเมลที่ให้ไว้ในเว็บได้ตลอดทุกวัน 24 ชม.</p></div>
            <br><br>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   function checkUser(){
      var username = $('#username').val();
      $.ajax({ 
         url: '<?php echo base_url(); ?>newuser/chkuseruselogin',
         type: 'POST',
         data : { username : username },  
         dataType: 'json',
         success: function (res) {
            if(res.status){
               $(document).ready(function(){ 
                  Swal.fire('Oops...', 'หมายเลขนี้ยังไม่มีในระบบ !!', 'warning');
               });
               $('#username').val('');
               $('#password').val('');                         
            }
         }
      });                 
   }    
</script>