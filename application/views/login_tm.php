<?php 
   $ss = $this->session->userdata();
   
?>
 <?php //debug($ss,true); ?>
<?php 
   $ss = $this->session->userdata();
   $s  = isset($data->cdate) ? date('d-m-Y',strtotime($data->cdate))  : date('d-m-Y');
   $d1 = explode('-', $s);
   $dd = $d1[2]."-".$d1[1]."-".$d1[0];
?>
<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>

<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>

<div id="__next">
   <div class="sc-9f4hzg-1 jGZAsY animate__animated animate__fadeIn animate__slower  3s">
      <div class="stbi-0 jVEzye bg-green justify-center">
      <h1 class=" mt-2">
         <a href="/">
            <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
               <div style="box-sizing: border-box; display: block; max-width: 100%;">
                  <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUwIiBoZWlnaHQ9IjU0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIvPg==" style="max-width: 100%; display: block;">
               </div>
               <img alt="" src="assets/images/logo.png" srcset="assets/images/logo.png" decoding="async" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
            </div>
         </a>
      </h1>
   </div>
   <p class="marquee" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
      <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บหวยออนไลน์ที่มาแรงที่สุดตอนนี้</span>
   </p>
</div>
<div class="text-center animate__animated animate__fadeIn animate__slower  3s container">
   <div class="justify-content-md-center row">
      <div class="col-lg-10">
         <div class="sc-9f4hzg-0 hJkCQZ">
            <div class="stbi-0 jVEzye bg-green justify-center">
               <h1 class=" mt-2">
                  <a href="/">
                     <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                        <div style="box-sizing: border-box; display: block; max-width: 100%;">
                           <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUwIiBoZWlnaHQ9IjU0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIvPg==" style="max-width: 100%; display: block;">
                        </div>
                        <img alt="" src="assets/images/logo.png" srcset="assets/images/logo.png" decoding="async" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                     </div>
                  </a>
               </h1>
            </div>
            <p class="marquee" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
               <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บหวยออนไลน์ที่มาแรงที่สุดตอนนี้</span>
            </p>
         </div>
         <div id="login" class="pb-0 mt-3 mb-3">
            <div class="card">
               <div class="text-left card-body">
                 <form  class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>login">
                     <div class="justify-content-center row">
                        <div class="col-lg-6 col-12">
                           <div class="form-group">
                              <input name="username" id="username" placeholder="หมายเลขโทรศัพท์" minlength="10" maxlength="10" required="" autocomplete="off" type="text" class="form-control"  onkeypress="return CharacterFormat(this,event,1);" value="">
                           </div>
                        </div>
                        <div class="col-lg-6 col-12">
                           <div class="form-group">
                              <input name="password" id="password" placeholder="รหัสผ่าน" minlength="4" maxlength="20" required="" type="password" class="form-control" value="">
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-7">
                           <div class="form-group">
                              <button type="button" class="pl-0  btn btn-link">
                                 <div class="float-left pl-3 form-check">
                                    <input type="checkbox" id="formBasicCheckbox" class="form-check-input" checked="">
                                    <label title="" for="formBasicCheckbox" class="form-check-label">จำฉันไว้ในระบบ</label>
                                 </div>
                              </button>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-5">
                           <div class="form-group">
                               <!-- <a href="<?php echo base_url();?>forgetpass" class="text-decoration-none text-danger text-right px-0 pr-0 btn btn-link" style="" >ลืมรหัสผ่าน</a> -->
                              <!-- <button type="button" class="text-decoration-none text-danger text-right px-0 pr-0 btn btn-link">ลืมรหัสผ่าน</button> -->
                           </div>
                        </div>
                        <div class="mb-3 col-lg-3 col-md-3 col-12 order-lg-1 order-md-1 order-2">
                          <!--  <button type="button" class="animate__animated animate__pulse animate__infinite animated-button1 btn btn-danger btn-block btn-lg">สมัครสมาชิก<span></span><span></span><span></span><span></span>
                           </button> -->
                           <a href="<?php echo base_url();?>register" class="animate__animated animate__pulse animate__infinite animated-button1 btn btn-danger btn-block btn-lg">สมัครสมาชิก<span></span><span></span><span></span><span></span></a>
                        </div>
                        <div class="mb-3 col-lg-3 col-md-3 col-12 order-lg-2 order-md-2 order-1">
                           <button type="submit" class="btn-green animate__animated animate__pulse animate__infinite animated-button1 btn btn-primary btn-block btn-lg" >เข้าสู่ระบบ<span></span><span></span><span></span><span></span>
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="row no-gutters">
               <!-- <div class="mt-2 pl-1 col-lg-6 col-md-6 col-sm-6 col-5">
                  <button type="button" class="text-white text-decoration-none px-0 text-left btn btn-link btn-block">อัตราการจ่าย</button>
               </div>
               <div class="mt-2 pr-1 col-lg-6 col-md-6 col-sm-6 col-7">
                  <button type="button" class="text-white text-decoration-none px-0 text-right btn btn-link btn-block" data-toggle="modal"  data-target="#modal_rule">กฎกติกาและข้อบังคับ</button>
               </div> -->

               <div class="mt-2 mb-2 px-lg-1 col-lg-3 col-12">
                  <button type="button" class="px-0 btn btn-success btn-block">คู่มือและวิธีการใช้งาน</button>
               </div>
               <div class="mt-2 mb-2 px-lg-1 col-lg-3 col-12">
                  <button type="button" class="px-0 btn-orange btn btn-success btn-block">นโยบายความเป็นส่วนตัว</button>
               </div>
               <!-- <div class="mb-2 pr-1 px-lg-1 col-lg-3 col-6">
                  <button type="button" class="btn-green btn btn-primary btn-block" data-toggle="modal" data-target="#modal_download">
                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" class="svg-inline--fa fa-download fa-w-16 mr-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                     </svg>
                     <span style="overflow: hidden;">ดาวน์โหลด</span>
                  </button>
               </div> -->
               <div class="mt-2 mb-2 pr-1 px-lg-1 col-lg-3 col-6">
                  <button type="button" class="btn-green btn btn-primary btn-block" data-toggle="modal" data-target="#modal_download">
                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" class="svg-inline--fa fa-download fa-w-16 mr-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path>
                     </svg>
                     <span style="overflow: hidden;">กฎกติกาและข้อบังคับ</span>
                  </button>
               </div>
               <div class="mt-2 mb-2 pl-1 col-lg-3 col-6">
                  <button type="button" class="btn-green btn btn-primary btn-block" data-toggle="modal" data-target="#modal_contact">
                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="headset" class="svg-inline--fa fa-headset fa-w-16 mr-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor" d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z"></path>
                     </svg>ติดต่อเรา
                  </button>
               </div>
            </div>
          
            <div class="mt-0 mb-4">
               <div class="autoplay" dir="ltr">
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/pgAct.jpeg" srcset="assets/images/pgAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/spadeAct.jpeg" srcset="assets/images/spadeAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/redAct.jpeg" srcset="assets/images/redAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/yggAct.jpeg" srcset="assets/images/yggAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/ambAct.jpeg" srcset="assets/images/ambAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/sexyAct.jpeg" srcset="assets/images/sexyAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
                  <div>
                     <div tabindex="-1" class="sc-1659p0g-0 cKhskH" style="width: 100%; display: inline-block;">
                        <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                           <div style="box-sizing: border-box; display: block; max-width: 100%;">
                              <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjM0MCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" style="max-width: 100%; display: block;">
                           </div>
                           <img alt="img" src="assets/images/kingmakerAct.jpeg" srcset="assets/images/kingmakerAct.jpeg" decoding="async" class="btn bg-transparent" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                        </div>
                     </div>
                  </div>
               </div>
               <span id="banner-movie">
                  <!-- <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['l_bank'])) ?  $data['l_bank']['value']  : "255x150.png"?>" alt="" width="100%" style="cursor: pointer;">
                  <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['l_bank'])) ?  $data['l_bank']['value']  : "255x150.png"?>" alt="" width="100%" style="cursor: pointer;"> -->
               </span>
            </div>
            <div class="mt-4">
               <!-- <div class="sc-12khjtj-1 iBWnNu">
                  <div class="sc-12khjtj-2 iLa-dcB  selectTab">ประกาศ</div>
                  <div class="sc-12khjtj-2 iLa-dcB  defaultTab">เลขเด็ด</div>
               </div> -->
               <div class="sc-12khjtj-0 gleQTT">
                  <div class="sc-12khjtj-4 dBEYHH">
                     <div style="width: 100%;">
                        <div class="sc-12khjtj-3 lbRzWc">
                           <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="newspaper" class="svg-inline--fa fa-newspaper fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="margin: 0px 8px;">
                              <path fill="currentColor" d="M552 64H88c-13.255 0-24 10.745-24 24v8H24c-13.255 0-24 10.745-24 24v272c0 30.928 25.072 56 56 56h472c26.51 0 48-21.49 48-48V88c0-13.255-10.745-24-24-24zM56 400a8 8 0 0 1-8-8V144h16v248a8 8 0 0 1-8 8zm236-16H140c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm208 0H348c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm-208-96H140c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm208 0H348c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm0-96H140c-6.627 0-12-5.373-12-12v-40c0-6.627 5.373-12 12-12h360c6.627 0 12 5.373 12 12v40c0 6.627-5.373 12-12 12z"></path>
                           </svg>
                           <span>ประกาศ</span>
                           <span style="font-size: 12px; margin: 7px;">ข่าวสารจากทีมงาน</span>
                        </div>
                        <div>
                           <div class="react-multi-carousel-list react-multi-carousel-track  autoplay_news">
                              <?php if($data['post1']['other'] == '1'):?>
                              <div data-index="0" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                 <div name="2" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post1'])) ?  $data['post1']['value']  : "255x150.png"?>" alt="News"  loading="lazy" style="width: 200px; height: 128px;  min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                   <!--  <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">สร้างรายได้ไปกับเราเพียงแนะนำเพื่อน แทงหวย เว็บ HUAYLIKE วันนี้!!รับเลยทันที 8% ฟรีๆ</span>
                                       <span class="dv90yj-7 YmTEp">แทงหวย</span><p class="sc-12khjtj-9 bhZmsW">9 พ.ย. 2563</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post2']['other'] == '1'):?>
                              <div data-index="1" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                 <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post2'])) ?  $data['post2']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                    <!-- <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">เงื่อนไขกติกา หากตรวจสอบพบว่าสมาชิกมีเจตนาทุจริตแอบแฝง</span>
                                       <span class="dv90yj-7 YmTEp">เนื่องจากปัจจุบันมีสมาชิกบางท่านได้กระทำผิดกฏกติกาทางบริษัท</span>
                                       <p class="sc-12khjtj-9 bhZmsW">27 พ.ย. 2563</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>
                                
                              <?php if($data['post3']['other'] == '1'):?>
                              <div data-index="2" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post3'])) ?  $data['post3']['value']  : "255x150.png"?>" alt="News" width="auto"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                    <!-- <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">หวยลาว หวยจากเพื่อนบ้านที่เล่นง่ายและออกบ่อยกว่าของไทย</span>
                                       <span class="dv90yj-7 YmTEp">หวยลาวตัวเลือกการเล่นหวย จากประเทศเพื่อนบ้านที่ทำให้คุณสามารถเล่นหวยได้มากกว่าที่เคย พร้อมอัตราต่อรองที่เร้าใจยิ่งหวยไทย หรือ สลากกินแบ่งรัฐบาล แล้วหวยลาวเล่นยังไง สามารถเล่นได้ที่ไหน วันนี้ HUAYLIKE&nbsp;จะพาคุณไปหาคำตอบ</span>
                                       <p class="sc-12khjtj-9 bhZmsW">16 ต.ค. 2563</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post4']['other'] == '1'):?>
                              <div data-index="3" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                 <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post4'])) ?  $data['post4']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                   <!--  <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">การอัพเดทลิงค์แนะนำเพื่อนรูปแบบใหม่ ป้องกันการถูกแบนจาก Facebook</span>
                                       <span class="dv90yj-7 YmTEp">Huaylike หวยอันดับ 1 ของคนไทย เจ้าใหญ่ จ่ายจริง แจ้งให้สมาชิกทุกท่านทราบเรื่องการอัพเดทลิงค์แนะนำเพื่อนรูปแบบใหม่ ป้องกันการถูกแบนจาก Facebook</span>
                                       <p class="sc-12khjtj-9 bhZmsW">17 ม.ค. 2564</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post5']['other'] == '1'):?>
                              <div data-index="4" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                 <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post5'])) ?  $data['post5']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                   <!--  <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">รับส่วนแบ่งสูงสุดในไทย AF Huaylike</span>
                                       <span class="dv90yj-7 YmTEp"></span>
                                       <p class="sc-12khjtj-9 bhZmsW">22 ม.ค. 2564</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post6']['other'] == '1'):?>
                              <div data-index="5" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post6'])) ?  $data['post6']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                    <!-- <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">ประกาศปิดปรับปรุงระบบเซิร์ฟเวอร์ วันที่ 24/3/2564</span>
                                       <span class="dv90yj-7 YmTEp">เวลา 04.00 - 07.00 น.</span>
                                       <p class="sc-12khjtj-9 bhZmsW">15 ก.พ. 2564</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post7']['other'] == '1'):?>
                              <div data-index="6" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                 <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post7'])) ?  $data['post7']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                     <!--   <span class="dv90yj-6 dBjKsU">เงื่อนไขการรับ โปรโมชั่นวันเกิด รับเครดิตฟรี 500 บาท</span>
                                       <span class="dv90yj-7 YmTEp">โปรวันเกิดมามาแล้ว!!</span>
                                       <p class="sc-12khjtj-9 bhZmsW">27 พ.ย. 2563</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post8']['other'] == '1'):?>
                              <div data-index="7" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post8'])) ?  $data['post8']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                    <!-- <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">แจ้งปรับเปลี่ยนเรทราคาจ่าย</span>
                                       <span class="dv90yj-7 YmTEp">Huaylike หวยอันดับ 1 ของคนไทย เจ้าใหญ่ จ่ายจริง มีการแจ้งปรับเปลี่ยนเรทราคาจ่าย </span>
                                       <p class="sc-12khjtj-9 bhZmsW">14 ม.ค. 2564</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else:?>
                              <?php endif?>

                              <?php if($data['post9']['other'] == '1'):?>
                              <div data-index="8" aria-hidden="true" class="react-multi-carousel-item  carousel-item-padding-40-px" style="flex: 1 1 auto; position: relative; width: 301px;">
                                <div name="5" class="sc-12khjtj-4 dBEYHH" style="justify-content: center;">
                                    <img src="<?php echo base_url();?>assets/uploads/<?php echo (isset($data['post9'])) ?  $data['post9']['value']  : "255x150.png"?>" alt="News"  loading="lazy"  style="width: 200px; height: 128px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                                      
                                   <!--  <div class="sc-12khjtj-8 kgDyYr">
                                       <span class="dv90yj-6 dBjKsU">สร้างรายได้ไปกับเราเพียงแนะนำเพื่อน แทงหวย เว็บ HUAYLIKE วันนี้!!รับเลยทันที 8% ฟรีๆ</span>
                                       <span class="dv90yj-7 YmTEp">แทงหวย</span>
                                       <p class="sc-12khjtj-9 bhZmsW">9 พ.ย. 2563</p>
                                    </div> -->
                                 </div>
                              </div>
                              <?php else: ?>
                              <?php endif?>  

                            
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="sc-12khjtj-4 dBEYHH"></div>
               </div>
            </div>
         </div>
         <div style="margin-top:0" class="sc-9f4hzg-0 hJkCQZ">
            <div class="o37fw8-1 oTMwd bg-green mt-0-4">
               <div class="text-center">
                  <h3 class="mb-0" style="cursor: pointer; font-size: 1rem; line-height: 70px;">2021 COPYRIGHT <?php echo $website;?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div style="margin-top:0" class="sc-9f4hzg-1 jGZAsY">
      <div class="o37fw8-1 oTMwd bg-green mt-0-4">
         <div class="text-center">
            <h3 class="mb-0" style="cursor: pointer; font-size: 1rem; line-height: 70px;">2021 COPYRIGHT <?php echo $website;?></h3>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="modal_download"><div class="sc-1de8oot-0 hXpDUX"><div class="sc-1de8oot-1 hNmSUG pr-2 pl-2 " style="max-width: 480px;"><div class="p-2 text-light bg-danger" style="cursor: pointer;"><span style="font-size: 1rem;" data-dismiss="modal">ปิด</span></div><img src="assets/images/app.jpg" class="sc-1de8oot-4 dyRFWT p-0"><div class="p-2 text-light" style="background-color: rgb(56, 193, 114); cursor: pointer;"><span style="font-size: 1.2rem;">ดาวน์โหลด</span></div></div></div>
</div>
<div class="modal" id="modal_rule">
   <div class="sc-1de8oot-0 hXpDUX"><div class="sc-1de8oot-1 hNmSUG"><div class="sc-1de8oot-3 cyyUrM"><h4>กฏและกติกา</h4><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" style="font-size: 1.4rem; cursor: pointer;" data-dismiss="modal"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></div><div class="sc-1de8oot-2 cIBFsd bg-light text-left pr-4 pl-4 pb-1 pt-4"><div class="pb-0"><p>ยินดีต้อนรับสู่หวยออนไลน์&nbsp;เพื่อให้ท่านทราบถึงสิทธิประโยชน์ของสมาชิกกรุณาอ่านข้อตกลงอย่างละเอียดก่อนที่จะลงทะเบียนสมัครเป็นสมาชิกและใช้บริการ <a href="Huaylike.com">Huaylike.com </a> เมื่อสมัครสมาชิกแล้วทางเว็บจะถือว่า สมาชิกได้อ่านและยอมรับ&nbsp;"<strong>นโยบายและเงื่อนไขการให้บริการของเว็บไซต์</strong>"</p><p><strong>การรักษาและช่วยเหลือการเดิมพันออนไลน์</strong></p><p>ถ้าสมาชิกหลุดออกจากเว็บไซต์ระหว่างการเดิมพัน บัญชีของท่านจะไม่มีผลกระทบต่อผลลัพธ์ของการเดิมพัน
ไม่ต้องวิตกกังวล ถ้าสมาชิกออกจากเว็บไซด์ระหว่างการเดิมพัน สมาชิกจะถูกออกจากระบบช่วงเวลาหนึ่ง จากนั้นสมาชิกสามารถเข้าสู่ระบบ  <a href="Huaylike.com">Huaylike.com </a>&nbsp;ได้อีกครั้งตามปกติ</p><p><strong>การให้ความคุ้มครอง</strong></p><p><a href="Huaylike.com">Huaylike.com </a>&nbsp;ได้มีการจัดเตรียม ระบบที่มีประสิทธิภาพ ความรวดเร็วและการบริการลูกค้าที่เป็นมิตร ถ้าหากท่านมีคำถามหรือคำแนะนำใด ๆ กรุณาอย่าลังเลใจที่จะโทรศัพท์มาทางฝ่ายบริการลูกค้า ซึ่งจะมีให้บริการตลอด 24 ชั่วโมง ทุกวันไม่มีวันหยุดและทางเราจะตอบกลับอย่างรวดเร็วที่สุด&nbsp; <a href="Huaylike.com">Huaylike.com </a>&nbsp;ขอรับรองว่าจะรักษาข้อมูลส่วนตัวของท่านไว้เป็นความลับอย่างปลอดภัย</p><p><strong>ข้อตกลงในการเดิมพัน</strong></p><p>เพื่อหลีกเลี่ยงปัญหาต่างๆในขณะที่ใช้งานเว็บไซต์ สมาชิกกรุณาอ่านกฎกติกาของบริษัทอย่างละเอียดและระมัดระวัง เมื่อสมาชิกเข้าสู่หน้าเดิมพันทางบริษัทจะถือว่าท่านได้ยอมรับข้อตกลงของทาง&nbsp; <a href="Huaylike.com">Huaylike.com </a>
การเดิมพันจะต้องอยู่ใน&nbsp;"ระยะเวลาที่กำหนด"&nbsp;มิฉะนั้นจะถือว่าการเดิมครั้งนั้นเป็น&nbsp;"<strong>โมฆะ</strong>"&nbsp;ถ้าการเดิมพันได้มีการยกเลิกหรือหยุดพักชั่วคราว เนื่องจากเหตุผลใดๆก็ตามก่อนการเล่น การเดิมพันครั้งนั้นจะถือว่าเป็น"โมฆะ"และทางบริษัทจะคืนเครดิตให้กับสมาชิก
เป็นความรับผิดชอบของสมาชิกที่จะคอยดูผลลัพธ์ จากช่องหน้าต่างที่แสดงให้เห็นถึงผลการเดิมพันแพ้หรือชนะ ทางบริษัทจะยึดถือข้อมูล&nbsp;"รายละเอียดการวางเดิมพัน"&nbsp;ของสมาชิก ในกรณีที่สมาชิกมีความสงสัย และต้องการตรวจสอบข้อมูลในการคำนวณของการเดิมพันนั้นๆ
ถ้าหากเกิดความผิดพลาดของระบบระหว่างการเดิมพันหรือความผิดพลาดโดยไม่ได้เจตนาของพนักงาน ทางบริษัทขอสงวนสิทธ์ที่จะแก้ไขผลลัพธ์ให้ถูกต้อง และจะมีการพิมพ์ข้อความที่ได้มีการแก้ไขเผยแพร่ไว้ที่แถบตัววิ่งในหน้าเว็บไซต์ บริษัทจะไม่มีการแจ้งสมาชิกเป็นการส่วนตัว
เป็นความรับชอบของสมาชิกที่จะทำให้แน่ใจว่าชื่อผู้ใช้และรหัสผ่านสำหรับเข้าเว็บไซต์นั้นถูกต้อง และกรุณาเปลี่ยนรหัสผ่านของสมาชิกอย่างน้อยเดือนละครั้ง เพื่อความปลอดภัย ถ้าหากท่านพบหรือสงสัยว่ามีผู้อื่นได้เข้าไปเล่นในบัญชีของท่านโดยไม่รับอนุญาต กรุณา แจ้งตัวแทนของท่านทันทีเพื่อเปลี่ยนรหัสส่วนตัว (หากมีการเดิมพันก่อนที่จะเปลี่ยนรหัสเดิมจะถือว่าการเดิมพันนั้นเป็นผล)
ก่อนเริ่มการเดิมพันแต่ละครั้ง ท่านสมาชิกควรจะตรวจสอบวงเงินของท่านเสียก่อน ถ้าหากท่านมีข้อสงสัยใดๆ เกี่ยวกับวงเงิน กรุณาแจ้งตัวแทนของท่านสมาชิกทันที
ในกรณีที่เหตุการณ์ไม่คาดคิดเกิดขึ้น อย่างเช่น ข้อมูลสูญหายเนื่องจากระบบอินเตอร์เน็ตขัดข้อง ทางบริษัทจะมีการประกาศให้รู้ถึงสาเหตุ และวิธีการแก้ปัญหา
ชั่วโมงการให้บริการ 24 ชั่วโมง/วัน และไม่มีวันหยุด
ระบบการรักษาความปลอดภัย</p><p>ข้อมูลทางด้านบัญชีเพื่อวัตถุประสงค์เฉพาะบุคคล ข้อมูลที่ทางสมาชิกให้มาในการเปิดบัญชีจะถูกเก็บเป็นความลับขั้นสูงสุด ทางบริษัทจะไม่เผยแพร่ข้อมูลนี้ให้กับบุคคลที่สามหรือองค์กรอื่นๆ ล่วงรู้ ไม่ว่าจะเป็นบัญชีอีเมล&nbsp;หรือข้อมูลอื่นๆ ที่ทางสมาชิกกรอกในใบสมัคร ข้อมูลเหล่านี้จะใช้ในจุดมุ่งหมายที่ตั้งใจเอาไว้ให้เท่านั้น เช่น การส่งเช็คเงินสดสำหรับผู้ชนะหรือข้อมูลอื่นๆ ตามที่สมาชิกร้องขอ
ความปลอดภัยทางบริษัทได้ใช้วิธีที่มีประสิทธิภาพมากที่สุดเพื่อทำให้ท่านแน่ใจในความปลอดภัยของข้อมูลของท่าน ทางเราจะพยายามให้ดีที่สุดที่จะรักษาความถูกต้องแน่นอน และความลับเฉพาะของข้อมูล เพื่อป้องกันไม่ให้ข้อมูลรั่วไหลหรือนำไปใช้อย่างไม่ถูกต้อง ถ้าท่านมีข้อสงสัยใดๆ ที่เกี่ยวข้องกับการรักษาความลับ ความปลอดภัยของข้อมูล กรุณาติดต่อฝ่ายบริการลูกค้า พวกเรายินดีให้บริการ 24 ชั่วโมง/วัน ทุกวันไม่มีวันหยุด
บริษัทมีความยินดีเป็นอย่างยิ่งที่ได้รับใช้ท่าน</p></div></div><div class="sc-1de8oot-2 cIBFsd bg-light text-left pt-0 pr-4 pl-4 pb-4 pt-4"><button type="button" class="btn btn-light btn-block" style="background-color: rgb(0, 191, 161); color: rgb(255, 255, 255);">ฉันเข้าใจและยอมรับ</button></div></div><br><br></div>
</div>
<div class="modal" id="modal_contact">
   <div class="sc-1de8oot-0 hXpDUX">
      <div class="sc-1de8oot-1 hNmSUG pr-2 pl-2 ">
         <div class="sc-1de8oot-3 cyyUrM" style="max-width: 480px;">
            <h4>เลือกช่องทางการติดต่อเรา</h4>
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" style="font-size: 1.4rem; cursor: pointer;" data-dismiss="modal">
               <path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
            </svg>
         </div>
         <div class="sc-1de8oot-2 cIBFsd p-3 bg-light" style="max-width: 480px;">
            <button type="button" class="mb-2 btn btn-success btn-block">ติดต่อผ่านไลน์</button>
            <div class="accordion">
               <button type="button" class="btn-block btn btn-danger">ติดต่อผ่านโทรศัพท์</button>
               <div class="collapse">
                  <div class="pb-0 card-body">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                           <span><b>เบอร์ติดต่อ 1 </b></span>
                           <button type="button" class="btn btn-success btn-block" style="background-color: rgb(0, 191, 161);">098-731-0564</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                           <span><b>เบอร์ติดต่อ 2 </b></span>
                           <button type="button" class="btn btn-success btn-block" style="background-color: rgb(0, 191, 161);">092-425-6870</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   function checkUser(){
      var username = $('#username').val();
      $.ajax({ 
         url: '<?php echo base_url(); ?>newuser/chkuseruselogin',
         type: 'POST',
         data : { username : username },  
         dataType: 'json',
         success: function (res) {
            if(res.status){
               $(document).ready(function(){ 
                  Swal.fire('Oops...', 'หมายเลขนี้ยังไม่มีในระบบ !!', 'warning');
               });
               $('#username').val('');
               $('#password').val('');                         
            }
         }
      });                 
   }    
</script>