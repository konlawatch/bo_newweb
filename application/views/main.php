<?php 
   $ss = $this->session->userdata();
   $d  = json_decode(decode($ss['isLoggedIn'],$this->key),true);
?>
<div class="sc-5g65yh-0 dxeklP">
   <div class=" d-flex justify-content-between pl-2 pr-2">
      <span>จำนวนเครดิต</span> <h1 style="font-size:1rem">ID : <?php echo $ss['data']['userid'];?></h1>
   </div>
   <div class="d-flex text-center justify-content-center pt-2">
      <span><h3 class="pr-2 text-light"><span><b id="sync_credit"><?php echo number_format($d['credit'],2);?> บาท</b></span></h3></span>
      <div>
         <div class="sc-5g65yh-1 jnuzDq mt-2" onclick="get_credit(this,'IMI');" id="sync_balance_btn">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sync" class="svg-inline--fa fa-sync fa-w-16 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M440.65 12.57l4 82.77A247.16 247.16 0 0 0 255.83 8C134.73 8 33.91 94.92 12.29 209.82A12 12 0 0 0 24.09 224h49.05a12 12 0 0 0 11.67-9.26 175.91 175.91 0 0 1 317-56.94l-101.46-4.86a12 12 0 0 0-12.57 12v47.41a12 12 0 0 0 12 12H500a12 12 0 0 0 12-12V12a12 12 0 0 0-12-12h-47.37a12 12 0 0 0-11.98 12.57zM255.83 432a175.61 175.61 0 0 1-146-77.8l101.8 4.87a12 12 0 0 0 12.57-12v-47.4a12 12 0 0 0-12-12H12a12 12 0 0 0-12 12V500a12 12 0 0 0 12 12h47.35a12 12 0 0 0 12-12.6l-4.15-82.57A247.17 247.17 0 0 0 255.83 504c121.11 0 221.93-86.92 243.55-201.82a12 12 0 0 0-11.8-14.18h-49.05a12 12 0 0 0-11.67 9.26A175.86 175.86 0 0 1 255.83 432z"></path></svg>
         </div>
      </div>
   </div>
   <div class="d-flex justify-content-center">
      <button class="sc-5g65yh-2 eYxMlR full-width" style="background-color:#00bfa1;border:none" onclick="goMenu('deposit');">
         <div style="background-color:#fff" class="sc-5g65yh-1 jnuzDq">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="color:#00c19e">
               <path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
            </svg>
         </div>
         <span class="ml-2">เติมเงิน</span>
      </button>
      <button class="sc-5g65yh-2 eYxMlR full-width" style="background-color:#fe0000" onclick="goMenu('withdraw');">
         <div style="background-color:#fff" class="sc-5g65yh-1 jnuzDq">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="minus" class="svg-inline--fa fa-minus fa-w-14 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="color:#fe0000">
               <path fill="currentColor" d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
            </svg>
         </div>
         <span class="ml-2">ถอนเงิน</span>
      </button>
   </div>
</div>

<div class="animate__animated animate__fadeInDown mt-lg-3">
   <div class="mt-3 mb-3 animate__animated animate__fadeIn row">
      <?php if(isset($d['user_imi'])):?>
         <?php if($d['user_imi'] == 'pending'):?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <img src="<?php echo base_url();?>assets/images/imi_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                  <h5 class="py-2  text-light">กำลังตรวจสอบ</h5>
               </div>
            </div>
         <?php else:?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);" onclick="partnerInfo('imi');">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <?php if($d['user_imi'] != ''):?>
                     <img src="<?php echo base_url();?>assets/images/imi_icon.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">IMI</h5>
                  <?php else:?>
                     <img src="<?php echo base_url();?>assets/images/imi_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">เปิดรหัส</h5>
                  <?php endif;?>
               </div>
            </div>
         <?php endif;?>
      <?php else:?>
         <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
            <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
               <img src="<?php echo base_url();?>assets/images/imi_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
               <h5 class="py-2  text-light">เร็วๆ นี้ </h5>
            </div>
         </div>
      <?php endif;?>
      <?php if(isset($d['user_ts'])):?>
         <?php if($d['user_ts'] == 'pending'):?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <img src="<?php echo base_url();?>assets/images/ts911_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                  <h5 class="py-2  text-light">กำลังตรวจสอบ</h5>
               </div>
            </div>
         <?php else:?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);" onclick="partnerInfo('ts911');">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <?php if($d['user_ts'] != ''):?>
                     <img src="<?php echo base_url();?>assets/images/ts911_icon.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">TS911</h5>
                  <?php else:?>
                     <img src="<?php echo base_url();?>assets/images/ts911_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">เปิดรหัส</h5>
                  <?php endif;?>
               </div>
            </div>
         <?php endif;?>
      <?php else:?>
         <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
            <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
               <img src="<?php echo base_url();?>assets/images/ts911_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
               <h5 class="py-2  text-light">เร็วๆ นี้ </h5>
            </div>
         </div>
      <?php endif;?>
      <?php if(isset($d['user_lsm'])):?>
         <?php if($d['user_lsm'] == 'pending'):?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <img src="<?php echo base_url();?>assets/images/lsm_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                  <h5 class="py-2  text-light">กำลังตรวจสอบ</h5>
               </div>
            </div>
         <?php else:?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);" onclick="partnerInfo('lsm');">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <?php if($d['user_lsm'] != ''):?>
                     <img src="<?php echo base_url();?>assets/images/lsm_icon.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">LSM99</h5>
                  <?php else:?>
                     <img src="<?php echo base_url();?>assets/images/lsm_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">เปิดรหัส</h5>
                  <?php endif;?>
               </div>
            </div>
         <?php endif;?>
      <?php else:?>
         <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
            <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
               <img src="<?php echo base_url();?>assets/images/lsm_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
               <h5 class="py-2  text-light">เร็วๆ นี้ </h5>
            </div>
         </div>
      <?php endif;?>
      <?php if(isset($d['user_ufa'])):?>
         <?php if($d['user_ufa'] == 'pending'):?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <img src="<?php echo base_url();?>assets/images/ufa_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                  <h5 class="py-2  text-light">กำลังตรวจสอบ</h5>
               </div>
            </div>
         <?php else:?>
            <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);" onclick="partnerInfo('ufa');">
               <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
                  <?php if($d['user_ufa'] != ''):?>
                     <img src="<?php echo base_url();?>assets/images/ufa_icon.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">UFABET</h5>
                  <?php else:?>
                     <img src="<?php echo base_url();?>assets/images/ufa_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
                     <h5 class="py-2  text-light">เปิดรหัส</h5>
                  <?php endif;?>
               </div>
            </div>
         <?php endif;?>
      <?php else:?>
         <div class="animate__animated animate__bounceIn col-lg-3" style="filter: drop-shadow(rgb(0, 0, 0) 4px 4px 4px);">
            <div class="sc-1ma5cn6-1 cbXZwp mb-3 glow-on-hover">
               <img src="<?php echo base_url();?>assets/images/ufa_icon_close.jpg" alt="game" class="sc-1ma5cn6-2 bEDLoS">
               <h5 class="py-2  text-light">เร็วๆ นี้ </h5>
            </div>
         </div>
      <?php endif;?>
   </div>
</div>
<div class="text-center" id="partner_info">
   
</div>

<div class="sc-5g65yh-3 kOObDs pt-2 pb-3 mt-3">
   <div style="width:100% !important;margin-left:2px;margin-right:2px" class="row">
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" onclick="goMenu('statement');">
            <img width="100%" height="100%" src="assets/images/dep-daw.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu"><span class="sc-1qrnk4o-10 gBUvoq">ฝาก-ถอน</span>
         </div>
      </div>
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" onclick="goMenu('invite');">
            <img width="100%" height="100%" src="assets/images/chat-group.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">ระบบแนะนำเพื่อน</span>
         </div>
      </div>
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" data-toggle="modal" data-target="#modal_contact">
            <img width="100%" height="100%" src="assets/images/contact.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu"><span class="sc-1qrnk4o-10 gBUvoq">ติดต่อเรา</span>
         </div>
      </div>
      
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-7 cpoYdY animate__animated animate__pulse animate__infinite" onclick="goMenu('promotion');">
            <img width="100%" height="100%" src="assets/images/HL_PROMOTION.gif" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">โปรโมชั่น</span>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="modal_contact">
   <div class="sc-1de8oot-0 hXpDUX">
      <div class="sc-1de8oot-1 hNmSUG pr-2 pl-2 ">
         <div class="sc-1de8oot-3 cyyUrM" style="max-width: 480px;">
            <h4>เลือกช่องทางการติดต่อเรา</h4>
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" style="font-size: 1.4rem; cursor: pointer;" data-dismiss="modal">
               <path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
            </svg>
         </div>
         <div class="sc-1de8oot-2 cIBFsd p-3 bg-light" style="max-width: 480px;">
            <button type="button" class="mb-2 btn btn-success btn-block">ติดต่อผ่านไลน์</button>
            <div class="accordion">
               <button type="button" class="btn-block btn btn-danger">ติดต่อผ่านโทรศัพท์</button>
               <div class="collapse">
                  <div class="pb-0 card-body">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                           <span><b>เบอร์ติดต่อ 1 </b></span>
                           <button type="button" class="btn btn-success btn-block" style="background-color: rgb(0, 191, 161);">098-731-0564</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                           <span><b>เบอร์ติดต่อ 2 </b></span>
                           <button type="button" class="btn btn-success btn-block" style="background-color: rgb(0, 191, 161);">092-425-6870</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">

   var cnt = 0;
   var token = '<?php echo $ss['data']['token'];?>';

   // cntTimecn();
   
   function cntTimecn(){
      if(cnt == 0){
         var int = setInterval(function(){ 
      if(cnt <= 0){
         cnt = 60;
         int = '';
           info();
      }else{
         cnt--;
      }
         $('#cnt').html(cnt);
      }, 1000*1);
         cnt++;
      }
   }  

   function get_credit(e,partner){
      $('#sync_credit').html('กรุณารอสักครู่...');
      $('#sync_balance_btn').html('<i class="fa fa-spinner fa-pulse fa-fw "></i>');
      $('#sync_balance_btn').prop('disabled', true);

      $.ajax({
         type: 'GET',
         url: '<?php echo base_url();?>service/get_balance',
         data: { partner : partner },
         dataType: "json",
         success: function(data, dataType, state){
            $('#sync_balance_btn').html('<i class="fas fa-sync my-0"></i>');
            $('#sync_balance_btn').prop('disabled', false);        
            if(data.status){
               $('#sync_credit').html(data.data + ' บาท');
            }else {
               Swal.fire('Opps...', data.msg, 'error');
               $('#sync_credit').html('0.00 บาท');
            }
         }
      });
   }

   function partnerInfo(p){  
      $('#partner_info').html('<span class="x-loading">กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw "></i></span>');

      $.ajax({
         url: '<?php echo base_url();?>partner',
         type: 'GET',
         data : { 'partner' : p},
         dataType: 'json',
         success: function (res) {
            if(res.status == true){
               $('#partner_info').html(res.data);
               $('#partner_info').fadeIn();
            }else{
               alert(res.msg);
            }
         }
      });

   }

</script>     