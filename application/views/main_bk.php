<section class="wallet">
   <div class="wallet-circle">
      <div class="swiper-container swiper-wallet">
         <div class="swiper-wrapper">
            <div class="swiper-slide">
               <small>เครดิตคงเหลือ</small>
               <a href="<?php echo base_url();?>/transaction" class="wallet-money">฿ <span id="total_balance">กำลังโหลด...</span></a>
            </div>
         </div>
      </div>
      <a href="javascript:void(0)" class="btn-refresh">
         <i class="far fa-sync-alt"></i>
      </a>
   </div>
</section>
<section class="main-menu">
   <div class="container">
      <div class="row">
         <div class="col-4">
            <div class="two_third">
               <div class="button-container">
                  <!-- <a href="#"></a> -->
                  <img src="./assets/images/logots911h.png"/>
               </div>
            </div>
         </div>
         <div class="col-4">
            <div class="two_third">
               <div class="button-container">
                  <!-- <a href="#"></a> -->
                  <img src="./assets/images/logolsm99h.png"/>
               </div>
            </div>
         </div>
         <div class="col-4">
            <div class="two_third">
               <div class="button-container">
                  <!-- <a href="#"></a> -->
                  <img src="./assets/images/logoimiwinh.png"/>
               </div>
            </div>
         </div>
      </div>
      <div class="row" style="margin-top:10px;">
         <div class="col-3">
            <a href="<?php echo base_url();?>/deposit">
               <i class="fal fa-wallet fa-2x"></i><br>
               <span>เติมเงิน</span>
            </a>
         </div>
         <div class="col-3">
            <a href="<?php echo base_url();?>/withdrawal">
               <i class="fal fa-usd-circle fa-2x"></i><br>
               <span>ถอนเงิน</span>
            </a>
         </div>
         <div class="col-3">
            <a href="<?php echo base_url();?>/bonus">
               <i class="fal fa-gift fa-2x"></i><br>
               <span>โบนัส</span>
            </a>
         </div>
         <div class="col-3">
            <a href="<?php echo base_url();?>/transaction">
               <i class="fal fa-history fa-2x"></i><br>
               <span>ประวัติ</span>
            </a>
         </div>
      </div>
      <div class="row" style="margin-top:10px;">
         <div class="col-3">
            <a href="<?php echo base_url();?>/profile">
               <i class="fal fa-user fa-2x"></i><br>
               <span>บัญชีผู้ใช้</span>
            </a>
         </div>
         <div class="col-3">
            <a href="<?php echo base_url();?>/transaction">
               <i class="fal fa-university fa-2x"></i><br>
               <span>ธนาคาร</span>
            </a>
         </div>
         <div class="col-3">
            <a href="<?php echo base_url();?>/transaction">
               <i class="fal fa-question-circle fa-2x"></i><br>
               <span>วิธีใช้งาน</span>
            </a>
         </div>
         <div class="col-3">
            <a target="_blank" href="http://line.me/R/ti/p/@lineid">
               <i class="fal fa-comments fa-2x"></i><br>
               <span>แชทสด</span>
            </a>
         </div>
      </div>
   </div>
</section>
<!-- <section class="product">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <img src="<?php echo base_url();?>/assets/images/promotion.png" class="img-fluid">
         </div>
      </div>
   </div>
</section> -->
<section class="btn-play-game">
   <div class="container">
      <button onclick="playgame()" class="btn form-control btn-primary-lg" style="height:auto;"><i class="fas fa-dice"></i> เข้าเล่น</button>
   </div>
</section>

<div class="whzjgj-6 dBPUzO mt-2 mb-1" style="border-radius: 4px;"><button class="whzjgj-7 YTeyD refActive"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chart-bar" class="svg-inline--fa fa-chart-bar fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="font-size: 1.4rem;"><path fill="currentColor" d="M332.8 320h38.4c6.4 0 12.8-6.4 12.8-12.8V172.8c0-6.4-6.4-12.8-12.8-12.8h-38.4c-6.4 0-12.8 6.4-12.8 12.8v134.4c0 6.4 6.4 12.8 12.8 12.8zm96 0h38.4c6.4 0 12.8-6.4 12.8-12.8V76.8c0-6.4-6.4-12.8-12.8-12.8h-38.4c-6.4 0-12.8 6.4-12.8 12.8v230.4c0 6.4 6.4 12.8 12.8 12.8zm-288 0h38.4c6.4 0 12.8-6.4 12.8-12.8v-70.4c0-6.4-6.4-12.8-12.8-12.8h-38.4c-6.4 0-12.8 6.4-12.8 12.8v70.4c0 6.4 6.4 12.8 12.8 12.8zm96 0h38.4c6.4 0 12.8-6.4 12.8-12.8V108.8c0-6.4-6.4-12.8-12.8-12.8h-38.4c-6.4 0-12.8 6.4-12.8 12.8v198.4c0 6.4 6.4 12.8 12.8 12.8zM496 384H64V80c0-8.84-7.16-16-16-16H16C7.16 64 0 71.16 0 80v336c0 17.67 14.33 32 32 32h464c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path></svg><br>ภาพรวม</button><button class="whzjgj-7 YTeyD reDeactive"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-friends" class="svg-inline--fa fa-user-friends fa-w-20 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" style="font-size: 1.4rem;"><path fill="currentColor" d="M192 256c61.9 0 112-50.1 112-112S253.9 32 192 32 80 82.1 80 144s50.1 112 112 112zm76.8 32h-8.3c-20.8 10-43.9 16-68.5 16s-47.6-6-68.5-16h-8.3C51.6 288 0 339.6 0 403.2V432c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48v-28.8c0-63.6-51.6-115.2-115.2-115.2zM480 256c53 0 96-43 96-96s-43-96-96-96-96 43-96 96 43 96 96 96zm48 32h-3.8c-13.9 4.8-28.6 8-44.2 8s-30.3-3.2-44.2-8H432c-20.4 0-39.2 5.9-55.7 15.4 24.4 26.3 39.7 61.2 39.7 99.8v38.4c0 2.2-.5 4.3-.6 6.4H592c26.5 0 48-21.5 48-48 0-61.9-50.1-112-112-112z"></path></svg><br>สมาชิก</button><button class="whzjgj-7 YTeyD reDeactive"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="credit-card" class="svg-inline--fa fa-credit-card fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="font-size: 1.4rem;"><path fill="currentColor" d="M0 432c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V256H0v176zm192-68c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H204c-6.6 0-12-5.4-12-12v-40zm-128 0c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H76c-6.6 0-12-5.4-12-12v-40zM576 80v48H0V80c0-26.5 21.5-48 48-48h480c26.5 0 48 21.5 48 48z"></path></svg><br>รายได้</button><button class="whzjgj-7 YTeyD reDeactive"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="hand-holding-usd" class="svg-inline--fa fa-hand-holding-usd fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" style="font-size: 1.4rem;"><path fill="currentColor" d="M271.06,144.3l54.27,14.3a8.59,8.59,0,0,1,6.63,8.1c0,4.6-4.09,8.4-9.12,8.4h-35.6a30,30,0,0,1-11.19-2.2c-5.24-2.2-11.28-1.7-15.3,2l-19,17.5a11.68,11.68,0,0,0-2.25,2.66,11.42,11.42,0,0,0,3.88,15.74,83.77,83.77,0,0,0,34.51,11.5V240c0,8.8,7.83,16,17.37,16h17.37c9.55,0,17.38-7.2,17.38-16V222.4c32.93-3.6,57.84-31,53.5-63-3.15-23-22.46-41.3-46.56-47.7L282.68,97.4a8.59,8.59,0,0,1-6.63-8.1c0-4.6,4.09-8.4,9.12-8.4h35.6A30,30,0,0,1,332,83.1c5.23,2.2,11.28,1.7,15.3-2l19-17.5A11.31,11.31,0,0,0,368.47,61a11.43,11.43,0,0,0-3.84-15.78,83.82,83.82,0,0,0-34.52-11.5V16c0-8.8-7.82-16-17.37-16H295.37C285.82,0,278,7.2,278,16V33.6c-32.89,3.6-57.85,31-53.51,63C227.63,119.6,247,137.9,271.06,144.3ZM565.27,328.1c-11.8-10.7-30.2-10-42.6,0L430.27,402a63.64,63.64,0,0,1-40,14H272a16,16,0,0,1,0-32h78.29c15.9,0,30.71-10.9,33.25-26.6a31.2,31.2,0,0,0,.46-5.46A32,32,0,0,0,352,320H192a117.66,117.66,0,0,0-74.1,26.29L71.4,384H16A16,16,0,0,0,0,400v96a16,16,0,0,0,16,16H372.77a64,64,0,0,0,40-14L564,377a32,32,0,0,0,1.28-48.9Z"></path></svg><br>ถอนรายได้</button></div>
