<?php $ss = $this->session->userdata();?>
<?php //debug($ss,true);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style-n.css"  />
<?php
date_default_timezone_set('Asia/Bangkok');

##=========================================================================================

//util class
class Util {

    public static function cUrl($url, $method = "get", $data = "", $ssl = false){
        if ($method == "post"){
            if ($data == "") return false;
        }
        $ch = curl_init();
        if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
        else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post"){
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }
};

$curl = Util::cUrl('https://demoapi.botbo21.com/api/getkey?sign=C5Z10zzL4M7BiOSmEgyoAcnw5g38CvO2','get');

$key = json_decode($curl,true);
//debug($key,true);

?>
<div class="sc-5g65yh-0 dxeklP">
   <div class=" d-flex justify-content-between pl-2 pr-2">
      <!-- <span>จำนวนเครดิต</span> -->
      <h1 style="font-size:1rem">ID : <?php echo $ss['data']['userid'];?></h1>
   </div>
   <div class="d-flex text-center justify-content-center pt-2"></div>
   <div class="d-flex justify-content-center">
      <button class="sc-5g65yh-2 eYxMlR full-width" style="background-color:#00bfa1;border:none" onclick="goMenu('deposit');">
         <div style="background-color:#fff" class="sc-5g65yh-1 jnuzDq">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="color:#00c19e">
               <path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
            </svg>
         </div>
         <span class="ml-2">เติมเงิน</span>
      </button>
      <button class="sc-5g65yh-2 eYxMlR full-width" style="background-color:#fe0000" onclick="goMenu('withdraw');">
         <div style="background-color:#fff" class="sc-5g65yh-1 jnuzDq">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="minus" class="svg-inline--fa fa-minus fa-w-14 iconBC" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" style="color:#fe0000">
               <path fill="currentColor" d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
            </svg>
         </div>
         <span class="ml-2">ถอนเงิน</span>
      </button>
   </div>
  <!--  <div style="display:none">
      <span class="cus_member_id">185730</span>
      <span class="cus_username">hlfuubtjlzae</span>
      <span class="cus_activegame"></span>
      <span class="cus_activetime">0</span>
      <span class="cus_turnover">0</span>
      <span class="cus_winloss">0</span>
      <span class="cus_clickid"></span>
      <span id="display_time" class="display_time">2021-05-22T11:34:46+07:00</span>
   </div> -->
</div>
<div class="row no-gutters" style="display: flex;justify-content: center;">
   <div class="mb-2 px-lg-1 col-lg-4 col-12">
      <?php if($data['but_ts']['value'] == 'show'):?>
      <?php if($ss['data']['user_ts'] != ''):?>
      <?php if($ss['data']['user_ts'] == 'pending'):?>
      <div class="">
         <div class="" style="border-radius: 20px;">
            <p class="" style="font-size: 60px;">TS911</p>
            <p class="" style="font-size: 25px;">กำลังดำเนินการ</p>
            <h5 class="nk-feature-title textsilver noise">*ไม่ได้ user ภายใน5 นาที กรุณาตืดต่อ admin</h5>
            <a href="<?php echo base_url(); ?>u_info/user_info">
               <div style="z-index: 1000;">
                  <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;"><i class="fa fa-sync-alt"></i> รีเฟรช</button><br>
               </div>
            </a>
         </div>     
      </div>
      <?php else:?>
      <div id="TS1">
         <div class="">
            <div class="" style="border-radius: 20px;">
               <p class="" style="font-size: 60px;">TS911</p>
               
               <p class="" id="imi_user" style="font-size: 20px;">ID : <?php echo $ss['data']['user_ts']; ?></p>
               <p class="" ><span>เครดิต</span><p class="" id="ts_balance"><?php echo (isset($ss['balance_ts'])) ?  $ss['balance_ts']['data']['bet_credit']  : "0.00";?></p>
               <a id="loading_ts" style="display: block; color: #fff;" class="fas fa-sync-alt" style="color: #fff;" onclick="balance_ts()"></a>
               <img src="assets/images/loading.gif" id="loading_img_ts" style="display: none; width: 30px;height: 30px; margin: auto;" > 
               </p>


               <form method="post" id="form3" name="form3" method="post" action="https://www.ts911.com/Default9.aspx?lang=EN-GB"  target="_blank" >
                 <!--  <input type="hidden" name="__EVENTTARGET"       id="__EVENTTARGET"      value="<?php echo $key['__EVENTTARGET'];?>">
                  <input type="hidden" name="__EVENTARGUMENT"     id="__EVENTARGUMENT"    value="<?php echo $key['__EVENTARGUMENT'];?>">
                  <input type="hidden" name="__VIEWSTATE"         id="__VIEWSTATE"        value="<?php echo $key['__VIEWSTATE'];?>">          
                  <input type="hidden" name="__EVENTVALIDATION"   id="__EVENTVALIDATION"  value="<?php echo $key['__EVENTVALIDATION'];?>">
                  <input type="hidden" name="txtUserName"         id="txtUserName"        value="<?php echo $ss['data']['user_ts'];?>" >
                  <input type="hidden" name="password"            id="password"           value="<?php echo $ss['data']['pass_ts'];?>" > -->
                  <div>
                     <a class="btn btn-outline-secondary"  id="btnLogin" href="javascript:__doPostBack('btnLogin','')" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;">เข้าเล่น</a>
                     <!--  <a href="<?php echo base_url();?>deposit_lsm"  style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;" class="btn btn-outline-secondary">สมัคร</a> -->
                  </div>
               </form>
            </div>     
         </div> 
      </div>

      <?php endif;?>
      <?php else:?>
      <div id="TS"> 
         <div class="">
            <div class="" style="border-radius: 20px;">
               <p class="" style="font-size: 60px;">TS911</p>
               <p class="">สมัคร</p>
               <h3 class="nk-feature-title textsilver noise">Register</h3>
               <a href="<?php echo base_url();?>deposit_ts" id='tttt' >
                  <div style="z-index: 1000;">
                     <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;">สมัคร</button>
               </div>
               </a>
            </div>     
         </div>
      </div> 
      <?php endif;?> 
      <?php else:?> 

      <div class="">
         <div class="" style="border-radius: 20px;">
            <!-- <img src="assets/images/ts-service.png" alt=""style="height: 50px; width: 50%;"> -->
            <p class="" style="font-size: 60px;">TS911</p>
            <p class="" style="font-size: 20px;">ปิดปรับปรุงระบบ</p>
            <p class=""><span>เวลา</span><br><?php echo (isset($data['but_ts'])) ?  $data['but_ts']['detail']  : "ไม่มีข้อมูล";?></p>

         </div>
      </div> 
      <?php endif?>
   </div>

   <div class="mb-2 px-lg-1 col-lg-4 col-12">
      <?php if($data['but_imi']['value'] == 'show'):?>
      <?php if($ss['data']['user_imi'] != ''):?>
      <?php if($ss['data']['user_imi'] == 'pending'):?>
      <div class="">
         <div class="" style="border-radius: 20px;">
            <p class="" style="font-size: 60px;">IMI</p>
            <p class="" style="font-size: 25px;">กำลังดำเนินการ</p>
            <h5 class="textsilver noise">*ไม่ได้ user ภายใน5 นาที กรุณาตืดต่อ admin</h5>
            <a href="<?php echo base_url(); ?>u_info/user_info">
               <div style="z-index: 1000;">
                  <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;"><i class="fa fa-sync-alt"></i> รีเฟรช</button><br>
               </div>
            </a>
         </div>     
      </div> 
      <?php else:?>
      <div id="IMI1">
         <div class="">
            <div class="" style="border-radius: 20px;">
               <p class="" style="font-size: 60px;">IMI</p>
               <p class="" id="imi_user" style="font-size: 20px;"><span>ID : </span><?php echo $ss['data']['user_imi']; ?></p>    
               <p class="" ><span>เครดิต</span><p class="" id="imi_balance"><?php echo (isset($ss['balance_imi'])) ?  $ss['balance_imi']['data']['bet_credit']  : "0.00";?></p>  
               <a style="display: block; color: #fff;" id="loading_imi" class="fas fa-sync-alt" style="color: #3a3939;" onclick="balance_imi()" ></a>
               <img src="assets/images/loading.gif" id="loading_img_imi" style="display: none; width: 30px;height: 30px; margin: auto;" > 
               </p>
                  <form class="login-form" role="form" method="post" enctype="multipart/form-data" action="https://ts911api.botbo21.com/api/login_imi" target="_blank" style="max-width: 100%;">
                  <input type="hidden" name="user"    id="user"     value="<?php echo $ss['data']['user_imi']?>">
                  <input type="hidden" name="pass"    id="pass"     value="<?php echo $ss['data']['pass_imi']?>"> 
                  <input type="hidden" name="mobile"  id="mobile"   value="false">
                  <button class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;">เข้าเล่น</button>
               </form> 
            </div>     
         </div>
      </div>
      <?php endif?>
      <?php else:?>
      <div id="IMI">
         <div class="">
            <div class="" style="border-radius: 20px;">  
               <p class="" style="font-size: 60px;">IMI</p>
               <p class="">สมัคร</p>
               <h3 class="nk-feature-title textsilver noise">Register</h3>
               <a href="<?php echo base_url();?>deposit_imi"  >
                  <div class="nk-feature-cont">
                     <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;">สมัคร</button>
                  </div>
               </a>
            </div>
         </div>
      </div>   
      <?php endif?>
      <?php else:?> 
      <div class="">
         <div class="" style="border-radius: 20px;">
            <img src="assets/images/imi-service.png" alt=""style="height: 50px; width: 50%;">
            <p class="" style="font-size: 30px;">ปิดปรับปรุงระบบ</p>
            <p class="card__name"><span>เปิดเวลา</span><br><?php echo (isset($data['but_imi'])) ?  $data['but_imi']['detail']  : "ไม่มีข้อมูล";?></p>
            <div class="nk-feature-cont">
            <!-- <h3 class="nk-feature-title textsilver noise">Register</h3> -->
            </div>
         </div>
      </div>
      <?php endif?>
   </div>
   <div class="mb-2 px-lg-1 col-lg-4 col-12">
      <?php if($data['but_lsm']['value'] == 'show'):?>
      <?php if($ss['data']['user_lsm'] != ''):?>
      <?php if($ss['data']['user_lsm'] == 'pending'):?>

      <div class="">
         <div class="card__front" style="border-radius: 20px;">
            <p class="card__num" style="font-size: 60px;">LSM99</p>
            <!-- <img src="assets/images/client-11.png" alt=""style="height: 80px;width: 150px;"> -->
            <p class="card__name" style="font-size: 25px;">กำลังดำเนินการ</p>
            <h5 class="nk-feature-title textsilver noise">*ไม่ได้ user ภายใน5 นาที กรุณาตืดต่อ admin</h5>
            <a href="<?php echo base_url(); ?>u_info/user_info">
               <div style="z-index: 1000;">
                  <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;"><i class="fa fa-sync-alt"></i> รีเฟรช</button><br>
               </div>
            </a>
         </div>     
      </div>
      <?php else:?>
      <div id="LSM1"> 
         <div class="">
            <div class="card__front" style="border-radius: 20px;">
               <p class="card__num" style="font-size: 60px;">LSM99</p>
               <p class="card__num" id="" style="font-size: 20px;"><span>ID : </span><?php echo $ss['data']['user_lsm']; ?></p> 
               <p class="card__name" ><span>เครดิต</span><p class="card__name" id="lsm_balance"><?php echo (isset($ss['balance_lsm'])) ?  $ss['balance_lsm']['data']['bet_credit']  : "0.00";?></p>  
               <a id="loading_lsm" style="display: block;color: #fff;" class="fas fa-sync-alt" style="color: #3a3939;" onclick="balance_lsm()"></a></p>
               <img src="assets/images/loading.gif" id="loading_img_lsm" style="display: none; width: 30px;height: 30px; margin: auto;" >  
               <form class="form-horizontal" id="formLogin" role="form" method="post" action="https://999lsm.com/login" target="_blank">
                  <input type="hidden" name="username"         id="username"      value="<?php echo $ss['data']['user_lsm']?>">
                  <input type="hidden" name="usernamelogin"    id="usernamelogin" value="<?php echo $ss['data']['user_lsm']?>">
                  <input type="hidden" name="password"         id="password"      value="<?php echo $ss['data']['pass_lsm']?>">
                  <input type="hidden" name="_token"           id="_token"        value="WETuMu1PdNCXtb2eSUp1gAAWk1VZcL2i0r5xyD6e"> 
                  <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;">เข้าเล่น</button>
               </form>
            </div> 
         </div>     
      </div> 
      <?php endif?>                 
      <?php else:?>
      <div id="LSM">
         <div class="">
            <div class="card__front" style="border-radius: 20px;">
            <p class="card__num" style="font-size: 60px;">LSM99</p>
            <!-- <img src="assets/images/client-33.png" alt=""style="height: 80px;width: 150px;"> -->
            <p class="card__name">สมัคร</p> 
            <h3 class="nk-feature-title textsilver noise">Register</h3>
            <a href="<?php echo base_url();?>deposit_lsm">
               <button  class="btn btn-outline-secondary" style=" font-size: 12px;text-align: center;  width: 150px;border-radius: 10px;">สมัคร</button>
            </a>
         </div>     
      </div>
      <?php endif?>
      <?php else:?> 

      <div class="">
         <div class="card__front" style="border-radius: 20px;">
         <!-- <img src="assets/images/lsm-service.png" alt=""style="height: 50px; width: 50%;"> -->
            <p class="card__num" style="font-size: 60px;">LSM99</p>
            <p class="card__num" style="font-size: 30px;">web</p>
            <p class="card__num" style="font-size: 30px;">maintenance</p>
            <p class="card__num" style="font-size: 30px;">services</p><br>
            <p class="card__name"><span>เปิดเวลา</span><br><?php echo (isset($data['but_lsm'])) ?  $data['but_lsm']['detail']  : "ไม่มีข้อมูล";?></p>
            <div class="nk-feature-cont">
            <!-- <h3 class="nk-feature-title textsilver noise">Register</h3> -->

            </div>
         </div>
      </div> 
      </div>
      <?php endif?>     
   </div>
</div>


<div class="sc-5g65yh-3 kOObDs pt-2 pb-3 mt-3">
   <div style="width:100% !important;margin-left:2px;margin-right:2px" class="row">
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" onclick="goMenu('deposit_withdraw');">
            <img width="100%" height="100%" src="assets/images/dep-daw.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu"><span class="sc-1qrnk4o-10 gBUvoq">ฝาก-ถอน</span>
         </div>
      </div>
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" onclick="goMenu('deposit_withdraw');">
            <img width="100%" height="100%" src="assets/images/history-act.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">ประวัติการเล่น</span>
         </div>
      </div>
    <!--   <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" onclick="goMenu('invite');">
            <img width="100%" height="100%" src="assets/images/chat-group.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">ระบบแนะนำเพื่อน</span>
         </div>
      </div>
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" onclick="goMenu('agent');">
            <img width="100%" height="100%" src="assets/images/check-hand.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">ระบบเอเย่นต์</span>
         </div>
      </div> -->
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN" >
            <img width="100%" height="100%" src="assets/images/contact.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu"><span class="sc-1qrnk4o-10 gBUvoq">ติดต่อเรา</span>
         </div>
      </div>
     <!--  <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-6 bqkdwN">
            <img width="100%" height="100%" src="assets/images/ICON_WALLET_150x150px-16.png" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">ดูหนังออนไลน์ฟรี</span>
         </div>
      </div> -->
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-4">
         <div class="sc-1qrnk4o-7 cpoYdY animate__animated animate__pulse animate__infinite" onclick="goMenu('promotion');">
            <img width="100%" height="100%" src="assets/images/HL_PROMOTION.gif" class="sc-1qrnk4o-8 hYOUfD pt-3" alt="home-menu">
            <span class="sc-1qrnk4o-10 gBUvoq">โปรโมชั่น</span>
         </div>
      </div>
   </div>
</div>

      <script type="text/javascript">//<![CDATA[
         var theForm=document.forms['form3'];
         if(!theForm){
         theForm=document.form1;
         }
         function __doPostBack(eventTarget,eventArgument){
         if(!theForm.onsubmit||(theForm.onsubmit()!=false)){
         theForm.__EVENTTARGET.value=eventTarget;theForm.__EVENTARGUMENT.value=eventArgument;theForm.submit();
         }
         }
         $(function(){
         var $src=$('.uu1'),$dst=$('.uu2');
         $dst10=$('.uu3');
         $src.on('input',function(){
         $dst.val($src.val());
         $dst10.val($src.val());
         });
         var $src2=$('.uu2'),$dst20=$('.uu1');
         $dst30=$('.uu3');
         $src2.on('input',function(){
         $dst20.val($src2.val());
         $dst30.val($src2.val());
         });
         var $src3=$('.uu3'),$dst1=$('.uu1');
         $dst2=$('.uu2');
         $src3.on('input',function(){
         $dst1.val($src3.val());
         $dst2.val($src3.val());
         });
         var $src11=$('.pp1'),$dst11=$('.pp2');
         $dst12=$('.pp3');
         $src11.on('input',function(){
         $dst11.val($src11.val());
         $dst12.val($src11.val());
         });
         var $src22=$('.pp2'),$dst22=$('.pp1');
         $dst22c=$('.pp3');
         $src22.on('input',function(){
         $dst22.val($src22.val());
         $dst22c.val($src22.val());});
         var $src33=$('.pp3'),$dst22z=$('.pp1');
         $dst22x=$('.pp2');
         $src33.on('input',function(){
         $dst22z.val($src33.val());
         $dst22x.val($src33.val());
         });
         });
      </script>

    <!--   <script type="text/javascript">
         var post_status = document.getElementById("post_status").value;
         var post_img =document.getElementById('post_img');

         if (post_status =='show') {
            post_img.style.display = "block"
         }else{post_img.style.display = "none"}

      </script> --> 
      <script type="text/javascript">

         var cnt = 0;
         var token = '<?php echo $xx['data']['token'];?>';
   
         cntTimecn();
         
         function cntTimecn(){
            if(cnt == 0){
               var int = setInterval(function(){ 
            if(cnt <= 0){
               cnt = 60;
               int = '';
               // alert('5555555555555555');
                 info();
                  // balance_imi();
                  // balance_ts();
                 // test_clogin();
            }else{
               cnt--;
            }
               $('#cnt').html(cnt);
            }, 1000*1);
               cnt++;
            }
         }  

         function info(){  
            $.ajax({
               url: '<?php echo base_url();?>main/user_info',
               type: 'POST',
               data : { token : token },
               dataType: 'json',
               success: function (res) {
               // alert(res.status); 
            if(res.status == true){
               $('#ts_user').html(res.data.user_ts);
               $('#imi_user').html(res.data.user_imi);
               $('#lsm_user').html(res.data.user_lsm);
            }else{
                  window.location = './logout';
                 }
            }
            });

         }
         function balance_imi(){  
            document.getElementById("loading_img_imi").style.display = "block";
            document.getElementById("loading_imi").style.display = "none";

            $.ajax({ 
               url: '<?php echo base_url(); ?>balance_imi',
               type: 'POST',
               data : {web:'IMI',user:'<?php echo $ss['data']['userid']?>' ,token : '<?php echo $xx['data']['token'];?>' },  
               dataType: 'json',
               success: function (res) {
                         // alert(res.status); 
                  if(res.status == true){                
                    $('#imi_balance').html(res.data.bet_credit);
                    document.getElementById("loading_img_imi").style.display = "none";
                    document.getElementById("loading_imi").style.display = "block";
                  }
               }
            });
        }

        function balance_ts(){  
            document.getElementById("loading_img_ts").style.display = "block";
            document.getElementById("loading_ts").style.display = "none";
            $.ajax({ 
               url: '<?php echo base_url(); ?>balance_ts',
               type: 'POST',
               data : {web:'TS911',user:'<?php echo $ss['data']['userid']?>' ,token : '<?php echo $xx['data']['token'];?>' },  
               dataType: 'json',
               success: function (res) {
                        // alert(res.status); 
                  if(res.status == true){                
                    $('#ts_balance').html(res.data.bet_credit);
                    document.getElementById("loading_img_ts").style.display = "none";
                    document.getElementById("loading_ts").style.display = "block";

                  }
               }
            });
        }

        function balance_lsm(){  
            document.getElementById("loading_img_lsm").style.display = "block";
            document.getElementById("loading_lsm").style.display = "none";

            $.ajax({ 
               url: '<?php echo base_url(); ?>balance_lsm',
               type: 'POST',
               data : {web:'LSM',user:'<?php echo $ss['data']['userid']?>' ,token : '<?php echo $xx['data']['token'];?>' },  
               dataType: 'json',
               success: function (res) {
                         // alert(res.status); 
                  if(res.status == true){                
                    $('#lsm_balance').html(res.data.bet_credit);
                    document.getElementById("loading_img_lsm").style.display = "none";
                    document.getElementById("loading_lsm").style.display = "block"; 

                  }
               }
            });
        }
      </script>     