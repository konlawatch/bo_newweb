<div id="__next">
   <div class="sc-9f4hzg-1 jGZAsY animate__animated animate__fadeIn animate__slower  3s">
      <div class="stbi-0 jVEzye bg-green justify-center">
      <h1 class=" mt-2">
         <a href="/">
            <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
               <div style="box-sizing: border-box; display: block; max-width: 100%;">
                  <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUwIiBoZWlnaHQ9IjU0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIvPg==" style="max-width: 100%; display: block;">
               </div>
               <img alt="" src="assets/images/logo.png" srcset="assets/images/logo.png" decoding="async" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
            </div>
         </a>
      </h1>
   </div>
   <p class="marquee" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
      <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บหวยออนไลน์ที่มาแรงที่สุดตอนนี้</span>
   </p>
</div>
<div class="text-center animate__animated animate__fadeIn animate__slower  3s container">
   <div class="justify-content-md-center row">
      <div class="col-lg-10">
         <div class="sc-9f4hzg-0 hJkCQZ">
            <div class="stbi-0 jVEzye bg-green justify-center">
               <h1 class=" mt-2">
                  <a href="/">
                     <div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
                        <div style="box-sizing: border-box; display: block; max-width: 100%;">
                           <img alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUwIiBoZWlnaHQ9IjU0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIvPg==" style="max-width: 100%; display: block;">
                        </div>
                        <img alt="" src="assets/images/logo.png" srcset="assets/images/logo.png" decoding="async" style="visibility: visible; position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;">
                     </div>
                  </a>
               </h1>
            </div>
            <p class="marquee" style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);">
               <span>ยินดีต้อนรับทุกท่านเข้าสู่เว็บ <?php echo $website;?> เว็บหวยออนไลน์ที่มาแรงที่สุดตอนนี้</span>
            </p>
         </div>
         <div id="login" class="pb-0 mt-3 mb-3">
           <div style="background-color:#00bfa1;border:none" class="dv90yj-3 ekmdOJ text-center mt-3 mb-0 d-flex justify-content-between"><span><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="book" class="svg-inline--fa fa-book fa-w-14 mt-1 mr-2 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" ><path fill="currentColor" d="M448 360V24c0-13.3-10.7-24-24-24H96C43 0 0 43 0 96v320c0 53 43 96 96 96h328c13.3 0 24-10.7 24-24v-16c0-7.5-3.5-14.3-8.9-18.7-4.2-15.4-4.2-59.3 0-74.7 5.4-4.3 8.9-11.1 8.9-18.6zM128 134c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm0 64c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm253.4 250H96c-17.7 0-32-14.3-32-32 0-17.6 14.4-32 32-32h285.4c-1.9 17.1-1.9 46.9 0 64z"></path></svg>คู่มือการใช้งาน</span><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="reply" class="svg-inline--fa fa-reply fa-w-16 mt-1 mr-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="cursor:pointer" onclick="goMenu('main');"><path fill="currentColor" d="M8.309 189.836L184.313 37.851C199.719 24.546 224 35.347 224 56.015v80.053c160.629 1.839 288 34.032 288 186.258 0 61.441-39.581 122.309-83.333 154.132-13.653 9.931-33.111-2.533-28.077-18.631 45.344-145.012-21.507-183.51-176.59-185.742V360c0 20.7-24.3 31.453-39.687 18.164l-176.004-152c-11.071-9.562-11.086-26.753 0-36.328z"></path></svg></div>
         </div>
         <div id="af-academy" class="mt-4 bg-light">
            <div class="title">คู่มือการใช้งาน</div>
            <!-- <?php foreach($mlist as $k => $v):?>
               <div class="accordion" id="accordion_<?php echo $k;?>">
                  <div class="card">
                     <div class="card-header">
                        <div class="row no-gutters">
                           <div class="text-left col-lg-8 col-md-8 col-sm-8 col-6">
                              <span><?php echo $v['title'];?></span>
                           </div>

                           <div class="text-right col-lg-4 col-md-4 col-sm-4 col-6">&nbsp;
                              <button type="button" class="btn btn-primary btn-sm" style="width: 90px;" data-toggle="collapse" data-target="#collapsevideo_<?php echo $k;?>" aria-expanded="false" aria-controls="collapsevideo_<?php echo $k;?>" ><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="video" class="svg-inline--fa fa-video fa-w-18 mr-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M336.2 64H47.8C21.4 64 0 85.4 0 111.8v288.4C0 426.6 21.4 448 47.8 448h288.4c26.4 0 47.8-21.4 47.8-47.8V111.8c0-26.4-21.4-47.8-47.8-47.8zm189.4 37.7L416 177.3v157.4l109.6 75.5c21.2 14.6 50.4-.3 50.4-25.8V127.5c0-25.4-29.1-40.4-50.4-25.8z"></path></svg>วีดีโอ</button>&nbsp;
                              <button type="button" class="btn btn-success btn-sm" style="width: 90px;" data-toggle="collapse" data-target="#collapseimage_<?php echo $k;?>" aria-expanded="false" aria-controls="collapseimage_<?php echo $k;?>" ><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="image" class="svg-inline--fa fa-image fa-w-16 mr-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 448H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h416c26.51 0 48 21.49 48 48v288c0 26.51-21.49 48-48 48zM112 120c-30.928 0-56 25.072-56 56s25.072 56 56 56 56-25.072 56-56-25.072-56-56-56zM64 384h384V272l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L208 320l-55.515-55.515c-4.686-4.686-12.284-4.686-16.971 0L64 336v48z"></path></svg>รูปภาพ</button>&nbsp;
                           </div>
                        </div>
                     </div>
                     <div class="collapse" id="collapsevideo_<?php echo $k;?>" data-parent="#accordion_<?php echo $k;?>">
                        <div class="card-body">
                           <iframe src="<?php echo $v['video_url'];?>" class="mb-0" id="myId1" width="100%" height="600px" style="position: relative; display: inline;"></iframe>
                        </div>
                     </div>
                     <div class="collapse" id="collapseimage_<?php echo $k;?>" data-parent="#accordion_<?php echo $k;?>">
                        <div class="card-body">
                           <img src="<?php echo $v['image_url'];?>" alt="img" style="width: 100%;">
                        </div>
                     </div>
                  </div>
               </div>
            <?php endforeach;?>
         </div> -->
         <div style="margin-top:0" class="sc-9f4hzg-0 hJkCQZ">
            <div class="o37fw8-1 oTMwd bg-green mt-0-4">
               <div class="text-center">
                  <h3 class="mb-0" style="cursor: pointer; font-size: 1rem; line-height: 70px;">2021 COPYRIGHT <?php echo $website;?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div style="margin-top:0" class="sc-9f4hzg-1 jGZAsY">
      <div class="o37fw8-1 oTMwd bg-green mt-0-4">
         <div class="text-center">
            <h3 class="mb-0" style="cursor: pointer; font-size: 1rem; line-height: 70px;">2021 COPYRIGHT <?php echo $website;?></h3>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   function checkUser(){
      var username = $('#username').val();
      $.ajax({ 
         url: '<?php echo base_url(); ?>newuser/chkuseruselogin',
         type: 'POST',
         data : { username : username },  
         dataType: 'json',
         success: function (res) {
            if(res.status){
               $(document).ready(function(){ 
                  Swal.fire('Oops...', 'หมายเลขนี้ยังไม่มีในระบบ !!', 'warning');
               });
               $('#username').val('');
               $('#password').val('');                         
            }
         }
      });                 
   }    
</script>