<?php 
   $ss = $this->session->userdata();
   $banklist = $this->config->config['banklist'];
   $banklist_color = $this->config->config['banklist_color'];

   $ubank = explode('-', $ss['data']['bank']);
   $user_bank_name   = $ss['data']['name'];
   $user_bank        = $ubank[0];
   $user_bank_desc   = $banklist[$ubank[0]];
   $user_bank_number = $ubank[1];
?>
<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>
<style type="text/css">
  .btn-dark {
    /*color: #fff;*/
    background-color: #3a9a82;
    /*border-color: #343a40;*/
}
</style>
<div id="withdraw" class="mt-3 mb-3">
   <div class="dv90yj-3 ekmdOJ text-left d-flex" style="background-color: rgb(0, 191, 161); border: none;">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="vote-yea" class="svg-inline--fa fa-vote-yea fa-w-20 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" style="font-size: 1.4em;">
         <path fill="currentColor" d="M608 320h-64v64h22.4c5.3 0 9.6 3.6 9.6 8v16c0 4.4-4.3 8-9.6 8H73.6c-5.3 0-9.6-3.6-9.6-8v-16c0-4.4 4.3-8 9.6-8H96v-64H32c-17.7 0-32 14.3-32 32v96c0 17.7 14.3 32 32 32h576c17.7 0 32-14.3 32-32v-96c0-17.7-14.3-32-32-32zm-96 64V64.3c0-17.9-14.5-32.3-32.3-32.3H160.4C142.5 32 128 46.5 128 64.3V384h384zM211.2 202l25.5-25.3c4.2-4.2 11-4.2 15.2.1l41.3 41.6 95.2-94.4c4.2-4.2 11-4.2 15.2.1l25.3 25.5c4.2 4.2 4.2 11-.1 15.2L300.5 292c-4.2 4.2-11 4.2-15.2-.1l-74.1-74.7c-4.3-4.2-4.2-11 0-15.2z"></path>
      </svg>
      <p class="ml-2">แจ้งถอนเงิน</p>
   </div>
   <div class="mb-3 row">
      <div class="col-01 text-wallet col"><div>
         <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="wallet" class="svg-inline--fa fa-wallet fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M461.2 128H80c-8.84 0-16-7.16-16-16s7.16-16 16-16h384c8.84 0 16-7.16 16-16 0-26.51-21.49-48-48-48H64C28.65 32 0 60.65 0 96v320c0 35.35 28.65 64 64 64h397.2c28.02 0 50.8-21.53 50.8-48V176c0-26.47-22.78-48-50.8-48zM416 336c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32z"></path>
         </svg> กระเป๋าเงิน</div>
         <div>0.00</div>
      </div>
   </div>
   <div class="mb-3 row">
      <div class="col">
         <div class="card">
            <div class="card-header"><h5>เลือกบัญชีธนาคารของท่าน</h5></div>
            <div class="card-body">
               <form class="">
                  <div class="mb-3 col-12">
                     <div class="card-bank card" style="border-color: <?php echo $banklist_color[$user_bank];?>;">
                        <div class="text-center card-header" style="background-color: <?php echo $banklist_color[$user_bank];?>;">บัญชีธนาคารของลูกค้า</div>
                        <div class="sc-9f4hzg-1 jGZAsY">
                           <div class="text-center card" style="display: grid; justify-items: center;">
                              <div class="text-left p-2" style="width: fit-content;">
                                 <div class="d-flex">
                                    <img width="26px" height="26px" alt="img" src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $user_bank;?>.png">
                                    <h5 class="pl-2 pt-0 mb-1" style="color: <?php echo $banklist_color[$user_bank];?>;"><?php echo $user_bank_desc;?></h5>
                                 </div>
                                 <div>
                                    <p class="mb-0 text-secondary">ชื่อบัญชี: <?php echo $user_bank_name;?></p>
                                    <p class="mb-0 text-secondary">เลขที่บัญชี : <?php echo $user_bank_number;?></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="sc-9f4hzg-0 hJkCQZ">
                           <div class="text-left card-body">
                              <img width="100%" alt="bank" src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $user_bank;?>.png">
                              <div class="pl-2 pr-0 px-md-3">
                                 <b style="color: <?php echo $banklist_color[$user_bank];?>;"><?php echo $user_bank_desc;?></b><p class="mb-0">ชื่อบัญชี: <?php echo $user_bank_name;?> </p><p>เลขที่บัญชี : <?php echo $user_bank_number;?></p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <hr>เลือกจำนวนเงินที่ต้องการถอน
                  <div class="justify-content-center mt-3 row">
                     <div class="mb-2 col-md-2 col-6">
                        <button type="button" class="mb-2 btn btn-primary btn-block" onclick="auto_dnum('wid_amt','100');">100 บาท</button>
                     </div>
                     <div class="mb-2 col-md-2 col-6">
                        <button type="button" class="mb-2 btn btn-secondary btn-block" onclick="auto_dnum('wid_amt','500');">500 บาท</button>
                     </div>
                     <div class="mb-2 col-md-2 col-6">
                        <button type="button" class="mb-2 btn btn-success btn-block" onclick="auto_dnum('wid_amt','1000');">1,000 บาท</button>
                     </div>
                     <div class="mb-2 col-md-2 col-6">
                        <button type="button" class="mb-2 btn btn-warning btn-block" onclick="auto_dnum('wid_amt','2000');">2,000 บาท</button>
                     </div>
                     <div class="mb-2 col-md-2 col-6">
                        <button type="button" class="mb-2 btn btn-danger btn-block" onclick="auto_dnum('wid_amt','5000');">5,000 บาท</button>
                     </div>
                  </div>
                  <div class="justify-content-center mt-3 row">
                     <div class="col-md-8 col-12">
                        <div class="mb-3 input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">฿</span>
                           </div>
                           <input inputmode="numeric" placeholder="ระบุจำนวนเงินที่ต้องการถอน" class="form-control form-control-lg" maxlength="9" type="tel" value="" id="wid_amt">
                           <div class="input-group-append">
                              <button type="button" class="btn btn-warning">ทั้งหมด</button>
                           </div>
                           <div class="text-center text-danger w-100 pt-2">
                              <h5>ถอนขั้นต่ำ "ครั้งละ 100 บาท"</h5>
                           </div>
                        </div>
                     </div>
                     <div class="text-left col-md-8 col-12">
                        <p>* ถอนขั้นต่ำ 100 บาท</p>
                        <p>* ถอนสูงสุดต่อครั้ง 1,000,000 บาท</p>
                        <p>* ถอนสูงสุดต่อวัน 10,000,000 บาท</p>
                     </div>
                     <div class="mb-2 col-md-8 col-12">
                        <hr>
                        <p class="text-danger">
                           <u> หากทำรายการถอนไปแล้ว โปรดรออีก 5 นาทีถึงทำรายการถอนได้อีกครั้ง!!</u>
                        </p>
                        <button type="submit" class="btn btn-success btn-block btn-lg">ถอนเงิน</button>
                     </div>
                     <div class="col-md-8 col-12">
                        <button type="button" class="btn btn-danger btn-block btn-lg" onclick="goMenu('withdraw');">ยกเลิก</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
