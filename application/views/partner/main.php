<div class="text-left py-2 pl-3 mt-3" style="background: rgb(0, 191, 161); border-top-left-radius: 8px; border-top-right-radius: 8px;">
   <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users" class="svg-inline--fa fa-users fa-w-20 ml-0 pl-0 pt-0 text-light" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" style="font-size: 1.4em;">
      <path fill="currentColor" d="M448 360V24c0-13.3-10.7-24-24-24H96C43 0 0 43 0 96v320c0 53 43 96 96 96h328c13.3 0 24-10.7 24-24v-16c0-7.5-3.5-14.3-8.9-18.7-4.2-15.4-4.2-59.3 0-74.7 5.4-4.3 8.9-11.1 8.9-18.6zM128 134c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm0 64c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm253.4 250H96c-17.7 0-32-14.3-32-32 0-17.6 14.4-32 32-32h285.4c-1.9 17.1-1.9 46.9 0 64z"></path>
   </svg>
   <span class="text-light" style="font-size: 1.4em;">ข้อมูลพาทเนอร์ <?php echo $web;?>.</span>
</div>
<div class="animate__animated animate__fadeIn mt-0 bg-light mb-3" style="background: #d0ebf0; border-bottom-left-radius: 8px; border-bottom-right-radius: 16px;">
   <div class="sc-9f4hzg-1 jGZAsY">
      <div class="text-center card" style="display: grid; justify-items: center;">
         <div class="text-center p-2" style="width: fit-content;">
            <div class="">
               <img class="img-fluid img-enjoy911" alt="<?php echo $web;?> INFO" src="<?php echo base_url();?>assets/images/partner-<?php echo strtolower($web);?>.png">
            </div><br/>
            <div class="pr-0 px-md-3">
               <!-- <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-prepend">
                        <span class="input-group-text">เครดิต</span>
                     </div>
                     <input class="form-control input-dis" disabled="disabled" value="<?php echo number_format($credit,true);?>" />
                     <div class="input-group-append input-group-addon">
                        <button class="btn btn-outline-secondary btn-append" type="button"><i class="fas fa-sync my-0"></i></button>
                     </div>
                  </div>
               </div> -->
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-prepend">
                        <span class="input-group-text">ยูซเซอร์เนม</span>
                     </div>
                     <input class="form-control input-dis" disabled="disabled" value="<?php echo $user;?>" />
                     <div class="input-group-append input-group-addon">
                        <button class="btn btn-outline-secondary btn-append" type="button" onclick="copy_text('<?php echo $user;?>');"><i class="fas fa-copy my-0"></i></button>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="input-group">
                     <div class="input-group-prepend">
                        <span class="input-group-text">รหัสผ่าน</span>
                     </div>
                     <input type="password" class="form-control input-dis" disabled="disabled" value="<?php echo $pass;?>" />
                     <div class="input-group-append input-group-addon">
                        <button class="btn btn-outline-secondary btn-append" type="button" onclick="copy_text('<?php echo $pass;?>');"><i class="fas fa-copy my-0"></i></button>
                     </div>
                  </div>
               </div>
               <a href="<?php echo base_url();?>partner/login_partner?p=<?php echo $web;?>&k=<?php echo $login;?>&t=<?php echo $time;?>" target="_blank" class="mt-0 rounded-0 btn btn-enjoy911 btn-block" ><i class="fas fa-dice"></i> เข้าเกมส์</a>
            </div>
         </div>
      </div>
   </div>
   <div class="mt-0 hJkCQZ">
      <div class="p-3 justify-content-center row">
         <div class="form-group col-md-3">
            <img class="mt-4 img-fluid img-enjoy911" width="100%" alt="<?php echo $web;?> INFO." src="<?php echo base_url();?>assets/images/partner-<?php echo strtolower($web);?>.png">
         </div>
         <div class="mb-2 align-self-end col-md-6 col-12">
            <div class="sc-9f4hzg-0">
               <div class="text-left card-body">
                  <div class="pl-2 pr-0 px-md-3">
                     <!-- <div class="form-group">
                        <div class="input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">เครดิต</span>
                           </div>
                           <input class="form-control input-dis" disabled="disabled" id="user_credit" value="<?php echo number_format($credit,true);?>" />
                           <div class="input-group-append input-group-addon">
                              <button class="btn btn-outline-secondary btn-append" type="button" id="sync_balance" onclick="get_balance(this,'<?php echo $web;?>');" ><i class="fas fa-sync my-0"></i></button>
                           </div>
                        </div>
                     </div> -->
                     <div class="form-group">
                        <div class="input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">ยูซเซอร์เนม</span>
                           </div>
                           <input class="form-control input-dis" disabled="disabled" value="<?php echo $user;?>" />
                           <div class="input-group-append input-group-addon">
                              <button class="btn btn-outline-secondary btn-append" type="button" onclick="copy_text('<?php echo $user;?>');"><i class="fas fa-copy my-0"></i></button>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">รหัสผ่าน</span>
                           </div>
                           <input type="password" class="form-control input-dis" disabled="disabled" value="<?php echo $pass;?>" />
                           <div class="input-group-append input-group-addon">
                              <button class="btn btn-outline-secondary btn-append" type="button" onclick="copy_text('<?php echo $pass;?>');"><i class="fas fa-copy my-0"></i></button>
                           </div>
                        </div>
                     </div>
                     <a href="<?php echo base_url();?>partner/login_partner?p=<?php echo $web;?>&k=<?php echo $login;?>&t=<?php echo $time;?>" target="_blank" class="mt-0 rounded-0 btn btn-enjoy911 btn-block" ><i class="fas fa-dice"></i> เข้าเกมส์</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div role="alert" class="fade text-center alert alert-info show"><span>&nbsp;</span></div>
   </div>
</div>   
<script type="text/javascript">
   function get_balance(e,partner){
      $('#sync_balance').html('<i class="fas fa-sync my-0 spinning"></i>');
      $('#sync_balance').prop('disabled', true);

      $.ajax({
         type: 'GET',
         url: '<?php echo base_url();?>service/get_balance',
         data: { partner : partner },
         dataType: "json",
         success: function(data, dataType, state){
            $('#sync_balance').html('<i class="fas fa-sync my-0"></i>');
            $('#sync_balance').prop('disabled', false);        
            if(data.status){
               $('#user_credit').val(data.data);
            }else {
               Swal.fire('Opps...', data.msg, 'error');
            }
         }
      });
   }
</script>