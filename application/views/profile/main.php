<?php 
   // debug($user_info);
   $ex = explode(' ',$user_info['name']);
   $uname = $ex[0];
   $lname = $ex[1];
?>
<div class="dv90yj-3 ekmdOJ text-left d-flex" style="background-color: rgb(0, 191, 161); border: none;">
   <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-cog" class="svg-inline--fa fa-user-cog fa-w-20 ml-2" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" style="font-size: 1.4em;">
      <path fill="currentColor" d="M610.5 373.3c2.6-14.1 2.6-28.5 0-42.6l25.8-14.9c3-1.7 4.3-5.2 3.3-8.5-6.7-21.6-18.2-41.2-33.2-57.4-2.3-2.5-6-3.1-9-1.4l-25.8 14.9c-10.9-9.3-23.4-16.5-36.9-21.3v-29.8c0-3.4-2.4-6.4-5.7-7.1-22.3-5-45-4.8-66.2 0-3.3.7-5.7 3.7-5.7 7.1v29.8c-13.5 4.8-26 12-36.9 21.3l-25.8-14.9c-2.9-1.7-6.7-1.1-9 1.4-15 16.2-26.5 35.8-33.2 57.4-1 3.3.4 6.8 3.3 8.5l25.8 14.9c-2.6 14.1-2.6 28.5 0 42.6l-25.8 14.9c-3 1.7-4.3 5.2-3.3 8.5 6.7 21.6 18.2 41.1 33.2 57.4 2.3 2.5 6 3.1 9 1.4l25.8-14.9c10.9 9.3 23.4 16.5 36.9 21.3v29.8c0 3.4 2.4 6.4 5.7 7.1 22.3 5 45 4.8 66.2 0 3.3-.7 5.7-3.7 5.7-7.1v-29.8c13.5-4.8 26-12 36.9-21.3l25.8 14.9c2.9 1.7 6.7 1.1 9-1.4 15-16.2 26.5-35.8 33.2-57.4 1-3.3-.4-6.8-3.3-8.5l-25.8-14.9zM496 400.5c-26.8 0-48.5-21.8-48.5-48.5s21.8-48.5 48.5-48.5 48.5 21.8 48.5 48.5-21.7 48.5-48.5 48.5zM224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm201.2 226.5c-2.3-1.2-4.6-2.6-6.8-3.9l-7.9 4.6c-6 3.4-12.8 5.3-19.6 5.3-10.9 0-21.4-4.6-28.9-12.6-18.3-19.8-32.3-43.9-40.2-69.6-5.5-17.7 1.9-36.4 17.9-45.7l7.9-4.6c-.1-2.6-.1-5.2 0-7.8l-7.9-4.6c-16-9.2-23.4-28-17.9-45.7.9-2.9 2.2-5.8 3.2-8.7-3.8-.3-7.5-1.2-11.4-1.2h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c10.1 0 19.5-3.2 27.2-8.5-1.2-3.8-2-7.7-2-11.8v-9.2z"></path></svg>
      <p class="ml-2">ตั้งค่าบัญชีผู้ใช้</p>
</div>
<div>
   <div class="dv90yj-1 gNathv pt-4 pb-0" style="background-color: rgb(255, 255, 255);">
      <form id="form-updateinfo" class="pl-3 pr-3 text-left" autocomplete="off"  method="post" action="<?php echo base_url();?>service/updateinfo">
         <div class="form-group">
            <label class="form-label" style="color: rgb(0, 193, 158);">ชื่อผู้ใช้งาน username</label>
            <input disabled="" type="text" class="form-control" value="<?php echo $user_info['userid'];?>">
         </div>
         <div class="pt-3 row">
            <div class="form-group col-lg-6 col-md-6">
               <label class="form-label" style="color: rgb(0, 193, 158);">ชื่อจริง</label>
               <input disabled="" type="text" class="form-control" value="<?php echo $uname;?>">
            </div>
            <div class="form-group col-lg-6 col-md-6">
               <label class="form-label" style="color: rgb(0, 193, 158);">นามสกุล</label>
               <input disabled="" type="text" class="form-control" value="<?php echo $lname;?>">
            </div>
         </div>
         <div class="pt-3 row">
           <!--  <div class="form-group col-lg-4 col-md-4">
               <label class="form-label" style="color: rgb(0, 193, 158);">วันเดือนปีเกิด</label>
               <div class="react-datepicker-wrapper">
                  <div class="react-datepicker__input-container">
                     <input type="text" placeholder="วัน / เดือน / ปี" class="thCalendar" value="">
                  </div>
               </div>
            </div> -->
            <div class="form-group col-lg-4 col-md-4">
               <label class="form-label" style="color: rgb(0, 193, 158);">เบอร์โทรศัพท์</label>
               <input disabled="" type="text" name="info_tel" class="form-control" value="<?php echo $user_info['tel'];?>">
            </div>
            <div class="form-group col-lg-4 col-md-4">
               <label class="form-label" style="color: rgb(0, 193, 158);">Line</label>
               <input type="text" class="form-control" name="info_line" value="<?php echo $user_info['lineid'];?>">
            </div>
            <div class="form-group col-lg-4 col-md-4">
               <label class="form-label" style="color: rgb(0, 193, 158);">อีเมล</label>
               <input maxlength="100" required="" name="info_email" placeholder="your@email.com" type="email" class="form-control" value="<?php echo $user_info['email'];?>">
               <div class="invalid-feedback"></div>
            </div>
         </div>
         <div class="pb-4 pt-4 mt-0 mb-0 text-center form-group">
            <button type="submit" class="btn btn-primary btn-block btn-updateinfo" style="background-color: rgb(0, 191, 161); border: none;">บันทึก </button>
         </div>
      </form>
   </div>
</div>
<div class="dv90yj-1 gNathv" style="background-color: rgb(255, 255, 255);">
   <h5 class="mt-2 p-3 pt-4 text-left">จัดการรหัสผ่าน</h5>
   <form id="form-changepass" class="pl-3 pr-3 text-left" autocomplete="off"  method="post" action="<?php echo base_url();?>service/changepass">
      <div class="form-group">
         <label class="form-label" style="color: rgb(0, 193, 158);">รหัสผ่าน</label>
         <input placeholder="รหัสผ่านเดิม" name="old_pass" id="old_pass" required="" type="password" class="form-control" onkeyup="return checkValidPass(this,'old_pass_msg');">
         <div class="invalid-feedback" id="old_pass_msg"></div>
      </div>
      <div class="form-group">
         <label class="form-label" style="color: rgb(0, 193, 158);">รหัสผ่านใหม่</label>
         <input placeholder="รหัสผ่านใหม่" name="new_pass" id="new_pass" required="" type="password" class="form-control" onkeyup="return checkValidPass(this,'new_pass_msg');">
         <div class="invalid-feedback" id="new_pass_msg"></div>
      </div>
      <div class="form-group">
         <label class="form-label" style="color: rgb(0, 193, 158);">ยืนยันรหัสผ่านใหม่</label>
         <input placeholder="ยืนยันรหัสผ่านใหม่" name="con_new_pass" id="con_new_pass" required="" type="password" class="form-control"  onblur="checkPass(this);"  onkeyup="return checkPass(this);">
         <div class="invalid-feedback" id="con_new_pass_msg"></div>
      </div>
      <div class="pb-4 pt-4 mt-0 mb-4 text-center form-group">
         <button type="submit" class="btn btn-primary btn-block btn-changepass" style="background-color: rgb(0, 191, 161); border: none;">เปลี่ยนรหัสผ่าน</button>
      </div>
   </form>
</div>

<script type="text/javascript">
   function checkPass(th){
      var pass = $('#new_pass').val();
      var con_pass = $('#con_new_pass').val();
      if(pass != con_pass){
         $('#con_new_pass_msg').html('รหัสผ่านไม่ตรงกัน ').show();
         $(th).removeClass('is-valid');
         $(th).addClass('is-invalid');
      }else if(pass == $('#old_pass').val()){
         $('#con_new_pass_msg').html('รหัสผ่านใหม่จะต้องไม่ซ้ำรหัสเดิม ').show();
         $(th).removeClass('is-valid');
         $(th).addClass('is-invalid');
      }else{
         $(th).removeClass('is-invalid');
         $(th).addClass('is-valid');
         $('#con_new_pass_msg').html('').hide();
      }
   }     
   function checkValidPass(th,id){
      if($(th).val().length < 5){
         $(th).removeClass('is-valid');
         $(th).addClass('is-invalid');
         $('#'+id).html('รหัสผ่านอย่างน้อย 6 ตัว').show();
      }else{
         $(th).removeClass('is-invalid');
         $(th).addClass('is-valid');
         $('#'+id).html('').hide();
      }
   }  
   $(document).on('submit','form#form-updateinfo',function(e){
      $('.btn-updateinfo').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
      $('.btn-updateinfo').prop('disabled', true);

      e.preventDefault();

      $.ajax({
         type: $(this).attr('method'),
         url: $(this).attr('action'),
         data: $(this).serialize(),
         dataType: "json",
         success: function(data, dataType, state){
            if(data.status){
               $('.btn-updateinfo').html('บันทึก');
               Swal.fire('Done...', data.msg, 'success');
               
            }else {
               Swal.fire('Done...', data.msg, 'error');
               $('.btn-updateinfo').html('บันทึก');
               $('.btn-updateinfo').prop('disabled', false);        
            }
         }
      });
   });
   $(document).on('submit','form#form-changepass',function(e){
      $('.btn-changepass').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
      $('.btn-changepass').prop('disabled', true);

      e.preventDefault();

      $.ajax({
         type: $(this).attr('method'),
         url: $(this).attr('action'),
         data: $(this).serialize(),
         dataType: "json",
         success: function(data, dataType, state){
            if(data.status){
               $('.btn-changepass').html('เปลี่ยนรหัสผ่าน');
               Swal.fire('Done...', data.msg, 'success');
               
            }else {
               Swal.fire('Done...', data.msg, 'error');
               $('.btn-changepass').html('เปลี่ยนรหัสผ่าน');
               $('.btn-changepass').prop('disabled', false);         
            }
         }
      });
   });
</script>