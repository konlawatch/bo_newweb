<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<style type="text/css">
	.ciYzvW {
	   background: linear-gradient(rgb(20, 44, 44) 0%, rgb(4, 101, 95) 100%);
	   width: 100%;
	   height: 100%;
	   text-align: -webkit-center;
	   position: fixed;
	   inset: 0px;
	   padding: 80px 16px 8px;
	   overflow: auto;
 	}

	.bzMnnX {
	   position: absolute;
	   top: 16px;
	   right: 16px;
	   color: rgb(255, 255, 255);
	   border-radius: 50%;
	   width: 28px;
	   height: 28px;
	   padding: 0px;
	   border: 3px solid rgb(255, 255, 255);
	   text-align: center;
	   font-weight: bolder;
	   cursor: pointer;
	}
	.kHgnGx {
	   border-radius: 50%;
	   width: 36px;
	   height: 36px;
	   padding: 2px 0px 0px 1px;
	   border: 2px solid rgb(255, 255, 255);
	   color: rgb(102, 102, 102);
	   text-align: center;
	}

	.iNHEab {
	   width: 28px;
	   height: 1px;
	   padding: 2px 0px 0px;
	   margin: 16px 0px 0px;
	   background: rgb(255, 255, 255);
	   border: none;
	   color: rgb(102, 102, 102);
	}

	.jwTQia {
	   border-radius: 50%;
	   width: 26px;
	   height: 26px;
	   padding: 1px 0px 0px 1px;
	   margin: 1px 0px 0px 2px;
	   text-align: center;
	}
</style>

<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>

<div class="jfkxw4-0 ciYzvW" id="reg_step_1">
	<a href="<?php echo base_url();?>" class="jfkxw4-1 bzMnnX">X</a>
	<h3 class="text-light">สมัครสมาชิก</h3>
	<div class="d-flex justify-content-center mb-2 mt-3">
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">1</div>
		</div>
		<div class="jfkxw4-5 iNHEab"></div>
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="color: rgb(255, 255, 255);">2</div>
		</div>
		<div class="jfkxw4-5 iNHEab"></div>
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="color: rgb(255, 255, 255);">3</div>
		</div>
	</div>
	<img src="assets/images/phone.png" alt="phone" class="mt-4 mb-4" style="height: 150px;">
	<div style="max-width: 400px;">
		 	<form id="form-regis" class="form-horizontal text-center" autocomplete="off"  method="post" action="<?php echo base_url();?>service/submit_rg">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<div class="mb-3 input-group">
							<div class="input-group-prepend">
								<span class="BasicAddon input-group-text"><img src="assets/images/phone-active.png" alt="phone" style="height: 30px;"></span>
							</div>
							<input name="reg_username" id="reg_username" placeholder="กรอกเบอร์โทรศัพท์" minlength="10" maxlength="10" required="" aria-describedby="basic-addon1" autocomplete="off" type="tel" onblur="checkUser(this);" onkeypress="return checkValid(this);" class="ml-1 rounded-right form-control form-control-lg isnumeric" value="">
							<div class="invalid-feedback" id="reg_step_1_msg"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="mb-3 col-lg-12 col-md-6 col-12 order-lg-1 order-md-2 order-1">
					<button type="submit" class="btn btn-success btn-block btn-lg btn-login">ยืนยัน</button>
					<button type="button" class="btn btn-warning btn-block btn-lg" onclick="goMenu('index');">มีไอดีแล้วคลิ๊กที่นี่</button>
				</div>
			</div>
		</form><br>
		<div>
			<span style="color: rgb(213, 178, 79);">ติดต่อฝ่ายบริการลูกค้า</span>
			<p class="text-light">มีเจ้าหน้าที่ดูแล และแก้ปัญหาตลอด 24 ชม.</p>
		</div>
	</div>
</div>

<div class="jfkxw4-0 ciYzvW" id="reg_step_2" style="display: none;">
	<a href="<?php echo base_url();?>" class="jfkxw4-1 bzMnnX">X</a>
	<div class="jfkxw4-2 WQpA" onclick="showstep('1');">
		<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-left" class="svg-inline--fa fa-angle-left fa-w-8 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z"></path></svg>
	</div>
	<h3 class="text-light">ตั้งรหัสผ่าน</h3>
	<div class="d-flex justify-content-center mb-2 mt-3">
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">1</div>
		</div>
		<div class="jfkxw4-5 iNHEab"></div>
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">2</div>
		</div>
		<div class="jfkxw4-5 iNHEab"></div>
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="color: rgb(255, 255, 255);">3</div>
		</div>
	</div>
	<img src="assets/images/phone.png" alt="phone" class="mt-4 mb-4" style="height: 150px;">
	<div style="max-width: 400px;">
		 	<form>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<div class="mb-3 input-group">
							<div class="input-group-prepend">
								<span class="BasicAddon input-group-text"><img src="assets/images/phone-active.png" alt="phone" style="height: 30px;"></span>
							</div>
							<input name="reg_password" id="reg_password" placeholder="รหัสผ่าน" required="" aria-describedby="basic-addon1" autocomplete="off" type="password" class="ml-1 rounded-right form-control form-control-lg" value="" onkeyup="return checkValidPass(this);">
						</div>
					</div>
					<div class="form-group">
						<div class="mb-3 input-group">
							<div class="input-group-prepend">
								<span class="BasicAddon input-group-text"><img src="assets/images/phone-active.png" alt="phone" style="height: 30px;"></span>
							</div>
							<input name="reg_con_password" id="reg_con_password" placeholder="ยืนยันรหัสผ่าน" required="" aria-describedby="basic-addon1" autocomplete="off" type="password" onblur="checkPass(this);"  onkeyup="return checkPass(this);" class="ml-1 rounded-right form-control form-control-lg" value="">
							<div class="invalid-feedback" id="reg_step_2_msg"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="mb-3 col-lg-12 col-md-6 col-12 order-lg-1 order-md-2 order-1">
					<button type="button" class="btn btn-success btn-block btn-lg" onclick="checkPassAgain();">ยืนยัน</button>
				</div>
			</div>
		</form><br>
		<div>
			<span style="color: rgb(213, 178, 79);">ติดต่อฝ่ายบริการลูกค้า</span>
			<p class="text-light">มีเจ้าหน้าที่ดูแล และแก้ปัญหาตลอด 24 ชม.</p>
		</div>
	</div>
</div>

<div class="jfkxw4-0 ciYzvW" id="reg_step_3" style="display:none;">
	<a href="<?php echo base_url();?>" class="jfkxw4-1 bzMnnX">X</a>
	<div class="jfkxw4-2 WQpA" onclick="showstep('2');">
		<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-left" class="svg-inline--fa fa-angle-left fa-w-8 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z"></path></svg>
	</div>
	<h3 class="text-light">เลือกธนาคาร</h3>
	<div class="d-flex justify-content-center mb-2 mt-3">
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">1</div>
		</div>
		<div class="jfkxw4-5 iNHEab"></div>
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">2</div>
		</div>
		<div class="jfkxw4-5 iNHEab"></div>
		<div class="jfkxw4-3 kHgnGx">
			<div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">3</div>
		</div>
	</div>
	<div class="mt-2 mb-0"></div>
	<div style="width: 100%; max-width: 800px;">
		<div class="mt-2 mb-0">
			<div style="cursor: pointer;">
				<img id="select_bank" src="" alt="bank" class="gFFUTF" style="display: none;"  onclick="show_bank();">
				<p class="mb-1 text-light" id="select_bank_name">กรุณาเลือกธนาคาร</p>
			</div>
		</div>
		<div class="row" style="margin: 18px;" id="banklist">
			<?php foreach($banklist as $k => $v):?>
				<div class="mb-3 col-lg-2 col-md-3 col-sm-3 col-6" onclick="select_bank('<?php echo $k;?>','<?php echo $v;?>');">
					<img src="<?php echo base_url();?>assets/img/logo-bank/<?php echo $k;?>.png" alt="bank" class="jfkxw4-7 gFFUTF"><br>
					<span class="text-white"><?php echo $v;?></span>
				</div>
			<?php endforeach;?>
		</div>
		<form id="form-regis-done" class="form-horizontal text-center" autocomplete="off"  method="post" action="<?php echo base_url();?>service/submit_lg" style="max-width: 400px;display: none;">
			<div class="row">
				<div class="col">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="BasicAddon input-group-text"><img src="assets/images/bank-number.png" alt="phone" style="height: auto; width: 30px;"></span>
							</div>
							<input type="hidden" name="reg_bank" id="reg_bank" value="">
							<input name="reg_bankno" id="reg_bankno" placeholder="หมายเลขบัญชี" minlength="10" maxlength="20" required="" type="tel" class="ml-1 rounded-right form-control form-control-lg isnumeric" value="" onblur="checkBankNo(this);" onkeypress="return checkValidBankNo(this);">
							<div class="invalid-feedback" id="reg_step_3_msg"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="BasicAddon input-group-text">
									<img src="assets/images/full-name.png" alt="phone" style="height: auto; width: 30px;">
								</span>
							</div>
							<input name="reg_name"  id="reg_name" placeholder="ชื่อ" maxlength="50" required="" type="text" class="ml-1  form-control form-control-lg" value="">
							<input name="reg_lname" id="reg_lname" placeholder="นามสกุล" maxlength="50" required="" type="text" class="rounded-right form-control form-control-lg" value="">
							<div class="invalid-feedback">กรุณาใส่ชื่อ-นามสกุล</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="BasicAddon input-group-text"><img src="assets/images/bank-number.png" alt="phone" style="height: auto; width: 30px;"></span>
							</div>
							<input name="reg_lineid" id="reg_lineid" placeholder="ไอดีไลน์" type="text" class="ml-1 rounded-right form-control form-control-lg" value="" >
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="mb-3 col-lg-12 col-md-6 col-12 order-lg-1 order-md-2 order-1">
					<button type="submit" class="btn btn-success btn-block btn-lg">ยืนยัน</button>
				</div>
			</div>
		</form>
		<br>
		<div>
			<span style="color: rgb(213, 178, 79);">ติดต่อฝ่ายบริการลูกค้า</span>
			<p class="text-light">มีเจ้าหน้าที่ดูแล และแก้ปัญหาตลอด 24 ชม.</p>
		</div>
	</div>
</div>


<script type="text/javascript">
   function checkUser(th){
     	var username = $(th).val();
     	if (username.length !== 10) {
     		$('#reg_step_1_msg').html('กรุณากรอกหมายเลขโทรศัพท์ให้ครบ 10 หลัก !!').show();
     		$(th).addClass('is-invalid');
     		$(th).focus();
     	}else if(username.length === 10){
     		$(th).removeClass('is-invalid');
     		$(th).addClass('is-valid');
     		$('#reg_step_1_msg').html('').hide();
     	}
   }

   function checkBankNo(th){
     	var username = $(th).val();
     	if (username.length !== 10) {
     		$('#reg_step_3_msg').html('เลขบัญชีต้องไม่น้อยกว่า 10 หลัก !!').show();
     		$(th).addClass('is-invalid');
     		$(th).focus();
     	}else if(username.length === 10){
     		$(th).removeClass('is-invalid');
     		$(th).addClass('is-valid');
     		$('#reg_step_3_msg').html('').hide();
     	}
   }

   function checkPass(th){
     	var pass = $('#reg_password').val();
     	var con_pass = $('#reg_con_password').val();
		if(pass != con_pass){
     		$('#reg_step_2_msg').html('รหัสผ่านไม่ตรงกัน ').show();
     		$(th).removeClass('is-valid');
     		$(th).addClass('is-invalid');
     	}else{
     		$(th).removeClass('is-invalid');
     		$(th).addClass('is-valid');
     		$('#reg_step_2_msg').html('').hide();
     	}
   }  
   function checkPassAgain(){
     	var pass = $('#reg_password').val();
     	var con_pass = $('#reg_con_password').val();
		if(pass != con_pass){
     		$('#reg_step_2_msg').html('รหัสผ่านไม่ตรงกัน ').show();
     		$(th).removeClass('is-valid');
     		$(th).addClass('is-invalid');
     	}else{
     		$('#reg_step_2').hide();
     		$('#reg_step_3').show();
     	}
   }    
   function checkValid(th){
      if($(th).val().length === 9){
     		$(th).removeClass('is-invalid');
     		$(th).addClass('is-valid');
     		$('#reg_step_1_msg').html('').hide();
     	}
   } 
   function checkValidPass(th){
      if($(th).val().length < 5){
     		$(th).removeClass('is-valid');
     		$(th).addClass('is-invalid');
     		$('#reg_step_2_msg').html('กรุณาตั้งรหัสผ่านอย่างน้อย 6 ตัว').show();
     	}else{
     		$(th).removeClass('is-invalid');
     		$(th).addClass('is-valid');
     		$('#reg_step_2_msg').html('').hide();
     	}
   }  

   function checkValidBankNo(th){
      if($(th).val().length === 9){
     		$(th).removeClass('is-invalid');
     		$(th).addClass('is-valid');
     		$('#reg_step_3_msg').html('').hide();
     	}
   } 

   function showstep(t){
   	if(t == 1){
   		$('#reg_step_1').fadeIn();
   		$('#reg_step_2').hide();
   		$('#reg_step_3').hide();
   	}else if(t == 2){
   		$('#reg_step_1').hide();
   		$('#reg_step_2').fadeIn();
   		$('#reg_step_3').hide();
   	}else if(t == 3){
   		$('#reg_step_1').hide();
   		$('#reg_step_2').hide();
   		$('#reg_step_3').fadeIn();
   	}else{
   		$('#reg_step_1').fadeIn();
   		$('#reg_step_2').hide();
   		$('#reg_step_3').hide();

   	}
   }

   function show_bank(){
   	$('#banklist').fadeIn();
   	$('#form-regis-done').hide();
   }

   function select_bank(t,txt){
   	$('#reg_bank').val(t);
   	$('#select_bank').attr('src','<?php echo base_url();?>assets/img/logo-bank/'+t+'.png').show();
   	$('#select_bank_name').html(txt);

   	$('#banklist').hide();
   	$('#form-regis-done').show();
   }


   $(document).on('submit','form#form-regis',function(e){
      $('.btn-login').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
      $('.btn-login').prop('disabled', true);

      e.preventDefault();

      $.ajax({
         type: $(this).attr('method'),
         url: $(this).attr('action'),
         data: $(this).serialize(),
         dataType: "json",
         success: function(data, dataType, state){
            if(data.status){
               $('.btn-login').html('ยืนยัน');
               showstep(2);
            }else {
            	$('#reg_username').removeClass('is-valid');
     				$('#reg_username').addClass('is-invalid');
	            $('.btn-login').html('ยืนยัน');
	            $('.btn-login').prop('disabled', false);
	            $('#reg_step_1_msg').html(data.msg).show();            
            }
         }
      });
   });
   $(document).on('submit','form#form-regis-done',function(e){
      $('.btn-login').html('กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw"></i>');
      $('.btn-login').prop('disabled', true);

      e.preventDefault();

      $.ajax({
         type: $(this).attr('method'),
         url: $(this).attr('action'),
         data: {
         	reg_username 	  : $('#reg_username').val(),
         	reg_password 	  : $('#reg_password').val(),
         	reg_con_password : $('#reg_con_password').val(),
         	reg_bank 	  	  : $('#reg_bank').val(),
         	reg_bankno 	  	  : $('#reg_bankno').val(),
         	reg_name 	  	  : $('#reg_name').val(),
         	reg_lname 	  	  : $('#reg_lname').val(),
         	reg_lineid 	  	  : $('#reg_lineid').val(),
         },
         dataType: "json",
         success: function(data, dataType, state){
            if(data.status){
               $('.btn-login').html('ยืนยัน');
               Swal.fire('Done...', data.msg, 'success').then(function(){
               	window.location.assign("<?php echo base_url();?>");
               });
               
            }else {
            	$('#reg_username').removeClass('is-valid');
     				$('#reg_username').addClass('is-invalid');
	            $('.btn-login').html('ยืนยัน');
	            $('.btn-login').prop('disabled', false);
	            $('#reg_step_3_msg').html(data.msg).show();            
            }
         }
      });
   });
</script>  


