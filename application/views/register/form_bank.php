<?php
date_default_timezone_set('Asia/Bangkok');

##=========================================================================================

//util class
class Util {

    public static function cUrl($url, $method = "get", $data = "", $ssl = false){
        if ($method == "post"){
            if ($data == "") return false;
        }
        $ch = curl_init();
        if ($method == "get") curl_setopt($ch, CURLOPT_URL, $url.($data != "" ? "?".$data : ""));
        else if ($method == "post") curl_setopt($ch, CURLOPT_URL, $url);
        if ($method == "post"){
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }
};

$curl = Util::cUrl('https://ts911api.botbo21.com/api/getbanklist?sign=C5Z10zzL4M7BiOSmEgyoAcnw5g38CvO2','get');

$banklist = json_decode($curl,true);
?>
<?php $ss = $this->session->userdata();?>
<?php $datab = $this->session->userdata();?>
<?php $error = $this->session->flashdata('error');?>
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
               <div class="modal-content  tab-content" style="background-color: #181c23;">
                  <div class="modal-body">
                     <h3 class="text-primary mb-0 center"><?php echo $error; ?> </h3>
                     <hr class="x-hr-border-glow">
                  </div>
                  <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                     <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                  </div>
               </div>
            </div>
         </div>

         <?php if(isset($error)):?>

            <script type="text/javascript">
               $(document).ready(function(){
               $('#exampleModal').modal('show');
               });
            </script>

         <?php endif;?>


         <div class="container" data-aos="fade-up"style="padding-right: 0px; padding-left: 0px;">
            <div class="text-center animate__animated animate__fadeIn mb-4 animate__slower 3s container"  style="padding:30px; align-items: center !important; max-width: 100%; margin-left: auto; margin-right: auto; width: 900px;">
               <div class="justify-content-md-center mb-4 row">
                  <div class="col-lg-10">
                     <div class="dv90yj-3 ekmdOJ text-left d-flex" style="background-color: rgb(0, 191, 161); border: none;">

                     <p class="ml-2">ตั้งค่าบัญชีผู้ใช้</p>
                     </div>


                     <div>
                        <div class="dv90yj-1 gNathv pt-4 pb-0" style="background-color: rgb(255, 255, 255);" style="padding:30px;">
                           <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>abank/Updatebank">
                           <input type="hidden" name="token" id="token" value="<?php echo $ss['data']['token'];?>">
                              <input type="hidden" name="user" id="user" value="<?php echo $ss['data']['userid'];?>">
                              <div class="form-group ">
                                 <label class="form-label " style="color: rgb(0, 193, 158);">ชื่อบัญชี</label>
                                 <input  type="text" id="name" name="name" class="form-control col-md-6 col-sm-4" placeholder="ชื่อบัญชี" value="" style="margin: auto;">
                                 <label class="form-label" style="color: rgb(0, 193, 158);">นามสกุล</label>
                                 <input  type="text" id="lname" name="lname" class="form-control col-md-6 col-sm-4" placeholder="นามสกุล" value="" style="margin: auto;">
                              </div>
                              <div class="form-group">
                                 <label class="form-label" style="color: rgb(0, 193, 158);" >ธนาคาร</label>
                                 <select class="col-md-6 col-sm-4 form-control"  name="bankid" style="border-radius: 10px !important; margin:auto;">
                                 <option value="">-- กรุณาเลือกธนาคาร --</option>
                                 <?php foreach ($banklist['data'] as $k => $v):?>
                                 <option value="<?php echo $k;?>">
                                 <?php echo $v;?>   z
                                 </option>
                                 <?php endforeach; ?>
                                 </select>
                              </div>

                              <div class="form-group">
                                 <label class="form-label" style="color: rgb(0, 193, 158);">หมายเลขบัญชี</label>
                                 <input placeholder="หมายเลขบัญชี" required="" type="text" class="form-control col-md-6 col-sm-4"  id="bankno" name="bankno" style="margin: auto;">
                              <div class="invalid-feedback"></div>
                              </div>

                              <div class="form-group">
                                 <label class="form-label" style="color: rgb(0, 193, 158);">ยืนยันหมายเลขบัญชี</label>
                                 <input placeholder="ยืนยันหมายเลขบัญชี" required="" type="text" class="form-control col-md-6 col-sm-4" id="bankno_confirm" name="bankno_confirm" style="margin: auto;">
                              <div class="invalid-feedback"></div>
                              </div>


                              <div class="pb-4  form-group">
                              <button type="submit" class="btn btn-primary btn-block col-lg-6 col-md-6 " style="background-color: rgb(0, 191, 161); border: none; margin: auto;">บันทึก </button>

                              </div>
                           </form>
                        </div>
                     </div> 
                  </div>
               </div>
            </div>
         </div>
                     