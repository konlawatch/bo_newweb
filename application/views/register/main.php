<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>   
<?php endif;?>
   <div class="jfkxw4-0 ciYzvW bg">
   <a href="<?php echo base_url();?>index"><div class="jfkxw4-1 bzMnnX">X</div></a>
   <h3 class="text-light">สมัครสมาชิก</h3>
   <div class="d-flex justify-content-center mb-2 mt-3">
      <div class="jfkxw4-3 kHgnGx">
         <div class="jfkxw4-4 jwTQia" style="background: rgb(255, 255, 255); color: green;">1</div>
      </div>
      <div class="jfkxw4-5 iNHEab"></div>
      <div class="jfkxw4-3 kHgnGx">
         <div class="jfkxw4-4 jwTQia" style="color: rgb(255, 255, 255);">2</div>
      </div>
      <div class="jfkxw4-5 iNHEab"></div>
      <div class="jfkxw4-3 kHgnGx">
         <div class="jfkxw4-4 jwTQia" style="color: rgb(255, 255, 255);">3</div>
      </div>
   </div>

   <img src="assets/images/phone.png" alt="phone" class="mt-4 mb-4" style="height: 150px;">
   <div style="max-width: 400px;">
      <form class="form-horizontal text-center" action="<?php echo base_url(); ?>service/submit_rg" method="post">
         <div class="row">
            <div class="col">     
               <div class="form-group">
                  <div class="mb-3 input-group">
                     <div class="input-group-prepend">
                        <span class="BasicAddon input-group-text">
                        <img src="assets/images/phone-active.png" alt="phone" style="height: 30px;">
                        </span>
                     </div>
                        <input type="text" class="ml-1 rounded-right form-control form-control-lg" id="username"  name="username" onblur="checkUser();"  onkeypress="return CharacterFormat(this,event,1);" placeholder="กรุณกรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก"  maxlength="10" >
                     <div class="invalid-feedback"> กรุณาใส่หมายเลขโทรศัพท์ให้ถูกต้อง และต้องมีจำนวนไม่น้อยกว่า 10 ตัว</div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="mb-3 col-lg-12 col-md-6 col-12 order-lg-1 order-md-2 order-1">
               <button type="submit" class="btn btn-success btn-block btn-lg">ยืนยัน</button>
            </div>
         </div>
      </form><br>
      <div>
         <span style="color: rgb(213, 178, 79);">ติดต่อฝ่ายบริการลูกค้า</span>
         <p class="text-light">มีเจ้าหน้าที่ดูแล และแก้ปัญหาตลอด 24 ชม.</p>
      </die>
      </div>


      <?php $error  = $this->session->flashdata('error');
      $error1 = $this->session->flashdata('error1');
      // debug($error,true);
      ?>

      <div class="modal fade" id="exampleModal_number" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise">กรุณกรอกหมมายเลขโทรศัพท์ให้ครบ 10 หลัก</h3>
                  <hr class="x-hr-border-glow">
                  <!-- <p style="color: #fff;">ท่านต้องการเปลี่ยนรหัสผ่าน หรือไม่</p> -->
                  </div>
                  <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <button type="reset" class="btn btn-econdary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise"><?php echo $error1; ?> </h3>
                  <hr class="x-hr-border-glow">
                  </div>
                  <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <button type="reset" class="btn btn-secondary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content" style="background-color: #181c23;"> 
               <div class="modal-body">
                  <h3 class="text-primary mb-0 center textgold noise"><?php echo $error; ?> </h3>
                  <hr class="x-hr-border-glow">
                  <p style="color: #fff;">ท่านต้องการเปลี่ยนรหัสผ่าน หรือไม่</p>
               </div>
               <div class="modal-footer" style="border-top-color: #ecbd7b!important;background-color: #eff3f800; width: 100%">
                  <form id="form-register" class="form-horizontal text-center" method="post" action="<?php echo base_url(); ?>forgetpass" style="width: 100%;">
                  <button type="submit" id="submit" name="submit" class="btn btn-primary xs-8" style="border-radius: 10px; width: 30%;">ยืนยัน</button>
                  <button type="reset" class="btn btn-secondary mx-auto" style="border-radius: 10px; width: 30%;" data-dismiss="modal">ปิด</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

   <?php if(isset($error)):?>                             
     <script type="text/javascript">
       $(document).ready(function(){
       $('#exampleModal').modal('show');
       });
     </script>                    
   <?php endif;?>
   <?php if(isset($error1)):?>                             
     <script type="text/javascript">
       $(document).ready(function(){
       $('#exampleModal1').modal('show');
       });
     </script>                    
   <?php endif;?>


   </div>
</div>
<script type="text/javascript">
   function checkUser(){
      var username = $('#username').val();
      var nameRegex = /^[a-zA-Z0-9_]+$/;
      var chkFormat = $('#username').val().match(nameRegex);

      if (username.length == 10 || username.length == 0) {

      }else{
         Swal.fire('Oops...', 'กรุณกรอกหมมายเลขโทรศัพท์ให้ครบ 10 หลัก', 'error');
      }
   }    
</script>  

<script type="text/javascript">
function CharacterFormat(fld,e,format)
{
var ie = checkIE();
if( format == 1 )
var strCheck = '0123456789';
else if( format == 2 )
var strCheck = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
else if( format == 3 )
var strCheck = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-';
else if( format == 4 )
var strCheck = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-@';

var len = 0;
if( ie == 8 )
var whichCode = e.keyCode;
else
var whichCode = (window.Event) ? e.which : e.keyCode;

key = String.fromCharCode(whichCode);

if( whichCode == 8 || whichCode == 0 || whichCode == 13) {

}else{

key = String.fromCharCode(whichCode);

if(strCheck.indexOf(key) == -1) return false;
}
}
</script>



     
