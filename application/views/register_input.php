<div class="welcome">
   <img src="<?php echo base_url();?>assets/images/welcome.png" alt="">
</div>
<section id="login" style="display: block;
justify-content: center;
align-items: center;
margin-top: 10px;
margin-bottom: 100px;">
   <div class="container">
      <form id="form-regis" class="form-horizontal text-center" autocomplete="off"  method="post" action="<?php echo base_url();?>service/submit_rg">
      <h1>สมัครสมาชิก</h1>
         <div class="alert" role="alert" style="display: none;" id="form_alert">
            <i class="material-icons list-icon" id="alert_icon"></i>
            <span id="alert_text" style="font-weight:bold;"></span>
         </div>
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fas fa-user-alt"></i></span>
            </div>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username (0-9,a-z)" required="" maxlength="12">
         </div>
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>
            </div>
            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required="" maxlength="12">
         </div>
         <div class="input-group mb-3">
            <div class="input-group-prepend">
               <span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>
            </div>
            <input type="password" class="form-control" name="repassword" id="repassword" placeholder="กรอก Password อีกครั้ง" required="" maxlength="12">
         </div>
         <button type="submit" class="btn btn-primary btn-login"><i class="fa fa-unlock-alt" aria-hidden="true" style="font-size: 16px !important;"></i> สมัครสมาชิก !</button>
      </form>
      <div class="menu-bottom">
         <ul>
            <li><a href="<?php echo base_url();?>login">มีรหัสสมาชิกแล้ว !</a></li>
            <li><a href="<?php echo base_url();?>forgot-password">ลืมรหัสผ่าน ?</a></li>
         </ul>
      </div>
   </div>
</section>