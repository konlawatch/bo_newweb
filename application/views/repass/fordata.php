

<div class="bg-main">
     <?php 
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');
    ?>
    <?php if ($error) :?>
        <div class="pull-left alert alert-danger no-margin alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $error; ?>
        </div>
    <?php endif;?>
    <?php if ($success) :?>
        <div class="pull-left alert alert-success no-margin alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <?php echo $success; ?>
        </div>
    <?php endif;?>
    
       <header class="title">
                <div class="container text-center">
                    <a href="<?php echo base_url();?>home"><img src="assets/images/n_imi.png" style="height: 200px"  alt=""></a>
                    <h1 class="no-margin">ตั้งค่ารหัสใหม่</h1>
                </div>
            </header>
        </div>
        <section>
            <div class="container">
                <div class="white-box box-start" style="padding:30px;">
                    <small style="display:block; line-height: 14px; padding-bottom: 15px; color: #333; text-align: left;">* กรุณากรอกเฉพาะข้อมูลจริงเท่านั้น เพื่อประโยชน์ของตัวท่านเองในการถอนเงิน</small>
                    <div class="alert" role="alert" style="display: none;" id="form_alert">
                        <i class="material-icons list-icon" id="alert_icon"></i>
                        <span id="alert_text" style="font-weight:bold;"></span>
                    </div>




                    <form id="form-register" class="form-horizontal text-center" method="post" action="">


                     <!--   <input type="hidden" name="token" id="token" value="<?php echo $ss['data']['token'];?>">
                        <input type="hidden" name="user" id="user" value="<?php echo $ss['data']['userid'];?>">
                     !-->
                        
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fal fa-key"></i></span>
                                </div>
                                <input type="password" id="oldpass" name="oldpass" placeholder="รหัสเดิม" value="" class="form-control input-sm" ><p>
                               
                            </div>

                        </div> 
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fal fa-key"></i></span>
                                </div>
                                <input type="password" class="form-control isnumeric" name="forgetotp" id="forgetotp" value="" placeholder="รหัสOTP" required="">
                            </div>
                        </div>

                

                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fal fa-key"></i></span>
                                </div>
                                <input type="password" class="form-control isnumeric" name="newpass" id="newpass" value="" placeholder="รหัสใหม่" required="">
                            </div>
                        </div>

                       <!--  <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fal fa-lock"></i></span>
                                </div>
                                <input type="text" class="form-control isnumeric" name="email" id="email" value="" placeholder="ยืนยันรหัส" minlength="10" maxlength="12" required="">
                            </div>
                        </div>
                        !-->
                           

                        <button type="submit" name="submit" class="btn btn-primary btn-register">ยืนยัน</button>
                    </form>
                </div>
            </div>
        </section>
        <script type="text/javascript">
   
    function checkUser(){
       
        var username = $('#username').val();
        var nameRegex = /^[a-zA-Z0-9_]+$/;
        var chkFormat = $('#username').val().match(nameRegex);
        if(username != ''){
            if(chkFormat == null){
                $('#checkUserMsg').html(`<span style="display:block; line-height: 14px; color:red; padding-bottom: 15px; font-si  text-align: left;">(รูปแปบไม่ถูกต้อง) a-z,A-Z,0-9</span>`);
                $('input[name="username"]').css({"box-shadow":"0 0 5px #ea5d5d inset","border-color":"#af0000"});
            }else{
                if(username.length < 3 || username.length > 15){
                    $('#checkUserMsg').html(`<span style="display:block; line-height: 14px; color:red; padding-bottom: 15px;  text-align: left;">(ยูสเซอร์ไอดี ต้องมากกว่า 3 และน้อยกว่า 15 ตัวอักษร)</span>`);
                    $('input[name="username"]').css({"box-shadow":"0 0 5px #ea5d5d inset","border-color":"#af0000"});
                }else{
                     //$('#checkUserMsg').html('<span style="color:green">dddddddddddd</span>');
                    
                    $.ajax({
                        url: '<?php echo base_url(); ?>newuser/chkuseruse',
                        type: 'POST',
                        data : { username : username },
                        dataType: 'json',
                        success: function (res) {
                            $('#isUserValid').val(res.status);
                            console.log(res);
                            if(res.status){
                              $('#checkUserMsg').html('<span style="color:green; text-align: left;">'+res.msg+'</span>');
                              $('input[name="username"]').css({"box-shadow":"0 0 5px #42ff87 inset","border-color":"#14fa68"});
                              console.log(res);
                            }else{
                              $('#checkUserMsg').html('<span style="color:red; text-align: left;">'+res.msg+'</span>');
                               $('input[name="username"]').css({"box-shadow":"0 0 5px #ea5d5d inset","border-color":"#af0000"});
                                }
                            }
                        });
            
                    }
                }
            }else{
            $('#username').focus();
            }
}    

   
</script>
<script type="text/javascript">


    function check_telephone()
{
var telephone = document.getElementById("txt_telephone");
if(telephone.value == "")
{
alert("กรุณากรอกเบอร์โทรศัพท์ด้วยค่ะ");
telephone.focus();
return false
}
if(telephone.value.length > 10 || telephone.value.length < 10)
{
alert("กรุณากรอกเบอร์โทรศัพท์จำนวน 10 หลักค่ะ");
telephone.focus();
return false
}
}

function getiever()
{
var rv = -1; // Return value assumes failure.
if(navigator.appName == 'Microsoft Internet Explorer')
{
var ua = navigator.userAgent;
var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
if(re.exec(ua) != null)
rv = parseFloat( RegExp.$1 );
}
return rv;
}

function checkIE()
{
var msg = "";
var ver = getiever();
if( ver> -1 )
{
if( ver>= 8.0 )
msg = "8";
else if( ver == 7.0 )
msg = "7";
else if( ver == 6.0 )
msg = "6";
else
msg = "<6";
}
return msg;
}

    function CharacterFormat(fld,e,format)
{
var ie = checkIE();
if( format == 1 )
var strCheck = '0123456789';
else if( format == 2 )
var strCheck = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
else if( format == 3 )
var strCheck = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-';
else if( format == 4 )
var strCheck = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890._-@';

var len = 0;
if( ie == 8 )
var whichCode = e.keyCode;
else
var whichCode = (window.Event) ? e.which : e.keyCode;

key = String.fromCharCode(whichCode);

if( whichCode == 8 || whichCode == 0 || whichCode == 13) {

}else{

key = String.fromCharCode(whichCode);

if(strCheck.indexOf(key) == -1) return false;
}
}
</script>
