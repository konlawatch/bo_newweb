<div class="form-page">
  <?php $ss = $this->session->userdata();?>
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
  
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <!-- ======= Breadcrumbs ======= -->
<style type="text/css">
  .form-control 
    {
      color: #5f5d5d;
    }

  .white-box 
    {
      background-color: #343a40;
      border-radius: 20px;
      margin-bottom: 10%;
    }

  label 
    { 
      font-weight: 600;
      color: #828282;
    }
  input 
    {
      border: 0;
      width: 57px;
      z-index: 10;
      text-align: center
    }
  span:focus input 
    {
      border: 5px solid red;
      transition: border .1s
    }
  input:focus 
    {
      border: 5px solid green;
    }

  .modal-footer 
    {
      padding-top: 12px;
      padding-bottom: 14px;
      border-top-color: #343a40;
      -webkit-box-shadow: none;
      box-shadow: none;
      background-color: #343a40;
    }
  .modal-content 
    {
      background-color: #343a40;
      border-radius: 30px;
      align-items: : center;  
    }
</style> 
<?php $error = $this->session->flashdata('error');?>


      <div class="container" data-aos="fade-up">
        <div class="container">
          <div class="white-box box-start bbox" style="padding:30px; align-items: center !important; max-width: 100%; margin-left: auto; margin-right: auto; width: 810px;">
            <a href="<?php echo base_url();?>profile" class="close" style="color: #fff;">
              <span aria-hidden="true">&times;</span>
            </a>
            <div style="padding-left: 5%;padding-right: 5%;"><h2 class="ext-primary mb-0 center textsilver noise">ตั้งรหัสผ่านใหม่</h2></div>
            <hr class="x-hr-border-glow">              
            <div class="form-group">   
              <div class="row"> 
                <div class="col-lg-12  tab-content"  data-aos="fade-center">            
                  <form class="form-horizontal text-center" action="<?php echo base_url(); ?>repass/npass" method="post">
                    <input type="hidden" name="token" id="token"  value="<?php echo $ss['data']['token'];?>">
                    <input type="hidden" name="user"  id="user"   value="<?php echo $ss['data']['userid'];?>">
                    <div class="row">
                      <div class="col-xs-12 col-md-12 hidden-xs hidden-sm"> 
                        <div class="form-group">  
                          <h5 class="textsilver noise">รหัสผ่านเดิม <hr class="x-hr-border-glow"></h5>
                          <input type="password" class="col-md-6 col-sm-4 " id="oldpass" name="oldpass" placeholder="รหัสผ่านเดิม" style="border-radius: 10px !important;">
                        </div>                 
                        <div class="form-group">
                          <h5 class="textsilver noise">รหัสผ่านใหม่ <hr class="x-hr-border-glow"></h5>
                          <input type="password" class="col-md-6 col-sm-4 " id="newpass" name="newpass" placeholder="รหัสผ่านใหม่" style="border-radius: 10px !important;">
                        </div>
                        <div class="form-group">
                          <h5 class="textsilver noise">ยืนยันรหัสผ่าน <hr class="x-hr-border-glow"></h5>
                          <input type="password" class="col-md-6 col-sm-4 " id="cfnewpass" name="cfnewpass" placeholder="ยืนยันรหัสผ่าน" style="border-radius: 10px !important;">
                        </div>
                          <button type="submit" id="submit" name="submit" class="custom-btn btn-11" style="border-radius: 10px; width: 20%;">ยืนยัน</button>
                          <button type="reset" class="custom-btn btn-12" style="border-radius: 10px; width: 20%;">เคลียร์</button>
                          <br><br>
                          <hr class="x-hr-border-glow">             
                      </div>
                    </div>                     
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content  tab-content">
              <div class="modal-body">
                <?php echo $error; ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" style="border-radius: 30px;" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <?php if(isset($error)):?>
        <script type="text/javascript">
        $(document).ready(function(){
        $('#exampleModal').modal('show');
        });
        </script>           
        <?php endif;?>
      </div>