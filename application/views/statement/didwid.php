<?php if($type == 'all'):?>
<div class="animate__animated animate__fadeIn">
   <div class="table-responsive">
      <table class="table table-hover text-center">
         <thead>
            <tr>
               <th style="width: 20%;">พาทเนอร์</th>
               <th style="width: 15%;">ฝาก</th>
               <th style="width: 15%;">ถอน</th>
               <th style="width: 30%;">เวลา</th>
               <th style="width: 25%;">สถานะ</th>
            </tr>
         </thead>
         <tbody>
            <?php if(count($data) == 0):?>
               <tr>
                  <td colspan="4" class="text-danger">ไม่มีข้อมูล</td>
               </tr>
            <?php endif;?>
            <?php foreach($data as $k => $v):?>
               <tr>
                  <td><?php echo $v['web'];?></td>
                  <td><?php echo ($v['type'] == '1') ? $v['amount'] : '-';?></td>
                  <td><?php echo ($v['type'] == '2') ? $v['amount'] : '-';?></td>
                  <td><?php echo $v['date'];?></td>
                  <td><?php echo $v['status'];?></td>
               </tr>
            <?php endforeach;?>
         </tbody>
      </table>
   </div>
</div>
<?php endif;?>

<?php if($type == 'deposit'):?>
<div class="animate__animated animate__fadeIn">
   <div class="table-responsive">
      <table class="table table-hover text-center">
         <thead>
            <tr>
               <th style="width: 20%;">พาทเนอร์</th>
               <th style="width: 25%;">ฝาก</th>
               <th style="width: 25%;">เวลา</th>
               <th style="width: 25%;">สถานะ</th>
            </tr>
         </thead>
         <tbody>
            <?php if(count($data) == 0):?>
               <tr>
                  <td colspan="4" class="text-danger">ไม่มีข้อมูล</td>
               </tr>
            <?php endif;?>
            <?php foreach($data as $k => $v):?>
               <tr>
                  <td><?php echo $v['web'];?></td>
                  <td><?php echo ($v['type'] == '1') ? $v['amount'] : '-';?></td>
                  <td><?php echo $v['date'];?></td>
                  <td><?php echo $v['status'];?></td>
               </tr>
            <?php endforeach;?>
         </tbody>
      </table>
   </div>
</div>
<?php endif;?>

<?php if($type == 'withdraw'):?>
<div class="animate__animated animate__fadeIn">
   <div class="table-responsive">
      <table class="table table-hover text-center">
         <thead>
            <tr>
               <th style="width: 20%;">พาทเนอร์</th>
               <th style="width: 25%;">ถอน</th>
               <th style="width: 25%;">เวลา</th>
               <th style="width: 25%;">สถานะ</th>
            </tr>
         </thead>
         <tbody>
            <?php if(count($data) == 0):?>
               <tr>
                  <td colspan="4" class="text-danger">ไม่มีข้อมูล</td>
               </tr>
            <?php endif;?>
            <?php foreach($data as $k => $v):?>
               <tr>
                  <td><?php echo $v['web'];?></td>
                  <td><?php echo ($v['type'] == '2') ? $v['amount'] : '-';?></td>
                  <td><?php echo $v['date'];?></td>
                  <td><?php echo $v['status'];?></td>
               </tr>
            <?php endforeach;?>
         </tbody>
      </table>
   </div>
</div>
<?php endif;?>