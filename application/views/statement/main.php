<?php 
   $ss = $this->session->userdata();
?>
<?php 
   $error    = $this->session->flashdata('error');
   $success  = $this->session->flashdata('success');
?>
<?php if(isset($success)):?>      
   <script type="text/javascript">
      $(document).ready(function(){
         Swal.fire('Done...', '<?php echo $success;?>', 'success');
      });
   </script>       
<?php endif;?>
<?php if(isset($error)):?>          
   <script type="text/javascript">
      $(document).ready(function(){ 
         Swal.fire('Oops...', '<?php echo $error;?>', 'error');
      });
   </script>                   
<?php endif;?>
<style type="text/css">
  .btn-dark {
    /*color: #fff;*/
    background-color: #3a9a82;
    /*border-color: #343a40;*/
}
</style>
<div id="statement" class="mt-3 mb-3">
   <div class="dv90yj-3 ekmdOJ text-left d-flex" style="background-color: rgb(0, 191, 161); border: none;">
      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tasks" class="svg-inline--fa fa-tasks fa-w-16 ml-2 mt-1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="font-size: 1em;">
         <path fill="currentColor" d="M139.61 35.5a12 12 0 0 0-17 0L58.93 98.81l-22.7-22.12a12 12 0 0 0-17 0L3.53 92.41a12 12 0 0 0 0 17l47.59 47.4a12.78 12.78 0 0 0 17.61 0l15.59-15.62L156.52 69a12.09 12.09 0 0 0 .09-17zm0 159.19a12 12 0 0 0-17 0l-63.68 63.72-22.7-22.1a12 12 0 0 0-17 0L3.53 252a12 12 0 0 0 0 17L51 316.5a12.77 12.77 0 0 0 17.6 0l15.7-15.69 72.2-72.22a12 12 0 0 0 .09-16.9zM64 368c-26.49 0-48.59 21.5-48.59 48S37.53 464 64 464a48 48 0 0 0 0-96zm432 16H208a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h288a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0-320H208a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h288a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16zm0 160H208a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h288a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z"></path>
      </svg>
      <span class="ml-2">รายงานฝาก-ถอน</span>
   </div>
   <div class="card">
      <div class="text-left card-body">
         <div class="row">
            <div class="col">
               <nav class="nav card-header-tabs nav-tabs" role="tablist">
                  <a id="tabs-tab-all" href="#" role="tab" data-rb-event-key="all" aria-controls="tabs-tabpane-all" aria-selected="true" class="nav-item nav-link" onclick="get_statement(this,'all');">
                     <span>
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="wallet" class="svg-inline--fa fa-wallet fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M461.2 128H80c-8.84 0-16-7.16-16-16s7.16-16 16-16h384c8.84 0 16-7.16 16-16 0-26.51-21.49-48-48-48H64C28.65 32 0 60.65 0 96v320c0 35.35 28.65 64 64 64h397.2c28.02 0 50.8-21.53 50.8-48V176c0-26.47-22.78-48-50.8-48zM416 336c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32z"></path></svg> ทั้งหมด
                     </span>
                  </a>
                  <a id="tabs-tab-deposit" href="#" role="tab" data-rb-event-key="deposit" aria-controls="tabs-tabpane-deposit" aria-selected="false" class="nav-item nav-link" tabindex="-1" onclick="get_statement(this,'deposit');">
                     <span>
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="folder-plus" class="svg-inline--fa fa-folder-plus fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path>
                        </svg> ฝาก
                     </span>
                  </a>
                  <a id="tabs-tab-withdraw" href="#" role="tab" data-rb-event-key="withdraw" aria-controls="tabs-tabpane-withdraw" aria-selected="false" class="nav-item nav-link" tabindex="-1" onclick="get_statement(this,'withdraw');">
                     <span>
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="folder-minus" class="svg-inline--fa fa-folder-minus fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 128H272l-64-64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V176c0-26.51-21.49-48-48-48zm-96 168c0 8.84-7.16 16-16 16H160c-8.84 0-16-7.16-16-16v-16c0-8.84 7.16-16 16-16h192c8.84 0 16 7.16 16 16v16z"></path>
                        </svg> ถอน
                     </span>
                  </a>
               </nav>
               <div class="tab-content">
                  <div id="tabs-tabpane" aria-labelledby="tabs-tab-all" role="tabpanel" aria-hidden="false" class="fade tab-pane active show">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<span class="x-loading" id="load_statement" style="display: none;">กรุณารอสักครู่ <i class="fa fa-spinner fa-pulse fa-fw "></i></span>
<script type="text/javascript">
   get_statement(this,'all');
   function get_statement(th,type){
      $('.nav-link').removeClass('active');
      $('#tabs-tab-'+type).addClass('active');
      $('#load_statement').show();
      $.ajax({
         type: 'get',
         url: '<?php echo base_url();?>service/get_statement',
         data: { type : type },
         dataType: "json",
         success: function(data, dataType, state){
            $(th).addClass('active');
            if(data.status){
               $('#tabs-tabpane').html(data.data);
               $('#load_statement').hide();
            }else{
               Swal.fire('Oops...', data.msg, 'error');
            }
         }
      });
   }
</script>
